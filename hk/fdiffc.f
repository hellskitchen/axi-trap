C***********************************************************************
C
C     Fourier differentiation utilities.
c
C     fdiffc is a simple FFT based routine.
c
C     diffmat, diffmat2 are brute force dense matrix routines
C     (useful for assembly of dense matrix approximations of 
C     various differential operators, including the surface Laplacian 
C     on surfaces of revolution, etc.)
C
C***********************************************************************
      subroutine fdiffc(fvalue,fderiv,npts,period,wfft,work)
C***********************************************************************
C
C     Fourier differentiation for periodic function on
C     interval of length PERIOD sampled at NPTS equispaced points.
C
C     INPUT:
C
C     fvalue   = array of function values 
C     npts     = number of grid points
C     period   = length of interval
C     wfft     = FFT workspace, must have been preceded by call
C                to dcffti(npts,wfft). Must have length >= 4*npts+15.
C     work     = complex workspace of length npts
C
C     output:
C
C     fderiv = array of derivative values (at same pt locations)
C-----------------------------------------------------------------------
      implicit real *8 (a-h,o-z)
      integer npts,lw,ipt,nsc
      complex *16 fvalue(npts),fderiv(npts)
      complex *16 work(1),eye
C
      eye = dcmplx(0.0D0,1.0d0)
      pi = 4.0D0*datan(1.0d0)
      ipt = 1
      nsc = npts/2
C
c---- copy FVALUE into workspace
c
      do i = 1,npts
	 work(i) = fvalue(i)
      enddo
c
c     Fourier transform
c
ccc      call prinf(' calling zfftf with npts = *',npts,1)
      call zfftf(npts,work(ipt),wfft)
ccc      call prinf(' after zfftf with npts = *',npts,1)
c
c     Fourier differentiate
c
      do i = 0,nsc
	 work(ipt+i) = i*eye*work(ipt+i)/npts
      enddo
      do n = 1,nsc-1
	 work(ipt+npts-n) = -n*eye*work(ipt+npts-n)/npts
      enddo
      if ( mod(npts,2).eq.1 ) then 
         n = nsc
	 work(ipt+npts-n) = -n*eye*work(ipt+npts-n)/npts
      endif
c
ccc      call prin2(' work after fft, diff = *',work(ipt),2*npts)
c
c     Inverse Fourier transform
c
      call zfftb(npts,work(ipt),wfft)
c
c     Scale and write into output array
c
      sc = 2*pi/period
      do i = 1,npts
	 fderiv(i) = work(i)*sc
      enddo
c
      return
      end
C
C
C
C
C
C***********************************************************************
      subroutine diffmat(amat,npts,period)
C***********************************************************************
C
C     Construct dense Fourier differentiation matrix
C     for periodic function on
C     interval of length PERIOD sampled at NPTS equispaced points.
C
C     No intelligence is used to accelerate the calculation.
C
C     INPUT:
C
C     npts     = number of grid points
C     period   = length of interval
C
C     OUTPUT:
C
C     amat = differentiation matrix
C
C-----------------------------------------------------------------------
      implicit real *8 (a-h,o-z)
      integer npts
      complex *16 amat(npts,npts),eye
C
      eye = dcmplx(0.0D0,1.0d0)
      pi = 4.0D0*datan(1.0d0)
      ipt = 1
      nsc = npts/2
C
      sc = 2*pi/period
      do i = 1,npts
      do j = 1,npts
         amat(i,j) = 0.0d0
         do k = 1,nsc+1
            amat(i,j) = amat(i,j) + (sc/npts)*(k-1)*eye*
     1               cdexp(2*pi*eye*(i-j)*(k-1)/npts)
         enddo
         do k = 1,nsc-1
            kuse = 1+npts-k
            amat(i,j) = amat(i,j) + (sc/npts)*(-k)*eye*
     1               cdexp(2*pi*eye*(i-j)*(kuse-1)/npts)
         enddo
         if ( mod(npts,2).eq.1 ) then 
            k = nsc
            kuse = 1+npts-k
            amat(i,j) = amat(i,j) + (sc/npts)*(-k)*eye*
     1               cdexp(2*pi*eye*(i-j)*(kuse-1)/npts)
         endif
      enddo
      enddo
      return
      end
C
C***********************************************************************
      subroutine cmatvec11(amat,npts,x,b)
C***********************************************************************
C
C     complex matvec utility function
C
C     INPUT:
C
C     amat     = complex (square) matrix
C     npts     = matrix dimensions
C     x        = complex vector
C
C     OUTPUT:
C
C     b        = complex vector  amat*x
C
C-----------------------------------------------------------------------
      implicit real *8 (a-h,o-z)
      complex *16 amat(npts,npts),x(1),b(1)
c
      do i = 1,npts
         b(i) = 0
         do j = 1,npts
            b(i) = b(i) + amat(i,j)*x(j)
         enddo
      enddo
      return
      end
C
C
C
C
C***********************************************************************
      subroutine diffmat2(amat,amat2,npts,period)
C***********************************************************************
C
C     Construct dense Fourier differentiation matrix and second
C     derivative matrix for periodic function on
C     interval of length PERIOD sampled at NPTS equispaced points.
C
C     No intelligence is used to accelerate the calculation.
C
C     INPUT:
C
C     npts     = number of grid points
C     period   = length of interval
C
C     OUTPUT:
C
C     amat =  differentiation matrix
C     amat2 = second derivative matrix
C
C-----------------------------------------------------------------------
      implicit real *8 (a-h,o-z)
      integer npts
      complex *16 amat(npts,npts),eye
      complex *16 amat2(npts,npts)
C
      eye = dcmplx(0.0D0,1.0d0)
      pi = 4.0D0*datan(1.0d0)
      ipt = 1
      nsc = npts/2
C
      sc = 2*pi/period
ccc      call prin2(' period *',period,1)
      do i = 1,npts
      do j = 1,npts
         amat(i,j) = 0.0d0
         amat2(i,j) = 0.0d0
         do k = 1,nsc+1
            amat(i,j) = amat(i,j) + (sc/npts)*(k-1)*eye*
     1               cdexp(2*pi*eye*(k-1)*(i-j)/npts)
            amat2(i,j) = amat2(i,j) - (sc**2/npts)*((k-1)**2)*
     1               cdexp(2*pi*eye*(k-1)*(i-j)/npts)
         enddo
         do k = 1,nsc-1
            kuse = 1+npts-k
            amat(i,j) = amat(i,j) + (sc/npts)*(-k)*eye*
     1               cdexp(2*pi*eye*(kuse-1)*(i-j)/npts)
            amat2(i,j) = amat2(i,j) - (sc**2/npts)*(k**2)*
     1               cdexp(2*pi*eye*(kuse-1)*(i-j)/npts)
         enddo
         if ( mod(npts,2).eq.1 ) then 
            k = nsc
            kuse = 1+npts-k
            amat(i,j) = amat(i,j) + (sc/npts)*(-k)*eye*
     1               cdexp(2*pi*eye*(kuse-1)*(i-j)/npts)
            amat2(i,j) = amat2(i,j) - (sc**2/npts)*(k**2)*
     1               cdexp(2*pi*eye*(kuse-1)*(i-j)/npts)

         endif
      enddo
      enddo
      return
      end
C
