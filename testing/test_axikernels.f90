program testkernels

  implicit real *8 (a-h,o-z)
  real *8 :: targ(10), src(10), as(100), ks(100)
  real *8 :: ctarg(10), csrc(10), powers(1000000)
  real *8 :: srcs(2,10000), targs(2,10000), par1(1000)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)
  real *8 :: u(10000), dudn(10000), dnorms(2,10000)
  real *8 :: dgrad(100), dgrad0(100)
  real *8 :: dsdt(10000), w(1000000), w2(1000000)
  real *8 :: src3(10), targ3(10)
  real *8 :: xyz(10), v(10000), whts(10000), x(10000)
  real *8 :: s(10000)

  real *8, allocatable :: uall(:,:), dvals(:), dgrads(:,:)
  real *8, allocatable :: dgrad0s(:,:), rerrs(:), zerrs(:)
  real *8, allocatable :: dudnall(:,:), umat(:,:), amat(:,:)

  complex *16 :: ima, cd, cfac, cd2, cdtot, zk
  
  complex *16, allocatable :: umodes(:,:), dudnmodes(:,:)
  complex *16, allocatable :: uall2(:,:), dudnall2(:,:)
  complex *16, allocatable :: vals(:), grads(:,:), grad0s(:,:)
  complex *16, allocatable :: cerrs(:)
  
  real *16 :: qpi, qone, qsrc(10), qtarg(10), qpar1, hq, rq, zq
  real *16 :: qsrcs(2,10000), qtargs(2,10000), parsq(100)
  real *16 :: r0q, z0q, doneq, piq, epsq, aq, bq, qval, qpar0

  complex *32 :: imaq, zkq, qcd, qzval, qzgrad(10), qzgrad0(10)
  complex *32 :: qzval2, qzgrad2(10), qzgrad02(10)
  
  complex *32, allocatable :: qvals(:), qgrads(:,:), qgrad0s(:,:)
  complex *32, allocatable :: qvals1(:), qzvals2(:), qgrads2(:,:)

  ima = (0,1)
  
  call prini(6,13)

  done = 1
  qone = 1
  qpi = 4*atan(qone)
  pi = 4*atan(done)

  write(*,*) 'pi = ', pi
  write(*,*) 'qpi = ', qpi



  
  print *
  print *
  write(6,*) '------------- testing helmholtz kernels ------'
  write(13,*) '------------- testing helmholtz kernels ------'

  !
  ! first test the laplace routine against a quadruple precision
  ! integrator
  !
  imaq = (0,1)
  zkq = qone + imaq/1000
  zkq = 31.1234q0 + imaq/10000
 
  maxm = 20
  mode = 3
  qsrc(1) = 1.21q0
  qsrc(2) = 2.4q0
  qtarg(1) = 1.2q0
  qtarg(2) = 2.41q0


  call prinq('source = *', qsrc, 2)
  call prinq('target = *', qtarg, 2)
  call prinf('mode = *', mode, 1)
  call prinq('wavenumber = *', zkq, 2)

  allocate(qvals(-maxm:maxm))
  allocate(qgrads(2,-maxm:maxm))
  allocate(qgrad0s(2,-maxm:maxm))
  call gkmall_quad_adaptive(zkq, qsrc, qtarg, qpar1, &
      maxm, qvals, qgrads, qgrad0s)

  call prinq('from gkmall_quad_adaptive, qvals = *', qvals, &
      2*(2*maxm+1))
  call prinq('from gkmall_quad_adaptive, qgrads = *', qgrads, &
      2*2*(2*maxm+1))
  call prinq('from gkmall_quad_adaptive, qgrad0s = *', qgrad0s, &
      2*2*(2*maxm+1))

  !
  ! now test the split routines which use fft convolutions
  !
  src(1) = qsrc(1)
  src(2) = qsrc(2)
  targ(1) = qtarg(1)
  targ(2) = qtarg(2)
  zk = zkq

  allocate(vals(-maxm:maxm))
  allocate(grads(2,-maxm:maxm))
  allocate(grad0s(2,-maxm:maxm))
  call gkmall(zk, src, targ, par1, maxm, vals, grads, grad0s)

  call prin2('gk vals = *', vals, 2*(2*maxm+1))
  call prin2('g0 grads = *', grads, 2*2*(2*maxm+1))
  call prin2('g0 grad0s = *', grad0s, 2*2*(2*maxm+1))

  do i = -maxm,maxm
    vals(i) = vals(i) - qvals(i)
    grads(1,i) = grads(1,i) - qgrads(1,i)
    grads(2,i) = grads(2,i) - qgrads(2,i)
    grad0s(1,i) = grad0s(1,i) - qgrad0s(1,i)
    grad0s(2,i) = grad0s(2,i) - qgrad0s(2,i)
  enddo

  call prin2('from recurrence, err in vals= *', vals, 2*(2*maxm+1))

  call prin2('from recurrence, err in grads= *', &
      grads, 2*2*(2*maxm+1))
  call prin2('from recurrence, err in grad0s= *', &
      grad0s, 2*2*(2*maxm+1))


  !
  ! test green's identity for the laplace kernels
  !
  print *
  print *
  write(6,*) '---------- testing greens identity ------'
  write(6,*) '---------- single mode  ------'
  write(13,*) '---------- testing greens identity ------'
  write(13,*) '---------- single mode  ------'


  targ(1) = 5
  targ(2) = 5
  src(1) = 2.2d0
  src(2) = .9d0

  mode = 2
  call prinf('mode = *', mode, 1)
  call g0mone(par0, src, targ, par1, mode, dval, dgrad, dgrad0)

  call prin2('src = *', src, 2)
  call prin2('targ = *', targ, 2)
  call prin2('potential = *', dval, 1)
  !!call prin2('target gradient = *', dgrad, 2)
  !!call prin2('source gradient = *', dgrad0, 2)

  !
  ! generate a geometry
  !
  dltot = 2*pi
  n = 100
  h = dltot/n
  do i = 1,n
    t = (i-1)*h
    call funcurve(t, xys(1,i), dxys(1,i), d2xys(1,i))
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do

  call prin2('xys = *', xys, 20)
  call prin2('dxys = *', dxys, 20)
  call prin2('d2xys = *', d2xys, 20)
  call prin2('dsdt = *', dsdt, 20)
  call prin2('dnorms = *', dnorms, 20)
  
  do i = 1,n
    call g0mone(par0, src, xys(1,i), par1, mode, u(i), &
        dgrad, dgrad0)
    dudn(i) = dgrad(1)*dnorms(1,i) + dgrad(2)*dnorms(2,i)
  end do

  !call prin2('u = *', u, n)
  !call prin2('dudn = *', dudn, n)
  !stop
  
  pot = 0
  do i = 1,n
    call g0mone(par0, xys(1,i), targ, par1, mode, slp, &
        dgrad, dgrad0)
    dlp = dgrad0(1)*dnorms(1,i) + dgrad0(2)*dnorms(2,i)
    pot = pot + (dlp*u(i) - slp*dudn(i))*dsdt(i)*h*xys(1,i)*2*pi
  end do

  call prin2('directly, pot = *', dval, 1)
  call prin2('from greens, pot = *', pot, 1)
  call prin2('diff = *', pot-dval, 1)
  call prin2('ratio = *', pot/dval, 1)
  call prin2('inverse ratio = *', dval/pot, 1)


  !
  ! now do a multi-mode greens identity check by decomposining a
  ! point source
  !
  print *
  print *
  write(6,*) '---------- testing greens identity ------'
  write(6,*) '---------- multi-modes ------'
  write(13,*) '---------- testing greens identity ------'
  write(13,*) '---------- multi-modes ------'

  theta = pi/5
  theta0 = -pi/1.2340d0
  targ3(1) = targ(1)*cos(theta)
  targ3(2) = targ(1)*sin(theta)
  targ3(3) = targ(2)
  src3(1) = src(1)*cos(theta0)
  src3(2) = src(1)*sin(theta0)
  src3(3) = src(2)

  call prin2('targ3 = *', targ3, 3)
  call prin2('src3 = *', src3, 3)

  call g0cart(par0, src3, targ3, par1, par2, dval, dgrad, dgrad0)
  call prin2('freespace potential, dval = *', dval, 1)
  call prin2('freespace grad, dgrad = *', dgrad, 3)
  call prin2('freespace grad0, dgrad0 = *', dgrad0, 3)

  !
  ! now evaluate the potential and dudn everywhere on the curve and
  ! fourier decompose, then integrate
  !
  nphi = 300
  hphi = 2*pi/nphi
  allocate(uall(nphi,n))
  allocate(dudnall(nphi,n))

  do i = 1,n
    do j = 1,nphi
      phi = (j-1)*hphi
      xyz(1) = xys(1,i)*cos(phi)
      xyz(2) = xys(1,i)*sin(phi)
      xyz(3) = xys(2,i)
      call g0cart(par0, src3, xyz, par1, par2, uall(j,i), &
          dgrad, dgrad0)
      dudnall(j,i) = dnorms(1,i)*cos(phi)*dgrad(1) + &
          dnorms(1,i)*sin(phi)*dgrad(2) + dnorms(2,i)*dgrad(3)
    enddo
  enddo

  !!call prin2('uall = *', uall, n*nphi)
  !!call prin2('dudnall = *', dudnall, n*nphi)

  !
  ! fourier decompose all the data
  !
  allocate(umodes(n,nphi))
  allocate(dudnmodes(n,nphi))
  ima = (0,1)
  do i = 1,n
    do j = 1,nphi
      cd = 0
      cd2 = 0
      do k = 1,nphi
        cd = cd + uall(k,i)*exp(-ima*2*pi*(j-1)*(k-1)/nphi)
        cd2 = cd2 + dudnall(k,i)*exp(-ima*2*pi*(j-1)*(k-1)/nphi)
      end do
      umodes(i,j) = cd/nphi
      dudnmodes(i,j) = cd2/nphi
    end do
  end do

  errmax = -1
  errmax2 = -1
  do i = 1,n
    nphi2 = nphi/2
    err = abs(umodes(i,nphi2))**2 + abs(umodes(i,nphi2-1))**2 &
        + abs(umodes(i,nphi2+1))**2 
    err = sqrt(err/3)
    if (err .gt. errmax) errmax = err
    err2 = abs(dudnmodes(i,nphi2))**2 &
        + abs(dudnmodes(i,nphi2-1))**2 &
        + abs(dudnmodes(i,nphi2+1))**2 
    err2 = sqrt(err2/3)
    if (err2 .gt. errmax2) errmax2 = err2
  end do
  
  call prin2('max error estimate in umodes= *', errmax, 1)
  call prin2('max error estimate in dudnmodes= *', errmax2, 1)


  !
  ! synthesize and test
  !
  allocate(uall2(nphi,n))
  allocate(dudnall2(nphi,n))

  do i = 1,n
    do j = 1,nphi
      cd = 0
      cd2 = 0
      do k = 1,nphi
        cd = cd + umodes(i,k)*exp(ima*2*pi*(j-1)*(k-1)/nphi)
        cd2 = cd2 + dudnmodes(i,k)*exp(ima*2*pi*(j-1)*(k-1)/nphi)
      end do
      uall2(j,i) = cd
      dudnall2(j,i) = cd2
    end do
  end do

  err1 = 0
  err2 = 0
  do i = 1,n
    do j = 1,nphi
      err1 = abs(uall2(j,i) - uall(j,i))**2
      err2 = abs(dudnall2(j,i) - dudnall(j,i))**2
    end do
  end do

  err1 = sqrt(err1/n/nphi)
  err2 = sqrt(err2/n/nphi)
  
  call prin2('rmse difference in uall = *', err1, 1)
  call prin2('rmse difference in dudnall = *', err2, 1)
  
  
  !
  ! now apply all the individual layer potentials
  !
  maxm = nphi/2 + 20
  deallocate(vals, grads, grad0s)
  allocate(vals(-maxm:maxm))
  allocate(grads(2,-maxm:maxm))
  allocate(grad0s(2,-maxm:maxm))

  nphi2 = nphi/2
  cdtot = 0
  
  do i = 1,nphi2

    cd = 0
    cd2 = 0
    do j = 1,n

      mode = i-1
      call g0mone(par0, xys(1,j), targ, par1, mode, slp, &
          dgrad, dgrad0)
      dlp = dgrad0(1)*dnorms(1,j) + dgrad0(2)*dnorms(2,j)

      wht = dsdt(j)*h*xys(1,j)
      cd = cd + (dlp*umodes(j,i) - slp*dudnmodes(j,i))*wht

      mode = -i
      call g0mone(par0, xys(1,j), targ, par1, mode, slp, &
          dgrad, dgrad0)
      dlp = dgrad0(1)*dnorms(1,j) + dgrad0(2)*dnorms(2,j)

      wht = dsdt(j)*h*xys(1,j)
      cd2 = cd2 + (dlp*umodes(j,nphi-i+1) &
          - slp*dudnmodes(j,nphi-i+1))*wht

    end do

    cdtot = cdtot + cd*exp(ima*(i-1)*theta) + cd2*exp(ima*(-i)*theta)
    
  end do

  cdtot = cdtot*2*pi


  
  call prin2('exactly, pot = *', dval, 1)
  call prin2('from 3d greens, pot = *', cdtot, 2)
  call prin2('difference = *', dval - cdtot, 2)
  call prin2('ratio = *', cdtot/dval, 2)
  call prin2('inverse ratio = *', dval/cdtot, 2)


  
end program testkernels





subroutine funcurve(t, xy, dxy, d2xy)
  implicit real *8 (a-h,o-z)
  real *8 :: xy(2), dxy(2), d2xy(2)

  x0 = 2
  y0 = 1
  a = 1d0
  b = 1d0

  xy(1) = x0 + a*cos(t)
  xy(2) = y0 + b*sin(t)

  dxy(1) = -a*sin(t)
  dxy(2) = b*cos(t)

  d2xy(1) = -a*cos(t)
  d2xy(2) = -b*sin(t)
  return
end subroutine funcurve




  
