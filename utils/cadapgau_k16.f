cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c        this is the end of the debugging code and the
c        start of the actual quadrature routines.
c 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c 
c 
c 
c 
        subroutine cadapgau(ier,a,b,fun,par1,par2,m7,eps,
     1      rint,maxrec,numint)
        implicit real *8 (a-h,o-z)
        complex *16 rint,vals(20000)
        dimension t(16),w(16),stack(40000),
     1      par1(1),par2(1)
c 
c       this subroutine uses the adaptive chebychev quadrature
c       to evaluate the integral of the user-supplied function
c       fun on the user-specified interval [a,b]
c 
c                       input parameters:
c 
c  a,b - the ends of the interval on which the integral is to
c       be evaluated
c  fun - the user-supplied function to be integrated. the calling
c       sequence of fun must be
c 
c        fun(x,par1,par2).                            (1)
c 
c        in (1), x is a point on the interval [a,b] where
c        the function is to be evaluated, and par1, par2
c        are two parameters to be used by fun; they can be
c        variables or arrays, real or integer, as desired.
c        fun is assumed to be real *8.
c  par1, par2 - partameters to be used by the user-supplied
c       function fun (see above)
c  m7 - the order of the quadrature to me used on each subinterval - not
c       used, see definition of m below
c  eps - the accuracy (absolute) to which the integral will be
c       evaluated
c 
c                       output parameters:
c 
c  ier - error return code.
c          ier=0 means normal conclusion
c          ier=8 means that at some point, one subinterval in the
c                subdivision was smaller than (b-a)/2**200. this
c                is a fatal error.
c          ier=16 means that the total number of subintervals in the
c                adaptive subdivision of [a,b] turned out to be greater
c                than 100000.  this is a fatal error.
c 
c  rint - the integral as evaluated
c  maxrec - the maximum depth to which the recursion went at its
c         deepest point. can not be greater than 200, since at that
c         point ier is set to 8 and the execution of the subroutine
c         terminated.
c  numint - the totla number of intervals in the subdivision. can not
c         be greater than 100000,  since at that
c         point ier is set to 16 and the execution of the subroutine
c         terminated.
c 
c 
c          . . . check if the chebychev quadarture has been
c                initialized at a preceeding call to this routine
c 
        data t/ -0.98940093499164994d0,
     2          -0.94457502307323260d0,
     3          -0.86563120238783176d0,
     4          -0.75540440835500300d0,
     5          -0.61787624440264377d0,
     6          -0.45801677765722737d0,
     7          -0.28160355077925892d0,
     8          -9.50125098376374266d-2,
     9           9.50125098376374266d-2,
     *           0.28160355077925892d0,
     1           0.45801677765722737d0,
     2           0.61787624440264377d0,
     3           0.75540440835500300d0,
     4           0.86563120238783176d0,
     5           0.94457502307323260d0,
     6           0.98940093499164994d0/
c
        data w/2.71524594117540236d-2,
     1         6.22535239386477895d-2,
     1         9.51585116824927996d-2,
     1         0.12462897125553392d0,     
     1         0.14959598881657676d0,     
     1         0.16915651939500259d0,     
     1         0.18260341504492361d0,     
     1         0.18945061045506850d0,     
     1         0.18945061045506850d0,     
     1         0.18260341504492361d0,     
     1         0.16915651939500259d0,     
     1         0.14959598881657676d0,     
     1         0.12462897125553392d0,     
     1         9.51585116824927996d-2,
     1         6.22535239386477895d-2,
     1         2.71524594117540236d-2/

c
c       force the use of m=16
c
        m=16

c 
c        integrate the user-supplied function using the
c        adaptive gaussian quadratures
c 
        nnmax=100000
        maxdepth=200
c 
        call cadinrec(ier,stack,a,b,fun,par1,par2,t,w,m,
     1      vals,nnmax,eps,rint,maxdepth,maxrec,numint)
        return
        end
c 
c 
c 
c 
c 
        subroutine cadinrec(ier,stack,a,b,fun,
     1      par1,par2,t,w,m,vals,nnmax,eps,
     2      rint,maxdepth,maxrec,numint)
        implicit real *8 (a-h,o-z)
        complex *16 rint,value2,value3,vals(1)
        dimension stack(2,1),t(1),w(1),par1(1),par2(1)
c 
c       start the recursion
c 
        stack(1,1)=a
        stack(2,1)=b
        call coneint(a,b,fun,par1,par2,t,w,m,vals(1))


c 
c       recursively integrate the thing
c 
        j=1
        rint=0
        ier=0
        maxrec=0

        do 3000 i=1,nnmax
c
        numint=i
        if(j .gt. maxrec) maxrec=j
c 
c       subdivide the current subinterval
c 
         c=(stack(1,j)+stack(2,j))/2
        call coneint(stack(1,j),c,fun,
     1      par1,par2,t,w,m,value2)
c 
        call coneint(c,stack(2,j),fun,
     1      par1,par2,t,w,m,value3)
c
cccc        dd=cdabs(value2+value3-vals(j))

        if (abs(vals(j)) .gt. 1.0d0) then
          dd = abs(value2+value3-vals(j))/abs(vals(j))
        else
          dd = abs(value2+value3-vals(j))
        end if
        
        ifdone=0
        if(dd .le. eps) ifdone=1
c 
c       if the function on this subinterval has been
c       integrated with sufficient accuracy - add the
c       value to that of the global integral and move up
c       in the stack
c 
        if(ifdone  .eq. 0) goto 2000
c 
        rint=rint+value2+value3
        j=j-1
c 
c        if the whole thing has been integrated - return
c 
        if(j .eq. 0) return
        goto 3000
 2000 continue
c 
c       if the function on this subinterval has not been
c       integrated with sufficient accuracy - move
c       down the stack
c 
        stack(1,j+1)=stack(1,j)
        stack(2,j+1)=(stack(1,j)+stack(2,j))/2
        vals(j+1)=value2
c 
        stack(1,j)=(stack(1,j)+stack(2,j))/2
        vals(j)=value3
c 
        j=j+1
c 
c       if the depth of the recursion has become excessive - bomb
c 
        if(j .le. maxdepth) goto 3000
        ier=8
        return
c
 3000 continue
        ier=16
c
        return
        end
c 
c 
c 
c 
c 
        subroutine coneint(a,b,fun,par1,par2,t,w,m,rint)
        implicit real *8 (a-h,o-z)
        dimension t(1),w(1),par1(1),par2(1)
        complex *16 rint,fun
c 
c       integrate the function fun on the interval [a,b]
c 
        rint=0
        u=(b-a)/2
        v=(b+a)/2
        do 1200 i=1,m
        tt=u*t(i)+v
        rint=rint+fun(tt,par1,par2)*w(i)
 1200 continue
        rint=rint*u
        return
        end
