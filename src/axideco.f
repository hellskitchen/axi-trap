c
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       this is the end of the debugging code and the beginning
c       of the subroutines for constructing electric and magnetic
c       field plane waves and their projections onto azimuthal 
c       modes in cylindrical coordinates
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
        subroutine axisynth1(n,nphi,rl,ps,zs,dpdt,dzdt,modemax,
     1      rhomodes,npts,xyzs,whts,zcharge)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),xyzs(3,1),whts(1),
     1      dsdt(10000)
        complex *16 rhomodes(n,-modemax:modemax),zcharge(1),ima,cd
c
c       synthesize a scalar function on a surface of revolution given
c       by its fourier modes
c
        done=1
        ima=(0,1)
        pi=4*atan(done)

        do 1400 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 1400   continue


        h=2*pi/nphi
        npts=0
c
        do 5800 i=1,n
c
        dpds=dpdt(i)/dsdt(i)
        dzds=dzdt(i)/dsdt(i)
        ds=dsdt(i)*rl/n
        dphi=ps(i)*h
c
        do 5600 j=1,nphi
c
        phi=(j-1)*h
        sn=sin(phi)
        cs=cos(phi)
c
        npts=npts+1
c
        xyzs(1,npts)=ps(i)*cs
        xyzs(2,npts)=ps(i)*sn
        xyzs(3,npts)=zs(i)
c
        whts(npts)=ds*dphi
c
        zcharge(npts)=0
c
        do 5400 mode=-modemax,modemax
c
        cd=exp(ima*mode*phi)
        zcharge(npts)=zcharge(npts)+rhomodes(i,mode)*cd
c
 5400 continue
c
 5600 continue
 5800 continue
c
        return
        end


c
c
c
c
c
        subroutine axisynth3(n,nphi,rl,ps,zs,dpdt,dzdt,modemax,
     1      zjmodes,npts,xyzs,whts,zcurrent)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),xyzs(3,1),whts(1),
     1      dsdt(10000)
        complex *16 zjmodes(2,n,-modemax:modemax),zcurrent(3,1),
     1      ima,cd,cd1,cd2,cd3
c
c       synthesize a vector valued function on a surface of revolution
c       given by its fourier modes, input is in local coordinates
c       (tau-hat and theta-hat, along the generating curve) and output
c       is in global cartesian coordinates.
c

        done=1
        ima=(0,1)
        pi=4*atan(done)

        do i=1,n
          dd=dpdt(i)**2+dzdt(i)**2
          dsdt(i)=sqrt(dd)
        enddo
c
        h=2*pi/nphi
        npts=0
c
        do i=1,n
c
          dpds=dpdt(i)/dsdt(i)
          dzds=dzdt(i)/dsdt(i)
          ds=dsdt(i)*rl/n
          dphi=ps(i)*h
c
          do j=1,nphi
c
            phi=(j-1)*h
            sn=sin(phi)
            cs=cos(phi)
c
            npts=npts+1
            xyzs(1,npts)=ps(i)*cs
            xyzs(2,npts)=ps(i)*sn
            xyzs(3,npts)=zs(i)
c
            whts(npts)=ds*dphi
            cd1=0
            cd2=0
            cd3=0
c
            do mode=-modemax,modemax
              cd=exp(ima*mode*phi)
              cd1=cd1+(zjmodes(1,i,mode)*dpds*cs
     1            -zjmodes(2,i,mode)*sn)*cd
              cd2=cd2+(zjmodes(1,i,mode)*dpds*sn
     2            +zjmodes(2,i,mode)*cs)*cd
              cd3=cd3+(zjmodes(1,i,mode)*dzds)*cd
            enddo
c
            zcurrent(1,npts)=cd1
            zcurrent(2,npts)=cd2
            zcurrent(3,npts)=cd3
c
          enddo
        enddo
c
        return
        end
c
c
c
c
c
c$$$        subroutine axidebmodes_cyl(center,zk,nterms,ampole,bmpole,
c$$$     1      n,ps,zs,nmodes,emodes,hmodes)
c$$$        implicit real *8 (a-h,o-z)
c$$$        real *8 center(3),ps(1),zs(1)
c$$$        complex *16 zk,ampole(0:nterms,-nterms:nterms),ima,
c$$$     1      bmpole(0:nterms,-nterms:nterms),
c$$$     2      emodes(3,n,nmodes),hmodes(3,n,nmodes),cdx,cdy
c$$$c
c$$$        real *8, allocatable :: xnodes(:,:,:)
c$$$        complex *16, allocatable :: esurf(:,:,:)
c$$$        complex *16, allocatable :: hsurf(:,:,:)
c$$$c
c$$$c       this routine just eliminates some manual-ness when constructing
c$$$c       the axisymmetric modes of a debye e/h field - it operates
c$$$c       exactly as the routine axidebmodes except in that it returns
c$$$c       things in cylindrical coordinates
c$$$c
c$$$c       input:
c$$$c
c$$$c         center - center of the debye expansion
c$$$c         zk - helmholtz parameter
c$$$c         nterms - order of partial wave expansion
c$$$c         ampole,bmpole - partial wave expansion coefficients
c$$$c         n - number of points on the generating curve
c$$$c         ps,zs - the generating curve
c$$$c         nmodes - number of fourier modes to expand into (i.e.
c$$$c             the number of pointz in the azimuthal direction)
c$$$c
c$$$c       output:
c$$$c
c$$$c         emodes,hmodes - the fourier transform of the fields, 
c$$$c             returned in cylindrical coordinates
c$$$c
c$$$c
c$$$        allocate( xnodes(3,n,nmodes) )
c$$$        allocate( esurf(3,n,nmodes) )
c$$$        allocate( hsurf(3,n,nmodes) )
c$$$c
c$$$        ima=(0,1)
c$$$        done=1
c$$$        pi=4*atan(done)
c$$$c
c$$$c       construct the nodes on the the surface of revolution
c$$$c
c$$$        h=2*pi/nmodes
c$$$        do 1400 i=1,nmodes
c$$$        phi=h*(i-1)
c$$$c
c$$$        do 1200 j=1,n
c$$$        xnodes(1,j,i)=ps(j)*cos(phi)
c$$$        xnodes(2,j,i)=ps(j)*sin(phi)
c$$$        xnodes(3,j,i)=zs(j)
c$$$ 1200 continue
c$$$ 1400 continue
c$$$
c$$$c
c$$$c       now evaluate the e/h fields at each point
c$$$c
c$$$        do 2400 i=1,nmodes
c$$$        do 2200 j=1,n
c$$$        call em3mpeval(zk,center,ampole,bmpole,nterms,xnodes(1,j,i),
c$$$     1      esurf(1,j,i),hsurf(1,j,i))
c$$$ 2200 continue
c$$$ 2400 continue
c$$$
c$$$c
c$$$c       go from cartesian to cylindrical
c$$$c
c$$$        do 3400 i=1,nmodes
c$$$        do 3200 j=1,n
c$$$c
c$$$        cdx=esurf(1,j,i)
c$$$        cdy=esurf(2,j,i)
c$$$c
c$$$        x=xnodes(1,j,i)
c$$$        y=xnodes(2,j,i)
c$$$        phi=atan2(y,x)
c$$$c
c$$$        esurf(1,j,i)=cdx*cos(phi)+cdy*sin(phi)
c$$$        esurf(2,j,i)=cdy*cos(phi)-cdx*sin(phi)
c$$$c
c$$$        cdx=hsurf(1,j,i)
c$$$        cdy=hsurf(2,j,i)
c$$$        hsurf(1,j,i)=cdx*cos(phi)+cdy*sin(phi)
c$$$        hsurf(2,j,i)=cdy*cos(phi)-cdx*sin(phi)
c$$$c
c$$$ 3200 continue
c$$$ 3400 continue
c$$$
c$$$c
c$$$c       and decompose into azimuthal modes
c$$$c
c$$$        call axideco(n,nmodes,esurf,hsurf,emodes,hmodes)
c$$$c
c$$$        return
c$$$        end
c
c
c
c
c
c$$$        subroutine axidebmodes(center,zk,nterms,ampole,bmpole,
c$$$     1      n,ps,zs,nmodes,emodes,hmodes)
c$$$        implicit real *8 (a-h,o-z)
c$$$        real *8 center(3),ps(1),zs(1)
c$$$        complex *16 zk,ampole(0:nterms,-nterms:nterms),ima,
c$$$     1      bmpole(0:nterms,-nterms:nterms),
c$$$     2      emodes(3,n,nmodes),hmodes(3,n,nmodes)
c$$$c
c$$$        real *8, allocatable :: xnodes(:,:,:)
c$$$        complex *16, allocatable :: esurf(:,:,:)
c$$$        complex *16, allocatable :: hsurf(:,:,:)
c$$$c
c$$$c       this routine just eliminates some manual-ness when constructing
c$$$c       the axisymmetric modes of a debye e/h field
c$$$c
c$$$c       input:
c$$$c
c$$$c         center - center of the debye expansion
c$$$c         zk - helmholtz parameter
c$$$c         nterms - order of partial wave expansion
c$$$c         ampole,bmpole - partial wave expansion coefficients
c$$$c         n - number of points on the generating curve
c$$$c         ps,zs - the generating curve
c$$$c         nmodes - number of fourier modes to expand into (i.e.
c$$$c             the number of pointz in the azimuthal direction)
c$$$c
c$$$c       output:
c$$$c
c$$$c         emodes,hmodes - the fourier transform of the fields
c$$$c
c$$$c
c$$$        allocate( xnodes(3,n,nmodes) )
c$$$        allocate( esurf(3,n,nmodes) )
c$$$        allocate( hsurf(3,n,nmodes) )
c$$$c
c$$$        ima=(0,1)
c$$$        done=1
c$$$        pi=4*atan(done)
c$$$c
c$$$c       construct the nodes on the the surface of revolution
c$$$c
c$$$        h=2*pi/nmodes
c$$$        do 1400 i=1,nmodes
c$$$        phi=h*(i-1)
c$$$c
c$$$        do 1200 j=1,n
c$$$        xnodes(1,j,i)=ps(j)*cos(phi)
c$$$        xnodes(2,j,i)=ps(j)*sin(phi)
c$$$        xnodes(3,j,i)=zs(j)
c$$$ 1200 continue
c$$$ 1400 continue
c$$$
c$$$c
c$$$c       now evaluate the e/h fields at each point
c$$$c
c$$$        do 2400 i=1,nmodes
c$$$        do 2200 j=1,n
c$$$        call em3mpeval(zk,center,ampole,bmpole,nterms,xnodes(1,j,i),
c$$$     1      esurf(1,j,i),hsurf(1,j,i))
c$$$ 2200 continue
c$$$ 2400 continue
c$$$
c$$$c
c$$$c       and decompose into azimuthal modes
c$$$c
c$$$        call axideco(n,nmodes,esurf,hsurf,emodes,hmodes)
c$$$c
c$$$        return
c$$$        end
c
c
c
c
c
        subroutine axideco(n,nphi,ein,hin,emodes,hmodes)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1)
        complex *16 ein(3,n,nphi),hin(3,n,nphi),emodes(3,n,nphi),
     1      hmodes(3,n,nphi),ima,e1(10000),e2(10000),e3(10000),
     2      h1(10000),h2(10000),h3(10000),wsave(2000000)
c
c       this routine receives as input an incoming field ein,hin
c       which has been evaluated at n*nphi points on the surface
c       of rotation with generating curve ps,zs. it then decomposes
c       this incoming field into n separate modes via fft.
c
c                 input parameters:
c
c  n - number of points on generating curve
c  nphi - number of points in the azimuthal direction, starting at phi=0,
c       needs to be LESS than 10000
c  ein,hin - incoming electric and magnetic fields, indexed as
c
c            ein(component, point on curve, point in phi)
c            hin(component, point on curve, point in phi)
c
c                 output parameters:
c
c  emodes,hmodes - incoming fields decomposed into modes so that
c
c            ein(i,j,k) = \sum_l=0^n-1  emodes(i,j,l) exp(ima theta l)
c            hin(i,j,k) = \sum_l=0^n-1  hmodes(i,j,l) exp(ima theta l)
c
c       where theta=2*pi*(k-1)/nphi, i.e., just the fourier 
c       decomposition of ein,hin in the phi variable
c
c
        ima=(0,1)
        done=1
c
c       for each e and h, need to do 3n ffts, one for each component
c
        call zffti(nphi,wsave)
c
        do 3000 i=1,n
c
        do 2600 j=1,nphi
        e1(j)=ein(1,i,j)
        e2(j)=ein(2,i,j)
        e3(j)=ein(3,i,j)
        h1(j)=hin(1,i,j)
        h2(j)=hin(2,i,j)
        h3(j)=hin(3,i,j)
 2600 continue

c      call prin2('e1 = *', e1, 12)
c      call prin2('e2 = *', e2, 12)
c      call prin2('e3 = *', e3, 12)
c      return
c
c       fft in phi...
c
        call zfftf(nphi,e1,wsave)
        call zfftf(nphi,e2,wsave)
        call zfftf(nphi,e3,wsave)
        call zfftf(nphi,h1,wsave)
        call zfftf(nphi,h2,wsave)
        call zfftf(nphi,h3,wsave)

c
c       ...load into emodes and hmodes
c
        do 2200 j=1,nphi
        emodes(1,i,j)=e1(j)/nphi
        emodes(2,i,j)=e2(j)/nphi
        emodes(3,i,j)=e3(j)/nphi
        hmodes(1,i,j)=h1(j)/nphi
        hmodes(2,i,j)=h2(j)/nphi
        hmodes(3,i,j)=h3(j)/nphi
 2200 continue

c      call prin2('emodes = *', emodes, 30)
c      return
c
 3000 continue

c
        return
        end
