


subroutine axideco_fast(nphi, n, xyzs, ein, hin, emodes, hmodes)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,nphi,n)
  complex *16 :: ein(3,nphi,n), hin(3,nphi,n), emodes(3,n,nphi)
  complex *16 :: hmodes(3,n,nphi)

  complex *16 :: ima, rtz(10), evec(10), hvec(10)

  complex *16, allocatable :: e1(:), e2(:), e3(:)
  complex *16, allocatable :: h1(:), h2(:), h3(:), wsave(:)

  !
  ! This routine receives as input an incoming field ein,hin which has
  ! been evaluated at n*nphi points on the surface of rotation with
  ! generating curve ps,zs and whose components are in CARTESIAN
  ! COORDINATES. It then decomposes this incoming field into n
  ! separate modes via fft IN CYLINDRICAL COORDINATES.  Note that the
  ! ordering of values in ein and hin is very important:
  !
  !     ein(i,j,k) = ith component, jth point in phi, kth point
  !           around the generating curve
  !
  ! Input:
  !   nphi, n - dimensions of ein,hin
  !   xyzs - points on the surface
  !   ein,hin - incoming electric and magnetic fields
  !
  ! Output:
  !   emodes,hmodes - incoming fields decomposed into modes so that
  !
  !        ein(i,j,k) = \sum_l=0^n-1  emodes(i,j,l) exp(ima theta l)
  !        hin(i,j,k) = \sum_l=0^n-1  hmodes(i,j,l) exp(ima theta l)
  !
  !      where theta=2*pi*(k-1)/nphi, i.e., just the fourier
  !      decomposition of ein,hin in the phi variable.
  ! 

  ima=(0,1)
  done=1

      
  
  !
  ! for each e and h, need to do 3n ffts, one for each component
  !
  allocate(wsave(10*nphi))
  call zffti(nphi,wsave)

  allocate(e1(nphi), e2(nphi), e3(nphi))
  allocate(h1(nphi), h2(nphi), h3(nphi))

  do i=1,n

    do j=1,nphi
      call axi_cart2cyl(xyzs(1,j,i), ein(1,j,i), rtz, evec)
      e1(j)=evec(1)
      e2(j)=evec(2)
      e3(j)=evec(3)
      call axi_cart2cyl(xyzs(1,j,i), hin(1,j,i), rtz, hvec)
      h1(j)=hvec(1)
      h2(j)=hvec(2)
      h3(j)=hvec(3)
    end do

    !
    ! fft in phi...
    !
    call zfftf(nphi,e1,wsave)
    call zfftf(nphi,e2,wsave)
    call zfftf(nphi,e3,wsave)
    call zfftf(nphi,h1,wsave)
    call zfftf(nphi,h2,wsave)
    call zfftf(nphi,h3,wsave)
    
    !
    ! ...load into emodes and hmodes
    !
    do j=1,nphi
      emodes(1,i,j)=e1(j)/nphi
      emodes(2,i,j)=e2(j)/nphi
      emodes(3,i,j)=e3(j)/nphi
      hmodes(1,i,j)=h1(j)/nphi
      hmodes(2,i,j)=h2(j)/nphi
      hmodes(3,i,j)=h3(j)/nphi
    enddo

  end do
    
  return
end subroutine axideco_fast




        
subroutine axisynth1_new(n, xys, dxys, h, maxm, nphi, &
    rhomodes, xyzs, whts, zcharge)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), xyzs(3,nphi,n), whts(nphi,n)
  complex *16 :: rhomodes(n,-maxm:maxm), zcharge(nphi,n)

  complex *16 :: ima,cd

  !
  ! synthesize a scalar function on a surface of revolution given
  ! by its fourier modes
  !

  done=1
  ima=(0,1)
  pi=4*atan(done)

  hphi = 2*pi/nphi



  
  !$omp parallel do default(shared) &
  !$omp     private(dsdt, phi, cd, i, j, mode)
  do i=1,n
    dsdt = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    do j=1,nphi
      phi=(j-1)*hphi
      xyzs(1,j,i) = xys(1,i)*cos(phi)
      xyzs(2,j,i) = xys(1,i)*sin(phi)
      xyzs(3,j,i) = xys(2,i)

      whts(j,i) = dsdt*h*xys(1,i)*hphi
      zcharge(j,i)=0

      do mode = -maxm,maxm
        cd = exp(ima*mode*phi)
        zcharge(j,i) = zcharge(j,i) + rhomodes(i,mode)*cd
      end do
      
    end do
  end do
  !$omp end parallel do
  
  return
end subroutine axisynth1_new




subroutine axisynth1_fast(n, xys, dxys, h, maxm, nphi, &
    rhomodes, zcharge)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: rhomodes(n,-maxm:maxm), zcharge(nphi,n)

  complex *16 :: ima, cd, wsave(500000)

  !
  ! synthesize a scalar function on a surface of revolution given
  ! by its fourier modes
  !

  done=1
  ima=(0,1)
  pi=4*atan(done)

  hphi = 2*pi/nphi

  !
  ! copy over rhomodes into zcharge, call inverse fft
  !
  call zffti(nphi, wsave)


!!!$omp parallel default(shared)
  !!!$omp do
  do j = 1,n
    do i = 1,nphi
      zcharge(i,j) = 0
    end do
  end do
  !!!$omp end do


!!!$omp do
  do i = 1,n
    do m = 0,maxm
      zcharge(m+1,i) = rhomodes(i,m)
    end do
  end do
  !!!$omp end do

  
  !!!$omp do
  do i = 1,n
    do m = 1,maxm
      zcharge(nphi-m+1,i) = rhomodes(i,-m)
    end do
  end do
  !!!$omp end do

  !!!$omp do private(wsave)
  do i = 1,n
    call zffti(nphi, wsave)
    call zfftb(nphi, zcharge(1,i), wsave)
  end do
  !!!$omp end do
  
  !!!!$omp end parallel
  
  !!!!$omp do private(dsdt, phi)
  !do i=1,n
  !  dsdt = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  !  do j=1,nphi
  !   phi=(j-1)*hphi
  !    xyzs(1,j,i) = xys(1,i)*cos(phi)
  !    xyzs(2,j,i) = xys(1,i)*sin(phi)
  !    xyzs(3,j,i) = xys(2,i)
  !    whts(j,i) = dsdt*h*xys(1,i)*hphi
  !  end do
  !end do
  !!!!$omp end do
  !!!$omp end parallel
  
  return
end subroutine axisynth1_fast





subroutine axi_3dwhts(n, xys, dxys, h, nphi, whts)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), whts(nphi,n)

  !
  ! return the smooth trapezoidal-based quadrature weights on the
  ! surface of revolution defined by xys
  !
  done=1
  ima=(0,1)
  pi=4*atan(done)

  hphi = 2*pi/nphi

  !$omp parallel do default(shared) private(dsdt, phi)
  do i=1,n
    dsdt = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    do j=1,nphi
      phi=(j-1)*hphi
      whts(j,i) = dsdt*h*xys(1,i)*hphi
    end do
  end do
  !$omp end parallel do
  
  return
end subroutine axi_3dwhts





subroutine axisynth3_new(n, xys, dxys, h, maxm, nphi, &
    zjmodes, xyzs, whts, zcurrent)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), xyzs(3,nphi,n), whts(nphi,n)
  complex *16 :: zjmodes(2,n,-maxm:maxm), zcurrent(3,nphi,n)

  complex *16 :: ima, cd, cd1, cd2, cd3

  !
  ! synthesize a tangential vector field on a surface of revolution
  ! given by its fourier modes into a vectorfield in cartesian coordinates
  !

  done=1
  ima=(0,1)
  pi=4*atan(done)

  hphi = 2*pi/nphi

  !$omp parallel do default(shared) &
  !$omp     private(dsdt, dpds, dzds, phi, cd1, cd2, cd3) &
  !$omp     private(cs, sn, cd,  i, j, mode)
  do i=1,n

    dsdt = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dpds = dxys(1,i)/dsdt
    dzds = dxys(2,i)/dsdt

    do j=1,nphi
      phi=(j-1)*hphi
      xyzs(1,j,i) = xys(1,i)*cos(phi)
      xyzs(2,j,i) = xys(1,i)*sin(phi)
      xyzs(3,j,i) = xys(2,i)

      whts(j,i) = dsdt*h*xys(1,i)*hphi
      cd1 = 0
      cd2 = 0
      cd3 = 0

      cs = cos(phi)
      sn = sin(phi)
      
      do mode = -maxm,maxm
        cd = exp(ima*mode*phi)
        cd1 = cd1 + (zjmodes(1,i,mode)*dpds*cs &
            - zjmodes(2,i,mode)*sn)*cd
        cd2 = cd2 + (zjmodes(1,i,mode)*dpds*sn &
            + zjmodes(2,i,mode)*cs)*cd
        cd3 = cd3 + (zjmodes(1,i,mode)*dzds)*cd
      enddo

      zcurrent(1,j,i) = cd1
      zcurrent(2,j,i) = cd2
      zcurrent(3,j,i) = cd3

    end do
  end do
  !$omp end parallel do
  
  return
end subroutine axisynth3_new





subroutine axisynth3_fast(n, xys, dxys, h, maxm, nphi, &
    zjmodes, zcurrent)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  
  complex *16 :: zjmodes(2,n,-maxm:maxm), zcurrent(3,nphi,n)

  complex *16 :: ima, cd, cd1, cd2, cd3, cd4, cd5
  complex *16 :: wsave(1000000)
  complex *16, allocatable :: zj1(:), zj2(:)

  !
  ! synthesize a tangential vector field on a surface of revolution
  ! given by its fourier modes into a vectorfield in cartesian coordinates
  !

  done=1
  ima=(0,1)
  pi=4*atan(done)

  hphi = 2*pi/nphi


  !
  ! fill up each of the things to FFT
  !
  allocate(zj1(nphi))
  allocate(zj2(nphi))
  
  call zffti(nphi, wsave)
  
  do i=1,n

    dsdt = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dpds = dxys(1,i)/dsdt
    dzds = dxys(2,i)/dsdt

    !
    ! zeropad each of the things to FFT
    !
    do j = 1,nphi
      zj1(j) = 0
      zj2(j) = 0
    end do

    do m = 0,maxm
      zj1(m+1) = zjmodes(1,i,m)
      zj2(m+1) = zjmodes(2,i,m)
    end do

    do m = 1,maxm
      zj1(nphi-m+1) = zjmodes(1,i,-m)
      zj2(nphi-m+1) = zjmodes(2,i,-m)
    end do

    call zfftb(nphi, zj1, wsave)
    call zfftb(nphi, zj2, wsave)
    
    do j=1,nphi

      phi=(j-1)*hphi
      cs = cos(phi)
      sn = sin(phi)
      
      cd1 = zj1(j)
      cd2 = zj2(j)
      
      zcurrent(1,j,i) = cd1*dpds*cs - cd2*sn
      zcurrent(2,j,i) = cd1*dpds*sn + cd2*cs
      zcurrent(3,j,i) = cd1*dzds
      
    end do
  end do
  
  return
end subroutine axisynth3_fast
