
!
! this is the new and updated debye builder, based on accelerated
! modal kernel evaluations
!
subroutine axidebye_pec_solver(zk, n, h, xys, &
    dxys, d2xys, norder, nphi, xyzs, ein, hin, &
    ind, surfa, cintb, whts, rho, sigma, jcurr, kcurr, modemax, &
    esterr)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n), xyzs(3,nphi,n)
  real *8 :: whts(nphi,n)
  complex *16 :: zk, ein(3,nphi,n), hin(3,nphi,n)
  complex *16 :: surfa, cintb
  complex *16 :: rho(nphi,n), sigma(nphi,n)
  complex *16 :: jcurr(3,nphi,n), kcurr(3,nphi,n)

  character(len=10) :: fmt

  real *8 :: dpdt(10000), dzdt(10000), xyz1(10), ps(10000)
  real *8 :: zs(10000), timings(100)

  real *8, allocatable :: dsdt(:), enorms(:), hnorms(:)
  real *8, allocatable :: dws0all(:,:,:), dws0gradall(:,:,:,:)
  real *8, allocatable :: dws0grad0all(:,:,:,:)

  complex *16 :: cd, ima, alpha, beta

  complex *16, allocatable :: emodes(:,:,:),hmodes(:,:,:)
  complex *16, allocatable :: amats(:,:,:)
  complex *16, allocatable :: rhomodes(:,:), sigmamodes(:,:)
  complex *16, allocatable :: ws0all(:,:,:), ws0grad0all(:,:,:,:)
  complex *16, allocatable :: ws0divall(:,:,:), rhses(:,:)
  complex *16, allocatable :: etan(:,:,:), sols(:,:)
  complex *16, allocatable :: jcurrmodes(:,:,:), kcurrmodes(:,:,:)


  external gkmall, g0mall
  !
  ! this routine solves, mode by mode, a full exterior surface of
  ! revolution scattering problem by first solving the MFIE, and
  ! then solving a secondary equation (n \cdot E = \rho) for the
  ! surface charge. This avoids low-frequency breakdown in the
  ! recovery of \rho. In the zero-mode, an additional stabilizing
  ! condition on the vector potential is added to the system matrix
  ! (line integral of the vector potential). See our IEEE paper...
  !
  ! input:
  !   surfa - this is the surface integral of Ein \cdot \tau along
  !     the scatterer, as multiple modes of the data contribute to a
  !     single line integral
  !   cintab - 
  !
  ! output:
  !   xyzs -
  !   whts -
  !   rho, sigma -
  !   jcurr, kcurr -
  !   modemax - the maximum fourier mode required for mach prec
  !   esterr -
  !
  !

  call cpu_time(tstart)
  !$ tstart = OMP_get_wtime()

  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)

  allocate( dsdt(n))
  do i=1,n
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do

  call prin2('h = *', h, 1)
  !call prin2('dsdt = *', dsdt, n)
  
  !
  ! switch to cylindrical coordinates
  !
  !call cart2cyl_self(n*nphi, pts, esurf)
  !call cart2cyl_self(n*nphi, pts, hsurf)

  !
  ! Compute L2 norm of data
  !
  print *
  print *, '. . . calculating norm of data'

  !enorm = 0
  !hnorm = 0
  enorm2 = 0
  hnorm2 = 0
  surfarea = 0
  hm = 2*pi/nphi
  !do i = 1,nphi
  !  do j = 1,n
  !    surfarea = surfarea + hm*xys(1,j)*dsdt(j)
  !    do k = 1,3
  !      enorm = enorm + hm*xys(1,j)*dsdt(j)*abs(esurf(k,j,i))**2
  !      hnorm = hnorm + hm*dsdt(j)*xys(1,j)*abs(hsurf(k,j,i))**2
  !    end do
  !  end do
  !end do

  do i = 1,n
    da = hm*xys(1,i)*dsdt(i)*h
    surfarea = surfarea + da
    do j = 1,nphi
      do k = 1,3
        enorm2 = enorm2 + da*abs(ein(k,j,i))**2
        hnorm2 = hnorm2 + da*abs(hin(k,j,i))**2
      end do
    end do
  end do

  enorm2 = sqrt(enorm2)
  hnorm2 = sqrt(hnorm2)

  call prin2('cartesian ein L2 norm = *', enorm2, 1)
  call prin2('cartesian hin L2 norm = *', hnorm2, 1)

  !call cart2cyl_self(n*nphi, xyzs, ein)
  !call cart2cyl_self(n*nphi, xyzs, hin)

  !
  ! decompose the incoming field esurf, hsurf
  !
  allocate( emodes(3,n,nphi) )
  allocate( hmodes(3,n,nphi) )

  print*
  print *, '. . . decomposing data into fourier modes'
  !call axideco(n,nphi,esurf,hsurf,emodes,hmodes)

  call axideco_fast(nphi, n, xyzs, ein, hin, emodes, hmodes)

  print *, '. . . determining truncation mode'
  allocate(enorms(nphi),hnorms(nphi))
  do i=1,nphi

    dd1=0
    dd2=0
    do j=1,n
      do k=1,3
        dd1=dd1+abs(emodes(k,j,i))**2
        dd2=dd2+abs(hmodes(k,j,i))**2
      end do
    end do

    enorms(i) = sqrt(dd1)
    hnorms(i) = sqrt(dd2)
  end do

  call prin2('enorms = *', enorms, nphi)
  call prin2('hnorms = *', hnorms, nphi)
  
  modemax = -1
  iffound = 0
  epsfft = 1.0d-13
  do i = 1,nphi/2-1
    if ((enorms(i)/enorm2 .lt. epsfft) &
        .and. (hnorms(i)/hnorm2 .lt. epsfft)) then
      iffound = 1
      modemax = i-2
      exit
    end if
  end do

  if (iffound .eq. 1) then
    modemax = i
  else
    modemax = nphi/2-1
  end if
  
  !if (modemax .lt. 0) modemax = nphi/2-1
  maxm = modemax + 5

  call prinf('maximum mode needed, modemax = *', modemax, 1)
  !stop
  !
  ! loop over all modes from -modemax to modemax, solve each
  ! problem, then recombine. First, generate all right hand sides
  !
  ! construct s0 surfdiv....
  ! first, build the laplace single layer matrix and its derivatives
  !

  print *, '. . . computing right hand side'
  ifscale = 1
  allocate(dws0all(n,n,-maxm:maxm))
  allocate(dws0gradall(n,n,2,-maxm:maxm))
  allocate(dws0grad0all(n,n,2,-maxm:maxm))
  call dalpertmodes_grads(ier, norder, n, xys, dxys, &
      h, g0mall, par0, par1, maxm, ifscale, dws0all, &
      dws0gradall, dws0grad0all)

  allocate(ws0all(n,n,-maxm:maxm))
  allocate(ws0grad0all(n,n,2,-maxm:maxm))
  call axidebye_d2zcopy(n*n*(2*maxm+1), dws0all, ws0all)
  call axidebye_d2zcopy(2*n*n*(2*maxm+1), dws0grad0all, ws0grad0all)

  !
  ! and assemble S_0 div = -\int grad' G_0
  !
  allocate( ws0divall(n,2*n,-maxm:maxm) )
  call crea_skdiv_modes0(n, xys, dxys, h, &
      maxm, ws0all, ws0grad0all, ws0divall)

  !
  ! extract tangential components
  !
  allocate(etan(2,n,-modemax:modemax))
  ijk = 0
  do m = 0,modemax
    ijk = ijk+1
    do i = 1,n
      dpds = dxys(1,i)/dsdt(i)
      dzds = dxys(2,i)/dsdt(i)
      etan(1,i,m) = dpds*emodes(1,i,ijk) + dzds*emodes(3,i,ijk)
      etan(2,i,m) = emodes(2,i,ijk)
    end do
  end do

  do m = 1,modemax
    ijk = nphi-m+1
    do i = 1,n
      dpds=dxys(1,i)/dsdt(i)
      dzds=dxys(2,i)/dsdt(i)
      etan(1,i,-m) = dpds*emodes(1,i,ijk) + dzds*emodes(3,i,ijk)
      etan(2,i,-m) = emodes(2,i,ijk)
    end do
  end do

  !
  ! fill the rhs now
  !
  do i = 1,n
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
  end do

  !call prin2('dpdt = *', dpdt, n)
  !call prin2('dzdt = *', dzdt, n)
  !stop

  allocate(rhses(2*n+2,-modemax:modemax))
  do m = -modemax,modemax
    if (m .lt. 0) mmm = nphi+m+1
    if (m .ge. 0) mmm = m+1
    call zmatvec(n, 2*n, ws0divall(1,1,m), etan(1,1,m), rhses(1,m))
    call ndotdirect(n, dpdt, dzdt, hmodes(1,1,mmm), rhses(n+1,m))
  end do

  do m = -modemax,modemax
    rhses(2*n+1,m) = 0
    rhses(2*n+2,m) = 0
  end do

  rhses(2*n+1,0) = surfa
  rhses(2*n+2,0) = cintb

  !
  ! construct all the matrices
  !
  print *, '. . . building all system matrices'
  allocate(amats(2*n+2,2*n+2,-maxm:maxm))
  
  call axidebye_pec_mats(ier, norder, n, xys, dxys, d2xys, h, &
      zk, maxm, ind, amats, timings, ntimes)


  !
  ! solve each system
  !
  print *, '. . . solving each system'
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  allocate(sols(2*n+2,-modemax:modemax))
  !$omp parallel do default(shared) private(info, dcond)
  do m = -modemax,modemax
    call zgausselim(2*n+2, amats(1,1,m), rhses(1,m), info, &
        sols(1,m), dcond)
  end do
  !$end parallel do
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  ntimes = ntimes + 1
  timings(ntimes) = t1-t0

  !
  ! synthesize the charge, evaluate at nphi points
  ! and construct the smooth quadrature weights
  !

  print *, '. . . synthesizing Debye sources'

  allocate(rhomodes(n,-modemax:modemax))
  allocate(sigmamodes(n,-modemax:modemax))

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  do m = -modemax,modemax
    do i = 1,n
      rhomodes(i,m) = sols(i,m)
      sigmamodes(i,m) = sols(n+i,m)
    end do
  end do

  alpha = sols(2*n+1,0)
  beta = sols(2*n+2,0)

  call axisynth1_fast(n, xys, dxys, h, modemax, nphi, &
      rhomodes, rho)
  call axisynth1_fast(n, xys, dxys, h, modemax, nphi, &
      sigmamodes, sigma)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  ntimes = ntimes + 1
  timings(ntimes) = t1-t0


  !
  ! calculate each of the modal currents, and then synthesis them
  !
  allocate(jcurrmodes(2,n,-modemax:modemax))
  allocate(kcurrmodes(2,n,-modemax:modemax))

  print *, '. . . building modal currents'
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  !$omp parallel do default(shared)
  do m = -modemax,modemax
    call axidebye_jmbuilder(zk, m, n, xys, dxys, d2xys, h, &
        rhomodes(1,m), sigmamodes(1,m), alpha, beta, &
        jcurrmodes(1,1,m), kcurrmodes(1,1,m))
    !call corrand3(2*n*n*2, jcurrmodes(1,1,m))
    !call corrand3(2*n*n*2, kcurrmodes(1,1,m))
  end do
  !$omp end parallel do
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  ntimes = ntimes + 1
  timings(ntimes) = t1-t0
  call prin2('time for modal currents=*', t1-t0, 1)


  print *, '. . . building cartesian currents'  
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  call axisynth3_fast(n, xys, dxys, h, modemax, nphi, &
      jcurrmodes, jcurr)
  call axisynth3_fast(n, xys, dxys, h, modemax, nphi, &
      kcurrmodes, kcurr)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  ntimes = ntimes + 1
  timings(ntimes) = t1-t0

  call cpu_time(tend)
  !$ tend = OMP_get_wtime()
  call prin2('time for cartesian currents=*', t1-t0, 1)

  call axi_3dwhts(n, xys, dxys, h, nphi, whts)


  ifprint = 1
  if (ifprint .eq. 1) then

    fmt = "(a40,f8.3)"
    print *
    print *
    write(6,*) '- - - - - - - timings for the solver - - - - - - -'
    nt = 1
    write(6,fmt) 'crea_gkmall_pieces: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'Sprime matrices: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'grad Sk matrices: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'S0 div matrices: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'vector potentials: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'mfie matrices: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'grad inv(surflaps): ', timings(nt)

    nt=nt+1
    write(6,fmt) 'assembling all modal matrices: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'extra conditions: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'gaussian eliminations: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'synthesizing Debye sources: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'building modal currents: ', timings(nt)

    nt=nt+1
    write(6,fmt) 'building cartesian currents: ', timings(nt)

    print *
    totaltime=tend-tstart
    write(6,fmt) 'TOTAL TIME: ', totaltime
    print *
    print *

  end if


  return
end subroutine axidebye_pec_solver





subroutine ndotdirect(n,dpdt,dzdt,field,vals)
  implicit real *8 (a-h,o-z)
  real *8 dpdt(1),dzdt(1)
  complex *16 field(3,1),vals(1)
  !c
  !c       apply n \cdot directly to the vector field
  !c
  do i=1,n
    dsdt=sqrt(dpdt(i)**2+dzdt(i)**2)
    dpds=dpdt(i)/dsdt
    dzds=dzdt(i)/dsdt
    vals(i)=dzds*field(1,i)-dpds*field(3,i)
  end do

  return
end subroutine ndotdirect





subroutine get2harmvecs(n,ps,zs,ch1,ch2)
  implicit real *8 (a-h,o-z)
  real *8 ps(1),zs(1)
  complex *16 ch1(2,1),ch2(2,1)
  ! c
  ! c       this subroutine fills ch1 and ch2 with 2 vectors
  ! c       who span the space of harmonic vector fields, following
  ! c       the convention that ch2 = n \times ch1
  ! c
  ! c       input:
  ! c         n - length of ps,zs
  ! c         ps,zs - points describing the generating curve for the
  ! c             surface of revolution
  ! c
  ! c       output:
  ! c         ch1,ch2 - two harmonic vector fields on a surface of rotation
  ! c             with 1 hole, constructed such that ch2 = n \times ch1
  ! c
  ! c
  do i=1,n
    ch1(1,i)=1/ps(i)
    ch1(2,i)=0
    ch2(1,i)=0
    ch2(2,i)=-1/ps(i)
  end do

  return
end subroutine get2harmvecs





subroutine axidebye_pec_mat1(ier, norder, n, xys, dxys, d2xys, h, &
    zk, mode, ind, amat)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n)
  complex *16 :: zk, amat(2*n+2,2*n+2)

  complex *16, allocatable :: amats(:,:,:)

  !
  ! This routine returns a single modal matrix for the gen. Debye PEC
  ! system, along the way building all using recurrences but only
  ! copying one.
  !
  ! Input:
  !   norder -
  !
  ! Output:
  !
  !

  !
  ! build all matrices, but only return a single one
  !

  maxm = abs(mode)+5
  allocate(amats(2*n+2,2*n+2,-maxm:maxm))

  call axidebye_pec_mats(ier, norder, n, xys, dxys, d2xys, h, &
    zk, maxm, ind, amats)

  ntot = (2*n+2)**2
  call zveccopy(ntot, amats(1,1,mode), amat)

  return
end subroutine axidebye_pec_mat1





subroutine axidebye_pec_mats(ier, norder, n, xys, dxys, d2xys, h, &
    zk, maxm, ind, amat, timings, ntimes)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n), timings(*)
  complex *16 :: zk, amat(2*n+2,2*n+2,-maxm:maxm)

  real *8 :: ps(10000), zs(10000), dpdt(10000), dzdt(10000)
  real *8 :: dpdt2(10000), dzdt2(10000), dsdt(10000)
  real *8, allocatable :: dws0(:,:), dws0all(:,:,:)
  real *8, allocatable :: dws0gradall(:,:,:,:), dws0grad0all(:,:,:,:)

  complex *16 :: ima
  complex *16 :: zk0,val
  complex *16 :: ch1(2,10000), ch2(2,10000)
  complex *16 :: ctemp1(10000), ctemp2(10000)
  complex *16 :: cd,cd0,cd1,cd2
  complex *16 :: cda, cdb
  complex *16 :: zdiag

  complex *16, allocatable :: wsprimes(:,:,:)
  complex *16, allocatable :: work(:), work2(:)
  complex *16, allocatable :: wgradreps(:,:,:)
  complex *16, allocatable :: wndot(:,:),wnxt(:,:)
  complex *16, allocatable :: w1(:,:),w2(:,:),w3(:,:)
  complex *16, allocatable :: a1(:,:),a2(:,:),a3(:,:),a4(:,:)
  complex *16, allocatable :: b1(:,:),b2(:,:)
  complex *16, allocatable :: e1(:,:),e3(:,:)
  complex *16, allocatable :: etop(:,:),ebottom(:,:)
  complex *16, allocatable :: wones(:,:)
  complex *16, allocatable :: a(:,:,:)
  complex *16, allocatable :: agrad(:,:,:,:)
  complex *16, allocatable :: agrad0(:,:,:,:)
  complex *16, allocatable :: a_cos(:,:,:)
  complex *16, allocatable :: agrad_cos(:,:,:,:)
  complex *16, allocatable :: agrad0_cos(:,:,:,:)
  complex *16, allocatable :: a_sin(:,:,:)
  complex *16, allocatable :: agrad_sin(:,:,:,:)
  complex *16, allocatable :: agrad0_sin(:,:,:,:)

  complex *16, allocatable :: vecpotmats0(:,:,:,:)
  complex *16, allocatable :: vecpotmats1(:,:,:,:)
  complex *16, allocatable :: vecpotmats22(:,:,:)
  complex *16, allocatable :: mfiemats(:,:,:)
  complex *16, allocatable :: andotcurls(:,:,:)
  complex *16, allocatable :: wgradskall(:,:,:)
  complex *16, allocatable :: ws0divall(:,:,:)
  complex *16, allocatable :: ws0all(:,:,:)
  complex *16, allocatable :: ws0grad0all(:,:,:,:)

  external g0mall

  !
  ! build the matrix for the genus one debye PEC code...
  !
  ima = (0,1)
  done = 1
  pi = 4*atan(done)

  !
  ! compute dsdt around the generating curve
  !
  do i=1,n
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    dpdt2(i) = d2xys(1,i)
    dzdt2(i) = d2xys(2,i)
  end do

  rl = h*n
  !call prin2('rl = *', rl, 1)
  !stop

  !
  ! build a bunch of matrices that we'll need
  !
  ! ...some layer potential matrices, a bunch of them
  !
  allocate(a(n,n,-maxm:maxm))
  allocate(a_cos(n,n,-maxm:maxm))
  allocate(a_sin(n,n,-maxm:maxm))
  allocate(agrad(n,n,2,-maxm:maxm))
  allocate(agrad_cos(n,n,2,-maxm:maxm))
  allocate(agrad_sin(n,n,2,-maxm:maxm))
  allocate(agrad0(n,n,2,-maxm:maxm))
  allocate(agrad0_cos(n,n,2,-maxm:maxm))
  allocate(agrad0_sin(n,n,2,-maxm:maxm))

  print *, '- - - crea_gmkall_pieces - - -'
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  call crea_gkmall_pieces(ier, norder, n, xys, dxys, h, zk, &
      maxm, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!!!print *, 'time in crea_gkmall_pieces = ', t1-t0

  itime = 1
  timings(itime) = t1-t0

  !
  ! manually build S' and gradient since we have the pieces
  !
  print *, '- - - wsprimes loop - - -'
  allocate(wsprimes(n,n,-maxm:maxm))
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  !$omp parallel do default(shared) &
  !$omp     private(dpds, dzds)
  do mmm = -maxm,maxm
    do i=1,n
      dpds = dxys(1,i)/dsdt(i)
      dzds = dxys(2,i)/dsdt(i)
      do j=1,n
        wsprimes(i,j,mmm) = dzds*agrad(i,j,1,mmm) &
            - dpds*agrad(i,j,2,mmm)
        if (i .eq. j) then
          wsprimes(i,j,mmm) = wsprimes(i,j,mmm) -.5d0
        end if
      end do
    end do
  end do
  !$omp end parallel do
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!!print *, 'time building sprime matrices = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0

  !
  ! ...surfgrad(S_k)
  !
  print *, '- - - crea_gradsk_modes0 - - -'
  allocate( wgradskall(2*n,n,-maxm:maxm) )
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  call crea_gradsk_modes0(n, xys, dxys, &
      maxm, a, agrad, wgradskall)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!!!print *, 'time building grad sk matrices = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0

  !
  ! construct s0 surfdiv....
  ! first, build the laplace single layer matrix and its derivatives
  !
  print *, '- - - creating s0 div - - -'
  ifscale = 1
  allocate(dws0all(n,n,-maxm:maxm))
  allocate(dws0gradall(n,n,2,-maxm:maxm))
  allocate(dws0grad0all(n,n,2,-maxm:maxm))
  call dalpertmodes_grads(ier, norder, n, xys, dxys, &
      h, g0mall, par0, par1, maxm, ifscale, dws0all, &
      dws0gradall, dws0grad0all)

  allocate(ws0all(n,n,-maxm:maxm))
  allocate(ws0grad0all(n,n,2,-maxm:maxm))
  call axidebye_d2zcopy(n*n*(2*maxm+1), dws0all, ws0all)
  call axidebye_d2zcopy(2*n*n*(2*maxm+1), dws0grad0all, ws0grad0all)

  !
  ! and assemble S_0 div = -\int grad' G_0
  !
  allocate( ws0divall(n,2*n,-maxm:maxm) )
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  call crea_skdiv_modes0(n, xys, dxys, h, &
      maxm, ws0all, ws0grad0all, ws0divall)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!!!print *, 'time building s0 div matrices = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0


  !
  ! ...and some vector operators
  !
  print *, '- - - crea_vecpot_modes0 - - -'
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  iftan=0
  allocate(vecpotmats0(3,n,2*n,-maxm:maxm))
  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk, &
      maxm, iftan, a, agrad, agrad0, a_cos, agrad_cos, &
      agrad0_cos, a_sin, agrad_sin, agrad0_sin, vecpotmats0)

  iftan = 1
  allocate(vecpotmats1(3,n,2*n,-maxm:maxm))
  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk, &
      maxm, iftan, a, agrad, agrad0, a_cos, agrad_cos, &
      agrad0_cos, a_sin, agrad_sin, agrad0_sin, vecpotmats1)


  !
  ! chop off every third row
  !
  allocate(vecpotmats22(2*n,2*n,-maxm:maxm))
  !$omp parallel do default(shared) private(i1,i2)
  do mmm = -maxm,maxm
    do i = 1,n
      i1 = 2*(i-1)+1
      i2 = i1+1
      do j = 1,2*n
        vecpotmats22(i1,j,mmm) = vecpotmats1(1,i,j,mmm)
        vecpotmats22(i2,j,mmm) = vecpotmats1(2,i,j,mmm)
      end do
    end do
  end do
  !$omp end parallel do

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!!print *, 'time building vector potentials = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0


  !
  ! . . . create the MFIE part of the representations
  !
  print *, '- - - crea_mfie_modes0 - - -'
  allocate(mfiemats(2*n,2*n,-maxm:maxm))
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  zdiag = .5d0
  call crea_mfie_modes0(ier, norder, n, xys, dxys, h, zk, &
      maxm, zdiag, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin, mfiemats)

  print *, '- - - crea_ndotcurl_modes0 - - -'
  allocate(andotcurls(n,2*n,-maxm:maxm))
  call crea_ndotcurl_modes0(ier, n, xys, dxys, h, zk, &
      maxm, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin, andotcurls)

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!print *, 'time building mfie parts = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0


  allocate(wndot(n,3*n))
  allocate(wnxt(2*n,2*n))
  call axidebye_creandotmat(n, dpdt, dzdt, wndot)
  call axidebye_creanxtmat(n, wnxt)


  !
  ! form the 'mean zero preserving' 'ones' matrix which is
  ! used in the 0th fourier mode
  !
  surfarea=0
  do i=1,n
    surfarea = surfarea + 2*pi*ps(i)*dsdt(i)*h
  end do

  call corrand_norm(2*n, ctemp1, ctemp2)
  dd = 0
  do i = 1,n
    dd = dd + abs(ctemp1(i))**2
  end do
  dd = sqrt(dd)

  allocate(wones(n,n))
  do  i=1,n
    do  j=1,n
      wones(i,j) = ps(j)*h*dsdt(j)*2*pi/surfarea*ctemp1(i)/dd
    end do
  end do


  !
  ! compute wgradrep = grad surflap^{-1}
  !
  allocate(wgradreps(2*n,n,-maxm:maxm))


  print *, '- - - axidebye_surfgradinvlap_mat1 - - -'
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  !$omp parallel do default(shared)
  do m = -maxm,maxm
    call axidebye_surfgradinvlap_mat1(n, ps, zs, dpdt, dzdt, &
        dpdt2, dzdt2, h, m, wgradreps(1,1,m))
    !call corrand3(2*n*n*2, wgradreps(1,1,m))
  end do
  !$omp end parallel do

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!!print *, 'time building grad inv(surflaps) = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0


  !
  ! now loop through all modes and fill in the system matrices
  !
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()

  print *, '- - - looping over all modes assembling - - -'

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
  !!!!if (1 .eq. 0) then
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      
  !$omp parallel do default(shared) &
  !$omp     private(a1,a2,a3,a4,work,work2)
  do m = -maxm,maxm


      
    print *, '. . . processing mode ', m

    allocate(a1(n,n),a2(n,n),a3(n,n),a4(n,n))
    allocate(work(9*n**2))
    allocate(work2(9*n**2))

    !
    ! fill the upper left quarter of the matrix, the rho contribution to e
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,vecpotmats22(1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n, 2*n, ws0divall(1,1,m), n, wgradskall(1,1,m), a2)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,2*n,mfiemats(1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a3)

    !
    ! add in the 'mean zero preserving' 'ones' matrix which is
    ! used in the 0th fourier mode
    !
    if (m .eq. 0) then
      do j = 1,n
        do i = 1,n
          a2(i,j) = a2(i,j) + wones(i,j)
        end do
      end do
    endif

    !
    ! fill the matrix
    !
    do j=1,n
      do i=1,n
        amat(i,j,m) = -zk**2*a1(i,j) - a2(i,j) + ima*zk*a3(i,j)
      end do
    end do


    !
    ! and now the right portion, corresponding to rhom
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,vecpotmats22(1,1,m),work2)
    call zmatmat(n,2*n,work2,2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work2)
    call zmatmat(n, 2*n, work2, 2*n, mfiemats(1,1,m), work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a3)

    do j=1,n
      do i=1,n
        amat(i,n+j,m) = zk**2*a1(i,j) + ima*zk*a3(i,j)
      end do
    end do


    !
    ! build the bottom half of the matrix for H
    ! first the left portion, corresponding to rho
    !
    call zmatmat(n, 2*n, andotcurls(1,1,m), n, wgradreps(1,1,m), a1)

    call zmatmat(n, 3*n, wndot, 2*n,vecpotmats0(1,1,1,m),work)
    call zmatmat(n, 2*n, work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a3)

    do j=1,n
      do i=1,n
        amat(n+i,j,m) = ima*zk*a1(i,j) - zk*zk*a3(i,j)
      end do
    end do


    !
    ! and now the right portion corresponding to rhom
    !
    call zmatmat(n, 2*n, andotcurls(1,1,m), 2*n, wnxt, work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,3*n,wndot,2*n,vecpotmats0(1,1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a3)

    do j=1,n
      do i=1,n
        amat(n+i,n+j,m)=-ima*zk*a1(i,j) - wsprimes(i,j,m) - zk*zk*a3(i,j)
      end do
    end do


    deallocate(a1, a2, a3, a4)
    deallocate(work, work2)


  end do
  !$omp end parallel do


  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!!print *, 'time assemling all modes = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0


  !
  ! first compute the two harmonic vector fields
  !
  call get2harmvecs(n,ps,zs,ch1,ch2)


  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  !$omp parallel do default(shared) &
  !$omp   private(a1, a2, a3, a4, b1, b2, work2, work) &
  !$omp   private(ctemp1, ctemp2, e1, e3, etop, ebottom) &
  !$omp   private(i1, i2, w1, w3, cda, eps, cdb)
  do m = -maxm,maxm


    !
    ! compute columns and rows 2n+1 and 2n+2 for h1 and h2
    !
    if (m .ne. 0) then
      do k=1,2*n+2
        amat(2*n+1,k,m)=0
        amat(2*n+2,k,m)=0
        amat(k,2*n+1,m)=0
        amat(k,2*n+2,m)=0
      end do
      amat(2*n+1,2*n+1,m)=1
      amat(2*n+2,2*n+2,m)=1

    else

      allocate(a1(n,n),a2(n,n),a3(n,n),a4(n,n))
      allocate(work(9*n**2))
      allocate(work2(9*n**2))
      
      !
      ! if here, m=0 - create contribution of harmonic fields to E
      ! and H
      !
      ! apply S0 div E to ch1 and ch2
      !
      allocate(b1(n,2*n), b2(n,2*n))
      call zmatmat(n,2*n,ws0divall(1,1,m),2*n,vecpotmats22(1,1,m),b1)

      call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work2)
      call zmatmat(n,2*n,work2,2*n,mfiemats(1,1,m), work)
      call zmatmat(n,2*n,work,2*n,wnxt,b2)

      do i=1,n
        do j=1,2*n
          b1(i,j)=ima*zk*b1(i,j)+b2(i,j)
        end do
      end do

      call zmatvec(n,2*n,b1,ch1,ctemp1)
      call zmatvec(n,2*n,b1,ch2,ctemp2)

      do i=1,n
        amat(i,2*n+1,m)=ctemp1(i)
        amat(i,2*n+2,m)=ctemp2(i)
      end do

      !
      ! apply the n dot H operator to ch1 and ch2
      !
      call zmatmat(n,3*n,wndot,2*n,vecpotmats0(1,1,1,m),work)
      call zmatmat(n,2*n,work,2*n,wnxt,b1)

      do i=1,n
        do j=1,2*n
          b1(i,j)=ima*zk*b1(i,j)+andotcurls(i,j,m)
        end do
      end do

      call zmatvec(n,2*n,b1,ch1,ctemp1)
      call zmatvec(n,2*n,b1,ch2,ctemp2)

      do i=1,n
        amat(n+i,2*n+1,m)=ctemp1(i)
        amat(n+i,2*n+2,m)=ctemp2(i)
      end do


      !
      ! now calculate the extra conditions, line integrals of E
      ! first construct contributions from rho to E
      !
      allocate(e1(2*n,n),e3(2*n,n))
      call zmatmat(2*n,2*n,vecpotmats22(1,1,m),n,wgradreps(1,1,m),e1)

      call zmatmat(2*n,2*n,wnxt,2*n,mfiemats(1,1,m),work)
      call zmatmat(2*n,2*n,work,2*n,wnxt,work2)
      call zmatmat(2*n,2*n,work2,n,wgradreps(1,1,m),e3)

      !
      ! can leave e2 out of ebottom since it is zero for m=0
      !
      allocate(etop(2*n,n),ebottom(2*n,n))
      do i=1,2*n
        do j=1,n
          etop(i,j)=-zk*zk*e1(i,j)+ima*zk*e3(i,j)
          ebottom(i,j)=-zk*e1(i,j)+ima*e3(i,j)
        end do
      end do

      do i=1,n
        i1=2*(i-1)+1
        i2=i1+1
        do j=1,n
          a1(i,j)=etop(i1,j)
          a2(i,j)=ebottom(i2,j)
        end do
      end do

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! new surface weights
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      do i=1,n
        !ctemp1(i)=h*dsdt(i)
        ctemp1(i)=h*dsdt(i)*xys(1,i)*2*pi
      end do

      call zmatmat(1,n,ctemp1,n,a1,work)

      do j=1,n
        amat(2*n+1,j,m)=work(j)*2*pi
        amat(2*n+2,j,m)=2*pi*ps(ind)*a2(ind,j)
      end do


      !
      ! and next the contribution to the line integrals from rhom
      !
      call zmatmat(2*n,2*n,vecpotmats22(1,1,m),2*n,wnxt,work)
      call zmatmat(2*n,2*n,work,n,wgradreps(1,1,m),e1)

      call zmatmat(2*n,2*n,wnxt,2*n,mfiemats(1,1,m),work)
      call zmatmat(2*n,2*n,work,n,wgradreps(1,1,m),e3)

      do i=1,2*n
        do j=1,n
          etop(i,j)=zk*zk*e1(i,j)+ima*zk*e3(i,j)
          ebottom(i,j)=zk*e1(i,j)+ima*e3(i,j)
        end do
      end do

      do i=1,n
        i1=2*(i-1)+1
        i2=i1+1
        do j=1,n
          a1(i,j)=etop(i1,j)
          a2(i,j)=ebottom(i2,j)
        end do
      end do

      call zmatmat(1,n,ctemp1,n,a1,work)

      do j=1,n
        amat(2*n+1,n+j,m)=work(j)*2*pi
        amat(2*n+2,n+j,m)=2*pi*ps(ind)*a2(ind,j)
      end do


      !
      ! and finally construct the last 2x2 system in the corner which
      ! contains the contributions to the line integrals by ch1 and ch2
      ! first the condition e dot t = 0
      !
      allocate(w1(2*n,2*n), w3(2*n,2*n))
      call zmatmat(2*n,2*n,wnxt,2*n,mfiemats(1,1,m),work)
      call zmatmat(2*n,2*n,work,2*n,wnxt,w3)

      do i=1,2*n
        do j=1,2*n
          w1(i,j)=ima*zk*vecpotmats22(i,j,m)+w3(i,j)
        end do
      end do

      do i=1,n
        i1=2*(i-1)+1
        i2=i1+1
        do j=1,2*n
          b1(i,j)=w1(i1,j)
          b2(i,j)=w1(i2,j)
        end do
      end do

      call zmatvec(n,2*n,b1,ch1,work)
      call zmatvec(n,2*n,b1,ch2,work2)

      !call cvecprod601(n,ctemp1,work,cda)
      !call cvecprod601(n,ctemp1,work2,cdb)
      call zvecvec(n,ctemp1,work,cda)
      call zvecvec(n,ctemp1,work2,cdb)

      amat(2*n+1,2*n+1,m)=cda*2*pi
      amat(2*n+1,2*n+2,m)=cdb*2*pi

      !
      ! and now other condition, integral of (e dot theta)/zk carefully
      !
      !!!!!
      !!!! WHY IS THIS TRUE??? !!!!!
      !!!!!!
      cda=0
      !!!!!!

      eps = 1.0d-12
      call jh2bcompute(eps,zk,n,rl,ps,zs,dpdt,dzdt,&
          norder, ind,cdb)

      amat(2*n+2,2*n+1,m)=cda
      amat(2*n+2,2*n+2,m)=cdb

      deallocate(a1, a2, a3, a4)
      deallocate(work, work2)

      deallocate(w1, w3)
      deallocate(b1, b2)
      deallocate(e1, e3)
      deallocate(etop, ebottom)

    endif


  end do
  !$omp end parallel do

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!endif
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  nnnn = 2*((2*n+2)**2)
  
  !!$omp parallel do default(shared)
  !do m = -maxm,maxm
  !  print *, 'filling random mode ', m
  !  call corrand3(nnnn, amat(1,1,m))
  !end do
  !!$omp end parallel do
  


  
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  !!print *, 'time adding in extra conditions = ', t1-t0

  itime = itime+1
  timings(itime) = t1-t0

  ntimes = itime

  return
end subroutine axidebye_pec_mats





subroutine axidebye_creandotmat(n,dpdt,dzdt,amat)
  implicit real *8 (a-h,o-z)
  real *8 :: dpdt(n), dzdt(n)
  complex *16 :: ima,amat(n,3*n)

  real *8, allocatable :: dsdt(:), rnp(:), rns(:), rnz(:)

  !  c
  !c       this subroutine builds a diagonal n x 3n matrix which
  !c       takes the dot product with the target normal
  !c
  done=1
  pi=4*atan(done)
  ima=(0,1)
  !c
  !c       fill rn with the normal vectors
  !c
  allocate(dsdt(n), rnp(n), rns(n), rnz(n))
  do i=1,n
    dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
    rnp(i)=dzdt(i)/dsdt(i)
    rns(i)=0
    rnz(i)=-dpdt(i)/dsdt(i)
  end do
  
  !c
  !c       fill diagonal elements with the normal at i, rest with zeros
  !c
  do  i=1,n

    do  j=1,n
      j1=3*(j-1)+1
      j2=j1+1
      j3=j2+1

      if  (i .eq. j) then
        amat(i,j1)=rnp(i)
        amat(i,j2)=rns(i)
        amat(i,j3)=rnz(i)
      else
        amat(i,j1)=0
        amat(i,j2)=0
        amat(i,j3)=0
      end if
    end do
  end do

  return
end subroutine axidebye_creandotmat





subroutine axidebye_creanxtmat(n,amat)
  implicit real *8 (a-h,o-z)
  real *8 :: dpdt(1),dzdt(1)
  complex *16 :: amat(2*n,2*n)
  !c
  !c       this subroutine creates a 2n by 2n matrix which
  !c       calculates n cross a t-hat, theta-hat vector
  !c
  !c
  do i=1,n
    i1=2*(i-1)+1
    i2=i1+1

    do j=1,n
      j1=2*(j-1)+1
      j2=j1+1

      amat(i1,j1)=0
      amat(i2,j1)=0
      amat(i1,j2)=0
      amat(i2,j2)=0

      if (i .eq. j) then
        amat(i1,j1)=0
        amat(i1,j2)=1
        amat(i2,j1)=-1
        amat(i2,j2)=0
      endif
      
    end do
  end do
  
  return
end subroutine axidebye_creanxtmat





subroutine axidebye_pre_ncross(m, n, amat, bmat)
  implicit real *8 (a-h,o-z)
  complex *16 :: amat(m,n), bmat(m,n)

  !
  ! it is assumed that n is even
  !
  !print *
  !print *
  !call prin2('col 1 of amat = *', amat(1,1), 6)
  !call prin2('col 2 of amat = *', amat(1,2), 6)

  nhalf = n/2
  do jjj = 1,nhalf
    j1 = 2*(jjj-1)+1
    j2 = j1 + 1
    do i = 1,m
      bmat(i,j1) = -amat(i,j2)
      bmat(i,j2) = amat(i,j1)
    end do
  end do

  return
end subroutine axidebye_pre_ncross





subroutine axidebye_creadftf(n,a)
  implicit real *8 (a-h,o-z)
  complex *16 a(n,n),ima

  ima=(0,1)
  done=1
  pi=4*atan(done)

  do i=1,n
    do j=1,n
      a(i,j)=exp(-ima*(i-1)*(j-1)*2*pi/n)
    end do
  end do

  return
end subroutine axidebye_creadftf





subroutine axidebye_creadftb(n,a)
  implicit real *8 (a-h,o-z)
  complex *16 a(n,n),ima

  ima=(0,1)
  done=1
  pi=4*atan(done)

  do i=1,n
    do j=1,n
      a(i,j)=exp(ima*(i-1)*(j-1)*2*pi/n)
    end do
  end do
  
  return
end subroutine axidebye_creadftb




subroutine axidebye_surfgradinvlap_mat1(n, ps, zs, dpdt, dzdt, &
    dpdt2, dzdt2, h, m, wgradinvsl)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(n),zs(n),dpdt(n),dzdt(n),dpdt2(n),dzdt2(n)
  complex *16 :: wgradinvsl(2*n,n)

  real *8 :: dsdt(10000)

  complex *16 :: ima
  !complex *16 :: alpha(10000),beta(10000)
  !complex *16 :: amat(2000000),cfftf(1000000),cfftb(1000000)

  complex *16, allocatable :: temp(:,:),temp2(:,:),temp3(:,:)
  complex *16, allocatable :: wxx(:,:),gradmuls(:,:)
  complex *16, allocatable :: gtemp(:,:)
  complex *16, allocatable :: a1(:,:),a2(:,:)
  complex *16, allocatable :: alpha(:), beta(:), amat(:,:)
  complex *16, allocatable :: cfftf(:,:), cfftb(:,:)

  !
  ! build a matrix which applies the operator surfgrad inv(surflap)
  !

  allocate( temp(n,n),temp2(n,n),temp3(n,n) )
  allocate( wxx(n,n), gradmuls(2*n,2*n), gtemp(2*n,n) )
  allocate( a1(n,n),a2(n,n) )
  allocate( alpha(n), beta(n))
  allocate( amat(n,n))
  allocate( cfftf(n,n), cfftb(n,n))

  

  done=1
  ima=(0,1)
  pi=4*atan(done)
  rl = h*n

  !
  ! construct ode coefficients
  !
  do i = 1,n

    dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
    fac2 = dsdt(i)**2
    alpha(i)=dpdt(i)/ps(i) &
        - (dpdt(i)*dpdt2(i)+dzdt(i)*dzdt2(i))/fac2

    beta(i) = 0
    if (m .ne. 0) beta(i) = fac2/(ps(i)**2)

  end do

  !
  ! construct the matrix which will invert the ode
  !
  call axidebye_ode_periodic_sl_mat(n, m, rl, ps, dsdt, &
      alpha, beta, amat)

  !
  ! now construct the gradient of the inverse laplacian
  !
  call axidebye_creadftf(n,cfftf)
  call axidebye_creadftb(n,cfftb)

  do i=1,n
    do j=1,n
      temp(i,j)=0
      temp2(i,j)=0
      temp3(i,j)=0
    end do
  end do

  if (m .ne. 0) then

    do i=1,n
      temp(i,i)=dsdt(i)**2/n
    enddo

    call zmatmat(n, n, cfftf, n, temp, temp2)
    call zmatmat(n, n, amat, n, temp2, wxx)

    !
    ! integrate w_xx twice to obtain w, needed for gradient
    !
    do i=1,n
      do j=1,n
        temp(i,j)=0
      end do
    end do

    do i=2,n
      iuse = i-1
      if (iuse .gt. n/2) iuse = iuse-n
      temp(i,i)=-2*pi/rl/(iuse*iuse)
    end do

    temp(1,1) = 2*pi/rl

    call zmatmat(n, n, temp, n, wxx, temp2)
    call zmatmat(n, n, cfftb, n, temp2, temp3)

    do i=1,2*n
      do j=1,2*n
        gradmuls(i,j)=0
      end do
    end do

    do i=1,n
      i1=2*i-1
      i2=i1+1
      gradmuls(i1,i)=1/dsdt(i)
      gradmuls(i2,n+i)=ima*m/ps(i)
    end do

    do i=1,n
      do j=1,n
        gtemp(n+i,j)=temp3(i,j)
      end do
    end do

    !
    ! integrate w_xx once to obtain w_x, also needed for gradient
    !
    do i=2,n
      iuse = i-1
      if (iuse .gt. n/2) iuse = iuse-n
      temp(i,i)=2*pi/rl/ima/iuse
    end do

    temp(1,1)=0

    call zmatmat(n,n,temp,n,wxx,temp2)
    call zmatmat(n,n,cfftb,n,temp2,temp3)

    do i=1,n
      do j=1,n
        gtemp(i,j)=temp3(i,j)
      end do
    end do

    call zmatmat(2*n,2*n,gradmuls,n,gtemp,wgradinvsl)
    return

  elseif (m .eq. 0) then

    !
    ! if here, build the matrix for m=0 including the mean condition
    !
    ! first construct w_xx
    !
    call zzero(n**2, temp)
    call zzero(n**2, temp2)

    do i=1,n
      temp(i,i)=dsdt(i)*dsdt(i)/n
      temp2(i,i)=1
    end do

    temp2(1,1)=0

    call zmatmat(n,n,temp2,n,cfftf,temp3)
    call zmatmat(n,n,temp3,n,temp,a1)

    do i=1,n
      temp(i,i)=ps(i)*dsdt(i)/n
      temp2(i,i)=0
    end do

    temp2(1,1)=1

    call zmatmat(n,n,temp2,n,cfftf,temp3)
    call zmatmat(n,n,temp3,n,temp,a2)

    do i=1,n
      do j=1,n
        temp(i,j)=a2(i,j)+a1(i,j)
      end do
    end do

    call zmatmat(n,n,amat,n,temp,wxx)

    !
    ! integrate w_xx once
    !
    call zzero(n**2, temp)

    do i=2,n
      iuse = i-1
      if (iuse .gt. n/2) iuse = iuse-n
      temp(i,i)=2*pi/rl/ima/iuse
    end do

    temp(1,1)=0

    call zmatmat(n,n,temp,n,wxx,temp2)
    call zmatmat(n,n,cfftb,n,temp2,temp3)

    do i=1,n
      do j=1,n
        gtemp(i,j)=temp3(i,j)
      end do
    end do

    !
    ! integrate w_xx twice
    !
    do i=2,n
      iuse = i-1
      if (iuse .gt. n/2) iuse = iuse-n
      temp(i,i)=-2*pi/rl/(iuse*iuse)
    end do

    temp(1,1)=2*pi/rl
    !call prin2('rl = *', rl, 1)

    call zmatmat(n,n,temp,n,wxx,temp2)
    call zmatmat(n,n,cfftb,n,temp2,temp3)

    do i=1,n
      do j=1,n
        gtemp(n+i,j)=temp3(i,j)
      end do
    end do


    do i=1,2*n
      do j=1,2*n
        gradmuls(i,j)=0
      end do
    end do

    do i=1,n
      i1=2*i-1
      i2=i1+1
      gradmuls(i1,i)=1/dsdt(i)
      gradmuls(i2,n+i)=ima*m/ps(i)
    end do

    call zmatmat(2*n,2*n,gradmuls,n,gtemp,wgradinvsl)

  endif

  deallocate( temp,temp2,temp3 )
  deallocate( wxx,gradmuls,gtemp )
  deallocate( a1,a2 )
  deallocate( alpha, beta)
  deallocate( amat)
  deallocate( cfftf, cfftb)


  return
end subroutine axidebye_surfgradinvlap_mat1





subroutine axidebye_ode_periodic_sl_mat(npts, mode, period, r, dsdt, &
    alpha,beta,amat)
  implicit real *8 (a-h,o-z)
  real *8 :: r(npts), dsdt(npts)
  complex *16 :: alpha(npts),beta(npts),amat(npts,npts)

  complex *16, allocatable :: work(:,:)

  complex *16 eye,c0
  integer, allocatable :: ipvt(:)
  complex *16, allocatable :: gam(:)
  complex *16, allocatable :: gamshift(:)
  complex *16, allocatable :: betat(:)
  complex *16, allocatable :: betatshift(:)
  complex *16, allocatable :: wfft(:)

  !
  ! This is a custom routine that computes the matrix that maps point
  ! values of the RHS to values of the second derivative of the
  ! solution to
  !
  !      surflap(u) = f
  !
  ! Assuming that the surflap is discretized as on ODE with
  ! coefficients
  !
  !      u'' + alpha * u' - mode**2 * beta * u = f
  !

  allocate(wfft(4*npts+15))
  allocate(ipvt(npts))
  allocate(gam(npts))
  allocate(betat(npts))
  allocate(gamshift(-npts:npts))
  allocate(betatshift(-npts:npts))

  eye=(0,1)
  pi=4*atan(1.0d0)
  call zffti(npts,wfft)

  !
  ! Fourier transform alpha, -mode*mode*beta (or r*dsdt if mode.eq.0).
  !
  do i = 1,npts
    gam(i) = alpha(i)
    if (mode.ne.0) then
      betat(i) = -mode*mode*beta(i)
    else
      betat(i) = r(i)*dsdt(i)
    endif
  enddo


  call zfftf(npts,gam,wfft)
  call zfftf(npts,betat,wfft)


  do i = -npts,npts
    gamshift(i) = 0.0d0
    betatshift(i) = 0.0d0
  enddo

  do i = 0,npts/2
    gamshift(i) = gam(i+1)/npts
    betatshift(i) = betat(i+1)/npts
  enddo

  if ( mod(npts,2).eq.0) then
    do i = -1,-npts/2+1,-1
      gamshift(i) = gam(npts+i+1)/npts
      betatshift(i) = betat(npts+i+1)/npts
    enddo
  else
    do i = -1,-npts/2,-1
      gamshift(i) = gam(npts+i+1)/npts
      betatshift(i) = betat(npts+i+1)/npts
    enddo
  endif

  do i = 1,npts
    do j = 1,npts
      amat(j,i) = 0.0d0
    enddo
  enddo

  do i = 2,npts
    amat(i,i) = 1.0d0
    do j = 2,npts
      iuse = i-1
      juse = j-1
      if (iuse .gt. npts/2) iuse = iuse-npts
      if (juse .gt. npts/2) juse = juse-npts
      amat(i,j) = amat(i,j) + gamshift(iuse-juse)/(eye*juse)
      if (mode.ne.0) then
        amat(i,j) = amat(i,j)-betatshift(iuse-juse)/(juse**2)
      endif
    enddo
  enddo

  if (mode .eq. 0) then
    amat(1,1) = betatshift(0)
    do i = 2,npts
      amat(i,1) = 0.0d0
      iuse = i-1
      if (iuse .gt. npts/2) iuse = iuse-npts
      amat(1,i) = -betatshift(-iuse)/(iuse**2)
    enddo
  else
    amat(1,1) = betatshift(0)
    do i = 2,npts
      iuse = i-1
      if (iuse .gt. npts/2) iuse = iuse-npts
      amat(i,1) = betatshift(iuse)
      amat(1,i) = gamshift(-iuse)/(eye*iuse)
      amat(1,i) = amat(1,i)-betatshift(-iuse)/(iuse**2)
    enddo
  endif

  !
  ! invert the linear system
  !
  allocate(work(npts,npts))
  incx = 1
  incy = 1
  call zcopy(npts*npts, amat, incx, work, incy)
  call zinverse(npts, work, info, amat)


  deallocate(wfft)
  deallocate(ipvt)
  deallocate(gam)
  deallocate(betat)
  deallocate(gamshift)
  deallocate(betatshift)
  deallocate(work)

  return
end subroutine axidebye_ode_periodic_sl_mat





subroutine axidebye_d2zcopy(n, xs, zs)
  implicit real *8 (a-h,o-z)
  real *8 :: xs(n)
  complex *16 :: zs(n)

  do i = 1,n
    zs(i) = xs(i)
  end do

  return
end subroutine axidebye_d2zcopy





subroutine axidebye_jmbuilder(zk, m, n, xys, dxys, d2xys, h, &
    rho, rhom, alpha, beta, zjs, zms)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n)
  complex *16 :: zk, rho(n), rhom(n), alpha, beta
  complex *16 :: zjs(2,n), zms(2,n)

  real *8 :: ps(10000), zs(10000), dpdt(10000), dzdt(10000)
  real *8 :: dpdt2(10000), dzdt2(10000), dsdt(10000)

  complex *16 :: ima, ch1(2,10000), ch2(2,10000)
  complex *16 :: zjr(2,10000), zjrm(2,10000)
  complex *16, allocatable :: wmat(:)

  ! c
  ! c       this subroutine constructs electric current and magnetic
  ! c       current from the electric and magnetic charges, rho, rhom
  ! c       and the coefficients of the harmonic vector fields, alpha,
  ! c       beta
  ! c
  ! c       input:
  ! c         zk - the complex helmholtz parameter
  ! c         m - fourier mode to compute
  ! c         n - number of points in curve discretization ps,zs
  ! c         rl - length of parameterization interval
  ! c         ps,zs - parameterization of curve
  ! c         dpdt,dzdt - first derivatives of parameterization
  ! c         dpdt2,dzdt2 - second derivatives of parameterization
  ! c         rho - the electric charge
  ! c         rhom - the magnetic charge
  ! c         alpha - coefficient of first harmonic vector form
  ! c         beta - coefficient of second harmonic vector form
  ! c
  ! c       output:
  ! c         zjs - tangential electric current
  ! c         zms - tangential magnetic current
  ! c
  ! c
  ima=(0,1)
  done=1
  pi=4*atan(done)

  do i=1,n
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    dpdt2(i) = d2xys(1,i)
    dzdt2(i) = d2xys(2,i)
    dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
  end do

  !
  ! use the standard rho and rhom representation...
  !
  rl = h*n
  allocate(wmat(2*n*n))
  call axidebye_surfgradinvlap_mat1(n, ps, zs, dpdt, dzdt, &
    dpdt2, dzdt2, h, m, wmat)

  call zmatvec(2*n,n,wmat,rho,zjr)
  call zmatvec(2*n,n,wmat,rhom,zjrm)

  !
  ! construct the harmonic vector fields if mode=0
  !
  if (m .eq. 0) call get2harmvecs(n,ps,zs,ch1,ch2)

  !
  ! and assemble
  !

  do i=1,n
    zjs(1,i) = ima*zk*(zjr(1,i)-zjrm(2,i))
    zjs(2,i) = ima*zk*(zjr(2,i)+zjrm(1,i))
  end do

  if (m .eq. 0) then
    do i = 1,n
      zjs(1,i)=zjs(1,i)+alpha*ch1(1,i)+beta*ch2(1,i)
      zjs(2,i)=zjs(2,i)+alpha*ch1(2,i)+beta*ch2(2,i)
    end do
  end if

  do i = 1,n
    zms(1,i) = zjs(2,i)
    zms(2,i) = -zjs(1,i)
  end do

  return
end subroutine axidebye_jmbuilder





subroutine crea_gradsk_modes0(n, xys, dxys, &
    maxm, a, agrad, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: a(n,n,-maxm:maxm), agrad(n,n,2,-maxm:maxm)
  complex *16 :: amats(2,n,n,-maxm:maxm)

  real *8 :: dsdt(10000)
  complex *16 :: ima

  !
  ! create the surface gradient of sk, assuming that the single layer
  ! and gradient of the slp has been pre-computed
  !
  ! input:
  !   n - number of discretization points
  !   xys, dxys, h - geometry info
  !   maxm - maxmmum mode to construct
  !   a - the single layers for each mode
  !   agrad - the gradient of the single layers for each mode
  !
  ! output:
  !   amats - the matrices which compute surfgrad S_k
  !

  ima = (0,1)
  done = 1
  pi = 4*atan(done)

  do i=1,n
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do

  !$omp parallel do default(shared) private(dpds, dzds)
  do m = -maxm,maxm
    do i=1,n
      dpds = dxys(1,i)/dsdt(i)
      dzds = dxys(2,i)/dsdt(i)
      do j=1,n
        amats(1,i,j,m) = (dpds*agrad(i,j,1,m) &
            + dzds*agrad(i,j,2,m))
        amats(2,i,j,m) = ima*m/xys(1,i)*a(i,j,m)
      end do
    end do
  end do
  !$omp end parallel do

  return
end subroutine crea_gradsk_modes0





subroutine crea_s0div1(norder, n, xys, dxys, h, mode, ws0div)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: ws0div(n,2*n)

  real *8 :: dsdt(10000)
  real *8, allocatable :: dws0all(:,:,:), dws0gradall(:,:,:,:)
  real *8, allocatable :: dws0grad0all(:,:,:,:)

  complex *16 :: ima
  !complex *16, allocatable :: ws0all(:,:,:), ws0grad0all(:,:,:)

  external g0mall

  maxm = abs(mode)+5

  allocate(dws0all(n,n,-maxm:maxm))
  allocate(dws0gradall(n,n,2,-maxm:maxm))
  allocate(dws0grad0all(n,n,2,-maxm:maxm))
  ifscale = 1
  call dalpertmodes_grads(ier, norder, n, xys, dxys, &
      h, g0mall, par0, par1, maxm, ifscale, dws0all, &
      dws0gradall, dws0grad0all)

  !allocate(ws0all(n,n,-maxm:maxm))
  !allocate(ws0grad0all(n,n,2,-maxm:maxm))
  !call axidebye_d2zcopy(n*n*(2*maxm+1), dws0all, ws0all)
  !call axidebye_d2zcopy(2*n*n*(2*maxm+1), dws0grad0all, ws0grad0all)

  !allocate( ws0divall(n,2*n,-maxm:maxm) )
  !call crea_skdiv_modes0(n, xys, dxys, h, &
  !    maxm, ws0all, ws0grad0all, ws0divall)

  ima = (0,1)
  done = 1
  pi = 4*atan(done)

  do i=1,n
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do

  m = mode
  do i=1,n
    do j=1,n
      dpds = dxys(1,j)/dsdt(j)
      dzds = dxys(2,j)/dsdt(j)
      j1 = 2*(j-1)+1
      j2 = j1+1
      ws0div(i,j1) = -dpds*dws0grad0all(i,j,1,m) &
          - dzds*dws0grad0all(i,j,2,m)
      ws0div(i,j2) = ima*m/xys(1,j)*dws0all(i,j,m)
    end do
  end do


  return
end subroutine crea_s0div1






subroutine crea_skdiv_modes0(n, xys, dxys, h, &
    maxm, a, agrad0, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: a(n,n,-maxm:maxm), agrad0(n,n,2,-maxm:maxm)
  complex *16 :: amats(n,2*n,-maxm:maxm)

  real *8 :: dsdt(10000)
  complex *16 :: ima

  !
  ! create the operator sk surfdiv using integration by parts,
  ! assuming that the single layer
  ! and gradient of the slp has been pre-computed
  !
  ! input:
  !   n - number of discretization points
  !   xys, dxys, h - geometry info
  !   maxm - maxmmum mode to construct
  !   a - the single layers for each mode
  !   agrad0 - the source gradient of the single layer for all modes
  !
  ! output:
  !   amats - the matrices which compute S_k Div = \int Grad G_k \cdot
  !

  ima = (0,1)
  done = 1
  pi = 4*atan(done)

  do i=1,n
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do

  !$omp parallel do default(shared) private(dpds, dzds, ji, j2)
  do m = -maxm,maxm
    do i=1,n
      do j=1,n
        dpds = dxys(1,j)/dsdt(j)
        dzds = dxys(2,j)/dsdt(j)
        j1 = 2*(j-1)+1
        j2 = j1+1
        amats(i,j1,m) = -dpds*agrad0(i,j,1,m) - dzds*agrad0(i,j,2,m)
        amats(i,j2,m) = ima*m/xys(1,j)*a(i,j,m)
      end do
    end do
  end do
  !$omp end parallel do

  return
end subroutine crea_skdiv_modes0





subroutine creagradsk_new(zk,m,n,rl,ps,zs,dpdt,dzdt, &
    w, wp, wz, wgradsk)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1), zs(1), dpdt(1), dzdt(1)
  real *8 :: dsdt(10000)
  complex *16 ima,zk,amat(2*n,n),w(n,n),wp(n,n),wz(n,n)
! c
! c       create the surface gradient of sk
! c
! c       input:
! c         eps - the precision to which kernel elements are to be computed
! c         zk - the complex helmholtz parameter
! c         m - the fourier mode for which to construct the matrix
! c         n - the number of points on the curve ps,zs
! c         rl - the length of the parameterizing interval NOT LENGTH OF CURVE
! c             unless ps,zs,dpdt,dzdt were created using an arclength
! c             parameterization
! c         ps,zs - the points of the generating curve
! c         dpdt,dzdt - derivatives with respect to parameterizing variable
! c         norder - order of Kapur-Rokhlin quadratures to use when
! c             constructing the matrix - can be 2, 6, or 10
! c
! c       output:
! c         amat - the 2n by n matrix mapping a charge density to the target
! c             gradient of s_k
! c
! c
        ima=(0,1)
        done=1
        pi=4*atan(done)

        do 2200 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 2200   continue


        do 5000 i=1,n
        i1=2*(i-1)+1
        i2=i1+1

        do 4600 j=1,n
        dpds=dpdt(i)/dsdt(i)
        dzds=dzdt(i)/dsdt(i)
        amat(i1,j)=dpds*wp(i,j)+dzds*wz(i,j)
        amat(i2,j)=ima*m/ps(i)*w(i,j)
 4600   continue
 5000   continue


        return
        end





subroutine crea_ndotcurl_modes0(ier, n, xys, dxys, h, zk, &
    maxm, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
    a_sin, agrad_sin, agrad0_sin, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: zk, amats(n,2*n,-maxm:maxm)
  complex *16 :: a(n,n,-maxm:maxm)
  complex *16 :: agrad(n,n,2,-maxm:maxm)
  complex *16 :: agrad0(n,n,2,-maxm:maxm)
  complex *16 :: a_cos(n,n,-maxm:maxm)
  complex *16 :: agrad_cos(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_cos(n,n,2,-maxm:maxm)
  complex *16 :: a_sin(n,n,-maxm:maxm)
  complex *16 :: agrad_sin(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_sin(n,n,2,-maxm:maxm)

  complex *16 :: ima, dp1,dp2,ds1,ds2,dz1,dz2
  complex *16 :: apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1
  complex *16 :: asdz2,as1,as2,asdp1,asdp2,ap1,ap2


  real *8 :: dsdt(10000), ps(10000), zs(10000), dpdt(10000)
  real *8 :: dzdt(10000)

  complex *16, allocatable :: w(:,:),wp(:,:)
  complex *16, allocatable :: wcc(:,:),wccp(:,:),wccz(:,:)
  complex *16, allocatable :: wss(:,:),wssp(:,:),wssz(:,:)

  !
  ! constructs all the MFIE matrices, i.e. n \times curl(A)
  !

  done=1
  pi=4*atan(done)
  ima=(0,1)

  do i = 1,n
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    sc=dpdt(i)**2+dzdt(i)**2
    sc=sqrt(sc)
    dsdt(i)=sc
  end do

  !
  ! fill the matrices, one mode at a time
  !
  !$omp parallel do default(shared) &
  !$omp   private(w, wp, wcc, wccp, wccz, wss, wssp, wssz) &
  !$omp   private(i1, i2, dpdsi, dzdsi, j1, j2, dpdsj, dzdsj) &
  !$omp   private(asdp1, asdp2, as1, as2, ap1, ap2, az1, az2) &
  !$omp   private(asdz1, asdz2, dp1, dp2, dz1, dz2)
  do m = -maxm,maxm

    allocate(w(n,n),wp(n,n))
    allocate(wcc(n,n),wccp(n,n),wccz(n,n))
    allocate(wss(n,n),wssp(n,n),wssz(n,n))

    !
    ! copy the matrices and fill
    !
    call zveccopy(n*n, a(1,1,m), w)
    call zveccopy(n*n, a_cos(1,1,m), wcc)
    call zveccopy(n*n, a_sin(1,1,m), wss)

    call zveccopy(n*n, agrad(1,1,1,m), wp)
    call zveccopy(n*n, agrad_cos(1,1,1,m), wccp)
    call zveccopy(n*n, agrad_sin(1,1,1,m), wssp)

    call zveccopy(n*n, agrad_cos(1,1,2,m), wccz)
    call zveccopy(n*n, agrad_sin(1,1,2,m), wssz)

    do i=1,n

      i1=2*(i-1)+1
      i2=i1+1
      dpdsi=dpdt(i)/dsdt(i)
      dzdsi=dzdt(i)/dsdt(i)

      do j=1,n

        j1=2*(j-1)+1
        j2=j1+1
        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)
        !
        ! compute all the kernel partial derivatives
        !
        asdp1=ima*dpdsj*wssp(i,j)
        asdp2=wccp(i,j)

        as1=ima*dpdsj*wss(i,j)
        as2=wcc(i,j)

        ap1=dpdsj*wcc(i,j)
        ap2=-ima*wss(i,j)

        az1=dzdsj*w(i,j)
        az2=0

        asdz1=ima*dpdsj*wssz(i,j)
        asdz2=wccz(i,j)
        !
        ! and compute the vector components of the curl
        !
        dp1=1/ps(i)*ima*m*az1-asdz1
        dp2=1/ps(i)*ima*m*az2-asdz2

        dz1=1/ps(i)*as1+asdp1-1/ps(i)*ima*m*ap1
        dz2=1/ps(i)*as2+asdp2-1/ps(i)*ima*m*ap2
        !
        ! and fill the matrix with n dot that
        !
        amats(i,j1,m)=dzdsi*dp1-dpdsi*dz1
        amats(i,j2,m)=dzdsi*dp2-dpdsi*dz2

      end do
    end do

    deallocate(w,wp)
    deallocate(wcc,wccp,wccz)
    deallocate(wss,wssp,wssz)

  end do

  return
end subroutine crea_ndotcurl_modes0





subroutine axidebye_eval3d(zk, nsrc, xyzs, whts, rho, &
    sigma, jcurr, kcurr, ntarg, targs, efield, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,nsrc), whts(nsrc), targs(3,ntarg)
  complex *16 :: zk, rho(nsrc), sigma(nsrc), jcurr(3,nsrc)
  complex *16 :: kcurr(3,nsrc), efield(3,ntarg), hfield(3,ntarg)

  complex *16 :: ima, val, grad(3), hess(3,3)
  complex *16 :: a(3), q(3)
  complex *16 :: phi(3), psi(3)
  complex *16 :: c(3), z(3)

  !
  ! this routine evaluations the full 3d E&M field for axisymmetric
  ! debye sources using cartesian versions of the sources and
  ! currents (using simple smooth quadrature for the farfield)
  !
  !
  !
  ima = (0,1)
  done = 1
  pi = 4*atan(done)

  ntot = ntarg+nsrc
  ntot = 1
  if (ntot .le. 20000) then
    
    !
    ! do the naive, direct calculation
    !
    !$omp parallel do default(shared) &
    !$omp     private(val, grad, hess, a, q, phi, psi, c, z)
    do i = 1,ntarg
      efield(1,i) = 0
      efield(2,i) = 0
      efield(3,i) = 0
      hfield(1,i) = 0
      hfield(2,i) = 0
      hfield(3,i) = 0

      do j = 1,nsrc
        
        call gk3d(zk, xyzs(1,j), targs(1,i), p1, p2, val, grad, hess)
        
        do k = 1,3
          a(k) = val*jcurr(k,j)
          q(k) = val*kcurr(k,j)
          phi(k) = grad(k)*rho(j)
          psi(k) = grad(k)*sigma(j)
        end do
        
        call zcross_prod3d(grad, kcurr(1,j), c)
        call zcross_prod3d(grad, jcurr(1,j), z)
        
        do k = 1,3
          efield(k,i)=efield(k,i) + whts(j)*(ima*zk*a(k)-phi(k)-c(k))
          hfield(k,i)=hfield(k,i) + whts(j)*(ima*zk*q(k)-psi(k)+z(k))
        end do
        
      end do
    end do
    !$omp end parallel do

  else

    !
    ! if here, ntot is sufficiently large to call the fmm
    !
    


  end if
  


    
  return
end subroutine axidebye_eval3d





subroutine axidebye_curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
    zjs,p,theta,z,vals)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1),zs(1),dpdt(1),dzdt(1)
  complex *16 :: ima,zk,vals(1),zjs(2,1),ee,ss,cc,eep,ssp,ccp
  complex *16 :: ssz,ccz,dp,ds,dz,zint1,zint2,zint3,zint4,zint5
  complex *16 :: zint6, zint7,zint8,zint9,zint10,zint11,zint12
  ! c
  ! c       this routine evaluates the curl of the vector potetial of a
  ! c       surface current at an arbitrary point.  the surface current
  ! c       can be arbitrarily specified in t-hat and theta-hat components.
  ! c
  ! c       input:
  ! c         eps - the precision with which to evaluate the kernels
  ! c         zk - complex helmholtz parameter
  ! c         m - the fourier mode we are computing
  ! c         n - the number of points sampled on the curve ps,zs
  ! c         rl - the length of the curve parameterizing interval, not
  ! c             necessarily arclength
  ! c         ps,zs - the generating curve
  ! c         dpdt,dzdt - the curve tangent vectors, not necessarily
  ! c             unit vectors
  ! c         zjs - 2 x n array containing surface current, t-hat and
  ! c             theta-hat components, in that order
  ! c         p,theta,z - the point in 3D at which to evaluate the
  ! c             curl of the potential
  ! c
  ! c       output:
  ! c         vals - length 3 array containing p,theta,z components of the
  ! c             curl of the vector potential
  ! c
  ! c       NOTE: the routine only uses the trapezoidal rule since
  ! c       the current is periodic and p,z does NOT lie on ps,zs.
  ! c       the routine will break when p,z is equal to one of the
  ! c       ps,zs.
  ! c
  ! c       NOTE: the output vals IS modulated by exp(ima*m*theta) in order
  ! c       to fully evaluate curl A at a point in 3D.
  ! c
  ! c
  done=1
  ima=(0,1)
  pi=4*atan(done)
  !c
  !c       evaluate each one of the integrals separately, then glue together
  !c
  zint1=0
  zint2=0
  zint3=0
  zint4=0
  zint5=0
  zint6=0
  zint7=0
  zint8=0
  zint9=0
  zint10=0
  zint11=0
  zint12=0

  h=rl/n
  do i=1,n
    !c
    !c       call each of the kernel routines, first single layer
    !c
    call ghfun2(eps,zk,m,p,z,ps(i),zs(i),ee)
    call ghfun2cc(eps,zk,m,p,z,ps(i),zs(i),cc)
    call ghfun2ss(eps,zk,m,p,z,ps(i),zs(i),ss)
    !c
    !c       now rho derivatives
    !c
    vnp=1
    vnz=0
    call ghfun4n1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,eep)
    call ghfun2ccn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ccp)
    call ghfun2ssn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ssp)
    !c
    !c       and lastly z derivatives
    !c
    vnp=0
    vnz=1
    call ghfun2ccn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ccz)
    call ghfun2ssn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ssz)


    !c
    !c       do each of the integrals now, they are calculated in the order
    !c       they appear in the curl equation my notes
    !c
    dsdt=dpdt(i)**2+dzdt(i)**2
    dsdt=sqrt(dsdt)
    dpds=dpdt(i)/dsdt
    dzds=dzdt(i)/dsdt

    zint1=zint1+h*zjs(1,i)*dzds*ee*dsdt
    zint2=zint2+h*zjs(1,i)*dpds*ssz*dsdt
    zint3=zint3+h*zjs(2,i)*ccz*dsdt

    zint4=zint4+h*zjs(1,i)*dpds*ccz*dsdt
    zint5=zint5+h*zjs(2,i)*ssz*dsdt
    zint6=zint6+h*zjs(1,i)*dzds*eep*dsdt

    zint7=zint7+h*zjs(1,i)*dpds*ss*dsdt
    zint8=zint8+h*zjs(2,i)*cc*dsdt
    zint9=zint9+h*zjs(1,i)*dpds*ssp*dsdt
    zint10=zint10+h*zjs(2,i)*ccp*dsdt
    zint11=zint11+h*zjs(1,i)*dpds*cc*dsdt
    zint12=zint12+h*zjs(2,i)*ss*dsdt

  end do
  !c
  !c       finally compute the curl in cylindrical coordinates
  !c
  dp=ima*m/p*zint1-(ima*zint2+zint3)
  ds=zint4-ima*zint5-zint6
  dz=1/p*(ima*zint7+zint8+p*(ima*zint9+zint10)- &
      ima*m*(zint11-ima*zint12))

  vals(1)=dp*exp(ima*m*theta)
  vals(2)=ds*exp(ima*m*theta)
  vals(3)=dz*exp(ima*m*theta)

  return
end subroutine axidebye_curlaeval






subroutine axidebye_mode_eval(eps, zk, m, n, rl, ps,zs, dpdt,dzdt, &
    rho, rhom, zjs, zms, targ, efield, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1), zs(1), dpdt(1), dzdt(1),  targ(3)
  complex *16 :: zk, rho(n), rhom(n), hfield(3)
  complex *16 :: efield(3)
  complex *16 :: zjs(2,n), zms(2,n)

  real *8 :: rtz(10), xyz(10)
  complex *16 :: ima, vals1(10), vals2(10), vals3(10)
  complex *16 :: evec(10), hvec(10)

  !
  ! this evaluates the generalized debye fields due to a single mode
  ! of debye current and charge. zjs and zms should have been
  ! pre-computed.
  !
  ! input:
  !   xys, dxys, h - geometry info
  !   rho, rhom - debye charges
  !   zjs, zjm - debye currents
  !   targ - target in cartesian coordinates
  !
  ! ouptut:
  !   efield - electric field (cartesian)
  !   hfield - magnetic field (cartesian)
  !
  ima=(0,1)
  done=1
  pi=4*atan(done)

  p = sqrt(targ(1)**2 + targ(2)**2)
  theta = atan2(targ(2), targ(1))
  z = targ(3)

  !
  ! ...calculate the vector potential Q
  !
  call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      zms,p,theta,z,vals1)

  !c
  !c       ...calculate the gradient of psi
  !c
  call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      rhom,p,theta,z,vals2)


  !c
  !c       ...calculate the curl of the vector potential A
  !c
  call axidebye_curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      zjs,p,theta,z,vals3)

  !c
  !c       ...and combine them
  !c
  hvec(1) = ima*zk*vals1(1) - vals2(1) + vals3(1)
  hvec(2) = ima*zk*vals1(2) - vals2(2) + vals3(2)
  hvec(3) = ima*zk*vals1(3) - vals2(3) + vals3(3)

  !
  ! and convert into cartesian coordinates
  !


  ! . . . now for the e field
  ! ...calculate the vector potential A
  !
  call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      zjs,p,theta,z,vals1)

  !c
  !c       ...calculate the gradient of phi
  !c
  call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      rho,p,theta,z,vals2)


  !c
  !c       ...calculate the curl of the vector potential Q
  !c
  call axidebye_curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      zms,p,theta,z,vals3)

  !c
  !c       ...and combine them
  !c
  evec(1) = ima*zk*vals1(1) - vals2(1) - vals3(1)
  evec(2) = ima*zk*vals1(2) - vals2(2) - vals3(2)
  evec(3) = ima*zk*vals1(3) - vals2(3) - vals3(3)

  !
  ! and convert into cartesian coordinates
  !
  rtz(1) = p
  rtz(2) = theta
  rtz(3) = z
  call axi_cyl2cart(rtz, evec, xyz, efield)
  call axi_cyl2cart(rtz, hvec, xyz, hfield)

  return
end subroutine axidebye_mode_eval





subroutine axidebye_mode_eval_dielectric(eps, omega, epsilon, mu, &
    m, n, rl, ps,zs, dpdt,dzdt, &
    rho, rhom, zjs, zms, targ, efield, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1), zs(1), dpdt(1), dzdt(1),  targ(3)
  complex *16 :: omega, epsilon, mu, rho(n), rhom(n), hfield(3)
  complex *16 :: efield(3)
  complex *16 :: zjs(2,n), zms(2,n)

  complex *16 :: zk

  zk = omega*sqrt(epsilon*mu)
  call axidebye_mode_eval(eps, zk, m, n, rl, ps,zs, dpdt,dzdt, &
    rho, rhom, zjs, zms, targ, efield, hfield)

  efield(1) = efield(1)*sqrt(mu)
  efield(2) = efield(2)*sqrt(mu)
  efield(3) = efield(3)*sqrt(mu)

  hfield(1) = hfield(1)*sqrt(epsilon)
  hfield(2) = hfield(2)*sqrt(epsilon)
  hfield(3) = hfield(3)*sqrt(epsilon)

  return
end subroutine axidebye_mode_eval_dielectric





subroutine axidebye_jmbuilder_dielectric(omega, e1, e0, u1, u0, &
    m, n, rl, ps, zs, dpdt, dzdt, dpdt2, dzdt2, norder, &
    rho1, rho0, rhom1, rhom0, aj, bj, am, bm, tclutch,&
    zjs1, zjs0, zms1, zms0)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1)
  complex *16 :: e1, e0, u1, u0, omega, rho1(n), rho0(n)
  complex *16 :: rhom1(n), rhom0(n), aj, bj, am, bm
  complex *16 :: zjs1(2,n), zjs0(2,n), zms1(2,n), zms0(2,n)

  complex *16 :: ima,ch1(2,10000),ch2(2,10000)
  complex *16 :: zr1(2,10000),zr0(2,10000)
  complex *16 :: fac1,fac2,vec1(2,10000),vec2(2,10000)
  complex *16, allocatable :: wmat(:)
  !
  ! this subroutine converts charge and harmonic coefficients into
  ! interior and exterior currents using the representation for
  ! the dielectric currents
  !
  done=1.0d0
  pi=4*atan(done)
  ima=(0,1)

  !
  ! first build exterior electric current, zjs1
  !

  !rl = h*n
  h = rl/n
  allocate(wmat(4*n*n))
  call axidebye_surfgradinvlap_mat1(n, ps, zs, dpdt, dzdt, &
      dpdt2, dzdt2, h, m, wmat)

  call zmatvec(2*n,n,wmat,rho1,zr1)
  call zmatvec(2*n,n,wmat,rho0,zr0)

  fac1=ima*omega*sqrt(u1*e1)
  fac2=-ima*omega*e0*sqrt(u0/e1)

  do i=1,n
    zjs1(1,i)=fac1*zr1(1,i)+fac2*zr0(2,i)
    zjs1(2,i)=fac1*zr1(2,i)-fac2*zr0(1,i)
  end do

  !
  ! now build the exterior magnetic current, zms1
  !
  call zmatvec(2*n,n,wmat,rhom1,zr1)
  call zmatvec(2*n,n,wmat,rhom0,zr0)

  fac1=ima*omega*sqrt(u1*e1)
  fac2=-ima*omega*u0*sqrt(e0/u1)

  do i=1,n
    zms1(1,i)=fac1*zr1(1,i)+fac2*zr0(2,i)
    zms1(2,i)=fac1*zr1(2,i)-fac2*zr0(1,i)
  end do

  !
  !  ...lastly, build the interior currents
  !
  fac1=sqrt(e1/e0)
  fac2=sqrt(u1/u0)

  do i=1,n
    zjs0(1,i)=fac1*zjs1(2,i)
    zjs0(2,i)=-fac1*zjs1(1,i)
    zms0(1,i)=fac2*zms1(2,i)
    zms0(2,i)=-fac2*zms1(1,i)
  end do

  if (m .ne. 0) return

  !
  ! if here, m=0, so add in the contribution from the harmonic vector
  ! fields according to the value of t controlling the clutching map.
  ! the clutching map is always n \times on the 'complement of the
  ! cohomology'..., but can vary between the identity and n \times on
  ! the harmonic fields - maybe
  !
  !    tclutch = 0 corresponds to identity
  !    tclutch = pi/2 corresponds to n \times
  !
  call get2harmvecs(n,ps,zs,ch1,ch2)

  fac1=sqrt(e1/e0)
  fac2=sqrt(u1/u0)

  do i=1,n

    zjs1(1,i)=zjs1(1,i)+aj*ch1(1,i)+bj*ch2(1,i)
    zjs1(2,i)=zjs1(2,i)+aj*ch1(2,i)+bj*ch2(2,i)

    zms1(1,i)=zms1(1,i)+am*ch1(1,i)+bm*ch2(1,i)
    zms1(2,i)=zms1(2,i)+am*ch1(2,i)+bm*ch2(2,i)
  end do

  call creaclutch(n,tclutch,wmat)
  call zmatvec(2*n,2*n,wmat,ch1,vec1)
  call zmatvec(2*n,2*n,wmat,ch2,vec2)

  !
  ! add the harmonic fields to the interior
  !
  do i=1,n

    zjs0(1,i)=zjs0(1,i)+fac1*(aj*vec1(1,i)+bj*vec2(1,i))
    zjs0(2,i)=zjs0(2,i)+fac1*(aj*vec1(2,i)+bj*vec2(2,i))

    zms0(1,i)=zms0(1,i)+fac2*(am*vec1(1,i)+bm*vec2(1,i))
    zms0(2,i)=zms0(2,i)+fac2*(am*vec1(2,i)+bm*vec2(2,i))
  end do

  return
end subroutine axidebye_jmbuilder_dielectric





subroutine axidebye_dielectric_solver(e1, u1, e0, u0, omega, &
    n, h, xys, dxys, d2xys, norder, nphi, xyzs, ein, hin, &
    whts, rho1, &
    sigma1, jcurr1, kcurr1, rho0, sigma0, jcurr0, kcurr0, modemax, &
    esterr)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n), xyzs(3,nphi,n)
  real *8 :: whts(nphi,n)
  complex *16 :: e1, u1, e0, u0, omega
  complex *16 :: ein(3,nphi,n), hin(3,nphi,n)
  complex *16 :: rho1(nphi,n), sigma1(nphi,n)
  complex *16 :: jcurr1(3,nphi,n), kcurr1(3,nphi,n)
  complex *16 :: rho0(nphi,n), sigma0(nphi,n)
  complex *16 :: jcurr0(3,nphi,n), kcurr0(3,nphi,n)  

  character(len=10) :: fmt

  real *8 :: dpdt(10000), dzdt(10000), xyz1(10), ps(10000)
  real *8 :: zs(10000), timings(100)
  real *8 :: dpdt2(10000), dzdt2(10000)

  real *8, allocatable :: dsdt(:), enorms(:), hnorms(:)
  real *8, allocatable :: dws0all(:,:,:), dws0gradall(:,:,:,:)
  real *8, allocatable :: dws0grad0all(:,:,:,:)

  complex *16 :: cd, ima, aj, bj, ak, bk
  complex *16 :: eacyl, ebcyl, hacyl, hbcyl
  complex *16 :: etau, htau

  
  
  complex *16, allocatable :: emodes(:,:,:),hmodes(:,:,:)
  complex *16, allocatable :: amats(:,:,:)
  complex *16, allocatable :: rhomodes0(:,:), sigmamodes0(:,:)
  complex *16, allocatable :: rhomodes1(:,:), sigmamodes1(:,:)
  complex *16, allocatable :: ws0all(:,:,:), ws0grad0all(:,:,:,:)
  complex *16, allocatable :: ws0divall(:,:,:), rhses(:,:)
  complex *16, allocatable :: etan(:,:,:), htan(:,:,:), sols(:,:)
  complex *16, allocatable :: jcurrmodes0(:,:,:), kcurrmodes0(:,:,:)
  complex *16, allocatable :: jcurrmodes1(:,:,:), kcurrmodes1(:,:,:)


  external g0mall
  !
  ! this routine solves, mode by mode, a full exterior surface of
  ! revolution scattering problem by first solving the MFIE, and
  ! then solving a secondary equation (n \cdot E = \rho) for the
  ! surface charge. This avoids low-frequency breakdown in the
  ! recovery of \rho. In the zero-mode, an additional stabilizing
  ! condition on the vector potential is added to the system matrix
  ! (line integral of the vector potential). See our IEEE paper...
  !
  ! input:
  !   e1,u1 -
  !   e0,u0 -
  !   omega - the angular frequency
  !
  ! output:
  !   xyzs -
  !   whts -
  !   rho, sigma -
  !   jcurr, kcurr -
  !   modemax - the maximum fourier mode required for mach prec
  !   esterr -
  !
  !

  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)

  allocate( dsdt(n))
  do i=1,n
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do

  !
  ! compute the a-cycle line integrals
  ! NOPE - this have to be surface integrals
  !
  eacyl = 0
  hacyl = 0
  iphi = 1

  dph = 2*pi/nphi
  do i = 1,nphi
    ph = 2*pi*(i-1)/nphi
    do j = 1,n
      dx = dxys(1,j)/dsdt(j)*cos(ph)
      dy = dxys(1,j)/dsdt(j)*sin(ph)
      dz = dxys(2,j)/dsdt(j)
      etau = ein(1,i,j)*dx + ein(2,i,j)*dy + ein(3,i,j)*dz
      htau = hin(1,i,j)*dx + hin(2,i,j)*dy + hin(3,i,j)*dz
      da = dph*xys(1,j)*h*dsdt(j)
      eacyl = eacyl + da*etau
      hacyl = hacyl + da*htau
    end do
  end do

  
  ! do j = 1,n
  !   dx = dxys(1,j)/dsdt(j)
  !   dy = 0
  !   dz = dxys(2,j)/dsdt(j)
  !   eacyl = eacyl + h*dsdt(j)*(ein(1,iphi,j)*dx &
  !       + ein(2,iphi,j)*dy + ein(3,iphi,j)*dz)
  !   hacyl = hacyl + h*dsdt(j)*(hin(1,iphi,j)*dx &
  !       + hin(2,iphi,j)*dy + hin(3,iphi,j)*dz)
  ! end do



  
  !
  ! compute the b-cycle line integrals
  !

  ind = 1
  do i = 1,n
    if (xys(1,i) .lt. xys(1,ind)) ind = i
  end do

  ebcyl = 0
  hbcyl = 0
  hhh = 2*pi*xys(1,ind)/nphi
  do i = 1,nphi
    phi = 2*pi/nphi*(i-1)
    dx = -sin(phi)
    dy = cos(phi)
    dz = 0
    ebcyl = ebcyl + hhh*(dx*ein(1,i,ind) + &
        dy*ein(2,i,ind) + dz*ein(3,i,ind))
    hbcyl = hbcyl + hhh*(dx*hin(1,i,ind) + &
        dy*hin(2,i,ind) + dz*hin(3,i,ind))
  end do
  


  call prin2('eacyl = *', eacyl, 2)
  call prin2('ebcyl = *', ebcyl, 2)
  call prin2('hacyl = *', hacyl, 2)
  call prin2('hbcyl = *', hbcyl, 2)
  
  
  !
  ! Compute L2 norm of data
  !
  print *
  print *, '. . . calculating norm of data'

  enorm2 = 0
  hnorm2 = 0
  surfarea = 0
  hm = 2*pi/nphi

  do i = 1,n
    da = hm*xys(1,i)*dsdt(i)
    surfarea = surfarea + da
    do j = 1,nphi
      do k = 1,3
        enorm2 = enorm2 + da*abs(ein(k,j,i))**2
        hnorm2 = hnorm2 + da*abs(hin(k,j,i))**2
      end do
    end do
  end do

  enorm2 = sqrt(enorm2)
  hnorm2 = sqrt(hnorm2)
  call prin2('cartesian ein L2 norm = *', enorm2, 1)
  call prin2('cartesian hin L2 norm = *', hnorm2, 1)

  !
  ! decompose the incoming field
  !
  allocate( emodes(3,n,nphi) )
  allocate( hmodes(3,n,nphi) )

  print*
  print *, '. . . decomposing data into fourier modes'
  call axideco_fast(nphi, n, xyzs, ein, hin, emodes,hmodes)

  print *, '. . . determining truncation mode'
  allocate(enorms(nphi),hnorms(nphi))
  do i=1,nphi

    dd1=0
    dd2=0
    do j=1,n
      do k=1,3
        dd1=dd1+abs(emodes(k,j,i))**2
        dd2=dd2+abs(hmodes(k,j,i))**2
      end do
    end do

    enorms(i) = sqrt(dd1)
    hnorms(i) = sqrt(dd2)
  end do

  !call prin2('enorms = *', enorms, nphi)
  !call prin2('hnorms = *', hnorms, nphi)

  
  modemax = -1
  epsfft = 1.0d-15
  do i = 1,nphi/2-1
    d1 = enorms(i)/enorm2
    d2 = hnorms(i)/hnorm2
    if ((enorms(i)/enorm2 .lt. epsfft) &
        .and. (hnorms(i)/hnorm2 .lt. epsfft)) then
      modemax = i
      call prinf('modemax = *', modemax, 1)
      exit
    end if
  end do

  !modemax = 1
  
  if (modemax .lt. 0) modemax = nphi/2-1
  maxm = modemax + 5

  call prinf('maximum mode needed, modemax = *', modemax, 1)



  !
  ! loop over all modes from -modemax to modemax, solve each
  ! problem, then recombine. First, generate all right hand sides
  !
  ! construct s0 surfdiv....
  ! first, build the laplace single layer matrix and its derivatives
  !

  print *, '. . . computing right hand side'


  ifscale = 1
  allocate(dws0all(n,n,-maxm:maxm))
  allocate(dws0gradall(n,n,2,-maxm:maxm))
  allocate(dws0grad0all(n,n,2,-maxm:maxm))
  call dalpertmodes_grads(ier, norder, n, xys, dxys, &
      h, g0mall, par0, par1, maxm, ifscale, dws0all, &
      dws0gradall, dws0grad0all)

  allocate(ws0all(n,n,-maxm:maxm))
  allocate(ws0grad0all(n,n,2,-maxm:maxm))
  call axidebye_d2zcopy(n*n*(2*maxm+1), dws0all, ws0all)
  call axidebye_d2zcopy(2*n*n*(2*maxm+1), dws0grad0all, ws0grad0all)

  !
  ! and assemble S_0 div = -\int grad' G_0
  !
  allocate( ws0divall(n,2*n,-maxm:maxm) )
  call crea_skdiv_modes0(n, xys, dxys, h, &
      maxm, ws0all, ws0grad0all, ws0divall)

  !
  ! extract tangential components
  !
  allocate(etan(2,n,-modemax:modemax))
  allocate(htan(2,n,-modemax:modemax))

  ijk = 0
  do m = 0,modemax
    ijk = ijk+1
    do i = 1,n
      dpds = dxys(1,i)/dsdt(i)
      dzds = dxys(2,i)/dsdt(i)
      etan(1,i,m) = dpds*emodes(1,i,ijk) + dzds*emodes(3,i,ijk)
      etan(2,i,m) = emodes(2,i,ijk)
      htan(1,i,m) = dpds*hmodes(1,i,ijk) + dzds*hmodes(3,i,ijk)
      htan(2,i,m) = hmodes(2,i,ijk)
    end do
  end do

  do m = 1,modemax
    ijk = nphi-m+1
    do i = 1,n
      dpds=dxys(1,i)/dsdt(i)
      dzds=dxys(2,i)/dsdt(i)
      etan(1,i,-m) = dpds*emodes(1,i,ijk) + dzds*emodes(3,i,ijk)
      etan(2,i,-m) = emodes(2,i,ijk)
      htan(1,i,-m) = dpds*hmodes(1,i,ijk) + dzds*hmodes(3,i,ijk)
      htan(2,i,-m) = hmodes(2,i,ijk)
    end do
  end do

  !
  ! fill the rhs now
  !
  do i = 1,n
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
  end do

  allocate(rhses(4*n+4,-modemax:modemax))
  do m = -modemax,modemax
    if (m .lt. 0) mmm = nphi+m+1
    if (m .ge. 0) mmm = m+1
    call zmatvec(n, 2*n, ws0divall(1,1,m), etan(1,1,m), rhses(1,m))
    call zmatvec(n, 2*n, ws0divall(1,1,m), htan(1,1,m), rhses(n+1,m))
    call ndotdirect(n, dpdt, dzdt, emodes(1,1,mmm), rhses(2*n+1,m))
    call ndotdirect(n, dpdt, dzdt, hmodes(1,1,mmm), rhses(3*n+1,m))
  end do

  print *, 'make sure the rhs is correct for interior or exterior'
  stop
  

  !
  ! and assemble rhs
  !
  do m = -modemax,modemax
    do i = 1,n
      rhses(i,m) = rhses(i,m)
      rhses(n+i,m) = rhses(n+i,m)
      rhses(2*n+i,m) = -e1*rhses(2*n+i,m)
      rhses(3*n+i,m) = u1*rhses(3*n+i,m)
      !rhses(2*n+i,m) = -e0*rhses(2*n+i,m)
      !rhses(3*n+i,m) = u0*rhses(3*n+i,m)
    end do
  end do

  do m = -modemax,modemax
    do i = 1,4*n
      !rhses(i,m) = -rhses(i,m)
    end do
  end do
  
  
  do m = -modemax,modemax
    rhses(4*n+1,m) = 0
    rhses(4*n+2,m) = 0
    rhses(4*n+3,m) = 0
    rhses(4*n+4,m) = 0
  end do

  rhses(4*n+1,0) = eacyl
  rhses(4*n+2,0) = ebcyl
  rhses(4*n+3,0) = hacyl
  rhses(4*n+4,0) = hbcyl

  !rhses(4*n+1,0) = -eacyl
  !rhses(4*n+2,0) = -ebcyl
  !rhses(4*n+3,0) = -hacyl
  !rhses(4*n+4,0) = -hbcyl

  ! rhses(4*n+1,0) = 0
  ! rhses(4*n+2,0) = 0
  ! rhses(4*n+3,0) = 0
  ! rhses(4*n+4,0) = 0


  ! do m = -modemax,modemax
  !   call prinf('m = *', m, 1)
  !   call prin2('rhses(m) = *', rhses(1,m), 4*n*2+8)
  ! end do
  
  
  
  
  !
  ! construct all the matrices
  !
  print *, '. . . building all system matrices'
  allocate(amats(4*n+4,4*n+4,-maxm:maxm))
  call axidebye_dielectric_mats(ier, norder, n, xys, dxys, d2xys, h, &
    e1, u1, e0, u0, omega, maxm, ind, amats)
  
  !
  ! solve each system
  !
  print *, '. . . solving each system'

  allocate(sols(4*n+4,-modemax:modemax))
  !$omp parallel do default(shared) private(info, dcond)
  do m = -modemax,modemax
    call zgausselim(4*n+4, amats(1,1,m), rhses(1,m), info, &
        sols(1,m), dcond)
  end do
  !$end parallel do


  !
  ! synthesize the charge, evaluate at nphi points
  ! and construct the smooth quadrature weights
  !

  print *, '. . . synthesizing Debye sources'

  allocate(rhomodes0(n,-modemax:modemax))
  allocate(sigmamodes0(n,-modemax:modemax))
  allocate(rhomodes1(n,-modemax:modemax))
  allocate(sigmamodes1(n,-modemax:modemax))

  do m = -modemax,modemax
    do i = 1,n
      rhomodes1(i,m) = sols(i,m)
      sigmamodes1(i,m) = sols(n+i,m)
      rhomodes0(i,m) = sols(2*n+i,m)
      sigmamodes0(i,m) = sols(3*n+i,m)
    end do
  end do

  aj = sols(4*n+1,0)
  bj = sols(4*n+2,0)
  ak = sols(4*n+3,0)
  bk = sols(4*n+4,0)

  call prin2('after solve, aj = *', aj, 2)
  call prin2('after solve, bj = *', bj, 2)
  call prin2('after solve, ak = *', ak, 2)
  call prin2('after solve, bk = *', bk, 2)
  
  !aj = 0
  !bj = 0
  !ak = 0
  !bk = 0

  call axisynth1_fast(n, xys, dxys, h, modemax, nphi, &
      rhomodes1, rho1)
  call axisynth1_fast(n, xys, dxys, h, modemax, nphi, &
      sigmamodes1, sigma1)
  call axisynth1_fast(n, xys, dxys, h, modemax, nphi, &
      rhomodes0, rho0)
  call axisynth1_fast(n, xys, dxys, h, modemax, nphi, &
      sigmamodes0, sigma0)

  

  !
  ! calculate each of the modal currents, and then synthesis them
  !
  allocate(jcurrmodes0(2,n,-modemax:modemax))
  allocate(kcurrmodes0(2,n,-modemax:modemax))
  allocate(jcurrmodes1(2,n,-modemax:modemax))
  allocate(kcurrmodes1(2,n,-modemax:modemax))

  do i = 1,n
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    dpdt2(i) = d2xys(1,i)
    dzdt2(i) = d2xys(2,i)
  end do
  
  print *, '. . . building modal currents'
  rl = h*n
  !!rl = 2*pi
    tclutch = 0
  !$omp parallel do default(shared)
  do m = -modemax,modemax
    call axidebye_jmbuilder_dielectric(omega, e1, e0, u1, u0, &
        m, n, rl, ps, zs, dpdt, dzdt, dpdt2, dzdt2, norder, &
        rhomodes1(1,m), rhomodes0(1,m), sigmamodes1(1,m),&
        sigmamodes0(1,m), aj, bj, ak, bk, tclutch,&
        jcurrmodes1(1,1,m), jcurrmodes0(1,1,m), &
        kcurrmodes1(1,1,m), kcurrmodes0(1,1,m) )
  end do  
  !$omp end parallel do

    
  call axisynth3_fast(n, xys, dxys, h, modemax, nphi, &
      jcurrmodes1, jcurr1)
  call axisynth3_fast(n, xys, dxys, h, modemax, nphi, &
      kcurrmodes1, kcurr1)
  call axisynth3_fast(n, xys, dxys, h, modemax, nphi, &
      jcurrmodes0, jcurr0)
  call axisynth3_fast(n, xys, dxys, h, modemax, nphi, &
      kcurrmodes0, kcurr0)

  call axi_3dwhts(n, xys, dxys, h, nphi, whts)

  esterr = -7
  return
  


  !
  ! now estimate the error by examing the dft of rho and sigma,
  ! first the norms of the functions
  !
  
  rnorm1 = 0
  rnorm2 = 0
  rnorm3 = 0
  rnorm4 = 0
  do j = 1,n
    do i = 1,nphi
      rnorm1 = rnorm1 + whts(i,j)*abs(rho1(i,j))**2
      rnorm2 = rnorm2 + whts(i,j)*abs(sigma1(i,j))**2
      rnorm3 = rnorm3 + whts(i,j)*abs(rho0(i,j))**2
      rnorm4 = rnorm4 + whts(i,j)*abs(sigma0(i,j))**2
    end do
  end do

  rnorm1 = sqrt(rnorm1)
  rnorm2 = sqrt(rnorm2)
  rnorm3 = sqrt(rnorm3)
  rnorm4 = sqrt(rnorm4)
  call prin2('L2 norm of rho1 = *', rnorm1, 1)
  call prin2('L2 norm of sigma1 = *', rnorm2, 1)
  call prin2('L2 norm of rho0 = *', rnorm3,1)
  call prin2('L2 norm of sigma0 = *', rnorm4, 1)

  esterr = -7
  ! stop
  
  return
end subroutine axidebye_dielectric_solver





subroutine axidebye_dielectric_loopfields(omega, mu, epsilon, &
    center, targ, radius, par1, efield, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: center(3), targ(3), par1(*)
  complex *16 :: omega, mu, epsilon, efield(3), hfield(3)

  real *8 :: src2(10), targ2(10)
  complex *16 :: zk, ima, u1, u2, sigma, val, cdp, cdz, grad(10)
  complex *16 :: grad0(10), val2

  !
  ! calculate the e and h fields due to a current loop
  !
  ! input:
  !   zk -
  !   center - the center of the loop in cartesian coordinates
  !   targ - the target location in cartesian coordinates
  !   radius - the radius of the loop
  !   par1 - another paramter for future use
  !
  ! output:
  !   efield, hfield - the maxwell fields in cartesian coordinates
  !

  ima = (0,1)
  done = 1
  pi = 4*atan(done)
  zk = omega*sqrt(epsilon*mu)

  dx = targ(1) - center(1)
  dy = targ(2) - center(2)
  dz = targ(3) - center(3)

  p2 = sqrt(dx**2 + dy**2)
  phi2=atan2(dy,dx)
  z2 = targ(3)
  
  z0 = center(3)
        
  mode=0
  src2(1) = radius
  src2(2) = center(3)
  targ2(1) = p2
  targ2(2) = z2
  call gkmone_cos(zk, src2, targ2, par1, mode, val, grad, grad0)

  val = val*pi*2*radius
  cdp = grad(1)*pi*2*radius
  cdz = grad(2)*pi*2*radius
  
  hfield(1)=-cdz/(2*pi*radius)
  hfield(2)=0
  hfield(3)=done/p2/(2*pi*radius)*(val+p2*cdp)

  efield(1)=0
  efield(2)=ima*zk*val/(2*pi*radius)
  efield(3)=0

  !
  ! turn into cartesian coordinates
  !
  u1=hfield(1)
  u2=hfield(2)
  hfield(1)=u1*cos(phi2)-u2*sin(phi2)
  hfield(2)=u1*sin(phi2)+u2*cos(phi2)

  u1=efield(1)
  u2=efield(2)
  efield(1) = u1*cos(phi2)-u2*sin(phi2)
  efield(2) = u1*sin(phi2)+u2*cos(phi2)

  efield(1) = efield(1)*sqrt(mu)
  efield(2) = efield(2)*sqrt(mu)
  efield(3) = efield(3)*sqrt(mu)
  
  hfield(1) = hfield(1)*sqrt(epsilon)
  hfield(2) = hfield(2)*sqrt(epsilon)
  hfield(3) = hfield(3)*sqrt(epsilon)
  
  return
end subroutine axidebye_dielectric_loopfields





subroutine axidebye_dielectric_mat1(ier, norder, n, xys, dxys, d2xys, h, &
    e1, u1, e0, u0, omega, mode, ind, amat)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n)
  complex *16 :: e1, u1, e0, u0, omega, amat(4*n+4,4*n+4)
  
  complex *16, allocatable :: amats(:,:,:)
  
  !
  ! return a single, modal system matrix for the axisymmetric
  ! dielectric problem
  !
  
  maxm = abs(mode)+5
  n4 = 4*n+4
  allocate(amats(n4,n4,-maxm:maxm))
  
  call axidebye_dielectric_mats(ier, norder, n, xys, dxys, d2xys, h, &
      e1, u1, e0, u0, omega, maxm, ind, amats)
  
  nnn = (4*n+4)**2
  incx = 1
  incy = 1
  call zcopy(nnn, amats(1,1,mode), incx, amat, incy)

  return
end subroutine axidebye_dielectric_mat1





subroutine axidebye_dielectric_mats(ier, norder, n, xys, dxys, d2xys, h, &
    e1, u1, e0, u0, omega, maxm, ind, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n)
  complex *16 :: e1, u1, e0, u0, omega
  complex *16 :: amats(4*n+4,4*n+4,-maxm:maxm)

  real *8 :: ps(10000), zs(10000), dpdt(10000), dpdt2(10000)
  real *8 :: dzdt(10000), dzdt2(10000), dsdt(10000)

  !complex *16, allocatable :: amats(:,:,:)

  complex *16 :: ima,zk0,zk1,zk000
  complex *16 :: cd, u(20000000),v(20000000),sz(10000)
  complex *16 :: ch1(2,10000), ch2(2,10000)
  complex *16 :: cout1(10000000),cout2(10000000),cout3(10000000)
  complex *16 :: cout4(10000000),cin1(10000000),cin2(10000000)
  complex *16 :: cin3(10000000), cin4(10000000),cwhts(10000)
  complex *16 :: cd1,cd2,cd3,cd4,cd5,cd6,cd7,cd8

  real *8, allocatable :: dws0all(:,:,:), dws0gradall(:,:,:,:)
  real *8, allocatable :: dws0grad0all(:,:,:,:)

  complex *16, allocatable :: ws0all(:,:,:), ws0grad0all(:,:,:,:)
  complex *16, allocatable :: ws0divall(:,:,:)
  complex *16, allocatable :: wgradreps(:,:,:)

  complex *16, allocatable :: a_1(:,:,:)
  complex *16, allocatable :: agrad_1(:,:,:,:)
  complex *16, allocatable :: agrad0_1(:,:,:,:)
  complex *16, allocatable :: a_cos_1(:,:,:)
  complex *16, allocatable :: agrad_cos_1(:,:,:,:)
  complex *16, allocatable :: agrad0_cos_1(:,:,:,:)
  complex *16, allocatable :: a_sin_1(:,:,:)
  complex *16, allocatable :: agrad_sin_1(:,:,:,:)
  complex *16, allocatable :: agrad0_sin_1(:,:,:,:)

  complex *16, allocatable :: a_0(:,:,:)
  complex *16, allocatable :: agrad_0(:,:,:,:)
  complex *16, allocatable :: agrad0_0(:,:,:,:)
  complex *16, allocatable :: a_cos_0(:,:,:)
  complex *16, allocatable :: agrad_cos_0(:,:,:,:)
  complex *16, allocatable :: agrad0_cos_0(:,:,:,:)
  complex *16, allocatable :: a_sin_0(:,:,:)
  complex *16, allocatable :: agrad_sin_0(:,:,:,:)
  complex *16, allocatable :: agrad0_sin_0(:,:,:,:)

  complex *16, allocatable :: wgradskall_1(:,:,:), wgradskall_0(:,:,:)
  complex *16, allocatable :: wsprimes_1(:,:,:), wsprimes_0(:,:,:)
  complex *16, allocatable :: mfiemats_1(:,:,:), mfiemats_0(:,:,:)
  complex *16, allocatable :: andotcurls_1(:,:,:), andotcurls_0(:,:,:)
  complex *16, allocatable :: vecpotmats_1(:,:,:,:)
  complex *16, allocatable :: vecpotmats_0(:,:,:,:)
  complex *16, allocatable :: tempmats_0(:,:,:,:), tempmats_1(:,:,:,:)
  complex *16, allocatable :: tvpmats_0(:,:,:), tvpmats_1(:,:,:)

  complex *16, allocatable :: wones(:,:)
  complex *16, allocatable :: wnxt(:,:),wndot(:,:)
  complex *16, allocatable :: wclutch(:,:)
  complex *16, allocatable :: a1(:,:),a2(:,:),a3(:,:),a4(:,:)
  complex *16, allocatable :: b1(:,:),b2(:,:),b3(:,:)
  complex *16, allocatable :: work(:),work2(:)

  external g0mall

  !
  !       this subroutine builds the matrix which discritizes the
  !       dielectric scattering problem using generalized debye sources
  !
  !       the equations to be satisfied are:
  !
  !           [ -n x n x E1  + n x n x E0  ] = f1
  !           [ -n x n x H1  + n x n x H0  ] = f2
  !           [ -e1 n dot E1 + e0 n dot E0 ] = f3
  !           [ u1 n dot H1 - u0 n dot H0  ] = f4
  !           [ line integrals of E and H  ] = f5
  !
  ! where E1,H1 denote the exterior fields and E0,H0 denote the
  ! interior fields
  !
  !

  allocate(wones(n,n) )
  allocate( wnxt(2*n,2*n) )
  allocate( wndot(n,3*n) )
  allocate( wclutch(2*n,2*n) )

  done=1.0d0
  ima=(0,1)
  pi=4*atan(done)

  !
  ! compute dsdt around the generating curve
  !
  do i = 1,n
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    dpdt2(i) = d2xys(1,i)
    dzdt2(i) = d2xys(2,i)
    dd=dpdt(i)**2+dzdt(i)**2
    dd=sqrt(dd)
    dsdt(i)=dd
  end do


  !
  ! create all the kernel matrices
  !
  allocate(a_1(n,n,-maxm:maxm))
  allocate(a_cos_1(n,n,-maxm:maxm))
  allocate(a_sin_1(n,n,-maxm:maxm))
  allocate(agrad_1(n,n,2,-maxm:maxm))
  allocate(agrad_cos_1(n,n,2,-maxm:maxm))
  allocate(agrad_sin_1(n,n,2,-maxm:maxm))
  allocate(agrad0_1(n,n,2,-maxm:maxm))
  allocate(agrad0_cos_1(n,n,2,-maxm:maxm))
  allocate(agrad0_sin_1(n,n,2,-maxm:maxm))

  allocate(a_0(n,n,-maxm:maxm))
  allocate(a_cos_0(n,n,-maxm:maxm))
  allocate(a_sin_0(n,n,-maxm:maxm))
  allocate(agrad_0(n,n,2,-maxm:maxm))
  allocate(agrad_cos_0(n,n,2,-maxm:maxm))
  allocate(agrad_sin_0(n,n,2,-maxm:maxm))
  allocate(agrad0_0(n,n,2,-maxm:maxm))
  allocate(agrad0_cos_0(n,n,2,-maxm:maxm))
  allocate(agrad0_sin_0(n,n,2,-maxm:maxm))



  print *, '- - - crea_gmkall_pieces - - -'
  zk1=omega*sqrt(e1*u1)
  zk0=omega*sqrt(e0*u0)
  call crea_gkmall_pieces(ier, norder, n, xys, dxys, h, zk1, &
      maxm, a_1, agrad_1, agrad0_1, a_cos_1, agrad_cos_1, &
      agrad0_cos_1, a_sin_1, agrad_sin_1, agrad0_sin_1)

  call crea_gkmall_pieces(ier, norder, n, xys, dxys, h, zk0, &
      maxm, a_0, agrad_0, agrad0_0, a_cos_0, agrad_cos_0, &
      agrad0_cos_0, a_sin_0, agrad_sin_0, agrad0_sin_0)


  !
  ! create grad s_k terms
  !
  allocate( wgradskall_1(2*n,n,-maxm:maxm) )
  allocate( wgradskall_0(2*n,n,-maxm:maxm) )
  call crea_gradsk_modes0(n, xys, dxys, &
      maxm, a_1, agrad_1, wgradskall_1)
  call crea_gradsk_modes0(n, xys, dxys, &
      maxm, a_0, agrad_0, wgradskall_0)

  !
  ! compute wgradrep = grad surflap^{-1}
  !
  allocate(wgradreps(2*n,n,-maxm:maxm))

  print *, '- - - axidebye_surfgradinvlap_mat1 - - -'
  !!!!$omp parallel do default(shared)
  do mmm = -maxm,maxm
    !call prinf('mmm = *', mmm, 1)
    !call prinf('n = *', n, 1)
    !call prin2('ps = *', ps, n)
    !call prin2('zs = *', zs, n)
    !!call prin2('dpdt = *', dpdt, n)
    !call prin2('dzdt = *', dzdt, n)
    !call prin2('dpdt2 = *', dpdt2, n)
    !call prin2('dzdt2 = *', dzdt2, n)
    !call prin2('h = *', h, 1)
    call axidebye_surfgradinvlap_mat1(n, ps, zs, dpdt, dzdt, &
        dpdt2, dzdt2, h, mmm, wgradreps(1,1,mmm))
  end do
  !!!!$omp end parallel do

  !call prinf('finished constructing surfgradinvlap mats*', pi, 0)
  !stop

  !
  ! construct all the S' matrices
  !
  print *, '- - - wsprimes loop - - -'
  allocate(wsprimes_1(n,n,-maxm:maxm))
  allocate(wsprimes_0(n,n,-maxm:maxm))
  do mmm = -maxm,maxm
    do i=1,n
      dpds = dxys(1,i)/dsdt(i)
      dzds = dxys(2,i)/dsdt(i)
      do j=1,n
        wsprimes_1(i,j,mmm) = dzds*agrad_1(i,j,1,mmm) &
            - dpds*agrad_1(i,j,2,mmm)
        wsprimes_0(i,j,mmm) = dzds*agrad_0(i,j,1,mmm) &
            - dpds*agrad_0(i,j,2,mmm)
        if (i .eq. j) then
          wsprimes_1(i,j,mmm) = wsprimes_1(i,j,mmm) -.5d0
          wsprimes_0(i,j,mmm) = wsprimes_0(i,j,mmm) +.5d0
        end if
      end do
    end do
  end do




  print *, '- - - crea_vecpot_modes0 - - -'
  iftan=0
  allocate(vecpotmats_1(3,n,2*n,-maxm:maxm))
  allocate(vecpotmats_0(3,n,2*n,-maxm:maxm))
  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk1, &
      maxm, iftan, a_1, agrad_1, agrad0_1, a_cos_1, agrad_cos_1, &
      agrad0_cos_1, a_sin_1, agrad_sin_1, agrad0_sin_1, vecpotmats_1)
  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk0, &
      maxm, iftan, a_0, agrad_0, agrad0_0, a_cos_0, agrad_cos_0, &
      agrad0_cos_0, a_sin_0, agrad_sin_0, agrad0_sin_0, vecpotmats_0)

  iftan = 1
  allocate(tempmats_1(3,n,2*n,-maxm:maxm))
  allocate(tempmats_0(3,n,2*n,-maxm:maxm))
  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk1, &
      maxm, iftan, a_1, agrad_1, agrad0_1, a_cos_1, agrad_cos_1, &
      agrad0_cos_1, a_sin_1, agrad_sin_1, agrad0_sin_1, tempmats_1)
  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk0, &
      maxm, iftan, a_0, agrad_0, agrad0_0, a_cos_0, agrad_cos_0, &
      agrad0_cos_0, a_sin_0, agrad_sin_0, agrad0_sin_0, tempmats_0)


  !
  ! chop off every third row
  !
  allocate(tvpmats_1(2*n,2*n,-maxm:maxm))
  allocate(tvpmats_0(2*n,2*n,-maxm:maxm))
  do mmm = -maxm,maxm
    do i = 1,n
      i1 = 2*(i-1)+1
      i2 = i1+1
      do j = 1,2*n
        tvpmats_1(i1,j,mmm) = tempmats_1(1,i,j,mmm)
        tvpmats_1(i2,j,mmm) = tempmats_1(2,i,j,mmm)
        tvpmats_0(i1,j,mmm) = tempmats_0(1,i,j,mmm)
        tvpmats_0(i2,j,mmm) = tempmats_0(2,i,j,mmm)
      end do
    end do
  end do


  print *, '- - - creating s0 div - - -'
  ifscale = 1
  allocate(dws0all(n,n,-maxm:maxm))
  allocate(dws0gradall(n,n,2,-maxm:maxm))
  allocate(dws0grad0all(n,n,2,-maxm:maxm))
  call dalpertmodes_grads(ier, norder, n, xys, dxys, &
      h, g0mall, par0, par1, maxm, ifscale, dws0all, &
      dws0gradall, dws0grad0all)

  allocate(ws0all(n,n,-maxm:maxm))
  allocate(ws0grad0all(n,n,2,-maxm:maxm))
  call axidebye_d2zcopy(n*n*(2*maxm+1), dws0all, ws0all)
  call axidebye_d2zcopy(2*n*n*(2*maxm+1), dws0grad0all, ws0grad0all)

  allocate( ws0divall(n,2*n,-maxm:maxm) )
  call crea_skdiv_modes0(n, xys, dxys, h, &
      maxm, ws0all, ws0grad0all, ws0divall)



  !
  ! . . . create the MFIE part of the representations
  !
  print *, '- - - crea_mfie_modes0 - - -'
  allocate(mfiemats_1(2*n,2*n,-maxm:maxm))
  allocate(mfiemats_0(2*n,2*n,-maxm:maxm))

  zdiag = .5d0
  call crea_mfie_modes0(ier, norder, n, xys, dxys, h, zk1, &
      maxm, zdiag, a_1, agrad_1, agrad0_1, a_cos_1, agrad_cos_1, &
      agrad0_cos_1, &
      a_sin_1, agrad_sin_1, agrad0_sin_1, mfiemats_1)

  zdiag = -.5d0
  call crea_mfie_modes0(ier, norder, n, xys, dxys, h, zk0, &
      maxm, zdiag, a_0, agrad_0, agrad0_0, a_cos_0, agrad_cos_0, agrad0_cos_0, &
      a_sin_0, agrad_sin_0, agrad0_sin_0, mfiemats_0)


  print *, '- - - crea_ndotcurl_modes0 - - -'
  allocate(andotcurls_1(n,2*n,-maxm:maxm))
  allocate(andotcurls_0(n,2*n,-maxm:maxm))
  call crea_ndotcurl_modes0(ier, n, xys, dxys, h, zk1, &
      maxm, a_1, agrad_1, agrad0_1, a_cos_1, agrad_cos_1, &
      agrad0_cos_1, &
      a_sin_1, agrad_sin_1, agrad0_sin_1, andotcurls_1)
  call crea_ndotcurl_modes0(ier, n, xys, dxys, h, zk0, &
      maxm, a_0, agrad_0, agrad0_0, a_cos_0, agrad_cos_0, &
      agrad0_cos_0, &
      a_sin_0, agrad_sin_0, agrad0_sin_0, andotcurls_0)


  !
  ! n dot and n cross matrices
  !
  call axidebye_creanxtmat(n,wnxt)
  call axidebye_creandotmat(n,dpdt,dzdt,wndot)

  !
  ! form the mean preserving 'ones' matrix
  !
  surfarea=0
  do i=1,n
    surfarea=surfarea+2*pi*ps(i)*dsdt(i)*h
  end do

  do i=1,n
    do j=1,n
      wones(i,j)=ps(j)*h*dsdt(j)*2*pi/surfarea/4
    end do
  end do


  !$omp parallel do default(shared) &
  !$omp     private(work, a1, a2, work2, a3, a4, dd)
  do m = -maxm,maxm

    allocate( a1(n,n),a2(n,n), a3(n,n), a4(n,n) )
    allocate( work(4*n*n), work2(4*n*n) )

    print *, 'in parallel block, assembling mode m = ', m

    !
    ! there are 16 major blocks to build, begin with (1,1) block
    ! the contribution of rho1 to S0 Div n \times n \times (E_1-E_0)
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,tvpmats_1(1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n, 2*n, ws0divall(1,1,m), &
        n, wgradskall_1(1,1,m), a2)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,tvpmats_0(1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a3)


    ! ! ! ! ! ! ! !
    ! USE CORRECT ONES MATRIX
    ! ! ! ! ! ! ! !
    dd=0
    if (m .eq. 0) then
      dd=1
    endif

    do  i=1,n
      do j=1,n
        amats(i,j,m)=-sqrt(u1)*zk1**2*a1(i,j) &
            -sqrt(u1)*a2(i,j) &
            +sqrt(u0*e1/e0)*zk0*zk1*a3(i,j) &
            +dd*wones(i,j)
      end do
    end do


    !
    ! ...and (1,2) block...the contribution of rhom1
    ! to S0 Div n \times n \times (E_1-E_0)
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_1(1,1,m),work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_0(1,1,m),work2)
    call zmatmat(n,2*n,work2,2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(i,n+j,m)=ima*zk1*sqrt(u1)*a1(i,j) &
            -ima*zk1*sqrt(u1)*a2(i,j)
      end do
    end do


    !
    ! ...and (1,3) block...the contribution of rho0
    ! to S0 Div n \times n \times (E_1 - E_0)
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,&
        tvpmats_1(1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,tvpmats_0(1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    call zmatmat(n, 2*n, ws0divall(1,1,m), &
        n, wgradskall_0(1,1,m), a3)

    do i=1,n
      do j=1,n
        amats(i,2*n+j,m)=sqrt(e0*u1/e1)*zk0*zk1*a1(i,j) &
            +sqrt(u0)*zk0**2*a2(i,j) &
            +sqrt(u0)*a3(i,j) &
            +dd*wones(i,j)
      end do
    end do


    !c
    !    ...and (1,4) block...the contribution of rhom0
    !    to S0 Div n \times n \times (E_1 - E_0)
    !c
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_1(1,1,m),work2)
    call zmatmat(n,2*n,work2,2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_0(1,1,m),work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(i,3*n+j,m)=-ima*zk0*sqrt(u0)*a1(i,j) &
            -ima*zk0*sqrt(u0)*a2(i,j)
      end do
    end do


    !
    !    ...and (2,1) block...the contribution of rho1
    !    to S0 Div -n x n x (H_1 - H_0)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_1(1,1,m),work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_0(1,1,m),work2)
    call zmatmat(n,2*n,work2,2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(n+i,j,m)=-ima*zk1*sqrt(e1)*a1(i,j) &
            +ima*zk1*sqrt(e1)*a2(i,j)
      end do
    end do

    !
    !    ...and (2,2) block...the contribution of rhom1
    !    to S0 Div -n x n x (H_1 - H_0)
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,&
        tvpmats_1(1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n, 2*n, ws0divall(1,1,m), &
        n, wgradskall_1(1,1,m), a2)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,tvpmats_0(1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a3)

    do i=1,n
      do j=1,n
        amats(n+i,n+j,m)=-sqrt(e1)*zk1**2*a1(i,j) &
            -sqrt(e1)*a2(i,j) &
            +sqrt(e0*u1/u0)*zk0*zk1*a3(i,j) &
            +dd*wones(i,j)
      end do
    end do

    !
    !    ...and (2,3) block...the contribution of rho0
    !    to S0 Div -n x n x (H_1 - H_0)
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_1(1,1,m),work2)
    call zmatmat(n,2*n,work2,2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,2*n,mfiemats_0(1,1,m),work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(n+i,2*n+j,m)=ima*zk0*sqrt(e0)*a1(i,j) &
            +ima*zk0*sqrt(e0)*a2(i,j)
      end do
    end do

    !
    !    ...and (2,4) block...the contribution of rhom0
    !    to S0 Div n x n x (H_1 - H_0)
    !
    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,&
        tvpmats_1(1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,ws0divall(1,1,m),2*n,tvpmats_0(1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    call zmatmat(n, 2*n, ws0divall(1,1,m), &
        n, wgradskall_0(1,1,m), a3)

    do i=1,n
      do j=1,n
        amats(n+i,3*n+j,m)=sqrt(u0*e1/u1)*zk0*zk1*a1(i,j) &
            +zk0**2*sqrt(e0)*a2(i,j) &
            +sqrt(e0)*a3(i,j) &
            +dd*wones(i,j)
      end do
    end do

    !
    !    ...and (3,1) block...the contribution of rho1
    !    to n dot (-e1 E1+e0 E0)
    !
    call zmatmat(n,3*n,wndot,2*n,vecpotmats_1(1,1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,3*n,wndot,2*n,vecpotmats_0(1,1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a3)

    do i=1,n
      do j=1,n
        amats(2*n+i,j,m)=sqrt(u1)*e1*zk1**2*a1(i,j) &
            +sqrt(u1)*e1*wsprimes_1(i,j,m) &
            -sqrt(u0*e1*e0)*zk0*zk1*a3(i,j)
      end do
    end do

    !
    !    ...and (3,2) block...the contribution of rhom1
    !    to n dot (-e1 E1+e0 E0)
    !
    call zmatmat(n,2*n,andotcurls_1(1,1,m),n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,andotcurls_0(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(2*n+i,n+j,m)=ima*zk1*sqrt(u1)*e1*a1(i,j) &
            -ima*zk1*sqrt(u1)*e0*a2(i,j)
      end do
    end do

    !
    !    ...and (3,3) block...the contribution of rho0
    !    to n dot (-e1 E1+e0 E0)
    !
    call zmatmat(n,3*n,wndot,2*n,vecpotmats_1(1,1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a1)

    call zmatmat(n,3*n,wndot,2*n,vecpotmats_0(1,1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(2*n+i,2*n+j,m)=-sqrt(e0*e1*u1)*zk0*zk1*a1(i,j) &
            -e0*sqrt(u0)*zk0**2*a2(i,j) &
            -e0*sqrt(u0)*wsprimes_0(i,j,m)
      end do
    end do

    !
    !    ...and (3,4) block...the contribution of rhom0
    !    to n dot (-e1 E1+e0 E0)
    !
    call zmatmat(n,2*n,andotcurls_1(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,andotcurls_0(1,1,m),n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(2*n+i,3*n+j,m)=-ima*zk0*e1*sqrt(u0)*a1(i,j) &
            -ima*zk0*e0*sqrt(u0)*a2(i,j)
      end do
    end do

    !
    !    ...and (4,1) block...the contribution of rho1
    !    to n dot (u1 H1 - u0 H0)
    !
    call zmatmat(n,2*n,andotcurls_1(1,1,m),n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,andotcurls_0(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    do i=1,n
      do  j=1,n
        amats(3*n+i,j,m)=ima*zk1*u1*sqrt(e1)*a1(i,j) &
            -ima*zk1*u0*sqrt(e1)*a2(i,j)
      end do
    end do

    !
    !    ...and (4,2) block...the contribution of rhom1
    !    to n dot (u1 H1 - u0 H0)
    !
    call zmatmat(n,3*n,wndot,2*n,vecpotmats_1(1,1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,3*n,wndot,2*n,vecpotmats_0(1,1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a3)

    do i=1,n
      do j=1,n
        amats(3*n+i,n+j,m)=-u1*sqrt(e1)*zk1**2*a1(i,j) &
            -u1*sqrt(e1)*wsprimes_1(i,j,m) &
            +sqrt(e0*u1*u0)*zk0*zk1*a3(i,j)
      end do
    end do

    !
    !    ...and (4,3) block...the contribution of rho0
    !    to n dot (u1 H1 - u0 H0)
    !
    call zmatmat(n,2*n,andotcurls_1(1,1,m),2*n,wnxt,work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a1)

    call zmatmat(n,2*n,andotcurls_0(1,1,m),n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(3*n+i,2*n+j,m)=-ima*zk0*u1*sqrt(e0)*a1(i,j) &
            -ima*zk0*u0*sqrt(e0)*a2(i,j)
      end do
    end do

    !
    !    ...and finally (4,4) block, the contribution of rhom0
    !    to n dot (u1 H1 - u0 H0)
    !
    call zmatmat(n,3*n,wndot,2*n,vecpotmats_1(1,1,1,m),work)
    call zmatmat(n,2*n,work,2*n,wnxt,work2)
    call zmatmat(n,2*n,work2,n,wgradreps(1,1,m),a1)

    call zmatmat(n,3*n,wndot,2*n,vecpotmats_0(1,1,1,m),work)
    call zmatmat(n,2*n,work,n,wgradreps(1,1,m),a2)

    do i=1,n
      do j=1,n
        amats(3*n+i,3*n+j,m)=sqrt(u0*e1*u1)*zk0*zk1*a1(i,j) &
            +u0*zk0**2*sqrt(e0)*a2(i,j) &
            +u0*sqrt(e0)*wsprimes_0(i,j,m)
      end do
    end do


    deallocate( a1, a2, a3, a4)
    deallocate( work, work2)


  end do
  !$omp end parallel do


  !
  !    now build in extra rows and columns for harmonic
  !    forms if m=0



  allocate( a1(n,n),a2(n,n), a3(n,n), a4(n,n) )
  allocate( work(4*n*n), work2(4*n*n) )

  do m = -maxm,maxm

    !!print *, 'in sequential block, processing m = ', m

    if (m .ne. 0) then
      do i=1,4*n+4
        amats(4*n+1,i,m)=0
        amats(4*n+2,i,m)=0
        amats(4*n+3,i,m)=0
        amats(4*n+4,i,m)=0
        amats(i,4*n+1,m)=0
        amats(i,4*n+2,m)=0
        amats(i,4*n+3,m)=0
        amats(i,4*n+4,m)=0
      end do

      amats(4*n+1,4*n+1,m)=1
      amats(4*n+2,4*n+2,m)=1
      amats(4*n+3,4*n+3,m)=1
      amats(4*n+4,4*n+4,m)=1
    end if

    if (m .eq. 0) then

      !
      !    if here, then m=0, build entries for harmonic form conditions
      !    first form the 'clutching map' matrix
      !
      tclutch = 0
      call creaclutch(n,tclutch,wclutch)
      call get2harmvecs(n,ps,zs,ch1,ch2)

      !c
      !c       calculate contribution to tangential E fields...
      !c
      call zmatvec(2*n,2*n,tvpmats_1(1,1,m),ch1,work)
      call zmatvec(n,2*n,ws0divall(1,1,m),work,cout1)

      call zmatvec(2*n,2*n,tvpmats_1(1,1,m),ch2,work)
      call zmatvec(n,2*n,ws0divall(1,1,m),work,cout2)

      call zmatvec(2*n,2*n,mfiemats_1(1,1,m),ch1,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(n,2*n,ws0divall(1,1,m),work2,cout3)

      call zmatvec(2*n,2*n,mfiemats_1(1,1,m),ch2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(n,2*n,ws0divall(1,1,m),work2,cout4)


      call zmatvec(2*n,2*n,wclutch,ch1,work)
      call zmatvec(2*n,2*n,tvpmats_0(1,1,m),work,work2)
      call zmatvec(n,2*n,ws0divall(1,1,m),work2,cin1)

      call zmatvec(2*n,2*n,wclutch,ch2,work)
      call zmatvec(2*n,2*n,tvpmats_0(1,1,m),work,work2)
      call zmatvec(n,2*n,ws0divall(1,1,m),work2,cin2)

      call zmatvec(2*n,2*n,wclutch,ch1,work2)
      call zmatvec(2*n,2*n,mfiemats_0(1,1,m),work2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(n,2*n,ws0divall(1,1,m),work2,cin3)

      call zmatvec(2*n,2*n,wclutch,ch2,work2)
      call zmatvec(2*n,2*n,mfiemats_0(1,1,m),work2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(n,2*n,ws0divall(1,1,m),work2,cin4)

      do i=1,n
        amats(i,4*n+1,m)=sqrt(u1)*ima*zk1*cout1(i) &
            -sqrt(u0*e1/e0)*ima*zk0*cin1(i)
        amats(i,4*n+2,m)=sqrt(u1)*ima*zk1*cout2(i) &
            -sqrt(u0*e1/e0)*ima*zk0*cin2(i)
        amats(i,4*n+3,m)=sqrt(u1)*cout3(i)-sqrt(u0*u1/u0)*cin3(i)
        amats(i,4*n+4,m)=sqrt(u1)*cout4(i)-sqrt(u0*u1/u0)*cin4(i)
      end do



      !c
      !c       calculate the contribution to tangential H
      !c


      do i=1,n
        amats(n+i,4*n+1,m)=-sqrt(e1)*cout3(i)+sqrt(e0*e1/e0)*cin3(i)
        amats(n+i,4*n+2,m)=-sqrt(e1)*cout4(i)+sqrt(e0*e1/e0)*cin4(i)
        amats(n+i,4*n+3,m)=sqrt(e1)*ima*zk1*cout1(i) &
            -sqrt(e0*u1/u0)*ima*zk0*cin1(i)
        amats(n+i,4*n+4,m)=sqrt(e1)*ima*zk1*cout2(i) &
            -sqrt(e0*u1/u0)*ima*zk0*cin2(i)
      end do




      !c
      !c       calculate the contribution to e n \dot E
      !c
      call zmatvec(3*n,2*n,vecpotmats_1(1,1,1,m),ch1,work)
      call zmatvec(n,3*n,wndot,work,cout1)

      call zmatvec(3*n,2*n,vecpotmats_1(1,1,1,m),ch2,work)
      call zmatvec(n,3*n,wndot,work,cout2)

      call zmatvec(n,2*n,andotcurls_1(1,1,m),ch1,cout3)

      call zmatvec(n,2*n,andotcurls_1(1,1,m),ch2,cout4)
      !c
      call zmatvec(2*n,2*n,wclutch,ch1,work2)
      call zmatvec(3*n,2*n,vecpotmats_0(1,1,1,m),work2,work)
      call zmatvec(n,3*n,wndot,work,cin1)

      call zmatvec(2*n,2*n,wclutch,ch2,work2)
      call zmatvec(3*n,2*n,vecpotmats_0(1,1,1,m),work2,work)
      call zmatvec(n,3*n,wndot,work,cin2)

      call zmatvec(2*n,2*n,wclutch,ch1,work2)
      call zmatvec(n,2*n,andotcurls_0(1,1,m),work2,cin3)

      call zmatvec(2*n,2*n,wclutch,ch2,work2)
      call zmatvec(n,2*n,andotcurls_0(1,1,m),work2,cin4)
      !c
      do i=1,n
        amats(2*n+i,4*n+1,m)=-e1*sqrt(u1)*ima*zk1*cout1(i) &
            +sqrt(e1*e0*u0)*ima*zk0*cin1(i)
        amats(2*n+i,4*n+2,m)=-e1*sqrt(u1)*ima*zk1*cout2(i) &
            +sqrt(e1*e0*u0)*ima*zk0*cin2(i)
        amats(2*n+i,4*n+3,m)=e1*sqrt(u1)*cout3(i)-e0*sqrt(u1)*cin3(i)
        amats(2*n+i,4*n+4,m)=e1*sqrt(u1)*cout4(i)-e0*sqrt(u1)*cin4(i)
      end do


      !c
      !c       and the contribution to u n \cdot H...
      !c
      do i=1,n
        amats(3*n+i,4*n+1,m)=u1*sqrt(e1)*cout3(i)-u0*sqrt(e1)*cin3(i)
        amats(3*n+i,4*n+2,m)=u1*sqrt(e1)*cout4(i)-u0*sqrt(e1)*cin4(i)
        amats(3*n+i,4*n+3,m)=u1*sqrt(e1)*ima*zk1*cout1(i) &
            -sqrt(u0*e0*u1)*ima*zk0*cin1(i)
        amats(3*n+i,4*n+4,m)=u1*sqrt(e1)*ima*zk1*cout2(i) &
            -sqrt(u0*e0*u1)*ima*zk0*cin2(i)
      end do



      !c
      !c       now calculate line integrals around a-cycles and b-cycles
      !c
      do i=1,2*n
        cwhts(i)=0
      end do

      !c
      ! !! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !
      ! new surface weights ! !! ! ! !
      ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !
      do i=1,n
        i1=2*(i-1)+1
        !cwhts(i1)=dsdt(i)*h*ps(i)
        cwhts(i1)=dsdt(i)*h*xys(1,i)*2*pi
      end do



      !cccc        call prin2('cwhts=*',cwhts,2*n*2)

      !c
      !c       contribution of rho1 to e on a-cycle
      !c       contribution of rhom1 to h on a-cycle
      !c



      call zmatmat(1,2*n,cwhts,2*n,tvpmats_1(1,1,m),work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout1)


      call zmatmat(1,2*n,cwhts, n, wgradskall_1(1,1,m), cout2)

      call zmatmat(1,2*n,cwhts,2*n,tvpmats_0(1,1,m),work)
      call zmatmat(1,2*n,work,2*n,wnxt,work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout3)

      do j=1,n
        amats(4*n+1,j,m)=-sqrt(u1)*zk1**2*cout1(j) &
            -sqrt(u1)*cout2(j) &
            +sqrt(u0*e1/e0)*zk0*zk1*cout3(j)
      end do


      do j=1,n
        amats(4*n+3,n+j,m)=-sqrt(e1)*zk1**2*cout1(j) &
            -sqrt(e1)*cout2(j) &
            +sqrt(e0*u1/u0)*zk0*zk1*cout3(j)
      end do



      !c
      !c       contribution of rhom1 to e on a-cycle
      !c       contribution of rho1 to h on a-cycle
      !c
      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_1(1,1,m),work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout1)

      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_0(1,1,m),work2)
      call zmatmat(1,2*n,work2,2*n,wnxt,work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout2)

      do j=1,n
        amats(4*n+1,n+j,m)=ima*zk1*sqrt(u1)*cout1(j) &
            -ima*zk1*sqrt(u1)*cout2(j)
      end do


      do j=1,n
        amats(4*n+3,j,m)=-ima*zk1*sqrt(e1)*cout1(j) &
            +ima*zk1*sqrt(e1)*cout2(j)
      end do



      !c
      !c       contribution of rho0 to e on a-cycle
      !c       contribution of rhom0 to h on a-cycle
      !c
      call zmatmat(1,2*n,cwhts,2*n,tvpmats_1(1,1,m),work)
      call zmatmat(1,2*n,work,2*n,wnxt,work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout1)

      call zmatmat(1,2*n,cwhts,2*n,tvpmats_0(1,1,m),work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout2)


      call zmatmat(1, 2*n, cwhts, n, wgradskall_0(1,1,m), cout3)

      do j=1,n
        amats(4*n+1,2*n+j,m)=sqrt(e0*u1/e1)*zk0*zk1*cout1(j) &
            +sqrt(u0)*zk0**2*cout2(j) &
            +sqrt(u0)*cout3(j)
      end do


      do j=1,n
        amats(4*n+3,3*n+j,m)=sqrt(u0*e1/u1)*zk0*zk1*cout1(j) &
            +zk0**2*sqrt(e0)*cout2(j) &
            +sqrt(e0)*cout3(j)
      end do



      !c
      !c       contribution of rhom0 to e on a-cycle
      !c       contribution of rho0 to h on a-cycle
      !c
      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_1(1,1,m),work2)
      call zmatmat(1,2*n,work2,2*n,wnxt,work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout1)

      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_0(1,1,m),work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout2)

      do j=1,n
        amats(4*n+1,3*n+j,m)=-ima*zk0*sqrt(u0)*cout1(j) &
            -ima*zk0*sqrt(u0)*cout2(j)
      end do


      do j=1,n
        amats(4*n+3,2*n+j,m)=ima*zk0*sqrt(e0)*cout1(j) &
            +ima*zk0*sqrt(e0)*cout2(j)
      end do



      !c
      !c       contribution of aj, bj, am, bm to e on a-cycle
      !c
      call zmatvec(2*n,2*n,tvpmats_1(1,1,m),ch1,work)
      call zmatvec(1,2*n,cwhts,work,cd1)

      call zmatvec(2*n,2*n,tvpmats_1(1,1,m),ch2,work)
      call zmatvec(1,2*n,cwhts,work,cd2)

      call zmatvec(2*n,2*n,mfiemats_1(1,1,m),ch1,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd3)

      call zmatvec(2*n,2*n,mfiemats_1(1,1,m),ch2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd4)


      call zmatvec(2*n,2*n,wclutch,ch1,work)
      call zmatvec(2*n,2*n,tvpmats_0(1,1,m),work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd5)

      call zmatvec(2*n,2*n,wclutch,ch2,work)
      call zmatvec(2*n,2*n,tvpmats_0(1,1,m),work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd6)

      call zmatvec(2*n,2*n,wclutch,ch1,work2)
      call zmatvec(2*n,2*n,mfiemats_0(1,1,m),work2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd7)

      call zmatvec(2*n,2*n,wclutch,ch2,work2)
      call zmatvec(2*n,2*n,mfiemats_0(1,1,m),work2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd8)
      !c
      amats(4*n+1,4*n+1,m)=sqrt(u1)*ima*zk1*cd1 &
          -sqrt(u0*e1/e0)*ima*zk0*cd5
      amats(4*n+1,4*n+2,m)=sqrt(u1)*ima*zk1*cd2 &
          -sqrt(u0*e1/e0)*ima*zk0*cd6
      amats(4*n+1,4*n+3,m)=sqrt(u1)*cd3-sqrt(u0*u1/u0)*cd7
      amats(4*n+1,4*n+4,m)=sqrt(u1)*cd4-sqrt(u0*u1/u0)*cd8
      !c
      amats(4*n+3,4*n+1,m)=-sqrt(e1)*cd3+sqrt(e0*e1/e0)*cd7
      amats(4*n+3,4*n+2,m)=-sqrt(e1)*cd4+sqrt(e0*e1/e0)*cd8
      amats(4*n+3,4*n+3,m)=sqrt(e1)*ima*zk1*cd1 &
          -sqrt(e0*u1/u0)*ima*zk0*cd5
      amats(4*n+3,4*n+4,m)=sqrt(e1)*ima*zk1*cd2 &
          -sqrt(e0*u1/u0)*ima*zk0*cd6



      !c
      !c       now calculate some integrals on b-cycles, which are equivalent
      !c       to just a point on the generating curve
      !c
      do  i=1,2*n
        cwhts(i)=0
      end do


      i1=2*ind
      cwhts(i1)=2*pi*ps(ind)

      !cccc        call prinf('ind=*',ind,1)
      !cccc        call prin2('cwhts=*',cwhts,2*n*2)


      !c
      !c       contribution of rho1 to e on b-cycle
      !c       contribution of rhom1 to h on b-cycle
      !c
      call zmatmat(1,2*n,cwhts,2*n,tvpmats_1(1,1,m),work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout1)

      call zmatmat(1, 2*n, cwhts, n, wgradskall_1(1,1,m), cout2)

      call zmatmat(1,2*n,cwhts,2*n,tvpmats_0(1,1,m),work)
      call zmatmat(1,2*n,work,2*n,wnxt,work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout3)

      do  j=1,n
        amats(4*n+2,j,m)=-sqrt(u1)*zk1**2*cout1(j) &
            -sqrt(u1)*cout2(j) &
            +sqrt(u0*e1/e0)*zk0*zk1*cout3(j)
      end do


      do j=1,n
        amats(4*n+4,n+j,m)=-sqrt(e1)*zk1**2*cout1(j) &
            -sqrt(e1)*cout2(j) &
            +sqrt(e0*u1/u0)*zk0*zk1*cout3(j)
      end do


      !c
      !c       contribution of rhom1 to e on a-cycle
      !c       contribution of rho1 to h on a-cycle
      !c
      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_1(1,1,m),work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout1)

      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_0(1,1,m),work2)
      call zmatmat(1,2*n,work2,2*n,wnxt,work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout2)

      do j=1,n
        amats(4*n+2,n+j,m)=ima*zk1*sqrt(u1)*cout1(j) &
            -ima*zk1*sqrt(u1)*cout2(j)
      end do


      do j=1,n
        amats(4*n+4,j,m)=-ima*zk1*sqrt(e1)*cout1(j) &
            +ima*zk1*sqrt(e1)*cout2(j)
      end do



      !c
      !c       contribution of rho0 to e on a-cycle
      !c       contribution of rhom0 to h on a-cycle
      !c
      call zmatmat(1,2*n,cwhts,2*n,tvpmats_1(1,1,m),work)
      call zmatmat(1,2*n,work,2*n,wnxt,work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout1)

      call zmatmat(1,2*n,cwhts,2*n,tvpmats_0(1,1,m),work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout2)

      call zmatmat(1, 2*n, cwhts, n, wgradskall_0(1,1,m), cout3)

      do  j=1,n
        amats(4*n+2,2*n+j,m)=sqrt(e0*u1/e1)*zk0*zk1*cout1(j) &
            +sqrt(u0)*zk0**2*cout2(j) &
            +sqrt(u0)*cout3(j)
      end do


      do  j=1,n
        amats(4*n+4,3*n+j,m)=sqrt(u0*e1/u1)*zk0*zk1*cout1(j) &
            +zk0**2*sqrt(e0)*cout2(j) &
            +sqrt(e0)*cout3(j)
      end do



      !c
      !c       contribution of rhom0 to e on a-cycle
      !c       contribution of rho0 to h on a-cycle
      !c
      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_1(1,1,m),work2)
      call zmatmat(1,2*n,work2,2*n,wnxt,work)
      call zmatmat(1,2*n,work,n,wgradreps(1,1,m),cout1)

      call zmatmat(1,2*n,cwhts,2*n,wnxt,work)
      call zmatmat(1,2*n,work,2*n,mfiemats_0(1,1,m),work2)
      call zmatmat(1,2*n,work2,n,wgradreps(1,1,m),cout2)

      do j=1,n
        amats(4*n+2,3*n+j,m)=-ima*zk0*sqrt(u0)*cout1(j) &
            -ima*zk0*sqrt(u0)*cout2(j)
      end do


      do j=1,n
        amats(4*n+4,2*n+j,m)=ima*zk0*sqrt(e0)*cout1(j) &
            +ima*zk0*sqrt(e0)*cout2(j)
      end do



      !c
      !c       contribution of aj, bj, am, bm to e on a-cycle
      !c
      call zmatvec(2*n,2*n,tvpmats_1(1,1,m),ch1,work)
      call zmatvec(1,2*n,cwhts,work,cd1)

      call zmatvec(2*n,2*n,tvpmats_1(1,1,m),ch2,work)
      call zmatvec(1,2*n,cwhts,work,cd2)

      call zmatvec(2*n,2*n,mfiemats_1(1,1,m),ch1,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd3)

      call zmatvec(2*n,2*n,mfiemats_1(1,1,m),ch2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd4)


      call zmatvec(2*n,2*n,wclutch,ch1,work)
      call zmatvec(2*n,2*n,tvpmats_0(1,1,m),work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd5)

      call zmatvec(2*n,2*n,wclutch,ch2,work)
      call zmatvec(2*n,2*n,tvpmats_0(1,1,m),work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd6)

      call zmatvec(2*n,2*n,wclutch,ch1,work2)
      call zmatvec(2*n,2*n,mfiemats_0(1,1,m),work2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd7)

      call zmatvec(2*n,2*n,wclutch,ch2,work2)
      call zmatvec(2*n,2*n,mfiemats_0(1,1,m),work2,work)
      call zmatvec(2*n,2*n,wnxt,work,work2)
      call zmatvec(1,2*n,cwhts,work2,cd8)

      amats(4*n+2,4*n+1,m)=sqrt(u1)*ima*zk1*cd1 &
          -sqrt(u0*e1/e0)*ima*zk0*cd5
      amats(4*n+2,4*n+2,m)=sqrt(u1)*ima*zk1*cd2 &
          -sqrt(u0*e1/e0)*ima*zk0*cd6
      amats(4*n+2,4*n+3,m)=sqrt(u1)*cd3-sqrt(u0*u1/u0)*cd7
      amats(4*n+2,4*n+4,m)=sqrt(u1)*cd4-sqrt(u0*u1/u0)*cd8

      amats(4*n+4,4*n+1,m)=-sqrt(e1)*cd3+sqrt(e0*e1/e0)*cd7
      amats(4*n+4,4*n+2,m)=-sqrt(e1)*cd4+sqrt(e0*e1/e0)*cd8
      amats(4*n+4,4*n+3,m)=sqrt(e1)*ima*zk1*cd1 &
          -sqrt(e0*u1/u0)*ima*zk0*cd5
      amats(4*n+4,4*n+4,m)=sqrt(e1)*ima*zk1*cd2 &
          -sqrt(e0*u1/u0)*ima*zk0*cd6



    endif

  end do


  return
end subroutine axidebye_dielectric_mats





subroutine creaclutch(n,t,amat)
  implicit real *8 (a-h,o-z)
  complex *16 amat(2*n,2*n)
  !c
  !c       this subroutine constructs a 2n by 2n matrix which
  !c       applies the clutching map, i.e. the operator
  !c
  !c         U(v) = cos(t)*v + sin(t)* n \times v
  !c
  !c       this operator is nothing more than a rotation matrix in 2D,
  !c       with the angle of rotation being determined by t...(almost)
  !c
  do i=1,n
    i1=2*(i-1)+1
    i2=i1+1

    do j=1,n
      j1=2*(j-1)+1
      j2=j1+1

      amat(i1,j1)=0
      amat(i2,j1)=0
      amat(i1,j2)=0
      amat(i2,j2)=0

      if (i .eq. j) then
        amat(i1,j1)=cos(t)
        amat(i2,j1)=-sin(t)
        amat(i1,j2)=sin(t)
        amat(i2,j2)=cos(t)
      end if

    end do
  end do

  return
end subroutine creaclutch
