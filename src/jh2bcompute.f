        subroutine jh2bcompute(eps,zk,n,rl,ps,zs,dpdt,dzdt,norder,
     1      ind,cdb)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000)
        complex *16 zk,ima,cdb,c1,c2,c3,zk0,cdb0,cdb5,sigma(10000),
     1      cd
        external ghfun2cc,jh2fun1,jh2fun3
c
c       this subroutine carefully computes the integral of the 
c       e field due to j_h2 along a b cycle.  this is a special
c       purpose routine, and has no standalone purpose.
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
        m=0
c
        do 1400 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 1400   continue

c
c       evaluate each of the three terms carefully, first the one
c       from i*zk*S_k jh2
c
        do 1600 i=1,n
        sigma(i)=1/ps(i)
 1600   continue
c
        h=rl/n
        call alpertslp1(ier,norder,ps,zs,dsdt,
     1    h,n,ghfun2cc,eps,zk,m,sigma,ind,c2)

c
c       ...and the second term...
c
        do 2000 i=1,n
        sigma(i)=dpdt(i)/dsdt(i)/ps(i)
 2000   continue
c
        call alpertslp1(ier,norder,ps,zs,dsdt,
     1    h,n,jh2fun1,eps,zk,m,sigma,ind,c1)

c
c       ...and now the third
c
        do 2400 i=1,n
        sigma(i)=dzdt(i)/dsdt(i)/ps(i)
 2400   continue
c

        call alpertslp1(ier,norder,ps,zs,dsdt,
     1    h,n,jh2fun3,eps,zk,m,sigma,ind,c3)
c
        cdb=-ima*2*pi*ps(ind)*(c2-c1+c3)
c
        return
        end
c
c
c
c
c
        subroutine jh2fun1(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        real *8 p1(10),p2(10)
        complex *16 zk,val,ima,funj1,rint
        external funj1
c
c       this is a special purpose routine for evaluating part of
c       the line integral of e around a b cycle, it has no known
c       standalone purpose in life.
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=real(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
cccc        b4=pi/2
        b4=2*pi
        k4=16
c
        call cadapgau(ier,a4,b4,funj1,p1,p2,k4,eps,
     1      rint,maxrec,numint)
        val=rint*p0/4/pi
c
        return
        end
c
c
c
        function funj1(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funj1,ima,zz,z3,zk,zikr
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p**2+p0**2-2*p*p0*cos(t)+(z-z0)**2)
        d=sqrt(d)
        zikr=ima*zk*d
        sc=abs(zikr)
c
        thresh=1.0d-3
        if (sc .ge. thresh) z3=(1-exp(zikr))/zikr
        if (sc .lt. thresh) then
          z3=1+zikr/2+zikr**2/3/2+zikr**3/4/3/2+zikr**4/5/4/3/2
          z3=z3+zikr**5/6/5/4/3/2+zikr**6/7/6/5/4/3/2
          z3=z3+zikr**7/8/7/6/5/4/3/2
          z3=-z3
        endif
c
        zz=(z-z0)*cos(t)*(exp(zikr)+z3)/d/d
        funj1=zz
c
        return
        end
c
c
c
c
c
        subroutine jh2fun3(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        real *8 p1(10),p2(10)
        complex *16 zk,val,ima,funj3,rint
        external funj3
c
c       this is a special purpose routine for evaluating part of
c       the line integral of e around a b cycle, it has no known
c       standalone purpose in life.
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=real(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
cccc        b4=pi/2
        b4=2*pi
        k4=16
c
        call cadapgau(ier,a4,b4,funj3,p1,p2,k4,eps,
     1      rint,maxrec,numint)
        val=rint*p0/4/pi
c
        return
        end
c
c
c
        function funj3(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funj3,ima,zz,z3,zk,zikr
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p**2+p0**2-2*p*p0*cos(t)+(z-z0)**2)
        d=sqrt(d)
        zikr=ima*zk*d
        sc=abs(zikr)
c
        thresh=1.0d-3
        if (sc .ge. thresh) z3=(1-exp(zikr))/zikr
        if (sc .lt. thresh) then
          z3=1+zikr/2+zikr**2/3/2+zikr**3/4/3/2+zikr**4/5/4/3/2
          z3=z3+zikr**5/6/5/4/3/2+zikr**6/7/6/5/4/3/2
          z3=z3+zikr**7/8/7/6/5/4/3/2
          z3=-z3
        endif
c
        zz=(p-p0*cos(t))*(exp(zikr)+z3)/d/d
        funj3=zz
c
        return
        end
