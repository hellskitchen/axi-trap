


subroutine zalpertmodes_dlp(ier, norder, ns, xys, dxys, &
    h, gfuns, par0, par1, maxm, diag, ifscale, amat)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,ns), dxys(2,ns)
  complex *16 :: par0(*), par1(*), amat(ns,ns,-maxm:maxm)
  complex *16 :: diag

  integer :: its(100),its2(100)
  real *8 :: src(10), targ(10), grad(10), grad0(10)
  real *8 :: tpts(100), xpts(100), ypts(100), spts(100)
  real *8 :: rnxpts(100), rnypts(100), txtra(100), coefs(100)
  real *8 :: extranodes(30), extraweights(30)

  complex *16 :: zval, zgrad(10), zgrad0(10), cd
  complex *16, allocatable :: zvals(:), zgrads(:,:), zgrad0s(:,:)
  
  real *8, allocatable :: dnorms(:,:), dsdt(:), vals(:)
  real *8, allocatable :: grads(:,:), grad0s(:,:)

  ! 
  ! this routine builds the matrix which applies the double
  ! layer potential gfuns to a vector using alpert quadrature
  !
  ! the calling sequence must be
  !
  !    gfuns(par0, src, targ, par1, maxm, vals, grads, grad0s)
  !
  ! input:
  !     diag - the diagonal to add to the matrix
  !     ifscale - if set to ifscale=1, then scale for surface of
  !           revolution quadrature, i.e. aij = aij*2*pi*xys(1,j)
  ! 

  done=1
  pi=4*atan(done)
  
  call getalpert(norder, nskip, nextra, extranodes, &
      extraweights)

  ier = 1
  if (norder .eq. 0) ier = 0
  if (norder .eq. 2) ier = 0
  if (norder .eq. 4) ier = 0
  if (norder .eq. 8) ier = 0
  if (norder .eq. 16) ier = 0
  if (ier .ne. 0) then
    call prinf('wrong quad order, norder=*', norder, 1)
    stop
  end if
        
  !
  ! carry out "punctured" trapezoidal rule and fill in matrix
  ! entries, skipping entries within nskip of the diagonal
  !
  !$omp parallel do default(shared)
  do m = -maxm,maxm
    do j=1,ns
      do i=1,ns
        amat(i,j,m)=0
      end do
    end do
  end do
  !$omp end parallel do
  
  n=ns-2*nskip+1

  !
  ! compute norms and dsdt
  !
  allocate(dnorms(2,ns), dsdt(ns))
  do i = 1,ns
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do

  allocate(zvals(-maxm:maxm))
  allocate(zgrads(2,-maxm:maxm))
  allocate(zgrad0s(2,-maxm:maxm))
  
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  

  !$omp parallel do default(shared) &
  !$omp     private(iii, zvals, zgrads, zgrad0s, cd, k, m)
  do i = 1,ns
    iii = i-1+nskip
    do k=0,n-1
      iii = iii+1
      if (iii .gt. ns) iii = iii-ns
      call gfuns(par0, xys(1,iii), xys(1,i), par1, maxm, &
          zvals, zgrads, zgrad0s)

      do m = -maxm,maxm
        cd = dnorms(1,iii)*zgrad0s(1,m) + dnorms(2,iii)*zgrad0s(2,m)
        amat(i,iii,m)=cd*dsdt(iii)*h
      end do

    end do
  end do
  !$omp end parallel do
  
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()  
  !!!!call prin2('time for trap part = *', t1-t0, 1)

  if (norder .eq. 0) then

    if (ifscale .eq. 1) then
      do m = -maxm,maxm
        do j = 1,ns
          do i = 1,ns
            amat(i,j,m) = 2*pi*xys(1,j)*amat(i,j,m)
          end do
        end do
      end do
    end if

    do m = -maxm,maxm
      do i = 1,ns
        amat(i,i,m) = amat(i,i,m) + diag
      end do
    end do

    return
  end if

  !
  ! now add in corrections and interpolated stuff for alpert
  ! first determine all the interpolation coefficients
  !
  ninterp=norder+2

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  

  !$omp parallel do default(shared) &
  !$omp     private(txtra, i, n1, n2, nnn, j, its, its2) &
  !$omp     private(tpts, xpts, ypts, spts, rnxpts, rnypts) &
  !$omp     private(ttt, coefs, xxx, yyy, sss, rnxxx, rnyyy) &
  !$omp     private(src, zvals, zgrads, zgrad0s, m, cd, jjj)
  do ipt=1,ns

    do i=1,nextra
      txtra(i)=h*(ipt-1)+h*extranodes(i)
    end do


    do i=1,nextra

      !
      ! find the closest ninterp points to each of the txtra
      !
      
      n1=txtra(i)/h
      if (txtra(i) .lt. 0) n1=n1-1
      n2=n1+1
      nnn=n1-(ninterp-2)/2
      
      do j=1,ninterp
        its(j)=nnn+j-1
        its2(j)=its(j)+1
        if (its2(j) .le. 0) its2(j)=its2(j)+ns
        if (its2(j) .gt. ns) its2(j)=its2(j)-ns
      end do
      
      !
      ! fill interpolation nodes and function values
      !
      do j=1,ninterp
        tpts(j) = its(j)*h
        xpts(j) = xys(1,its2(j))
        ypts(j) = xys(2,its2(j))
        spts(j)=dsdt(its2(j))
        rnxpts(j)=dnorms(1,its2(j))
        rnypts(j)=dnorms(2,its2(j))
      end do
      
      !
      ! now compute the values of xs, ys, dsdt at ttt using barycentric
      ! interpolation
      !
      ttt=txtra(i)
      call bary1_coefs_new(ninterp,tpts,ttt,coefs)

      xxx=0
      yyy=0
      sss=0
      rnxxx=0
      rnyyy=0

      do j=1,ninterp
        xxx = xxx+xpts(j)*coefs(j)
        yyy = yyy+ypts(j)*coefs(j)
        sss = sss+spts(j)*coefs(j)
        rnxxx = rnxxx+rnxpts(j)*coefs(j)
        rnyyy = rnyyy+rnypts(j)*coefs(j)
      end do

      !
      ! evaluate the kernel at the new quadrature point xxx,yyy and
      ! add its contribution to the matrix at its interpolation points
      !
      src(1) = xxx
      src(2) = yyy
      call gfuns(par0, src, xys(1,ipt), par1, maxm, &
          zvals, zgrads, zgrad0s)

      do m = -maxm,maxm
        cd = rnxxx*zgrad0s(1,m) + rnyyy*zgrad0s(2,m)

        do j = 1,ninterp
          jjj = its2(j)
          amat(ipt,jjj,m) = amat(ipt,jjj,m) &
              + cd*sss*h*extraweights(i)*coefs(j)
        end do
      end do

    end do
  end do
  !$omp end parallel do

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()  
  !!!!call prin2('time for bary corrections = *', t1-t0, 1)
  
  
  !
  ! and scale if necessary
  !
  
  if (ifscale .eq. 1) then
    !$omp parallel do default(shared) &
    !$omp     private(m, j, i)
    do m = -maxm,maxm
      do j = 1,ns
        do i = 1,ns
          amat(i,j,m) = 2*pi*xys(1,j)*amat(i,j,m)
        end do
      end do
    end do
    !$omp end parallel do
  end if

  !$omp parallel do default(shared)
  do m = -maxm,maxm
    do i = 1,ns
      amat(i,i,m) = amat(i,i,m) + diag
    end do
  end do
  !$omp end parallel do
  
  return
end subroutine zalpertmodes_dlp



  




subroutine zalpertmodes_sprime(ier, norder, ns, xys, dxys, &
    h, gfuns, par0, par1, maxm, diag, ifscale, amat)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,ns), dxys(2,ns)
  complex *16 :: par0(*), par1(*), amat(ns,ns,-maxm:maxm)
  complex *16 :: diag

  integer :: its(100),its2(100)
  real *8 :: src(10), targ(10), grad(10), grad0(10)
  real *8 :: tpts(100), xpts(100), ypts(100), spts(100)
  real *8 :: rnxpts(100), rnypts(100), txtra(100), coefs(100)
  real *8 :: extranodes(30), extraweights(30)

  complex *16 :: zval, zgrad(10), zgrad0(10), cd
  complex *16, allocatable :: zvals(:), zgrads(:,:), zgrad0s(:,:)
  
  real *8, allocatable :: dnorms(:,:), dsdt(:), vals(:)
  real *8, allocatable :: grads(:,:), grad0s(:,:)

  ! 
  ! this routine builds the matrix which applies the S'
  ! layer potential gfuns to a vector using alpert quadrature
  !
  ! the calling sequence must be
  !
  !    gfuns(par0, src, targ, par1, maxm, vals, grads, grad0s)
  !
  ! input:
  !     diag - the diagonal to add to the matrix
  !     ifscale - if set to ifscale=1, then scale for surface of
  !           revolution quadrature, i.e. aij = aij*2*pi*xys(1,j)
  ! 

  done=1
  pi=4*atan(done)
  
  call getalpert(norder, nskip, nextra, extranodes, &
      extraweights)

  ier = 1
  if (norder .eq. 0) ier = 0
  if (norder .eq. 2) ier = 0
  if (norder .eq. 4) ier = 0
  if (norder .eq. 8) ier = 0
  if (norder .eq. 16) ier = 0
  if (ier .ne. 0) then
    call prinf('wrong quad order, norder=*', norder, 1)
    stop
  end if
        
  !
  ! carry out "punctured" trapezoidal rule and fill in matrix
  ! entries, skipping entries within nskip of the diagonal
  !
  do m = -maxm,maxm
    do j=1,ns
      do i=1,ns
        amat(i,j,m)=0
      end do
    end do
  end do
  
  n=ns-2*nskip+1

  !
  ! compute norms and dsdt
  !
  allocate(dnorms(2,ns), dsdt(ns))
  do i = 1,ns
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do

  allocate(zvals(-maxm:maxm))
  allocate(zgrads(2,-maxm:maxm))
  allocate(zgrad0s(2,-maxm:maxm))
  
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  

  !$omp parallel do default(shared) &
  !$omp     private(iii, zvals, zgrads, zgrad0s, cd, k, m)
  do i = 1,ns
    iii = i-1+nskip
    do k=0,n-1
      iii = iii+1
      if (iii .gt. ns) iii = iii-ns
      call gfuns(par0, xys(1,iii), xys(1,i), par1, maxm, &
          zvals, zgrads, zgrad0s)

      do m = -maxm,maxm
        !!!!cd = dnorms(1,iii)*zgrad0s(1,m) + dnorms(2,iii)*zgrad0s(2,m)
        cd = dnorms(1,i)*zgrads(1,m) + dnorms(2,i)*zgrads(2,m)
        amat(i,iii,m)=cd*dsdt(iii)*h
      end do

    end do
  end do
  !$omp end parallel do
  
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()  
  !!!!call prin2('time for trap part = *', t1-t0, 1)

  if (norder .eq. 0) then

    if (ifscale .eq. 1) then
      do m = -maxm,maxm
        do j = 1,ns
          do i = 1,ns
            amat(i,j,m) = 2*pi*xys(1,j)*amat(i,j,m)
          end do
        end do
      end do
    end if

    do m = -maxm,maxm
      do i = 1,ns
        amat(i,i,m) = amat(i,i,m) + diag
      end do
    end do

    return
  end if

  !
  ! now add in corrections and interpolated stuff for alpert
  ! first determine all the interpolation coefficients
  !
  ninterp=norder+2

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  

  !$omp parallel do default(shared) &
  !$omp     private(txtra, i, n1, n2, nnn, j, its, its2) &
  !$omp     private(tpts, xpts, ypts, spts, rnxpts, rnypts) &
  !$omp     private(ttt, coefs, xxx, yyy, sss, rnxxx, rnyyy) &
  !$omp     private(src, zvals, zgrads, zgrad0s, m, cd, jjj)
  do ipt=1,ns

    do i=1,nextra
      txtra(i)=h*(ipt-1)+h*extranodes(i)
    end do


    do i=1,nextra

      !
      ! find the closest ninterp points to each of the txtra
      !
      
      n1=txtra(i)/h
      if (txtra(i) .lt. 0) n1=n1-1
      n2=n1+1
      nnn=n1-(ninterp-2)/2
      
      do j=1,ninterp
        its(j)=nnn+j-1
        its2(j)=its(j)+1
        if (its2(j) .le. 0) its2(j)=its2(j)+ns
        if (its2(j) .gt. ns) its2(j)=its2(j)-ns
      end do
      
      !
      ! fill interpolation nodes and function values
      !
      do j=1,ninterp
        tpts(j) = its(j)*h
        xpts(j) = xys(1,its2(j))
        ypts(j) = xys(2,its2(j))
        spts(j)=dsdt(its2(j))
        !rnxpts(j)=dnorms(1,its2(j))
        !rnypts(j)=dnorms(2,its2(j))
      end do
      
      !
      ! now compute the values of xs, ys, dsdt at ttt using barycentric
      ! interpolation
      !
      ttt=txtra(i)
      call bary1_coefs_new(ninterp,tpts,ttt,coefs)

      xxx=0
      yyy=0
      sss=0
      !rnxxx=0
      !rnyyy=0

      do j=1,ninterp
        xxx = xxx+xpts(j)*coefs(j)
        yyy = yyy+ypts(j)*coefs(j)
        sss = sss+spts(j)*coefs(j)
        !rnxxx = rnxxx+rnxpts(j)*coefs(j)
        !rnyyy = rnyyy+rnypts(j)*coefs(j)
      end do

      !
      ! evaluate the kernel at the new quadrature point xxx,yyy and
      ! add its contribution to the matrix at its interpolation points
      !
      src(1) = xxx
      src(2) = yyy
      call gfuns(par0, src, xys(1,ipt), par1, maxm, &
          zvals, zgrads, zgrad0s)

      do m = -maxm,maxm
        !!!!cd = rnxxx*zgrad0s(1,m) + rnyyy*zgrad0s(2,m)
        cd = dnorms(1,ipt)*zgrads(1,m) + dnorms(2,ipt)*zgrads(2,m)

        do j = 1,ninterp
          jjj = its2(j)
          amat(ipt,jjj,m) = amat(ipt,jjj,m) &
              + cd*sss*h*extraweights(i)*coefs(j)
        end do
      end do

    end do
  end do
  !$omp end parallel do

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()  
  !!!!call prin2('time for bary corrections = *', t1-t0, 1)
  
  
  !
  ! and scale if necessary
  !
  
  if (ifscale .eq. 1) then
    !$omp parallel do default(shared) &
    !$omp     private(m, j, i)
    do m = -maxm,maxm
      do j = 1,ns
        do i = 1,ns
          amat(i,j,m) = 2*pi*xys(1,j)*amat(i,j,m)
        end do
      end do
    end do
    !$omp end parallel do
  end if
  
  do m = -maxm,maxm
    do i = 1,ns
      amat(i,i,m) = amat(i,i,m) + diag
    end do
  end do
  
  return
end subroutine zalpertmodes_sprime



  




subroutine zalpertmodes_grads(ier, norder, ns, xys, dxys, &
    h, gfuns, par0, par1, maxm, ifscale, amat, amat_grad, &
    amat_grad0)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,ns), dxys(2,ns)
  complex *16 :: par0(*), par1(*), amat(ns,ns,-maxm:maxm)
  complex *16 :: amat_grad(ns,ns,2,-maxm:maxm)
  complex *16 :: amat_grad0(ns,ns,2,-maxm:maxm)

  integer :: its(100),its2(100)
  real *8 :: src(10), targ(10), grad(10), grad0(10)
  real *8 :: tpts(100), xpts(100), ypts(100), spts(100)
  real *8 :: rnxpts(100), rnypts(100), txtra(100), coefs(100)
  real *8 :: extranodes(30), extraweights(30)

  complex *16 :: zval, zgrad(10), zgrad0(10), cd
  complex *16, allocatable :: zvals(:), zgrads(:,:), zgrad0s(:,:)
  
  real *8, allocatable :: dnorms(:,:), dsdt(:), vals(:)
  real *8, allocatable :: grads(:,:), grad0s(:,:)

  ! 
  ! this routine builds the matrix which applies the double
  ! layer potential gfuns to a vector using alpert quadrature
  !
  ! the calling sequence must be
  !
  !    gfuns(par0, src, targ, par1, maxm, vals, grads, grad0s)
  !
  ! input:
  !     ifscale - if set to ifscale=1, then scale for surface of
  !           revolution quadrature, i.e. aij = aij*2*pi*xys(1,j)
  ! 

  done=1
  pi=4*atan(done)
  
  call getalpert(norder, nskip, nextra, extranodes, &
      extraweights)

  ier = 1
  if (norder .eq. 0) ier = 0
  if (norder .eq. 2) ier = 0
  if (norder .eq. 4) ier = 0
  if (norder .eq. 8) ier = 0
  if (norder .eq. 16) ier = 0
  if (ier .ne. 0) then
    call prinf('wrong quad order, norder=*', norder, 1)
    stop
  end if
        
  !
  ! carry out "punctured" trapezoidal rule and fill in matrix
  ! entries, skipping entries within nskip of the diagonal
  !
  !$omp parallel do default(shared)
  do m = -maxm,maxm
    do j=1,ns
      do i=1,ns
        amat(i,j,m)=0
        amat_grad(i,j,1,m)=0
        amat_grad0(i,j,1,m)=0
        amat_grad(i,j,2,m)=0
        amat_grad0(i,j,2,m)=0
      end do
    end do
  end do
  !$omp end parallel do
  
  n=ns-2*nskip+1

  !
  ! compute norms and dsdt
  !
  allocate(dnorms(2,ns), dsdt(ns))
  do i = 1,ns
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do

  allocate(zvals(-maxm:maxm))
  allocate(zgrads(2,-maxm:maxm))
  allocate(zgrad0s(2,-maxm:maxm))
  
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  

  !$omp parallel do default(shared) &
  !$omp     private(iii, zvals, zgrads, zgrad0s, cd, k, m)
  do i = 1,ns
    iii = i-1+nskip
    do k=0,n-1
      iii = iii+1
      if (iii .gt. ns) iii = iii-ns
      call gfuns(par0, xys(1,iii), xys(1,i), par1, maxm, &
          zvals, zgrads, zgrad0s)

      do m = -maxm,maxm
        amat(i,iii,m)=zvals(m)*dsdt(iii)*h
        amat_grad(i,iii,1,m)=zgrads(1,m)*dsdt(iii)*h
        amat_grad(i,iii,2,m)=zgrads(2,m)*dsdt(iii)*h
        amat_grad0(i,iii,1,m)=zgrad0s(1,m)*dsdt(iii)*h
        amat_grad0(i,iii,2,m)=zgrad0s(2,m)*dsdt(iii)*h
      end do

    end do
  end do
  !$omp end parallel do
  
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()  
  !!!call prin2('time for trap part = *', t1-t0, 1)

  if (norder .eq. 0) then

    if (ifscale .eq. 1) then
      do m = -maxm,maxm
        do j = 1,ns
          do i = 1,ns
            amat(i,j,m) = 2*pi*xys(1,j)*amat(i,j,m)
            amat_grad(i,j,1,m) = 2*pi*xys(1,j)*amat_grad(i,j,1,m)
            amat_grad(i,j,2,m) = 2*pi*xys(1,j)*amat_grad(i,j,2,m)
            amat_grad0(i,j,1,m) = 2*pi*xys(1,j)*amat_grad0(i,j,1,m)
            amat_grad0(i,j,2,m) = 2*pi*xys(1,j)*amat_grad0(i,j,2,m)
          end do
        end do
      end do
    end if

    return
  end if

  !
  ! now add in corrections and interpolated stuff for alpert
  ! first determine all the interpolation coefficients
  !
  ninterp=norder+2

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  

  !$omp parallel do default(shared) &
  !$omp     private(txtra, i, n1, n2, nnn, j, its, its2) &
  !$omp     private(tpts, xpts, ypts, spts, rnxpts, rnypts) &
  !$omp     private(ttt, coefs, xxx, yyy, sss, rnxxx, rnyyy) &
  !$omp     private(src, zvals, zgrads, zgrad0s, m, cd, jjj)
  do ipt=1,ns

    do i=1,nextra
      txtra(i)=h*(ipt-1)+h*extranodes(i)
    end do


    do i=1,nextra

      !
      ! find the closest ninterp points to each of the txtra
      !
      
      n1=txtra(i)/h
      if (txtra(i) .lt. 0) n1=n1-1
      n2=n1+1
      nnn=n1-(ninterp-2)/2
      
      do j=1,ninterp
        its(j)=nnn+j-1
        its2(j)=its(j)+1
        if (its2(j) .le. 0) its2(j)=its2(j)+ns
        if (its2(j) .gt. ns) its2(j)=its2(j)-ns
      end do
      
      !
      ! fill interpolation nodes and function values
      !
      do j=1,ninterp
        tpts(j) = its(j)*h
        xpts(j) = xys(1,its2(j))
        ypts(j) = xys(2,its2(j))
        spts(j)=dsdt(its2(j))
        rnxpts(j)=dnorms(1,its2(j))
        rnypts(j)=dnorms(2,its2(j))
      end do
      
      !
      ! now compute the values of xs, ys, dsdt at ttt using barycentric
      ! interpolation
      !
      ttt=txtra(i)
      call bary1_coefs_new(ninterp,tpts,ttt,coefs)

      xxx=0
      yyy=0
      sss=0
      rnxxx=0
      rnyyy=0

      do j=1,ninterp
        xxx = xxx+xpts(j)*coefs(j)
        yyy = yyy+ypts(j)*coefs(j)
        sss = sss+spts(j)*coefs(j)
        rnxxx = rnxxx+rnxpts(j)*coefs(j)
        rnyyy = rnyyy+rnypts(j)*coefs(j)
      end do

      !
      ! evaluate the kernel at the new quadrature point xxx,yyy and
      ! add its contribution to the matrix at its interpolation points
      !
      src(1) = xxx
      src(2) = yyy
      call gfuns(par0, src, xys(1,ipt), par1, maxm, &
          zvals, zgrads, zgrad0s)

      do m = -maxm,maxm
        do j = 1,ninterp
          jjj = its2(j)
          amat(ipt,jjj,m) = amat(ipt,jjj,m) &
              + zvals(m)*sss*h*extraweights(i)*coefs(j)
          amat_grad(ipt,jjj,1,m) = amat_grad(ipt,jjj,1,m) &
              + zgrads(1,m)*sss*h*extraweights(i)*coefs(j)
          amat_grad(ipt,jjj,2,m) = amat_grad(ipt,jjj,2,m) &
              + zgrads(2,m)*sss*h*extraweights(i)*coefs(j)
          amat_grad0(ipt,jjj,1,m) = amat_grad0(ipt,jjj,1,m) &
              + zgrad0s(1,m)*sss*h*extraweights(i)*coefs(j)
          amat_grad0(ipt,jjj,2,m) = amat_grad0(ipt,jjj,2,m) &
              + zgrad0s(2,m)*sss*h*extraweights(i)*coefs(j)
        end do
      end do

    end do
  end do
  !$omp end parallel do

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()  
  !!!!call prin2('time for bary corrections = *', t1-t0, 1)
  
  
  !
  ! and scale if necessary
  !
  
  if (ifscale .eq. 1) then
    !$omp parallel do default(shared) &
    !$omp     private(m, j, i)
    do m = -maxm,maxm
      do j = 1,ns
        do i = 1,ns
          amat(i,j,m) = 2*pi*xys(1,j)*amat(i,j,m)
          amat_grad(i,j,1,m) = 2*pi*xys(1,j)*amat_grad(i,j,1,m)
          amat_grad(i,j,2,m) = 2*pi*xys(1,j)*amat_grad(i,j,2,m)
          amat_grad0(i,j,1,m) = 2*pi*xys(1,j)*amat_grad0(i,j,1,m)
          amat_grad0(i,j,2,m) = 2*pi*xys(1,j)*amat_grad0(i,j,2,m)
        end do
      end do
    end do
    !$omp end parallel do
  end if
  
  return
end subroutine zalpertmodes_grads







