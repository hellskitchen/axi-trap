
!
! (c) 2015 Mike O'Neil, oneil@cims.nyu.edu
!          
!
! These codes are for computing the Fourier modes of the 3D Helmholtz
! kernel, e^{ikr}/(4 \pi r) - basically the Fourier transform in
! \theta
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! User callable routines
! ----------------------
!
!     Laplace routines
!     ----------------
!
! g0_quad_adaptive: quad precision adaptive integration, single mode
!
! g0all_quad_adaptive: quad precision adaptive integration, range of
! modes, returns only values
!
!     Helmholtz routines
!     ------------------
!
! gmkquad32: quadruple precision adaptive trapezoidal rule
! integration for the Green's functions
!
! gmkquad32cos: quadruple precision adaptive trapezoidal rule
! integration for the cosine-Green's functions
!
! gmkquad32sin: quadruple precision adaptive trapezoidal rule
! integration for the sine-Green's functions
!
! gmall_fft: computes a range of modal Green's functions and
! gradients via the FFT and convolution
!
! gmallcos: given the output of gmall_fft, computes a range of modal
! Green's functions and gradients for the cosine-kernels via
! modulation
!
! gmallsin: given the output of gmall_fft, computes a range of modal
! Green's functions and gradients for the sine-kernels via
! modulation
!
!     Less useful routines
!     --------------------
!
! zfft_product: special purpose FFT convolution routine
!
! get_gmakn: precompute the size of the FFT required to obtain near
! machine precision for the Green's functions
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine gkmone_cos(zk, src, targ, par1, mode, val, grad, grad0)
  implicit real *8 (a-h,o-z)
  real *8 :: src(2), targ(2)
  complex *16 :: zk, val, grad(2), grad0(2)
  complex *16, allocatable :: vals(:), grads(:,:), grad0s(:,:)

  n = abs(mode) + 5
  allocate( vals(-n:n) )
  allocate( grads(2,-n:n) )
  allocate( grad0s(2,-n:n) )

  !call prin2('src = *', src, 2)
  !call prin2('targ = *', targ,2)

  !!!!stop

  !targ(1) = 0
  !targ(2) = 0
  

  call gkmall(zk, src, targ, par1, n, vals, grads, grad0s)

  !!call prin2('vals = *', vals, 2*(2*n+1))
  !!stop
  
  val = (vals(mode+1) + vals(mode-1))/2
  grad(1) = (grads(1,mode+1) + grads(1,mode-1))/2
  grad(2) = (grads(2,mode+1) + grads(2,mode-1))/2
  grad0(1) = (grad0s(1,mode+1) + grad0s(1,mode-1))/2
  grad0(2) = (grad0s(2,mode+1) + grad0s(2,mode-1))/2
  
  return
end subroutine gkmone_cos





subroutine gkmone(zk, src, targ, par1, mode, val, grad, grad0)
  implicit real *8 (a-h,o-z)
  real *8 :: src(2), targ(2)
  complex *16 :: zk, val, grad(2), grad0(2)
  complex *16, allocatable :: vals(:), grads(:,:), grad0s(:,:)

  n = abs(mode) + 5
  allocate( vals(-n:n) )
  allocate( grads(2,-n:n) )
  allocate( grad0s(2,-n:n) )
  call gkmall(zk, src, targ, par1, n, vals, grads, grad0s)

  val = vals(mode)
  grad(1) = grads(1,mode)
  grad(2) = grads(2,mode)
  grad0(1) = grad0s(1,mode)
  grad0(2) = grad0s(2,mode)
  
  return
end subroutine gkmone





subroutine gkmall(zk, src, targ, par1, maxm, vals, grads, grad0s)
  implicit real *8 (a-h,o-z)
  real *8 :: src(2), targ(2), par1(*)
  complex *16 :: zk, vals(-maxm:maxm), grads(2,-maxm:maxm)
  complex *16 :: grad0s(2,-maxm:maxm)

  real *8, parameter :: fourpi=12.5663706143591729538505735331180d0
  real *8, parameter :: pi=3.14159265358979323846264338327950288d0
  real *8 :: coefs(20)
  
  real *8, allocatable :: wsave(:), dvals(:), dgrads(:,:)
  real *8, allocatable :: dgrad0s(:,:)

  complex *16, parameter :: ima=(0,1)
  complex *16 :: zkappa, cd, cd2, cd3, zz, zzexp, zzminus

  complex *16, allocatable :: zs(:), zdr(:), zdz(:), zdr0(:)
  complex *16, allocatable :: zc(:), z4(:), zspad(:)
  complex *16, allocatable :: zcpad(:), z2pad(:), z3pad(:)
  complex *16, allocatable :: z4pad(:), z5pad(:), z6pad(:)
  complex *16, allocatable :: z7pad(:), cdzpad(:)
  complex *16, allocatable :: zdrpad(:), cdrpad(:), cdr0pad(:)
  complex *16, allocatable :: zdr0pad(:)
  

  
  done = 1
  
  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  dz = z-z0
  rzero = sqrt(r**2 + r0**2 + dz*dz)
  alpha = 2*r*r0/rzero**2
  x = 1/alpha
  !!!!xminus = ((r-r0)**2 + dz*dz)/2/r/r0

  zkappa = zk*rzero
  rkappa = abs(zkappa)

  athresh = 1/1.005d0

  !call prin2('x = *', x, 1)
  !call prin2('athresh = *', athresh, 1)
  !call prin2('alpha = *', alpha, 1)
  !stop
  
  !
  ! if xminus is very small, use the split method
  !
  !!!!if (x .lt. 1.005d0) then
  !!call prin2('alpha = *', alpha, 1)
  !!call prin2('athresh = *', athresh, 1)

  if (alpha .ge. athresh) then
    
    call gkmall_near(zk, src, targ, par1, maxm, vals, &
        grads, grad0s)
    return
    
  endif
    
  !
  ! if xminus is not small, then we can use the fft
  ! determine the number of points required
  !
  at1 = 1/1.1d0
  if (alpha .ge. at1) n = 1024
  if (alpha .lt. at1) n = 512

  if (rkappa .gt. 256) then
    nfac = log(rkappa/256)/log(2.0d0)+1
    n = 512*(2**nfac)
  endif

  if (n .le. maxm) then
    nexp = log(maxm*done)/log(2.0d0)
    nexp = nexp+1
    n = 2**nexp
  endif

  !n = n/2



  
  h = 2*pi/n
  allocate(zs(n), zdr(n), zdz(n), zdr0(n))
  allocate(wsave(5*n))

  !
  ! construct the values
  !
  do i = 1,n
    t = h*(i-1)
    ct = cos(t)
    rdist = rzero*sqrt(done-alpha*ct)
    zz = ima*zk*rdist
    zzminus = zz - done
    zzexp = exp(zz)
    denom = n*fourpi*rdist**3
    zs(i) = zzexp/(fourpi*rdist)/n
    zdr(i) = zzexp*zzminus*(r-r0*ct)/denom
    zdr0(i) = zzexp*zzminus*(r0-r*ct)/denom
    zdz(i) = zzexp*zzminus*(z-z0)/denom
  end do

  !
  ! take the ffts
  !
  call zffti(n, wsave)
  call zfftf(n, zs, wsave)
  call zfftf(n, zdr, wsave)
  call zfftf(n, zdr0, wsave)
  call zfftf(n, zdz, wsave)

  !call prin2('zs = *', zs, 2*n)
  !call prin2('zdr = *', zdr, 2*n)
  !call prin2('zdr0 = *', zdr0, 2*n)
  !call prin2('zdz = *', zdz, 2*n)
  !stop
  
  do i = 0,maxm
    vals(i) = zs(i+1)
    grads(1,i) = zdr(i+1)
    grads(2,i) = zdz(i+1)
    grad0s(1,i) = zdr0(i+1)
    grad0s(2,i) = -zdz(i+1)
  end do
  
  do i = 1,maxm
    vals(-i) = vals(i)
    grads(1,-i) = grads(1,i)
    grads(2,-i) = grads(2,i)
    grad0s(1,-i) = grad0s(1,i)
    grad0s(2,-i) = grad0s(2,i)
  end do
  
  deallocate(zs, zdr, zdz, zdr0, wsave)
  
  return
end subroutine gkmall





subroutine gkmall_near(zk, src, targ, par1, maxm, vals, &
    grads, grad0s)
  implicit real *8 (a-h,o-z)
  real *8 :: src(2), targ(2), par1(*)
  complex *16 :: zk, vals(-maxm:maxm), grads(2,-maxm:maxm)
  complex *16 :: grad0s(2,-maxm:maxm)

  real *8 :: coefs(20)
  
  real *8, allocatable :: wsave(:), dvals(:), dgrads(:,:)
  real *8, allocatable :: dgrad0s(:,:)
  
  complex *16 :: zkappa, ima, cd, cd2, cd3

  complex *16, allocatable :: zs(:), zdr(:), zdz(:), zdr0(:)
  complex *16, allocatable :: zc(:), z4(:), zspad(:)
  complex *16, allocatable :: zcpad(:), z2pad(:), z3pad(:)
  complex *16, allocatable :: z4pad(:), z5pad(:), z6pad(:)
  complex *16, allocatable :: z7pad(:), cdzpad(:)
  complex *16, allocatable :: zdrpad(:), cdrpad(:), cdr0pad(:)
  complex *16, allocatable :: zdr0pad(:)
  

  
  ima = (0,1)
  done = 1
  pi = 4*atan(done)
  
  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  rzero = sqrt(r**2 + r0**2 + (z-z0)**2)
  alpha = 2*r*r0/rzero**2
  x = 1/alpha
  xminus = ((r-r0)**2 + (z-z0)**2)/2/r/r0

  zkappa = zk*rzero
  rkappa = abs(zkappa)

  !!!x = 1.0d0
  !!!!call prin2('in gkmall, x = *', x, 1)


  
  !
  ! xminus is very small, by assumption, use the split method
  !
    nexp = log(rkappa)/log(2.0d0) + 2
    mexp = log(maxm*done)/log(2.0d0) + 2

    !call prin2('rkappa = *', rkappa, 1)
    !call prinf('nexp = *', nexp, 1)
    !call prinf('mexp = *', mexp, 1)

    if (nexp .lt. mexp) nexp = mexp
    n = 2**nexp
    if (n .lt. 32) n = 32

    n = n*2
    !call prinf('maxm = *', maxm, 1)
    !call prinf('n = *', n, 1)
    
    h = 2*pi/n
    allocate(zc(n), zs(n), z4(n))
    allocate(zdr(n), zdr0(n))
    allocate(wsave(5*n))

    !call prin2('zkappa = *', zkappa, 2)

    !
    ! calculate the Fourier modes of the smooth parts of the split
    ! kernels
    !

    coefs(1) = -done/3
    coefs(2) = done/30
    coefs(3) = -done/840
    coefs(4) = done/45360.0d0
    coefs(5) = -done/3991680.0d0
    coefs(6) = done/518918400.0d0
    coefs(7) = -done/93405312000.0d0
    coefs(8) = done/22230464256000.0d0
    coefs(9) = -done/6758061133824000.0d0
    
    do i = 1,n
      t = h*(i-1)
      zc(i) = cos(zkappa*sqrt(1-alpha*cos(t)))/n
      zs(i) = sin(zkappa*sqrt(1-alpha*cos(t))) &
          /(zkappa*sqrt(1-alpha*cos(t)))/n
      rdist = rzero*sqrt(1-alpha*cos(t))

      zdr(i) = (r - r0*cos(t))/n
      zdr0(i) = (r0 - r*cos(t))/n
      
      cd = zk*rdist
      if (abs(cd) .ge. 1) then
        z4(i) = (cd*cos(cd) - sin(cd))/n/cd**3
      else
        cd = cd**2
        z4(i) = coefs(1) + cd*(coefs(2) + cd*(coefs(3) &
            + cd*(coefs(4) + cd*(coefs(5) + cd*(coefs(6) &
            + cd*(coefs(7) + cd*(coefs(8) + cd*(coefs(9)))))))))
        z4(i) = z4(i)/n
      end if
      
    end do
    
    call zffti(n, wsave)
    call zfftf(n, zc, wsave)
    call zfftf(n, zs, wsave)
    call zfftf(n, z4, wsave)
    call zfftf(n, zdr, wsave)
    call zfftf(n, zdr0, wsave)

    !call prin2('zc = *', zc, 2*n)
    !call prin2('zs = *', zs, 2*n)
    !call prin2('z4 = *', z4, 2*n)
    !stop
    
    err1 = (abs(zc(n/2))**2 + abs(zc(n/2-1))**2 &
        + abs(zc(n/2+1))**2)
    err1 = sqrt(err1/3)
    
    err2 = (abs(zs(n/2))**2 + abs(zs(n/2-1))**2 &
        + abs(zs(n/2+1))**2)
    err2 = sqrt(err2/3)
    
    err3 = (abs(z4(n/2))**2 + abs(z4(n/2-1))**2 &
        + abs(z4(n/2+1))**2)
    err3 = sqrt(err3/3)
    
    !call prinf('n = *', n, 1)
    !call prin2('err cosine = *', err1, 1)
    !call prin2('err sine = *', err2, 1)
    !call prin2('err in z4 = *', err3, 1)

    deallocate(wsave)
    
    !
    ! compute the laplace vals using special function stuff
    !
    par0 = 0
    par7 = 0
    mexp = log(maxm*done)/log(2.0d0) + 1    
    mover = n/2 + 10
    allocate(dvals(-mover:mover))
    allocate(dgrads(2,-mover:mover))
    allocate(dgrad0s(2,-mover:mover))
    call g0mall(par0, src, targ, par7, mover, dvals, dgrads, dgrad0s)

    !
    ! now convolve
    !
    len = 2*n*2
    allocate(wsave(5*len))
    allocate(zcpad(len))
    allocate(z2pad(len))
    allocate(z3pad(len))
    allocate(z4pad(len))
    allocate(zspad(len))
    allocate(z5pad(len))
    allocate(cdzpad(len))
    allocate(z6pad(len))
    allocate(cdrpad(len))
    allocate(zdrpad(len))
    allocate(cdr0pad(len))
    allocate(zdr0pad(len))
    allocate(z7pad(len))

    do i = 1,len
      zcpad(i) = 0
      zspad(i) = 0
      z2pad(i) = 0
      cdzpad(i) = 0
      z4pad(i) = 0
      cdrpad(i) = 0
      zdrpad(i) = 0
      cdr0pad(i) = 0
      zdr0pad(i) = 0
    end do

    do i = 1,n/2
      zcpad(i) = zc(i)
      zcpad(len-i+1) = zc(n-i+1)
      z2pad(i) = dvals(i-1)
      z2pad(len-i+1) = dvals(-i)
      zspad(i) = zs(i)
      zspad(len-i+1) = zs(n-i+1)
      cdzpad(i) = dgrads(2,i-1)
      cdzpad(len-i+1) = dgrads(2,-i)
      cdrpad(i) = dgrads(1,i-1)
      cdrpad(len-i+1) = dgrads(1,-i)
      z4pad(i) = z4(i)
      z4pad(len-i+1) = z4(n-i+1)
      zdrpad(i) = zdr(i)
      zdrpad(len-i+1) = zdr(n-i+1)
      cdr0pad(i) = dgrad0s(1,i-1)
      cdr0pad(len-i+1) = dgrad0s(1,-i)
      zdr0pad(i) = zdr0(i)
      zdr0pad(len-i+1) = zdr0(n-i+1)
    end do

    !
    ! fft all for all discrete convolutions
    !
    call zffti(len, wsave)
    call zfftf(len, zcpad, wsave)
    call zfftf(len, z2pad, wsave)
    call zfftf(len, zspad, wsave)
    call zfftf(len, cdzpad, wsave)
    call zfftf(len, z4pad, wsave)
    call zfftf(len, cdrpad, wsave)
    call zfftf(len, zdrpad, wsave)
    call zfftf(len, cdr0pad, wsave)
    call zfftf(len, zdr0pad, wsave)
    
    !
    ! z3pad: cosine part of values
    ! z5pad: convolution part of z derivatives
    ! 
    !
    dz = z-z0
    do i = 1,len
      z3pad(i) = zcpad(i)*z2pad(i)/len
      z5pad(i) = (zcpad(i)*cdzpad(i) &
          - (zk**2)*zspad(i)*z2pad(i)*dz)/len
      z6pad(i) = (zcpad(i)*cdrpad(i) &
          - (zk**2)*zspad(i)*z2pad(i)*zdrpad(i) &
          + ima*(zk**3)*z4pad(i)*zdrpad(i)/4/pi)/len
      
      z7pad(i) = (zcpad(i)*cdr0pad(i) &
          - (zk**2)*zspad(i)*z2pad(i)*zdr0pad(i) &
          + ima*(zk**3)*z4pad(i)*zdr0pad(i)/4/pi)/len
      
    end do

    !
    ! ifft to compute values of convolutions
    !
    call zfftb(len, z3pad, wsave)
    call zfftb(len, z5pad, wsave)
    call zfftb(len, z6pad, wsave)
    call zfftb(len, z7pad, wsave)
    
    do i = 0,maxm
      vals(i) = z3pad(i+1) + zs(i+1)*ima*zk/4/pi
      grads(1,i) = z6pad(i+1)
      grads(2,i) = z5pad(i+1) + ima*(zk**3)*z4(i+1)/4/pi*dz
      grad0s(1,i) = z7pad(i+1)
      grad0s(2,i) = -grads(2,i)
    end do

    do i = 1,maxm
      vals(-i) = vals(i)
      grads(1,-i) = grads(1,i)
      grads(2,-i) = grads(2,i)
      grad0s(1,-i) = grad0s(1,i)
      grad0s(2,-i) = grad0s(2,i)
    end do
    
    deallocate(dvals, dgrads, dgrad0s)
    deallocate(zc, zs)
    deallocate(wsave)
    deallocate(zcpad)
    deallocate(z2pad)
    deallocate(z3pad)
    deallocate(z4pad)
    deallocate(zspad)
    deallocate(z5pad)
    deallocate(cdzpad)
    deallocate(z6pad)
    deallocate(cdrpad)
    deallocate(zdrpad)
    deallocate(cdr0pad)
    deallocate(zdr0pad)
    deallocate(z7pad)
  
  return
end subroutine gkmall_near





subroutine gkmall_quad_adaptive(zk, src, targ, par1, &
    maxm, valmodes, grads, grad0s)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2)
  complex *32 :: valmodes(-maxm:maxm)
  complex *32 :: grads(2,-maxm:maxm), grad0s(2,-maxm:maxm)
  complex *32 :: zk
  
  !
  ! this routine computes a range of Laplace Fourier modes via
  ! adaptive integration in quad precision
  !
  do i = -maxm,maxm
    call gk_quad_adaptive(src, targ, zk, i, valmodes(i))
    call gkdr_quad_adaptive(src, targ, zk, i, grads(1,i))
    call gkdz_quad_adaptive(src, targ, zk, i, grads(2,i))
    call gkdr0_quad_adaptive(src, targ, zk, i, grad0s(1,i))
    grad0s(2,i) = -grads(2,i)
  end do

  return
end subroutine gkmall_quad_adaptive






subroutine gk_quad_adaptive(src, targ, zk, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2)
  complex *32 :: zk, ima, parsq(100)
  external fgreenkq

  !
  ! this routine computes a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !

  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  parsq(5) = zk

  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call cadapgaus_quad(ier, aq, bq, fgreenkq, parsq, mode, k, epsq, &
      val, maxrec, numint)

  return
end subroutine gk_quad_adaptive

subroutine fgreenkq(t, parsq, mode, cval)
  implicit real *16 (a-h,o-z)
  complex *32 :: parsq(*), zk, ima, cval

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)
  zk = parsq(5)

  done = 1
  ima = (0,1)
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)

  cval = exp(ima*zk*rdist)/(4*pi*rdist)*exp(-ima*mode*t)/2/pi
  return
  
end subroutine fgreenkq





subroutine gkdr_quad_adaptive(src, targ, zk, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2)
  complex *32 :: zk, ima, parsq(100)
  external fgreenkq_dr

  !
  ! this routine computes a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !

  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  parsq(5) = zk

  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call cadapgaus_quad(ier, aq, bq, fgreenkq_dr, parsq, mode, &
      k, epsq, val, maxrec, numint)

  return
end subroutine gkdr_quad_adaptive

subroutine fgreenkq_dr(t, parsq, mode, cval)
  implicit real *16 (a-h,o-z)
  complex *32 :: parsq(*), zk, ima, cval

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)
  zk = parsq(5)

  done = 1
  ima = (0,1)
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)
  dr = (r - r0*cos(t))/rdist
  
  cval = exp(ima*zk*rdist)*(ima*zk*rdist - 1)*dr &
      /(4*pi*rdist**2)*exp(-ima*mode*t)/2/pi
  return
  
end subroutine fgreenkq_dr





subroutine gkdz_quad_adaptive(src, targ, zk, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2)
  complex *32 :: zk, ima, parsq(100)
  external fgreenkq_dz

  !
  ! this routine computes a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !

  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  parsq(5) = zk

  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call cadapgaus_quad(ier, aq, bq, fgreenkq_dz, parsq, mode, &
      k, epsq, val, maxrec, numint)

  return
end subroutine gkdz_quad_adaptive

subroutine fgreenkq_dz(t, parsq, mode, cval)
  implicit real *16 (a-h,o-z)
  complex *32 :: parsq(*), zk, ima, cval

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)
  zk = parsq(5)

  done = 1
  ima = (0,1)
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)
  dr = (z-z0)/rdist
  
  cval = exp(ima*zk*rdist)*(ima*zk*rdist - 1)*dr &
      /(4*pi*rdist**2)*exp(-ima*mode*t)/2/pi
  return
  
end subroutine fgreenkq_dz





subroutine gkdr0_quad_adaptive(src, targ, zk, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2)
  complex *32 :: zk, ima, parsq(100)
  external fgreenkq_dr0

  !
  ! this routine computes a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !

  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  parsq(5) = zk

  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call cadapgaus_quad(ier, aq, bq, fgreenkq_dr0, parsq, mode, &
      k, epsq, val, maxrec, numint)

  return
end subroutine gkdr0_quad_adaptive

subroutine fgreenkq_dr0(t, parsq, mode, cval)
  implicit real *16 (a-h,o-z)
  complex *32 :: parsq(*), zk, ima, cval

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)
  zk = parsq(5)

  done = 1
  ima = (0,1)
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)
  dr = (r0 - r*cos(t))/rdist
  
  cval = exp(ima*zk*rdist)*(ima*zk*rdist - 1)*dr &
      /(4*pi*rdist**2)*exp(-ima*mode*t)/2/pi
  return
  
end subroutine fgreenkq_dr0





subroutine glap2d(par0, src, targ, par1, par2, val, grad, grad0)
  implicit real *8 (a-h,o-z)
  real *8 :: src(2), targ(2), grad(2), grad0(2)

  !
  ! just the 2d free-space laplace green's function
  !
  done = 1
  pi = 4*atan(done)
  
  dx = targ(1)-src(1)
  dy = targ(2)-src(2)

  r = sqrt(dx**2 + dy**2)
  val = -log(r)/2/pi

  grad(1) = -dx/r**2/2/pi
  grad(2) = -dy/r**2/2/pi

  grad0(1) = -grad(1)
  grad0(2) = -grad(2)

  return
end subroutine glap2d




  
subroutine g0cart(par0, src, targ, par1, par2, val, grad, grad0)
  implicit real *8 (a-h,o-z)
  real *8 :: src(3), targ(3), grad(3), grad0(3)

  !
  ! the usual cartesian laplace kernel
  !
  x = targ(1)
  y = targ(2)
  z = targ(3)
  x0 = src(1)
  y0 = src(2)
  z0 = src(3)

  r = sqrt((x-x0)**2 + (y-y0)**2 + (z-z0)**2)

  done = 1
  pi = 4*atan(done)
  val = done/r/4/pi

  grad(1) = -(x-x0)*val/r**2
  grad(2) = -(y-y0)*val/r**2
  grad(3) = -(z-z0)*val/r**2

  grad0(1) = -grad(1)
  grad0(2) = -grad(2)
  grad0(3) = -grad(3)
  
  return
end subroutine g0cart





subroutine g0mone(par0, src, targ, par1, mode, val, grad, grad0)
  implicit real *8 (a-h,o-z)
  real *8 :: src(2), targ(2), grad(2), grad0(2)
  real *8, allocatable :: vals(:), grads(:,:), grad0s(:,:)

  n = abs(mode) + 5
  allocate( vals(-n:n) )
  allocate( grads(2,-n:n) )
  allocate( grad0s(2,-n:n) )
  call g0mall(par0, src, targ, par1, n, vals, grads, grad0s)

  val = vals(mode)
  grad(1) = grads(1,mode)
  grad(2) = grads(2,mode)
  grad0(1) = grad0s(1,mode)
  grad0(2) = grad0s(2,mode)
  
  return
end subroutine g0mone





subroutine g0mall(par0, src, targ, par1, maxm, vals, grads, grad0s)
  implicit real *8 (a-h,o-z)
  integer :: maxm
  real *8 :: src(2), targ(2), par0(*), par1(*)
  real *8 :: vals(-maxm:maxm), grads(2,-maxm:maxm)
  real *8 :: grad0s(2,-maxm:maxm)
  real *8 :: ders(0:50000)

  !
  ! run the recurrence for the half-order legendre functions, and
  ! scale them to equal the Laplace Fourier modes. If the points are
  ! *extremely* close together, then the forward (mildly unstable)
  ! recurrent is used. Otherwise, Miller's algorithm is used.
  !
  ! input:
  !   par0, par1 - dummy
  !   src, targ - source and target pairs in r,z coordinates
  !   maxm - the maximum mode to calcualte
  !
  ! output:
  !   vals - the azimuthal Fourier modes, -maxm:maxm
  !
  
  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  rzero = sqrt(r**2 + r0**2 + (z-z0)**2)
  alpha = 2*r*r0/rzero**2
  x = 1/alpha
  xminus = ((r-r0)**2 + (z-z0)**2)/2/r/r0

  dxdr = (r**2 - r0**2 - (z-z0)**2)/2/r0/r**2
  dxdz = 2*(z-z0)/2/r/r0
  dxdr0 = (r0**2 - r**2 - (z-z0)**2)/2/r/r0**2
  dxdz0 = -dxdz
  
  !
  ! if xminus is very small, use the forward recurrence
  !

  !!!!print *, 'inside g0mall, x = ', x
  
  
  iffwd = 0
  if (x .lt. 1.005d0) then
    iffwd = 1
    if ((x .ge. 1.0005d0) .and. (maxm .gt. 163)) iffwd = 0
    if ((x .ge. 1.00005d0) .and. (maxm .gt. 503)) iffwd = 0
    if ((x .ge. 1.000005d0) .and. (maxm .gt. 1438)) iffwd = 0
    if ((x .ge. 1.0000005d0) .and. (maxm .gt. 4380)) iffwd = 0
    if ((x .ge. 1.00000005d0) .and. (maxm .gt. 12307)) iffwd = 0
  end if

  
  if (iffwd .eq. 1) then

    !!xminus = .000005d0
    !!x = 1 + xminus
    
    call axi_q2lege01(x, xminus, q0, q1, dq0, dq1)

    done = 1
    half = done/2
    pi = 4*atan(done)
    
    fac = done/sqrt(r*r0)/4/pi**2
    vals(0) = q0
    vals(1) = q1

    derprev = dq0
    der = dq1

    grads(1,0) = (dq0*dxdr - q0/2/r)
    grads(1,1) = (dq1*dxdr - q1/2/r)
    
    grads(2,0) = dq0*dxdz
    grads(2,1) = dq1*dxdz
    
    grad0s(1,0) = (dq0*dxdr0 - q0/2/r0)
    grad0s(1,1) = (dq1*dxdr0 - q1/2/r0)
    
    grad0s(2,0) = dq0*dxdz0
    grad0s(2,1) = dq1*dxdz0

    do i = 1,maxm-1
      vals(i+1) = (2*i*x*vals(i) - (i-half)*vals(i-1))/(i+half)
      dernext = (2*i*(vals(i)+x*der) - (i-half)*derprev)/(i+half)
      grads(1,i+1) = (dernext*dxdr - vals(i+1)/2/r)
      grads(2,i+1) = dernext*dxdz
      grad0s(1,i+1) = (dernext*dxdr0 - vals(i+1)/2/r0)
      grad0s(2,i+1) = dernext*dxdz0
      derprev = der
      der = dernext
      ! if (abs(vals(i+1)) .gt. abs(vals(i))) then
      !   print *
      !   print *
      !   call prin2('x = *', x, 1)
      !   call prinf('i = *', i, 1)
      !   call prin2('vals(i+1) = *', vals(i+1), 1)
      !   call prin2('vals(i) = *', vals(i), 1)
      !   stop
      ! endif
      
    end do

    do i = 0,maxm
      vals(i) = vals(i)*fac
      grads(1,i) = grads(1,i)*fac
      grads(2,i) = grads(2,i)*fac
      grad0s(1,i) = grad0s(1,i)*fac
      grad0s(2,i) = grad0s(2,i)*fac
      vals(-i) = vals(i)
      grads(1,-i) = grads(1,i)
      grads(2,-i) = grads(2,i)
      grad0s(1,-i) = grad0s(1,i)
      grad0s(2,-i) = grad0s(2,i)
    end do

    return
  end if

  !
  ! if here, xminus > .005, so run forward and backward recurrence
  !

  !
  ! run the recurrence, starting from maxm, until it has exploded
  ! for BOTH the values and derivatives
  !
  done = 1
  half = done/2
  f = 1
  fprev = 0
  der = 1
  derprev = 0
  maxiter = 100000
  upbound = 1.0d19
  
  do i = maxm,maxiter
    fnext = (2*i*x*f - (i-half)*fprev)/(i+half)
    dernext = (2*i*(x*der+f) - (i-half)*derprev)/(i+half)
    if (abs(fnext) .ge. upbound) then
      if (abs(dernext) .ge. upbound) then
        nterms = i+1
        exit
      endif
    endif
    fprev = f
    f = fnext
    derprev = der
    der = dernext
  enddo

  !
  ! now start at nterms and recurse down to maxm
  !

  if (nterms .lt. 10) then
    nterms = 10
  endif

  fnext = 0
  f = 1
  dernext = 0
  der = 1

  do i = nterms,maxm,-1
    fprev = (2*i*x*f - (i+half)*fnext)/(i-half)
    fnext = f
    f = fprev
    derprev = (2*i*(x*der+f) - (i+half)*dernext)/(i-half)
    dernext = der
    der = derprev
  enddo

  vals(maxm-1) = f
  vals(maxm) = fnext

  ders(maxm-1) = der
  ders(maxm) = dernext
  
  do i = maxm-1,1,-1
    vals(i-1) = (2*i*x*vals(i) - (i+half)*vals(i+1))/(i-half)
    ders(i-1) = (2*i*(x*ders(i)+vals(i)) &
        - (i+half)*ders(i+1))/(i-half)
  end do


  !
  ! normalize the values, and use a formula for the derivatives
  !
  call axi_q2lege01(x, xminus, q0, q1, dq0, dq1)

  done = 1
  pi = 4*atan(done)
  ratio = q0/vals(0)

  do i = 0,maxm
    vals(i) = vals(i)*ratio
  enddo

  ders(0) = dq0
  ders(1) = dq1
  do i = 2,maxm
    ders(i) = -(i-.5d0)*(vals(i-1) - x*vals(i))/(1+x)/xminus
  end do

  !
  ! and scale everyone...
  !
  fac = 1/sqrt(r*r0)/4/pi**2
  
  do i = 0,maxm
    grads(1,i) = (ders(i)*dxdr - vals(i)/2/r)*fac
    grads(2,i) = ders(i)*dxdz*fac
    grad0s(1,i) = (ders(i)*dxdr0 - vals(i)/2/r0)*fac
    grad0s(2,i) = ders(i)*dxdz0*fac
    vals(i) = vals(i)*fac
    vals(-i) = vals(i)
    grads(1,-i) = grads(1,i)
    grads(2,-i) = grads(2,i)
    grad0s(1,-i) = grad0s(1,i)
    grad0s(2,-i) = grad0s(2,i)
  end do


  

  
  return
end subroutine g0mall





subroutine axi_q2lege01(x, xminus, val0, val1, der0, der1)
  implicit real *8 (a-h,o-z)
  real *16 :: e1
  !
  ! this subroutine returns the value of the first two
  ! half-order legendre functions of the
  ! second kind, q_-1/2 and q_1/2. this routine requires that
  ! the evaluation point x be larger than 1, i.e. in the toroidal
  ! regime of the associated functions - and the solution is accurate
  ! to full machine precision.
  !
  ! input:
  !
  !   x - evaluation point, must be larger than 1 or else you
  !       will get garbage
  !
  ! output:
  !
  !   val0 - value of Q_{-1/2}(x)
  !   val1 - value of Q_{1/2}(x)
  !

  xarg = 2/(1+x)
  rk = sqrt(xarg)

  y = 2/xminus + 1
  rkp = sqrt(1/y)

  call elliptic_vals(rk, rkp, valk, vale)
  val0 = rk*valk
  val1 = x*rk*valk - sqrt(2*(x+1))*vale

  der0 = (val1 - x*val0)/2/(x+1)/xminus
  der1 = (-val0 + x*val1)/2/(x+1)/xminus
  
  return
end subroutine axi_q2lege01





subroutine g0mall_quad_adaptive(par0, src, targ, par1, &
    maxm, valmodes, grads, grad0s)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2), valmodes(-maxm:maxm)
  real *16 :: grads(2,-maxm:maxm), grad0s(2,-maxm:maxm)

  !
  ! this routine computes a range of Laplace Fourier modes via
  ! adaptive integration in quad precision
  !
  do i = -maxm,maxm
    call g0_quad_adaptive(src, targ, i, valmodes(i))
    call g0dr_quad_adaptive(src, targ, i, grads(1,i))
    call g0dz_quad_adaptive(src, targ, i, grads(2,i))
    call g0dr0_quad_adaptive(src, targ, i, grad0s(1,i))
    grad0s(2,i) = -grads(2,i)
  end do

  return
end subroutine g0mall_quad_adaptive






subroutine g0_quad_adaptive(src, targ, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2), parsq(100)
  external fgreen0q

  !
  ! this routine computes a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !
  
  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call adapgaus_quad(ier, aq, bq, fgreen0q, mode, parsq, k, epsq, &
      val, maxrec, numint)

  return
end subroutine g0_quad_adaptive

function fgreen0q(t, mode, parsq)
  implicit real *16 (a-h,o-z)
  real *16 :: parsq(*)

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)

  done = 1
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)

  fgreen0q = done/pi/rdist/pi*cos(mode*t)/2/4  
  return
  
end function fgreen0q





subroutine g0dr_quad_adaptive(src, targ, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2), parsq(100)
  external fgreen0q_dr

  !
  ! this routine computes the d/dr derivative of
  ! a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !
  
  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call adapgaus_quad(ier, aq, bq, fgreen0q_dr, mode, parsq, k, epsq, &
      val, maxrec, numint)

  return
end subroutine g0dr_quad_adaptive

function fgreen0q_dr(t, mode, parsq)
  implicit real *16 (a-h,o-z)
  real *16 :: parsq(*)

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)

  done = 1
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)

  fgreen0q_dr = -(r-r0*cos(t))/pi/rdist**3/pi*cos(mode*t)/2/4  
  return
  
end function fgreen0q_dr





subroutine g0dr0_quad_adaptive(src, targ, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2), parsq(100)
  external fgreen0q_dr0

  !
  ! this routine computes the d/dr derivative of
  ! a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !
  
  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call adapgaus_quad(ier, aq, bq, fgreen0q_dr0, mode, parsq, k, epsq, &
      val, maxrec, numint)

  return
end subroutine g0dr0_quad_adaptive

function fgreen0q_dr0(t, mode, parsq)
  implicit real *16 (a-h,o-z)
  real *16 :: parsq(*)

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)

  done = 1
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)

  fgreen0q_dr0 = -(r0-r*cos(t))/pi/rdist**3/pi*cos(mode*t)/2/4  
  return
  
end function fgreen0q_dr0





subroutine g0dz_quad_adaptive(src, targ, mode, val)
  implicit real *16 (a-h,o-z)
  real *16 :: src(2), targ(2), parsq(100)
  external fgreen0q_dz

  !
  ! this routine computes the d/dr derivative of
  ! a single Laplace fourier mode via adaptive
  ! integration in quadruple precision
  !
  
  parsq(1) = src(1)
  parsq(2) = src(2)
  parsq(3) = targ(1)
  parsq(4) = targ(2)
  
  doneq = 1
  piq = 4*atan(doneq)
  epsq = 1.0q-20
  aq = -piq
  bq = piq
  k = 16
  
  call adapgaus_quad(ier, aq, bq, fgreen0q_dz, mode, parsq, k, epsq, &
      val, maxrec, numint)

  return
end subroutine g0dz_quad_adaptive

function fgreen0q_dz(t, mode, parsq)
  implicit real *16 (a-h,o-z)
  real *16 :: parsq(*)

  r0 = parsq(1)
  z0 = parsq(2)
  r = parsq(3)
  z = parsq(4)

  done = 1
  pi = 4*atan(done)

  rdist = sqrt(r**2 + r0**2 - 2*r*r0*cos(t) + (z-z0)**2)

  fgreen0q_dz = -(z-z0)/pi/rdist**3/pi*cos(mode*t)/2/4  
  return
  
end function fgreen0q_dz





subroutine gmkquad32(zk, src, targ, par1, mode, val, grad, grad0)
  implicit real *16 (a-h,o-z)
  integer :: mode
  real *16 :: src(2), targ(2), par1(*), src3(10), targ3(10)
  complex *32 :: zk, val, grad(2), val2, ima, grad3(3), hess3(3,3), cd
  complex *32 :: valold, dr, dz, drold, dzold, grad0(2)
  complex *32 :: dr0old, dz0old, dr0, dz0, cq

  !
  ! this is a quadruple precision routine for calculating axisymmetric
  ! kernels using globally adaptive trapezoidal integration
  !
  ! input:
  !   zk - helmholtz parameter
  !   src - source in cylindrical coordinates
  !   targ - target in cylindrical coordinates
  !   par1 - dummy parameter
  !   mode - integer fourier mode
  !
  ! output:
  !   val - the value of fourier mode "mode"
  !   grad - the d/dr and d/dz quantities
  !   grad0 - the d/dr' and d/dz' quantities
  !
  ! NOTE: All the inputs must be accurate in quadruple precision!
  !
  qone = 1
  pi = 4*atan(qone)

  eps = 1.0q-24
  nquad = 1000
  ima = (0,1)

  err = 1000
  valold = 2.3q0
  drold = 2.3q0
  dzold = 2.3q0
  dr0old = 2.3q0
  dz0old = 2.3q0

  do ijk = 1,100

    h = 2*pi/nquad
    val2 = 0
    dr = 0
    dz = 0
    dr0 = 0
    dz0 = 0

    targ3(1) = targ(1)
    targ3(2) = 0
    targ3(3) = targ(2)
  
    do i = 1,nquad
      t = h*(i-1)
      src3(1) = src(1)*cos(t)
      src3(2) = src(1)*sin(t)
      src3(3) = src(2)
      call gk3d_quad(zk, src3, targ3, p1, p2, cd, grad3, hess3)
      val2 = val2 + h*cd*exp(ima*mode*t)/2/pi

      r = sqrt((targ3(1) - src3(1))**2 + (targ3(2) - src3(2))**2 + &
          (targ3(3) - src3(3))**2)
      cq = (ima*zk - 1/r)*cd/r
      grad3(1) = cq*(targ(1) - src(1)*cos(t))
      dr = dr + h*grad3(1)*exp(ima*mode*t)/2/pi
      dz = dz + h*grad3(3)*exp(ima*mode*t)/2/pi

      grad3(1) = cq*(src(1) - targ(1)*cos(t))
      dr0 = dr0 + h*grad3(1)*exp(ima*mode*t)/2/pi
      dz0 = dz0 - h*grad3(3)*exp(ima*mode*t)/2/pi

    end do

    err = abs(val2 - valold)
    errr = abs(dr - drold)
    errz = abs(dz - dzold)
    
    if (err .lt. eps) then
      if (errr .lt. eps) then
        if (errz .lt. eps) then
          val = val2
          grad(1) = dr
          grad(2) = dz
          grad0(1) = dr0
          grad0(2) = dz0
          return
        end if
      end if
    end if

    valold = val2
    drold = dr
    dzold = dz
    dr0old = dr0
    dz0old = dz0
    nquad = nquad*2
  end do

  return
end subroutine gmkquad32





! subroutine gmkquad32cos(zk, src, targ, par1, mode, val, grad, grad0)
!   implicit real *16 (a-h,o-z)
!   integer :: mode
!   real *16 :: src(2), targ(2), par1(*), src3(10), targ3(10)
!   complex *32 :: zk, val, grad(2), grad0(2), val2, ima, grad3(3)
!   complex *32 :: hess3(3,3), cd, dr0, dz0, cq
!   complex *32 :: valold, dr, dz, drold, dzold, dr0old, dz0old

!   !
!   ! this is a quadruple precision routine for calculating axisymmetric
!   ! kernels using globally adaptive trapezoidal integration
!   !
!   ! input:
!   !   zk - helmholtz parameter
!   !   src - source in cylindrical coordinates
!   !   targ - target in cylindrical coordinates
!   !   par1 - dummy parameter
!   !   mode - integer fourier mode
!   !
!   ! output:
!   !   val - the value of cosine fourier mode "mode"
!   !   grad - the d/dr and d/dz quantities
!   !
!   ! NOTE: All the inputs must be accurate in quadruple precision!
!   !
!   qone = 1
!   pi = 4*atan(qone)

!   eps = 1.0q-24
!   nquad = 1000
!   ima = (0,1)

!   err = 1000
!   valold = 2.3q0
!   drold = 2.3q0
!   dzold = 2.3q0
!   dr0old = 2.3q0
!   dz0old = 2.3q0
  
!   do ijk = 1,100

!     h = 2*pi/nquad
!     val2 = 0
!     dr = 0
!     dz = 0
!     dr0 = 0
!     dz0 = 0


!     targ3(1) = targ(1)
!     targ3(2) = 0
!     targ3(3) = targ(2)
  
!     do i = 1,nquad
!       t = h*(i-1)
!       src3(1) = src(1)*cos(t)
!       src3(2) = src(1)*sin(t)
!       src3(3) = src(2)
!       call gk3d_quad(zk, src3, targ3, p1, p2, cd, grad3, hess3)
!       val2 = val2 + h*cd*cos(t)*exp(ima*mode*t)/2/pi


!       r = sqrt((targ3(1) - src3(1))**2 + (targ3(2) - src3(2))**2 + &
!           (targ3(3) - src3(3))**2)
!       cq = (ima*zk - 1/r)*cd/r
!       grad3(1) = cq*(targ(1) - src(1)*cos(t))
!       dr = dr + h*grad3(1)*cos(t)*exp(ima*mode*t)/2/pi
!       dz = dz + h*grad3(3)*cos(t)*exp(ima*mode*t)/2/pi

!       grad3(1) = cq*(src(1) - targ(1)*cos(t))
!       dr0 = dr0 + h*grad3(1)*cos(t)*exp(ima*mode*t)/2/pi
!       dz0 = dz0 - h*grad3(3)*cos(t)*exp(ima*mode*t)/2/pi


!     end do

!     err = abs(val2 - valold)
!     errr = abs(dr - drold)
!     errz = abs(dz - dzold)
!     errr0 = abs(dr0 - dr0old)
!     errz0 = abs(dz0 - dz0old)

!     errmax = err
!     if (errr .gt. errmax) errmax = errr
!     if (errz .gt. errmax) errmax = errz
!     if (errr0 .gt. errmax) errmax = errr0
!     if (errz0 .gt. errmax) errmax = errz0
    
!     if (errmax .lt. eps) then
!       val = val2
!       grad(1) = dr
!       grad(2) = dz
!       grad0(1) = dr0
!       grad0(2) = dz0
!       return
!     end if

!     valold = val2
!     drold = dr
!     dzold = dz
!     dr0old = dr0
!     dz0old = dz0
!     nquad = nquad*2
!   end do

!   return
! end subroutine gmkquad32cos





! subroutine gmkquad32sin(zk, src, targ, par1, mode, val, grad, grad0)
!   implicit real *16 (a-h,o-z)
!   integer :: mode
!   real *16 :: src(2), targ(2), par1(*), src3(10), targ3(10)
!   complex *32 :: zk, val, grad(2), val2, ima, grad3(3), grad0(2)
!   complex *32 :: hess3(3,3), cd, cq, dr0, dz0
!   complex *32 :: valold, dr, dz, drold, dzold, dr0old, dz0old

!   !
!   ! this is a quadruple precision routine for calculating axisymmetric
!   ! kernels using globally adaptive trapezoidal integration
!   !
!   ! input:
!   !   zk - helmholtz parameter
!   !   src - source in cylindrical coordinates
!   !   targ - target in cylindrical coordinates
!   !   par1 - dummy parameter
!   !   mode - integer fourier mode
!   !
!   ! output:
!   !   val - the value of fourier mode "mode"
!   !   grad - the d/dr and d/dz quantities
!   !
!   ! NOTE: All the inputs must be accurate in quadruple precision!
!   !
!   qone = 1
!   pi = 4*atan(qone)

!   eps = 1.0q-24
!   nquad = 1000
!   ima = (0,1)

!   err = 1000
!   valold = 2.3q0
!   drold = 2.3q0
!   dzold = 2.3q0
!   dr0old = 2.3q0
!   dz0old = 2.3q0
  
!   do ijk = 1,100

!     h = 2*pi/nquad
!     val2 = 0
!     dr = 0
!     dz = 0
!     dr0 = 0
!     dz0 = 0

!     targ3(1) = targ(1)
!     targ3(2) = 0
!     targ3(3) = targ(2)
  
!     do i = 1,nquad
!       t = h*(i-1)
!       src3(1) = src(1)*cos(t)
!       src3(2) = src(1)*sin(t)
!       src3(3) = src(2)
!       call gk3d_quad(zk, src3, targ3, p1, p2, cd, grad3, hess3)
!       val2 = val2 + h*cd*sin(t)*exp(ima*mode*t)/2/pi

!       r = sqrt((targ3(1) - src3(1))**2 + (targ3(2) - src3(2))**2 + &
!           (targ3(3) - src3(3))**2)
!       cq = (ima*zk - 1/r)*cd/r
!       grad3(1) = cq*(targ(1) - src(1)*cos(t))
!       dr = dr + h*grad3(1)*sin(t)*exp(ima*mode*t)/2/pi
!       dz = dz + h*grad3(3)*sin(t)*exp(ima*mode*t)/2/pi

!       grad3(1) = cq*(src(1) - targ(1)*cos(t))
!       dr0 = dr0 + h*grad3(1)*sin(t)*exp(ima*mode*t)/2/pi
!       dz0 = dz0 - h*grad3(3)*sin(t)*exp(ima*mode*t)/2/pi


!     end do

!     err = abs(val2 - valold)
!     errr = abs(dr - drold)
!     errz = abs(dz - dzold)
!     errr0 = abs(dr0 - dr0old)
!     errz0 = abs(dz0 - dz0old)

!     errmax = err
!     if (errr .gt. errmax) errmax = errr
!     if (errz .gt. errmax) errmax = errz
!     if (errr0 .gt. errmax) errmax = errr0
!     if (errz0 .gt. errmax) errmax = errz0
    
!     if (errmax .lt. eps) then
!       val = val2
!       grad(1) = dr
!       grad(2) = dz
!       grad0(1) = dr0
!       grad0(2) = dz0
!       return
!     end if

!     valold = val2
!     drold = dr
!     dzold = dz
!     dr0old = dr0
!     dz0old = dz0
!     nquad = nquad*2
!   end do

!   return
! end subroutine gmkquad32sin





subroutine gmallcos(maxm, vals_in, grads_in, grad0s_in, &
    vals_out, grads_out, grad0s_out)
  implicit real *8 (a-h,o-z)
  integer :: maxm
  complex *16 :: vals_in(-maxm:maxm), grads_in(2,-maxm:maxm)
  complex *16 :: vals_out(-maxm:maxm), grads_out(2,-maxm:maxm)
  complex *16 :: grad0s_in(2,-maxm:maxm)
  complex *16 :: grad0s_out(2,-maxm:maxm)

  !
  ! this routine uses the axisymmetric kernel values and gradients to
  ! contruct the Cos kernel values and gradients - just modulated
  ! versions
  !
  ! input:
  !   maxm - the maximum fourier mode of vals
  !   vals_in - fouier modes of the greens function
  !   grads_in - gradients,
  !
  ! NOTE: for the end modes, this routine sets them to zero. You
  ! should have oversampled maxm!!!!
  !
  vals_out(-maxm) = 0
  grads_out(1,-maxm) = 0
  grads_out(2,-maxm) = 0
  grad0s_out(1,-maxm) = 0
  grad0s_out(2,-maxm) = 0

  vals_out(maxm) = 0
  grads_out(1,maxm) = 0
  grads_out(2,maxm) = 0
  grad0s_out(1,maxm) = 0
  grad0s_out(2,maxm) = 0

  do i = -maxm+1,maxm-1
    vals_out(i) = (vals_in(i+1) + vals_in(i-1))/2
    grads_out(1,i) = (grads_in(1,i+1) + grads_in(1,i-1))/2
    grads_out(2,i) = (grads_in(2,i+1) + grads_in(2,i-1))/2
    grad0s_out(1,i) = (grad0s_in(1,i+1) + grad0s_in(1,i-1))/2
    grad0s_out(2,i) = (grad0s_in(2,i+1) + grad0s_in(2,i-1))/2
  end do

  return
end subroutine gmallcos





subroutine gmallsin(maxm, vals_in, grads_in, grad0s_in, &
    vals_out, grads_out, grad0s_out)
  implicit real *8 (a-h,o-z)
  integer :: maxm
  complex *16 :: vals_in(-maxm:maxm), grads_in(2,-maxm:maxm)
  complex *16 :: vals_out(-maxm:maxm), grads_out(2,-maxm:maxm)
  complex *16 :: ti
  complex *16 :: grad0s_in(2,-maxm:maxm)
  complex *16 :: grad0s_out(2,-maxm:maxm)

  !
  ! this routine uses the axisymmetric kernel values and gradients to
  ! contruct the SIN kernel values and gradients - just modulated
  ! versions
  !
  ! NOTE: for the end modes, this routine sets them to ZERO!!!
  !

  ti = 2*(0,1)
  
  vals_out(-maxm) = 0
  grads_out(1,-maxm) = 0
  grads_out(2,-maxm) = 0
  grad0s_out(1,-maxm) = 0
  grad0s_out(2,-maxm) = 0

  vals_out(maxm) = 0
  grads_out(1,maxm) = 0
  grads_out(2,maxm) = 0
  grad0s_out(1,maxm) = 0
  grad0s_out(2,maxm) = 0

  do i = -maxm+1,maxm-1
    vals_out(i) = (vals_in(i+1) - vals_in(i-1))/ti
    grads_out(1,i) = (grads_in(1,i+1) - grads_in(1,i-1))/ti
    grads_out(2,i) = (grads_in(2,i+1) - grads_in(2,i-1))/ti
    grad0s_out(1,i) = (grad0s_in(1,i+1) - grad0s_in(1,i-1))/ti
    grad0s_out(2,i) = (grad0s_in(2,i+1) - grad0s_in(2,i-1))/ti
  end do

  return
end subroutine gmallsin





subroutine gmall_split(zk, src, targ, par1, maxm, vals, &
    grads, grad0s)
  implicit real *8 (a-h,o-z)
  integer :: maxm
  real *8 :: src(2), targ(2), par1(*)
  complex *16 :: zk, vals(*), grads(*), grad0s(*)

  !
  ! based on how far the points are away from each other, call the
  ! near or the far routine. see the subsequent routines for more
  ! details on input and output variables.
  !

  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  rzero = sqrt(r**2 + r0**2 + (z-z0)**2)
  zkappa = zk*rzero
  dkappa = zkappa
  alpha = 2*r*r0/rzero**2

  print *
  call prin2('alpha = *', alpha, 1)
  
  !
  ! based on alpha, call one of two routines
  !
  thresh = .8d0
  if (alpha .gt. thresh) then
    call gmall_near(zk, src, targ, par1, maxm, vals, grads, grad0s)
  else
    call gmall_far(zk, src, targ, par1, maxm, vals, grads, grad0s)
  endif

  return
end subroutine gmall_split





subroutine gmall_far(zk, src, targ, par1, maxm, vals, grads, grad0s)
  implicit real *8 (a-h,o-z)
  integer :: maxm
  real *8 :: src(2), targ(2), par1(*)
  complex *16 :: zk, vals(-maxm:maxm), grads(2,-maxm:maxm)
  complex *16 :: grad0s(2,-maxm:maxm)

  real *8, parameter :: done = 1, pi = 4*atan(done)

  complex *16, parameter :: ima = (0,1)
  complex *16 :: zkappa
  complex *16, allocatable :: fvals(:), w(:), grvals(:)
  complex *16, allocatable :: gr0vals(:), gzvals(:)

  !!USE mkl_service

  !
  ! this routine computes all the axisymmetric greens functions from
  ! mode -maxm to maxm using FFTs, assuing that the source and target
  ! are not near each other (alpha < .8). The subroutine works by
  ! just FFT-ing the values and gradients directly.
  !
  ! input:
  !   zk - the complex wavenumber
  !   src - source location in r,z coordinates
  !   targ - target location in r,z, coordinates
  !   par1 - dummy parameter
  !   maxm - maximum Fourier mode to compute, -maxm through maxm
  !         are returned
  !
  ! output:
  !   vals - the values of the following integrals:
  !
  !      \frac{1}{2\pi} \int_0^{2\pi}
  !        \frac{ e^{ i zk R} }{ 4 \pi R } e^{-i m \theta} d\theta
  !
  !   grads - the TARGET, i.e. r and z, derivatives of vals, computed
  !       via more convolutions
  !   grads - the SOURCE, i.e. r' and z', derivatives of vals, computed
  !       via more convolutions
  !

  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  rzero = sqrt(r**2 + r0**2 + (z-z0)**2)
  zkappa = zk*rzero
  dkappa = zkappa
  alpha = 2*r*r0/rzero**2


  !
  ! we assume that alpha < .8d0, estimate the size of the fft
  !
  call prin2('dkappa = *', dkappa, 1)
  
  n = 128
  if (dkappa .gt. 32d0) n = 256
  if (dkappa .gt. 128d0) n = 512
  if (dkappa .gt. 256d0) n = 1024
  if (dkappa .gt. 512d0) n = 2048
  if (dkappa .gt. 1024d0) then
    nexp = dkappa/2048
    n = 2048*(2**nexp)
  endif

  nhalf = (n-1)/2
  if (nhalf .lt. maxm) then
    call next2(n, nextn)
    n = nextn
  endif

  !
  ! allocate and compute the ffts
  !
  allocate( fvals(n) )
  allocate( grvals(n) )
  allocate( gr0vals(n) )
  allocate( gzvals(n) )
  
  h = 2*pi/n
  do i = 1,n
    t = h*(i-1)
    d = sqrt(1-alpha*cos(t))
    fvals(i) = exp(ima*zkappa*d)/d
    grvals(i) = (ima*zkappa*d - 1)*(r - r0*cos(t))* &
        exp(ima*zkappa*d)/d**3
    gr0vals(i) = (ima*zkappa*d - 1)*(r0 - r*cos(t))* &
        exp(ima*zkappa*d)/d**3
    gzvals(i) = (ima*zkappa*d - 1)*(z - z0)* &
        exp(ima*zkappa*d)/d**3
  enddo
  
  lw = 4*n+1000
  allocate( w(lw) )
  
  call zffti(n, w)

  call prinf('dfti_double = *', DFTI_DOUBLE, 1)
  
  !idimension = 1
  !length = n
  !iforward_domain = 
  !status = DftiCreateDescriptor( desc_handle, precision, &
  !    forward_domain, idimension, length)

  call cpu_time(start)
  
  do i = 1,1000

    call zfftf(n, fvals, w)
    call zfftf(n, grvals, w)
    call zfftf(n, gr0vals, w)
    call zfftf(n, gzvals, w)

  enddo

  call cpu_time(end)

  tim = (end - start)/(5*1000)
  call prin2('time per fft = *', tim, 1)
  stop

  
  scale = 4*pi*rzero*n
  do i = 1,n
    fvals(i) = fvals(i)/scale
    grvals(i) = grvals(i)/scale/rzero**2
    gr0vals(i) = gr0vals(i)/scale/rzero**2
    gzvals(i) = gzvals(i)/scale/rzero**2
  end do

  vals(0) = fvals(1)
  grads(1,0) = grvals(1)
  grad0s(1,0) = gr0vals(1)
  grads(2,0) = gzvals(1)
  grad0s(2,0) = -gzvals(1)
  
  do i = 1,maxm
    vals(i) = fvals(i+1)
    vals(-i) = fvals(n-i+1)
    grads(1,i) = grvals(i+1)
    grads(1,-i) = grvals(n-i+1)
    grad0s(1,i) = gr0vals(i+1)
    grad0s(1,-i) = gr0vals(n-i+1)
    grads(2,i) = gzvals(i+1)
    grads(2,-i) = gzvals(n-i+1)
    grad0s(2,i) = -gzvals(i+1)
    grad0s(2,-i) = -gzvals(n-i+1)
  end do

  return
end subroutine gmall_far





subroutine next2(n, m)
  implicit real *8 (a-h,o-z)
  integer :: n, m, d

  !
  ! calculate the next power of two after n, we assume n is positive
  !
  x = log(n*1.0d0)/log(2.0d0)
  d = x
  m = 2**(d+1)

  return
end subroutine next2





subroutine gmall_near(zk, src, targ, par1, maxm, vals, grads, grad0s)
  implicit real *8 (a-h,o-z)
  integer :: maxm
  real *8 :: src(2), targ(2), par1(*)
  complex *16 :: zk, vals(-maxm:maxm), grads(2,-maxm:maxm)
  complex *16 :: grad0s(2,-maxm:maxm)

  complex *16 :: work(2000000)
  
  real *8, parameter :: done = 1, pi = 4*atan(done)
  real *8 :: dvals(0:10000), dvals2(0:10000)

  complex *16, parameter :: ima = (0,1)
  complex *16 :: zkappa

  complex *16, allocatable :: cosvals(:), sinvals(:)

  !
  ! evaluate the modal green's functions using explicit splitting of
  ! the kernel
  !


  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  rzero = sqrt(r**2 + r0**2 + (z-z0)**2)
  zkappa = zk*rzero
  dkappa = zkappa
  alpha = 2*r*r0/rzero**2
  !!!!call prin2('1 - alpha = *', 1-alpha, 1)

  !
  ! compute the fourier series of cos and sin/r first
  !
  if (dkappa .le. 4.0d0) nnn = 32
  if ((dkappa .gt. 4.0d0) .and. (dkappa .le. 16.0d0)) nnn = 64
  if ((dkappa .gt. 16.0d0) .and. (dkappa .le. 32.0d0)) nnn = 128
  if ((dkappa .gt. 32.0d0) .and. (dkappa .le. 128.0d0)) nnn = 256
  if ((dkappa .gt. 128.0d0) .and. (dkappa .le. 256.0d0)) nnn = 512
  if ((dkappa .gt. 256.0d0) .and. (dkappa .le. 512.0d0)) nnn = 1024
  if ((dkappa .gt. 512.0d0) .and. (dkappa .le. 1024.0d0)) nnn = 2048
  if ((dkappa .gt. 1024.0d0) .and. (dkappa .le. 2048.0d0)) nnn = 4096
  if (dkappa .gt. 2048d0) nnn = dkappa*2

  allocate(cosvals(nnn), sinvals(nnn))

  call prin2('dkappa = *', dkappa, 1)
  call prinf('nnn = *', nnn, 1)

  
  h = 2*pi/nnn
  do i = 1,nnn
    t = (i-1)*h
    d = sqrt(1 - alpha*cos(t))
    cosvals(i) = cos(zkappa*d)/nnn
    sinvals(i) = sin(zkappa*d)/zkappa/d/nnn
  end do

  !call prin2('before, sinvals = *', sinvals, 2*nnn)
  !call prin2('before, cosvals = *', cosvals, 2*nnn)
  
  call zffti(nnn, work)
  call zfftf(nnn, cosvals, work)
  call zfftf(nnn, sinvals, work)

  !call prin2('after, cosvals = *', sinvals, 2*nnn)
  !call prin2('after, sinvals = *', sinvals, 2*nnn)
  
  nnn2 = nnn/2
  prec1 = (abs(cosvals(nnn2-1)) + abs(cosvals(nnn2)) &
      + abs(cosvals(nnn2+1)))/3
  
  prec2 = (abs(sinvals(nnn2-1)) + abs(sinvals(nnn2)) &
      + abs(sinvals(nnn2+1)))/3

  
  
  !call prin2('prec1 = *', prec1, 1)
  !call prin2('prec2 = *', prec2, 1)
  !stop
  

  
  thresh = 1.0d-10
  if ((prec1 .gt. thresh) .or. (prec2 .gt. thresh)) then
    call prin2('zkappa = *', zkappa, 2)
    call prin2('alppha = *', alpha, 1)
    call prin2('cos error, prec1 = *', prec1, 1)
    call prin2('sin error, prec2 = *', prec2, 1)
    call prinf('not enough points, nnn = *', nnn, 1)
    stop
  endif
  

  !
  ! now compute the modes of 1/r analytically
  !

  x = 1/alpha
  lvals = 10000
  eps = 1.0d-16
  
!  if (x .lt. 1.005d0) then
!    call torlaps_near(src, targ, vals, nterms)
!  else
!    call torlaps_eps(ier, eps, src, targ, dvals, lvals, nterms)
!  endif

  call prin2('x = *', x, 1)
  write(*,*) 'x = ', x
  stop
  
  x = 1.005d0
  xminus = .005d0
  
  !!!!call q2leges_eps(ier, eps, x, xminus, dvals, lvals, nterms)
  call prin2('from eps, dvals = *', dvals, nterms+1)
  call prinf('nterms = *', nterms, 1)
  
  stop
  
  call prinf('nterms = *', nterms, 1)
  call prin2('src = *', src, 2)
  call prin2('targ = *', targ, 2)
  
  !!!!call torlaps_near(src, targ, dvals2, nterms)
  call prin2('from torlaps near, dval2 = *', nterms+1)
  

  
  stop


  


  
  return
end subroutine gmall_near





subroutine gmall_fft(zk, src, targ, par1, maxm, vals, grads, grad0s)
  implicit real *8 (a-h,o-z)
  integer :: maxm
  real *8 :: src(2), targ(2), par1(*)
  complex *16 :: zk, vals(-maxm:maxm), grads(2,-maxm:maxm)
  complex *16 :: grad0s(2,-maxm:maxm)

  real *8, parameter :: done = 1, pi = 4*atan(done)
  real *8 :: dvals(0:1000000)

  complex *16, parameter :: ima = (0,1)
  complex *16 :: zkappa

  complex *16, allocatable :: fvals(:), gvals(:), w(:)
  complex *16, allocatable :: dervals(:)
  complex *16, allocatable :: f3vals(:), f4vals(:), f12vals(:)
  complex *16, allocatable :: f123vals(:)
  
  !
  ! this routine computes all the axisymmetric greens functions from
  ! mode -maxm to maxm using FFTs (to near machine precision,
  ! hopefully), and then discrete convolutions as described in Young,
  ! Hao, Martinsson
  !
  ! input:
  !   zk - the complex wavenumber
  !   src - source location in r,z coordinates
  !   targ - target location in r,z, coordinates
  !   par1 - dummy parameter
  !   maxm - maximum Fourier mode to compute, -maxm through maxm
  !         are returned
  !
  ! output:
  !   vals - the values of the following integrals:
  !
  !      \frac{1}{2\pi} \int_0^{2\pi}
  !        \frac{ e^{ i zk R} }{ 4 \pi R } e^{i m \theta} d\theta
  !
  !   grads - the TARGET, i.e. r and z, derivatives of vals, computed
  !       via more convolutions
  !   grads - the SOURCE, i.e. r' and z', derivatives of vals, computed
  !       via more convolutions
  !

  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  rzero = sqrt(r**2 + r0**2 + (z-z0)**2)
  zkappa = zk*rzero
  dkappa = zkappa
  alpha = 2*r*r0/rzero**2

  !
  ! first compute all the toroidal harmonics that are greater than
  ! machine precision
  !
  eps = 1.0d-16
  !!!!x = 1/alpha
  !!!!call get_q2n(x, n, ntop)  
  lvals = 1000000

  !!!!!call torlaps_eps(ier, eps, src, targ, dvals, lvals, nterms)

  call prinf('in torlaps, lvals = *', lvals, 1)
  call prinf('in torlaps, nterms = *', nterms, 1)
  
  !
  ! now compute enough modes of the "smooth" part, e^{ikr}, so that
  ! it has decayed to less than eps
  !
  dkappa = zkappa
  call get_gmakn(alpha, dkappa, n)

  call prin2('1-alpha = *', 1-alpha, 1)
  call prin2('dkappa = *', dkappa, 1)
  call prinf('from get_gmakn, n = *', n, 1)
  
  nhalf = n/2
  if (n .ne. 2*nhalf) then
    call prinf('odd n!! n = *', n, 1)
    stop
  end if
  
  !
  ! compute the values symmetrically, on [-pi,pi]
  !
  allocate( fvals(n) )
  
  t = -pi
  d = sqrt(1-alpha*cos(t))
  fvals(1) = exp(ima*zkappa*d)
  
  t = 0
  d = sqrt(1-alpha*cos(t))
  fvals(n/2+1) = exp(ima*zkappa*d)

  h = 2*pi/n
  do i = 1,n/2-1
    t = h*i
    d = sqrt(1-alpha*cos(t))
    fvals(n/2+1+i) = exp(ima*zkappa*d)
    fvals(n/2+1-i) = fvals(n/2+1+i)
  end do

  lw = 4*n+1000
  allocate( w(lw) )
  
  call zffti(n, w)
  call zfftf(n, fvals, w)

  n2 = (n-1)/2
  do i = 1,n2
    fvals(i+1) = fvals(i+1)*(-1)**(i)
    fvals(n-i+1) = fvals(n-i+1)*(-1)**(i)
  end do
  
  do i = 1,n
    fvals(i) = fvals(i)/n
  end do

  allocate( gvals(2*nterms+1) )
  gvals(1) = dvals(0)
  do i = 1,nterms
    gvals(i+1) = dvals(i)
    gvals(2*nterms+1-i+1) = dvals(i)
  end do

  !
  ! compute the convolution as ifft( fft() fft() )
  !
  nnn = 2*nterms+1
  minlen = 2*maxm+1
  lll = 4*(n/2 + nterms + maxm) + 200
  allocate( f12vals(lll) )
  call zfft_product(n, fvals, nnn, gvals, minlen, ntot, f12vals)

  !
  ! and copy over the output into vals
  !
  vals(0) = f12vals(1)
  do i = 1,maxm
    vals(i) = f12vals(i+1)
    vals(-i) = f12vals(ntot-i+1)
  end do

  !
  ! now do the gradients as more convolutions, first d/dz
  !
  allocate( f3vals(nnn+50) )
  allocate( f4vals(nnn+50) )
  
  do i = 1,nnn
    f3vals(i) = gvals(i)*4*pi*(z - z0)
    f4vals(i) = -gvals(i)*4*pi
  end do

  f4vals(1) = f4vals(1) + ima*zk

  lf123 = ntot + nnn + minlen + 100
  lf123 = lf123*2
  allocate( f123vals(lf123) )
  call zfft_product(ntot, f12vals, nnn, f3vals, minlen, &
      n123, f123vals)

  ld = n123 + nnn + minlen + 50
  ld = ld*2
  allocate( dervals(ld) )
  call zfft_product(n123, f123vals, nnn, f4vals, minlen, &
      n1234, dervals)
  
  grads(2,0) = dervals(1)
  do i = 1,maxm
    grads(2,i) = dervals(i+1)
    grads(2,-i) = dervals(n1234-i+1)
  end do

  !
  ! the d/dz' derivatives are just -d/dz'
  !
  grad0s(2,0) = -grads(2,0)
  do i = 1,maxm
    grad0s(2,i) = -grads(2,i)
    grad0s(2,-i) = -grads(2,-i)
  end do

  
  !
  ! and now d/dr
  !
  f3vals(1) = r*gvals(1) - r0*(gvals(2) + gvals(nnn))/2
  f3vals(1) = f3vals(1)*4*pi

  f3vals(nnn) = r*gvals(nnn) - r0*(gvals(nnn-1) + gvals(1))/2
  f3vals(nnn) = f3vals(nnn)*4*pi

  do i = 1,nnn-2
    f3vals(i+1) = r*gvals(i+1) - r0*(gvals(i) + gvals(i+2))/2
    f3vals(i+1) = f3vals(i+1)*4*pi
  end do

  call zfft_product(ntot, f12vals, nnn, f3vals, minlen, &
      n123, f123vals)
  call zfft_product(n123, f123vals, nnn, f4vals, minlen, &
      n1234, dervals)

  grads(1,0) = dervals(1)
  do i = 1,maxm
    grads(1,i) = dervals(i+1)
    grads(1,-i) = dervals(n1234-i+1)
  end do

  !
  ! and d/dr', which involves switching r and r0 in the convolution
  !
  f3vals(1) = r0*gvals(1) - r*(gvals(2) + gvals(nnn))/2
  f3vals(1) = f3vals(1)*4*pi

  f3vals(nnn) = r0*gvals(nnn) - r*(gvals(nnn-1) + gvals(1))/2
  f3vals(nnn) = f3vals(nnn)*4*pi

  do i = 1,nnn-2
    f3vals(i+1) = r0*gvals(i+1) - r*(gvals(i) + gvals(i+2))/2
    f3vals(i+1) = f3vals(i+1)*4*pi
  end do

  call zfft_product(ntot, f12vals, nnn, f3vals, minlen, &
      n123, f123vals)
  call zfft_product(n123, f123vals, nnn, f4vals, minlen, &
      n1234, dervals)

  grad0s(1,0) = dervals(1)
  do i = 1,maxm
    grad0s(1,i) = dervals(i+1)
    grad0s(1,-i) = dervals(n1234-i+1)
  end do
  
  deallocate( fvals )
  deallocate( gvals )
  deallocate( w )
  deallocate( f3vals )
  deallocate( f4vals )
  deallocate( f12vals )
  deallocate( f123vals )
  deallocate( dervals )
  
  return
end subroutine gmall_fft





subroutine zfft_product(m, fhat, n, ghat, minlen, len, hhat)
  implicit real *8 (a-h,o-z)
  integer :: m, n, minlen, len
  complex *16 :: fhat(m), ghat(n), hhat(*)

  complex *16, allocatable :: fpad(:)
  complex *16, allocatable :: gpad(:)
  complex *16, allocatable :: wsave(:)

  !
  ! assumes that h = f \cdot g, and that fhat and ghat are known -
  ! then hhat = fhat * ghat (convolution).
  !
  ! input:
  !   m - length of fhat
  !   f - first signal
  !   n - length of ghat
  !   g - second signal
  !   minlen - the minimum allowable length of convolved signal -
  !       useful when some oversampling is needed...
  !
  ! output:
  !   len - the length of hhat
  !   hhat - the output signal, with bandlimit at least (m-1)/2 + (n-1)/2
  !

  !
  ! calculate the new bandwidth
  !
  mhalf = (m-1)/2
  nhalf = (n-1)/2
  limit = nhalf + mhalf + 3
  len = 2*limit + 1
  if (len .le. minlen) len = minlen + 2
  len = next235(len*1.0d0)

  !
  ! zero pad both f and g
  !
  allocate( fpad(len+100) )
  allocate( gpad(len+100) )

  do i = 1,len
    fpad(i) = 0
    gpad(i) = 0
  end do

  do i = 1,mhalf+1
    fpad(i) = fhat(i)
  end do

  do i = 1,mhalf
    fpad(len-i+1) = fhat(m-i+1)
  end do
  
  do i = 1,nhalf+1
    gpad(i) = ghat(i)
  end do

  do i = 1,nhalf
    gpad(len-i+1) = ghat(n-i+1)
  end do
  
  !
  ! compute the convolution as ifft( fft() fft() )
  !
  lw = 5*len + 100
  allocate( wsave(lw) )
  
  call zffti(len, wsave)
  call zfftf(len, fpad, wsave)
  call zfftf(len, gpad, wsave)

  do i = 1,len
    hhat(i) = fpad(i)*gpad(i)/len
  end do

  call zfftb(len, hhat, wsave)

  deallocate( fpad )
  deallocate( gpad )
  deallocate( wsave )

  return
end subroutine zfft_product





subroutine fintegrand(zk, src, targ, theta, val)
  implicit real *8 (a-h,o-z)
  real *8 :: alpha, theta, src(2), targ(2)
  complex *16 :: zkappa, val, zk
  complex *16, parameter :: ima = (0,1)

  real *8, parameter :: fac2 = 2.0d0
  real *8, parameter :: fac4 = 24.0d0
  real *8, parameter :: fac6 = 720.0d0
  real *8, parameter :: fac8 = 40320.0d0
  real *8, parameter :: fac10 = 3628800.0d0
  real *8, parameter :: fac12 = 479001600.0d0
  real *8, parameter :: fac14 = 87178291200.0d0
  real *8, parameter :: fac16 = 20922789888000.0d0

  !
  ! this routine computes the following function as accurately as
  ! possible, namely for small theta:
  !
  !      exp(i * zkappa * sqrt(1 - alpha*cos(theta))
  !
  ! where it is known that zkappa = zk*sqrt(r**2 + r'**2 + dz**2)
  ! and alpha = 2*r*r'/sqrt(r**2 + r'**2 + dz**2) \in [0,1]
  !
  ! Input:
  !   zk - the helmholtz parameter
  !   src - source in r,z coordinates
  !   targ - target in r,z coordinates
  !   theta - argument
  !
  ! Output:
  !   val - the value of the integrand
  !
  ! NOTE: this routine will break when theta is near 2pi, since the
  ! taylor series doesn't converge as quickly there. It will just
  ! return the naive calculation.

  r = targ(1)
  z = targ(2)
  r0 = src(1)
  z0 = src(2)
  rzero = (r**2 + r0**2 + (z-z0)**2)
  alpha = 2*r*r0/rzero
  zkappa = zk*sqrt(rzero)

  x = alpha*cos(theta)

  if ( (x .lt. .9d0) .or. (abs(theta) .gt. .1d0) ) then
    val = exp(ima*zkappa*sqrt(1 - x))
    return
  end if

  !
  ! if here, expand... use 14 terms to guarantee double precision
  !

  oneminusalpha = ((r-r0)**2 + (z-z0)**2)/rzero
  !  write(*,*) 'alpha = ', alpha
  !  write(*,*) 'oneminusalpha = ', oneminusalpha
  !  write(*,*) '1 - alpha = ', 1 - alpha
  
  ac = alpha*(theta**2/fac2 - theta**4/fac4 + theta**6/fac6 &
      - theta**8/fac8 + theta**10/fac10 - theta**12/fac12 &
      + theta**14/fac14)

  !  write(*,*) 'ac = ', ac
  !  write(*,*) '1 - cos = ', 1-cos(theta)
  !  stop
  
  val = exp(ima*zkappa*sqrt(oneminusalpha + ac))

  return
end subroutine fintegrand






subroutine gk3d(zk, src, targ, pars1, pars2, val, grad, hess)
  implicit real *8 (a-h,o-z)
  real *8 :: src(3), targ(3), pars1(*), pars2(*)
  complex *16 :: zk, val, grad(3), hess(3,3)
  complex *16 :: ima, cd
  
  !
  ! calculate the 3d Green's function for the Helmholtz equation
  !
  ! Input:
  !   zk - the Helmholtz parameter
  !   src - the source in x,y,z coordinates
  !   targ - the target in x,y,z coordinates
  !   pars1, pars2 - dummy arguments
  !
  ! Output:
  !   val - the value of the Green's function
  !   grad - the cartesian gradient
  !   hess - the MATRIX Hessian, at the target!!
  !
  
  r = sqrt((targ(1) - src(1))**2 + (targ(2) - src(2))**2 + &
      (targ(3) - src(3))**2)

  ima = (0,1)
  done = 1
  pi = 4*atan(done)
  val = exp(ima*zk*r)/(4*pi*r)

  cd = (ima*zk - 1/r)*val/r
  grad(1) = cd*(targ(1) - src(1))
  grad(2) = cd*(targ(2) - src(2))
  grad(3) = cd*(targ(3) - src(3))
  
  hess(1,1) = -7
  hess(2,1) = -7
  hess(1,2) = -7
  hess(2,2) = -7

  return
end subroutine gk3d






subroutine gk3d_quad(zk, src, targ, pars1, pars2, val, grad, hess)
  implicit real *16 (a-h,o-z)
  real *16 :: src(3), targ(3), pars1(*), pars2(*)
  complex *32 :: zk, val, grad(3), hess(3,3)
  complex *32 :: ima, cq
  
  !
  ! calculate the 3d Green's function for the Helmholtz equation
  !
  r = sqrt((targ(1) - src(1))**2 + (targ(2) - src(2))**2 + &
      (targ(3) - src(3))**2)

  ima = (0,1)
  done = 1
  pi = 4*atan(done)
  val = exp(ima*zk*r)/(4*pi*r)

  cq = (ima*zk - 1/r)*val/r
  grad(1) = cq*(targ(1) - src(1))
  grad(2) = cq*(targ(2) - src(2))
  grad(3) = cq*(targ(3) - src(3))

  hess(1,1) = -7
  hess(2,1) = -7
  hess(1,2) = -7
  hess(2,2) = -7
  
  return
end subroutine gk3d_quad





subroutine gm_alph_kap(eps, alpha, zkappa, m, val)
  implicit real *8 (a-h,o-z)
  integer :: m
  complex *16 :: ima, val, fgm_alph_kap
  complex *16 :: par1(10), zkappa
  external fgm_alph_kap

  ima=(0,1)
  done = 1
  pi=4*atan(done)

  par1(1) = alpha
  par1(2) = zkappa

  a = 0
  b = pi
  k = 16
  call cadapgau(ier, a, b, fgm_alph_kap, par1, m, k, eps, &
      val, maxrec, numint)
  
  if (ier .ne. 0) then
    call prinf('in cadapgau, ier=*',ier,1)
    stop
  endif

  return
end subroutine gm_alph_kap

function fgm_alph_kap(t, par1, m)
  implicit real *8 (a-h,o-z)
  integer :: m
  complex *16 :: fgm_alph_kap, ima, zz, zkappa
  complex *16 :: par1(2)
  
  ima=(0,1)
  
  alpha = par1(1)
  zkappa = par1(2)
  
  d = sqrt(1 - alpha*cos(t))
  !!!!zz = exp(ima*zkappa*d)/d*exp(ima*m*t)
  zz = exp(ima*zkappa*d)/d*cos(m*t)
  fgm_alph_kap = zz
  
  return
end function fgm_alph_kap





subroutine get_gmakn(alpha, kappa, n)
  implicit real *8 (a-h,o-z)
  real *8 :: alpha, rkappa
  complex *16 :: kappa
  integer :: n

  !
  ! return the size of the FFT to take for the "smooth" part of the
  ! axisymmetric kernels
  !
  rkappa = kappa

  !
  ! kappa < 50
  !
  
  if ((0.0d0 .le. rkappa) .and. (rkappa .le. 50.0d0)) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 48
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 60
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 90
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 108
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 144
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 384
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 1152
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 3200
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 4374
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 4500
      return
    endif
  endif
    
  !
  ! kappa < 100
  !
  
  if ((50.0d0 .lt. rkappa) .and. (rkappa .le. 100.0d0)) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 54
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 80
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 120
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 160
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 192
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 432
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 1152
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 3456
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 4500
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 5000
      return
    endif
  endif

  !
  ! kappa < 200
  !
  
  if ((100.0d0 .lt. rkappa) .and. (rkappa .le. 200.0d0)) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 72
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 120
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 192
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 270
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 320
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 486
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 1200
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 3456
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 4800
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 5000
      return
    endif
  endif

  !
  ! kappa < 400
  !
  
  if ((200.0d0 .lt. rkappa) .and. (rkappa .le. 400.0d0)) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 108
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 192
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 320
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 450
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 576
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 720
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 1350
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 3600
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 4800
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 5000
      return
    endif
  endif

  !
  ! kappa < 800
  !
  
  if ((400.0d0 .lt. rkappa) .and. (rkappa .le. 800.0d0)) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 160
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 300
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 540
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 800
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 1024
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 1250
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 1728
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 3750
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 5120
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 5300
      return
    endif
  endif

  !
  ! kappa < 1600
  !
  
  if ((800.0d0 .lt. rkappa) .and. (rkappa .le. 1600.0d0)) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 256
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 540
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 1000
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 1500
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 1920
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 2304
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 2700
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 4320
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 6000
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 6500
      return
    endif
  endif
 
  !
  ! kappa < 3200
  !
  
  if ((1600.0d0 .lt. rkappa) .and. (rkappa .le. 3200.0d0)) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 450
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 960
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 1920
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 2880
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 3750
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 4500
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 4800
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 6000
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 7500
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 8000
      return
    endif
  endif

  !
  ! kappa > 3200
  !
  
  if (3200.0d0 .lt. rkappa) then
    if ((0.0d0 .le. alpha) .and. (alpha .le. 0.1d0)) then
      n = 500
      return
    endif
    if ((0.1d0 .lt. alpha) .and. (alpha .le. 0.25d0)) then
      n = 1000
      return
    endif
    if ((0.25d0 .lt. alpha) .and. (alpha .le. 0.5d0)) then
      n = 2000
      return
    endif
    if ((0.5d0 .lt. alpha) .and. (alpha .le. 0.75d0)) then
      n = 3000
      return
    endif
    if ((0.75d0 .lt. alpha) .and. (alpha .le. 0.9d0)) then
      n = 4000
      return
    endif
    if ((0.9d0 .lt. alpha) .and. (alpha .le. 0.99d0)) then
      n = 5000
      return
    endif
    if ((0.99d0 .lt. alpha) .and. (alpha .le. 0.999d0)) then
      n = 5500
      return
    endif
    if ((0.999d0 .lt. alpha) .and. (alpha .le. 0.9999d0)) then
      n = 6750
      return
    endif
    if ((0.9999d0 .lt. alpha) .and. (alpha .le. 0.99995d0)) then
      n = 8000
      return
    endif
    if ((0.99995d0 .lt. alpha) .and. (alpha .le. 1.0d0)) then
      n = 9000
      return
    endif
  endif

  return
end subroutine get_gmakn
