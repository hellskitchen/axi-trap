!
! these routines are for the interior, psi = 0.5 boundary
!
subroutine load_interior_phi05_n26(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 26

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xfive.dat26', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yfive.dat26', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxfivedtheta.dat26', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyfivedtheta.dat26', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xfivedtheta2.dat26', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yfivedtheta2.dat26', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_phi05_n26



  

subroutine load_interior_phi05_n50(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 50

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xfive.dat50', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yfive.dat50', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxfivedtheta.dat50', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyfivedtheta.dat50', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xfivedtheta2.dat50', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yfivedtheta2.dat50', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_phi05_n50





subroutine load_interior_phi05_n100(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 100

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xfive.dat100', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yfive.dat100', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxfivedtheta.dat100', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyfivedtheta.dat100', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xfivedtheta2.dat100', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yfivedtheta2.dat100', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_phi05_n100



  

subroutine load_interior_phi05_n200(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 200

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xfive.dat200', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yfive.dat200', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxfivedtheta.dat200', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyfivedtheta.dat200', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xfivedtheta2.dat200', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yfivedtheta2.dat200', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_phi05_n200



  

subroutine load_interior_phi05_n400(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 400

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xfive.dat400', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yfive.dat400', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxfivedtheta.dat400', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyfivedtheta.dat400', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xfivedtheta2.dat400', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yfivedtheta2.dat400', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_phi05_n400



  



  








!
! these routines are for the interior, psi = 1.5 boundary
!
subroutine load_interior_n26(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 26

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xone.dat26', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yone.dat26', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxonedtheta.dat26', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyonedtheta.dat26', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xonedtheta2.dat26', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yonedtheta2.dat26', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_n26



  

subroutine load_interior_n50(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 50

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xone.dat50', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yone.dat50', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxonedtheta.dat50', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyonedtheta.dat50', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xonedtheta2.dat50', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yonedtheta2.dat50', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_n50



  

subroutine load_interior_n100(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 100

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xone.dat100', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yone.dat100', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxonedtheta.dat100', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyonedtheta.dat100', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xonedtheta2.dat100', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yonedtheta2.dat100', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_n100



  

subroutine load_interior_n200(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 200

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xone.dat200', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yone.dat200', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxonedtheta.dat200', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyonedtheta.dat200', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xonedtheta2.dat200', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yonedtheta2.dat200', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_n200



  

subroutine load_interior_n400(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 400

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/xone.dat400', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/yone.dat400', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxonedtheta.dat400', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dyonedtheta.dat400', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xonedtheta2.dat400', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2yonedtheta2.dat400', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_interior_n400



  

subroutine load_exterior_n400(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*), dzdt2(*)
  
  parameter n = 400

  !
  ! load the geometry from files
  !
  iunit = 77
  open(unit=iunit, file='geos/x.dat400', action='read')
  do i = 1,n
    read(iunit,*) ps(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/y.dat400', action='read')
  do i = 1,n
    read(iunit,*) zs(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dxdtheta.dat400', action='read')
  do i = 1,n
    read(iunit,*) dpdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/dydtheta.dat400', action='read')
  do i = 1,n
    read(iunit,*) dzdt(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2xdtheta2.dat400', action='read')
  do i = 1,n
    read(iunit,*) dpdt2(i)
  end do
  close(iunit)

  iunit = 77
  open(unit=iunit, file='geos/d2ydtheta2.dat400', action='read')
  do i = 1,n
    read(iunit,*) dzdt2(i)
  end do
  close(iunit)

  return
end subroutine load_exterior_n400



  

!
!     these routines are for the exterior, psi = 0 boundary
!


      subroutine load_ex1_n26(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
        implicit real *8 (a-h,o-z)

        parameter n = 26
        real *8 ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*)
        real *8 dzdt2(*)

        real *8 xs(n), ys(n), dxs(n), dys(n), d2xs(n), d2ys(n)
        
        data xs /1.9499999972733317E+00, &
           1.9380271977432499E+00, &
           1.8999816402481975E+00, &
           1.8285827740790157E+00, &
           1.7084970597503972E+00, &
           1.5123545106820453E+00, &
           1.2039820321682586E+00, &
           7.6970716692917773E-01, &
           3.5316678978843530E-01, &
           1.5591168899950303E-01, &
           9.1105955163100494E-02, &
           6.5072460500055218E-02, &
           5.3442068871469628E-02, &
           5.0000000853774651E-02, &
           5.3442068871469628E-02, &
           6.5072460500054885E-02, &
           9.1105955163100272E-02, &
           1.5591168899950292E-01, &
           3.5316678978843463E-01, &
           7.6970716692917640E-01, &
           1.2039820321682573E+00, &
           1.5123545106820453E+00, &
           1.7084970597503972E+00, &
           1.8285827740790155E+00, &
           1.8999816402481970E+00, &
           1.9380271977432497E+00/        

        data ys /0.0000000000000000E+00, &
           2.3120293916563042E-01, &
           4.7234680268707241E-01, &
           7.3406028311862070E-01, &
           1.0264351952183750E+00, &
           1.3509678151823832E+00, &
           1.6799431763099546E+00, &
           1.8966321168488589E+00, &
           1.7055590037133799E+00, &
           1.2228730357590707E+00, &
           8.0520986043837217E-01, &
           4.9068782548175455E-01, &
           2.3330557610053027E-01, &
           1.1634144581444132E-16, &
          -2.3330557610052957E-01, &
          -4.9068782548175444E-01, &
          -8.0520986043837117E-01, &
          -1.2228730357590702E+00, &
          -1.7055590037133792E+00, &
          -1.8966321168488589E+00, &
          -1.6799431763099555E+00, &
          -1.3509678151823832E+00, &
          -1.0264351952183757E+00, &
          -7.3406028311862115E-01, &
          -4.7234680268707308E-01, &
          -2.3120293916563131E-01/

        data dxs /1.6226336513752287E-16, &
          -1.0177638444334705E-01, &
          -2.1662358636103851E-01, &
          -3.8578580189061096E-01, &
          -6.2583936584937661E-01, &
          -1.0244018018458616E+00, &
          -1.5476710266436282E+00, &
          -1.9493676657877101E+00, &
          -1.3098490392989897E+00, &
          -4.2731368053616836E-01, &
          -1.6741110631762418E-01, &
          -6.4659277020155670E-02, &
          -3.2167376885261278E-02, &
          -3.9284814717505538E-16, &
           3.2167376885261383E-02, &
           6.4659277020153033E-02, &
           1.6741110631762698E-01, &
           4.2731368053616670E-01, &
           1.3098490392989863E+00, &
           1.9493676657877084E+00, &
           1.5476710266436320E+00, &
           1.0244018018458656E+00, &
           6.2583936584937438E-01, &
           3.8578580189061107E-01, &
           2.1662358636103748E-01, &
           1.0177638444334952E-01/
        
        data dys /9.6060093151870229E-01, &
           9.5929350461843732E-01, &
           1.0449414448324676E+00, &
           1.1261495735648672E+00, &
           1.3003133698903422E+00, &
           1.3607977036596528E+00, &
           1.3121188618233903E+00, &
           2.0384064712463912E-01, &
          -1.6839147407007264E+00, &
          -2.0041624689084299E+00, &
          -1.4659513926740826E+00, &
          -1.1667391728753245E+00, &
          -9.8780800893074339E-01, &
          -9.5835957436767683E-01, &
          -9.8780800893074228E-01, &
          -1.1667391728753231E+00, &
          -1.4659513926740844E+00, &
          -2.0041624689084330E+00, &
          -1.6839147407007216E+00, &
           2.0384064712462929E-01, &
           1.3121188618233934E+00, &
           1.3607977036596517E+00, &
           1.3003133698903417E+00, &
           1.1261495735648683E+00, &
           1.0449414448324637E+00, &
           9.5929350461844132E-01/
        
        data d2xs /-5.1694420063585356E-01, &
          -3.2687352428489069E-01, &
          -6.7258672284993815E-01, &
          -7.0258234474063963E-01, &
          -1.4009059098735970E+00, &
          -1.8111945193888208E+00, &
          -2.5158225810918373E+00, &
           1.7712500840093115E-01, &
           4.3372371409884165E+00, &
           2.2318177863655224E+00, &
           4.7463256746149862E-01, &
           3.2454984501129946E-01, &
           3.5917605683346239E-02, &
           2.1431549727326452E-01, &
           3.5917605683340605E-02, &
           3.2454984501130435E-01, &
           4.7463256746151133E-01, &
           2.2318177863654887E+00, &
           4.3372371409884343E+00, &
           1.7712500840093198E-01, &
          -2.5158225810918022E+00, &
          -1.8111945193888581E+00, &
          -1.4009059098735921E+00, &
          -7.0258234474064318E-01, &
          -6.7258672284992826E-01, &
          -3.2687352428489069E-01/
        
        data d2ys /-1.5338158094052163E-14, &
           1.6306596723886743E-01, &
           3.6533907486996769E-01, &
           5.0897117926746815E-01, &
           6.3749667719123349E-01, &
           7.6755099378105485E-02, &
          -1.3436168798224406E+00, &
          -7.8562024049233123E+00, &
          -5.4507871525140041E+00, &
           1.8701672962054632E+00, &
           1.7497427368963014E+00, &
           9.7978029602927919E-01, &
           3.8805456358505974E-01, &
           1.3049390627901839E-14, &
          -3.8805456358507828E-01, &
          -9.7978029602925920E-01, &
          -1.7497427368963456E+00, &
          -1.8701672962054088E+00, &
           5.4507871525139642E+00, &
             7.8562024049233008E+00, &
             1.3436168798224959E+00, &
            -7.6755099378157332E-02, &
            -6.3749667719118952E-01, &
            -5.0897117926750690E-01, &
            -3.6533907486994860E-01, &
            -1.6306596723885330E-01/

        do i = 1, n
          ps(i) = xs(i)
          zs(i) = ys(i)
          dpdt(i) = dxs(i)
          dzdt(i) = dys(i)
          dpdt2(i) = d2xs(i)
          dzdt2(i) = d2ys(i)
        enddo
                
        return
      end subroutine load_ex1_n26


        


        subroutine load_ex1_n50(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
        implicit real *8 (a-h,o-z)

        parameter n = 50
        real *8 ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*)
        real *8 dzdt2(*)

        real *8 xs(n), ys(n), dxs(n), dys(n), d2xs(n), d2ys(n)
        
        data xs /1.9499999972733320E+00, &
           1.9467957934385880E+00, &
           1.9370351817695741E+00, &
           1.9202576195786820E+00, &
           1.8956384364301631E+00, &
           1.8618968641522282E+00, &
           1.8171577764535527E+00, &
           1.7587704203224448E+00, &
           1.6831231406667138E+00, &
           1.5855796351625946E+00, &
           1.4607792210890547E+00, &
           1.3035095324823667E+00, &
           1.1101579338197538E+00, &
           8.8233545906563304E-01, &
           6.3888498437035568E-01, &
           4.2146950934694338E-01, &
           2.6558875272108995E-01, &
           1.7446751962501650E-01, &
           1.2466288212063015E-01, &
           9.5954274776324122E-02, &
           7.8133925635828216E-02, &
           6.6481430471110547E-02, &
           5.8727631208044584E-02, &
           5.3731253613860885E-02, &
           5.0912878638986303E-02, &
           5.0000000853774651E-02, &
           5.0912878638986303E-02, &
           5.3731253613860885E-02, &
           5.8727631208044584E-02, &
           6.6481430471110325E-02, &
           7.8133925635828216E-02, &
           9.5954274776324122E-02, &
           1.2466288212063015E-01, &
           1.7446751962501683E-01, &
           2.6558875272109050E-01, &
           4.2146950934694338E-01, &
           6.3888498437035601E-01, &
           8.8233545906563338E-01, &
           1.1101579338197545E+00, &
           1.3035095324823676E+00, &
           1.4607792210890544E+00, &
           1.5855796351625946E+00, &
           1.6831231406667135E+00, &
           1.7587704203224446E+00, &
           1.8171577764535529E+00, &
           1.8618968641522282E+00, &
           1.8956384364301631E+00, &
           1.9202576195786816E+00, &
           1.9370351817695743E+00, &
           1.9467957934385880E+00/        

        data ys /0.0000000000000000E+00, &
           1.1960812410048663E-01, &
           2.4058974280766718E-01, &
           3.6435576690071841E-01, &
           4.9238139711014067E-01, &
           6.2620472656105286E-01, &
           7.6736222920472341E-01, &
           9.1719587980285522E-01, &
           1.0764298270006381E+00, &
           1.2444200687184428E+00, &
           1.4181326230182199E+00, &
           1.5910526878904006E+00, &
           1.7509102190056240E+00, &
           1.8702243224141193E+00, &
           1.8930312058271475E+00, &
           1.7805337668368331E+00, &
           1.5607033440509870E+00, &
           1.3008310392854920E+00, &
           1.0581008121221764E+00, &
           8.4895544410213686E-01, &
           6.6977490815092355E-01, &
           5.1320617650684675E-01, &
           3.7267609471211305E-01, &
           2.4296051925183662E-01, &
           1.1989758613274273E-01, &
          -3.0554330316396611E-16, &
          -1.1989758613274293E-01, &
          -2.4296051925183682E-01, &
          -3.7267609471211366E-01, &
          -5.1320617650684708E-01, &
          -6.6977490815092378E-01, &
          -8.4895544410213730E-01, &
          -1.0581008121221771E+00, &
          -1.3008310392854932E+00, &
          -1.5607033440509883E+00, &
          -1.7805337668368331E+00, &
          -1.8930312058271481E+00, &
          -1.8702243224141191E+00, &
          -1.7509102190056238E+00, &
          -1.5910526878903997E+00, &
          -1.4181326230182203E+00, &
          -1.2444200687184428E+00, &
          -1.0764298270006374E+00, &
          -9.1719587980285444E-01, &
          -7.6736222920472297E-01, &
          -6.2620472656105219E-01, &
          -4.9238139711014067E-01, &
          -3.6435576690071814E-01, &
          -2.4058974280766687E-01, &
          -1.1960812410048619E-01/

        data dxs /4.0067948958721903E-15, &
          -5.1231877538794386E-02, &
          -1.0467515846026278E-01, &
          -1.6347328143394141E-01, &
          -2.2998752851193716E-01, &
          -3.0953816298136905E-01, &
          -4.0594107890766135E-01, &
          -5.2816006498476209E-01, &
          -6.8191819835604706E-01, &
          -8.7784183028753704E-01, &
          -1.1154297449641886E+00, &
          -1.3927937964447819E+00, &
          -1.6839127861279906E+00, &
          -1.9191388850648616E+00, &
          -1.8948061858884027E+00, &
          -1.5148025184516103E+00, &
          -9.6213590519496994E-01, &
          -5.2510256517114895E-01, &
          -2.9424632368107806E-01, &
          -1.7567118091950845E-01, &
          -1.1365684664019586E-01, &
          -7.4788237880397540E-02, &
          -4.9951078484317894E-02, &
          -3.0402551604434933E-02, &
          -1.4713368986225746E-02, &
           1.8474111129762605E-15, &
           1.4713368986222832E-02, &
           3.0402551604437208E-02, &
           4.9951078484314737E-02, &
           7.4788237880400316E-02, &
           1.1365684664019395E-01, &
           1.7567118091950923E-01, &
           2.9424632368108189E-01, &
           5.2510256517114928E-01, &
           9.6213590519496506E-01, &
           1.5148025184516081E+00, &
           1.8948061858884080E+00, &
           1.9191388850648627E+00, &
           1.6839127861279957E+00, &
           1.3927937964447761E+00, &
           1.1154297449641837E+00, &
           8.7784183028753926E-01, &
           6.8191819835604306E-01, &
           5.2816006498476797E-01, &
           4.0594107890766051E-01, &
           3.0953816298136660E-01, &
           2.2998752851193247E-01, &
           1.6347328143394221E-01, &
           1.0467515846027166E-01, &
           5.1231877538785969E-02/
        
        data dys /9.4989177334107022E-01, &
           9.5554635557671741E-01, &
           9.7180589664620332E-01, &
           9.9994274799288352E-01, &
           1.0397041517916681E+00, &
           1.0922077825448111E+00, &
           1.1562403309618865E+00, &
           1.2294577354528959E+00, &
           1.3041851682983756E+00, &
           1.3654544825294903E+00, &
           1.3910541071078533E+00, &
           1.3453332383774586E+00, &
           1.1666625893648632E+00, &
           6.5052798507909448E-01, &
          -3.4840290614203601E-01, &
          -1.3986090953494754E+00, &
          -2.0046822291972628E+00, &
          -2.0493669281197451E+00, &
          -1.7973616443855067E+00, &
          -1.5375093668850410E+00, &
          -1.3247771660844470E+00, &
          -1.1756205433053055E+00, &
          -1.0680049781106953E+00, &
          -1.0018637134522823E+00, &
          -9.6136920692143135E-01, &
          -9.5100136088301612E-01, &
          -9.6136920692142869E-01, &
          -1.0018637134522859E+00, &
          -1.0680049781107026E+00, &
          -1.1756205433052960E+00, &
          -1.3247771660844481E+00, &
          -1.5375093668850406E+00, &
          -1.7973616443855065E+00, &
          -2.0493669281197588E+00, &
          -2.0046822291972557E+00, &
          -1.3986090953494670E+00, &
          -3.4840290614203490E-01, &
           6.5052798507909071E-01, &
           1.1666625893648652E+00, &
           1.3453332383774623E+00, &
           1.3910541071078419E+00, &
           1.3654544825295074E+00, &
           1.3041851682983729E+00, &
           1.2294577354528884E+00, &
           1.1562403309618881E+00, &
           1.0922077825448133E+00, &
           1.0397041517916625E+00, &
           9.9994274799288574E-01, &
           9.7180589664620443E-01, &
           9.5554635557671519E-01/
        
        data d2xs /-4.1478754609177998E-01, &
          -4.0306057579607696E-01, &
          -4.5294588777377603E-01, &
          -4.8374871891296428E-01, &
          -5.8498626265558062E-01, &
          -6.8200391738728516E-01, &
          -8.6939932409976184E-01, &
          -1.0772021040303816E+00, &
          -1.3923105050633275E+00, &
          -1.7166570216443804E+00, &
          -2.0736810486429511E+00, &
          -2.2959123719341852E+00, &
          -2.2796324694003238E+00, &
          -1.1428140988249005E+00, &
           1.6824532937343282E+00, &
           4.0970671351734946E+00, &
           4.2636923902338637E+00, &
           2.5746395553694224E+00, &
           1.2719177042934462E+00, &
           6.7063288585002279E-01, &
           3.7008887819874997E-01, &
           2.5061047970978484E-01, &
           1.6231751676526243E-01, &
           1.4611090674677854E-01, &
           1.0987948745622220E-01, &
           1.2467569136080925E-01, &
           1.0987948745620897E-01, &
           1.4611090674679567E-01, &
           1.6231751676523515E-01, &
           2.5061047970983652E-01, &
           3.7008887819867925E-01, &
           6.7063288585010894E-01, &
           1.2719177042934047E+00, &
           2.5746395553694161E+00, &
           4.2636923902338015E+00, &
           4.0970671351735986E+00, &
           1.6824532937342969E+00, &
          -1.1428140988248767E+00, &
          -2.2796324694003425E+00, &
          -2.2959123719342771E+00, &
          -2.0736810486428592E+00, &
          -1.7166570216444086E+00, &
          -1.3923105050633218E+00, &
          -1.0772021040303155E+00, &
          -8.6939932409987120E-01, &
          -6.8200391738722399E-01, &
          -5.8498626265566944E-01, &
          -4.8374871891277449E-01, &
          -4.5294588777393474E-01, &
          -4.0306057579601789E-01/
        
        data d2ys /-1.4672707493446068E-14, &
           8.6747467506246567E-02, &
           1.7602480184256394E-01, &
           2.6926784220960059E-01, &
           3.6723698894098222E-01, &
           4.6502725078254786E-01, &
           5.5290660996486418E-01, &
           6.0146880968952299E-01, &
           5.6855192023809809E-01, &
           3.7788426786200541E-01, &
          -1.8933716706774618E-02, &
          -7.7005701786925684E-01, &
          -2.3511501788430764E+00, &
          -6.1902060730261814E+00, &
          -9.0104003276486626E+00, &
          -6.9985175856764794E+00, &
          -2.4865733431580548E+00, &
           1.3675678424621043E+00, &
           2.2068813659640862E+00, &
           1.9161714280458535E+00, &
           1.4213525202666517E+00, &
           1.0109015495470748E+00, &
           6.7747135043027296E-01, &
           4.1878691980372451E-01, &
           1.9829306365577992E-01, &
          -4.5474735088646413E-15, &
          -1.9829306365575236E-01, &
          -4.1878691980384020E-01, &
          -6.7747135043015361E-01, &
          -1.0109015495470444E+00, &
          -1.4213525202667483E+00, &
          -1.9161714280457687E+00, &
          -2.2068813659642141E+00, &
          -1.3675678424620745E+00, &
           2.4865733431582262E+00, &
           6.9985175856763702E+00, &
           9.0104003276486662E+00, &
           6.1902060730261113E+00, &
           2.3511501788432296E+00, &
           7.7005701786907021E-01, &
           1.8933716706889557E-02, &
          -3.7788426786190499E-01, &
          -5.6855192023833812E-01, &
          -6.0146880968941052E-01, &
          -5.5290660996484520E-01, &
          -4.6502725078257129E-01, &
          -3.6723698894101320E-01, &
          -2.6926784220950467E-01, &
          -1.7602480184266212E-01, &
          -8.6747467506192069E-02/

        do i = 1, n
          ps(i) = xs(i)
          zs(i) = ys(i)
          dpdt(i) = dxs(i)
          dzdt(i) = dys(i)
          dpdt2(i) = d2xs(i)
          dzdt2(i) = d2ys(i)
        enddo
                
        return
        end


        


        subroutine load_ex1_n100(ps, zs, dpdt, dzdt, dpdt2, &
          dzdt2)
        implicit real *8 (a-h, o-z)

        parameter n = 100
        real *8 ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*)
        real *8 dzdt2(*)

        real *8 xs(n), ys(n), dxs(n), dys(n), d2xs(n), d2ys(n)
        
        data xs /1.9499999972733315E+00, &
           1.9492012271243773E+00, &
           1.9467957934385876E+00, &
           1.9427560739514769E+00, &
           1.9370351817695741E+00, &
           1.9295656630595936E+00, &
           1.9202576195786811E+00, &
           1.9089962050179274E+00, &
           1.8956384364301626E+00, &
           1.8800092641360857E+00, &
           1.8618968641522282E+00, &
           1.8410471708204672E+00, &
           1.8171577764535529E+00, &
           1.7898715213873397E+00, &
           1.7587704203224448E+00, &
           1.7233710497385135E+00, &
           1.6831231406667135E+00, &
           1.6374137459388636E+00, &
           1.5855796351625946E+00, &
           1.5269299532018761E+00, &
           1.4607792210890547E+00, &
           1.3864881282731927E+00, &
           1.3035095324823667E+00, &
           1.2114465266967180E+00, &
           1.1101579338197538E+00, &
           9.9999999999999967E-01, &
           8.8233545906563304E-01, &
           7.6026686188004489E-01, &
           6.3888498437035568E-01, &
           5.2417246825848585E-01, &
           4.2146950934694316E-01, &
           3.3468868667314355E-01, &
           2.6558875272109006E-01, &
           2.1318575536963402E-01, &
           1.7446751962501650E-01, &
           1.4594068109181957E-01, &
           1.2466288212063059E-01, &
           1.0849100094955744E-01, &
           9.5954274776324122E-02, &
           8.6059652975241874E-02, &
           7.8133925635828438E-02, &
           7.1715069555062683E-02, &
           6.6481430471110547E-02, &
           6.2206144062880941E-02, &
           5.8727631208044584E-02, &
           5.5930228473155097E-02, &
           5.3731253613860885E-02, &
           5.2072230027327748E-02, &
           5.0912878638986303E-02, &
           5.0227035784869423E-02, &
           5.0000000853774651E-02, &
           5.0227035784869423E-02, &
           5.0912878638986303E-02, &
           5.2072230027327859E-02, &
           5.3731253613860885E-02, &
           5.5930228473155208E-02, &
           5.8727631208044584E-02, &
           6.2206144062880830E-02, &
           6.6481430471110770E-02, &
           7.1715069555063016E-02, &
           7.8133925635828216E-02, &
           8.6059652975242318E-02, &
           9.5954274776324233E-02, &
           1.0849100094955744E-01, &
           1.2466288212063015E-01, &
           1.4594068109181924E-01, &
           1.7446751962501705E-01, &
           2.1318575536963424E-01, &
           2.6558875272109050E-01, &
           3.3468868667314366E-01, &
           4.2146950934694316E-01, &
           5.2417246825848629E-01, &
           6.3888498437035612E-01, &
           7.6026686188004600E-01, &
           8.8233545906563338E-01, &
           9.9999999999999967E-01, &
           1.1101579338197545E+00, &
           1.2114465266967180E+00, &
           1.3035095324823676E+00, &
           1.3864881282731929E+00, &
           1.4607792210890544E+00, &
           1.5269299532018765E+00, &
           1.5855796351625946E+00, &
           1.6374137459388642E+00, &
           1.6831231406667138E+00, &
           1.7233710497385140E+00, &
           1.7587704203224450E+00, &
           1.7898715213873397E+00, &
           1.8171577764535529E+00, &
           1.8410471708204672E+00, &
           1.8618968641522282E+00, &
           1.8800092641360859E+00, &
           1.8956384364301626E+00, &
           1.9089962050179272E+00, &
           1.9202576195786816E+00, &
           1.9295656630595936E+00, &
           1.9370351817695743E+00, &
           1.9427560739514769E+00, &
           1.9467957934385880E+00, &
           1.9492012271243773E+00/        

        data ys /0.0000000000000000E+00, &
           5.9718679361286220E-02, &
           1.1960812410048660E-01, &
           1.7984033930976578E-01, &
           2.4058974280766718E-01, &
           3.0203419286986333E-01, &
           3.6435576690071819E-01, &
           4.2774114583892547E-01, &
           4.9238139711014056E-01, &
           5.5847086103838539E-01, &
           6.2620472656105286E-01, &
           6.9577472965929310E-01, &
           7.6736222920472363E-01, &
           8.4112773803030749E-01, &
           9.1719587980285522E-01, &
           9.9563483465234404E-01, &
           1.0764298270006378E+00, &
           1.1594512995869226E+00, &
           1.2444200687184428E+00, &
           1.3308731423219002E+00, &
           1.4181326230182199E+00, &
           1.5052718761072326E+00, &
           1.5910526878904003E+00, &
           1.6737716063957420E+00, &
           1.7509102190056243E+00, &
           1.8184826471945426E+00, &
           1.8702243224141200E+00, &
           1.8976831918968438E+00, &
           1.8930312058271475E+00, &
           1.8532258794291727E+00, &
           1.7805337668368340E+00, &
           1.6803845611911801E+00, &
           1.5607033440509865E+00, &
           1.4312097978471880E+00, &
           1.3008310392854920E+00, &
           1.1755118055551441E+00, &
           1.0581008121221760E+00, &
           9.4936065866493879E-01, &
           8.4895544410213686E-01, &
           7.5607720938593093E-01, &
           6.6977490815092344E-01, &
           5.8910753047978448E-01, &
           5.1320617650684675E-01, &
           4.4129229174431600E-01, &
           3.7267609471211305E-01, &
           3.0674686338717139E-01, &
           2.4296051925183662E-01, &
           1.8082689308858191E-01, &
           1.1989758613274273E-01, &
           5.9754650010107310E-02, &
          -3.0554330316396611E-16, &
          -5.9754650010107511E-02, &
          -1.1989758613274293E-01, &
          -1.8082689308858210E-01, &
          -2.4296051925183682E-01, &
          -3.0674686338717200E-01, &
          -3.7267609471211366E-01, &
          -4.4129229174431622E-01, &
          -5.1320617650684686E-01, &
          -5.8910753047978448E-01, &
          -6.6977490815092378E-01, &
          -7.5607720938593148E-01, &
          -8.4895544410213719E-01, &
          -9.4936065866493902E-01, &
          -1.0581008121221771E+00, &
          -1.1755118055551439E+00, &
          -1.3008310392854929E+00, &
          -1.4312097978471883E+00, &
          -1.5607033440509883E+00, &
          -1.6803845611911803E+00, &
          -1.7805337668368340E+00, &
          -1.8532258794291743E+00, &
          -1.8930312058271472E+00, &
          -1.8976831918968449E+00, &
          -1.8702243224141195E+00, &
          -1.8184826471945423E+00, &
          -1.7509102190056240E+00, &
          -1.6737716063957420E+00, &
          -1.5910526878903997E+00, &
          -1.5052718761072319E+00, &
          -1.4181326230182203E+00, &
          -1.3308731423218998E+00, &
          -1.2444200687184428E+00, &
          -1.1594512995869219E+00, &
          -1.0764298270006376E+00, &
          -9.9563483465234315E-01, &
          -9.1719587980285455E-01, &
          -8.4112773803030760E-01, &
          -7.6736222920472297E-01, &
          -6.9577472965929288E-01, &
          -6.2620472656105208E-01, &
          -5.5847086103838517E-01, &
          -4.9238139711014056E-01, &
          -4.2774114583892481E-01, &
          -3.6435576690071814E-01, &
          -3.0203419286986266E-01, &
          -2.4058974280766687E-01, &
          -1.7984033930976584E-01, &
          -1.1960812410048619E-01, &
          -5.9718679361286116E-02/

        data dxs /3.7769787297747829E-15, &
          -2.5449783312703530E-02, &
          -5.1190610833698698E-02, &
          -7.7521687709344964E-02, &
          -1.0475839104883519E-01, &
          -1.3324158035245548E-01, &
          -1.6334643618201969E-01, &
          -1.9549376046849390E-01, &
          -2.3016029990066714E-01, &
          -2.6789213482520852E-01, &
          -3.0931576126007915E-01, &
          -3.5515102802268245E-01, &
          -4.0621775640467628E-01, &
          -4.6343816487063477E-01, &
          -5.2782249304918949E-01, &
          -6.0043508873662144E-01, &
          -6.8232476151554866E-01, &
          -7.7441570405483418E-01, &
          -8.7735674458819002E-01, &
          -9.9134981011351342E-01, &
          -1.1160013788116294E+00, &
          -1.2502229845436594E+00, &
          -1.3921455311428459E+00, &
          -1.5387432997367494E+00, &
          -1.6845606743523973E+00, &
          -1.8184804170277418E+00, &
          -1.9187910090951183E+00, &
          -1.9529312590320225E+00, &
          -1.8944227401080496E+00, &
          -1.7426788712810086E+00, &
          -1.5158851976530752E+00, &
          -1.2415369038122244E+00, &
          -9.6081154161397875E-01, &
          -7.1569786524456080E-01, &
          -5.2628379590361141E-01, &
          -3.8960387859847118E-01, &
          -2.9332901134296729E-01, &
          -2.2528478532826590E-01, &
          -1.7636362807071126E-01, &
          -1.4034955828379231E-01, &
          -1.1314133799392181E-01, &
          -9.2018749649372969E-02, &
          -7.5167251742364063E-02, &
          -6.1343003236642150E-02, &
          -4.9684903095064251E-02, &
          -3.9576285855354103E-02, &
          -3.0572763024451534E-02, &
          -2.2341419407078577E-02, &
          -1.4630548594356441E-02, &
          -7.2391482551878283E-03, &
           3.2684965844964610E-15, &
           7.2391482551891788E-03, &
           1.4630548594352035E-02, &
           2.2341419407078858E-02, &
           3.0572763024458709E-02, &
           3.9576285855341807E-02, &
           4.9684903095071037E-02, &
           6.1343003236637939E-02, &
           7.5167251742370669E-02, &
           9.2018749649367779E-02, &
           1.1314133799392252E-01, &
           1.4034955828380441E-01, &
           1.7636362807069439E-01, &
           2.2528478532827187E-01, &
           2.9332901134295580E-01, &
           3.8960387859848516E-01, &
           5.2628379590361030E-01, &
           7.1569786524456136E-01, &
           9.6081154161398064E-01, &
           1.2415369038122119E+00, &
           1.5158851976530883E+00, &
           1.7426788712810122E+00, &
           1.8944227401080502E+00, &
           1.9529312590320256E+00, &
           1.9187910090951033E+00, &
           1.8184804170277475E+00, &
           1.6845606743524024E+00, &
           1.5387432997367438E+00, &
           1.3921455311428530E+00, &
           1.2502229845436370E+00, &
           1.1160013788116514E+00, &
           9.9134981011351198E-01, &
           8.7735674458818547E-01, &
           7.7441570405484239E-01, &
           6.8232476151553967E-01, &
           6.0043508873662410E-01, &
           5.2782249304918338E-01, &
           4.6343816487063721E-01, &
           4.0621775640467533E-01, &
           3.5515102802268073E-01, &
           3.0931576126007732E-01, &
           2.6789213482522306E-01, &
           2.3016029990064588E-01, &
           1.9549376046849942E-01, &
           1.6334643618202782E-01, &
           1.3324158035244976E-01, &
           1.0475839104884113E-01, &
           7.7521687709342688E-02, &
           5.1190610833699031E-02, &
         2.5449783312696681E-02/
        
        data dys /9.5000251586470175E-01, &
           9.5135472031562929E-01, &
           9.5544147168107674E-01, &
           9.6227184181544045E-01, &
           9.7191333619976805E-01, &
           9.8440707408440131E-01, &
           9.9984553066975945E-01, &
           1.0182807758098911E+00, &
           1.0397985379102805E+00, &
           1.0644132014707315E+00, &
           1.0921332191613355E+00, &
           1.1228324205019027E+00, &
           1.1562983578132415E+00, &
           1.1920665197243341E+00, &
           1.2294457788259479E+00, &
           1.2673127523807668E+00, &
           1.3041378047361407E+00, &
           1.3377859764821298E+00, &
           1.3656344316096707E+00, &
           1.3844616555906828E+00, &
           1.3906510828458842E+00, &
           1.3798287202618378E+00, &
           1.3462295539314977E+00, &
           1.2802875515735490E+00, &
           1.1647895526772210E+00, &
           9.6956236230149206E-01, &
           6.5424560928728370E-01, &
           1.9761785078205052E-01, &
          -3.5383195080179175E-01, &
          -9.0637102262321323E-01, &
          -1.3930419637136935E+00, &
          -1.7737082016945718E+00, &
          -2.0092511690703003E+00, &
          -2.0883212775402580E+00, &
          -2.0460275152579599E+00, &
          -1.9357652749644876E+00, &
          -1.7997906307637297E+00, &
          -1.6625685188953390E+00, &
          -1.5356542522993157E+00, &
          -1.4233119531743670E+00, &
          -1.3262865038118983E+00, &
          -1.2437447104257044E+00, &
          -1.1743347627966696E+00, &
          -1.1165822434616748E+00, &
          -1.0691557383288304E+00, &
          -1.0308766979092696E+00, &
          -1.0008026677413893E+00, &
          -9.7815850902392565E-01, &
          -9.6238740025164038E-01, &
          -9.5307501338203904E-01, &
          -9.5000194088845946E-01, &
          -9.5307501338203759E-01, &
          -9.6238740025164049E-01, &
          -9.7815850902392332E-01, &
          -1.0008026677413953E+00, &
          -1.0308766979092727E+00, &
          -1.0691557383288268E+00, &
          -1.1165822434616643E+00, &
          -1.1743347627966747E+00, &
          -1.2437447104257036E+00, &
          -1.3262865038119005E+00, &
          -1.4233119531743723E+00, &
          -1.5356542522993035E+00, &
          -1.6625685188953516E+00, &
          -1.7997906307637204E+00, &
          -1.9357652749644887E+00, &
          -2.0460275152579750E+00, &
          -2.0883212775402531E+00, &
          -2.0092511690703225E+00, &
          -1.7737082016945249E+00, &
          -1.3930419637137306E+00, &
          -9.0637102262319358E-01, &
          -3.5383195080178942E-01, &
           1.9761785078204155E-01, &
           6.5424560928730968E-01, &
           9.6956236230147086E-01, &
           1.1647895526772303E+00, &
           1.2802875515735468E+00, &
           1.3462295539315177E+00, &
           1.3798287202618076E+00, &
           1.3906510828458969E+00, &
           1.3844616555906839E+00, &
           1.3656344316096676E+00, &
           1.3377859764821409E+00, &
           1.3041378047361365E+00, &
           1.2673127523807806E+00, &
           1.2294457788259259E+00, &
           1.1920665197243410E+00, &
           1.1562983578132420E+00, &
           1.1228324205019025E+00, &
           1.0921332191613482E+00, &
           1.0644132014707119E+00, &
           1.0397985379102908E+00, &
           1.0182807758098931E+00, &
           9.9984553066975124E-01, &
           9.8440707408440742E-01, &
           9.7191333619975495E-01, &
           9.6227184181544734E-01, &
           9.5544147168107640E-01, &
         9.5135472031562240E-01/
        
        data d2xs /-4.0429216106425175E-01, &
          -4.0656935672804151E-01, &
          -4.1357850722929229E-01, &
          -4.2539926453028815E-01, &
          -4.4247449795312882E-01, &
          -4.6516482931818970E-01, &
          -4.9421710451436007E-01, &
          -5.3032372853366117E-01, &
          -5.7459351357480271E-01, &
          -6.2810479374040440E-01, &
          -6.9235946367845003E-01, &
          -7.6879436814326396E-01, &
          -8.5916160995350055E-01, &
          -9.6490714865969873E-01, &
          -1.0873526167639467E+00, &
          -1.2267877639084281E+00, &
          -1.3823344958662327E+00, &
          -1.5507794199187037E+00, &
          -1.7265115183449813E+00, &
          -1.9009844916326051E+00, &
          -2.0639194679600870E+00, &
          -2.2036128189363748E+00, &
          -2.3061781721082362E+00, &
          -2.3465522176472975E+00, &
          -2.2669365075992469E+00, &
          -1.9391108802237422E+00, &
          -1.1626104903808123E+00, &
           1.5354771422751604E-01, &
           1.7067500341041080E+00, &
           3.0715577369672906E+00, &
           4.0780393212584940E+00, &
           4.5415110499847824E+00, &
           4.2750073575474206E+00, &
           3.4742679490985622E+00, &
           2.5674048764463340E+00, &
           1.8183988075050610E+00, &
           1.2790530521078349E+00, &
           9.1085888104578006E-01, &
           6.6263740021389239E-01, &
           4.9438557460759136E-01, &
           3.7878974735875504E-01, &
           2.9827376140760853E-01, &
           2.4139074929421056E-01, &
           2.0088592221883203E-01, &
           1.7182959301834658E-01, &
           1.5109305904235412E-01, &
           1.3637938430554938E-01, &
           1.2628751800616583E-01, &
           1.1968917126074104E-01, &
           1.1602310780920164E-01, &
           1.1480671908698241E-01, &
           1.1602310780909790E-01, &
           1.1968917126066884E-01, &
           1.2628751800641611E-01, &
           1.3637938430531699E-01, &
           1.5109305904233339E-01, &
           1.7182959301857878E-01, &
           2.0088592221858831E-01, &
           2.4139074929452392E-01, &
           2.9827376140711520E-01, &
           3.7878974735941584E-01, &
           4.9438557460701860E-01, &
           6.6263740021405271E-01, &
           9.1085888104578971E-01, &
           1.2790530521078047E+00, &
           1.8183988075054034E+00, &
           2.5674048764458490E+00, &
           3.4742679490989756E+00, &
           4.2750073575469818E+00, &
           4.5415110499850497E+00, &
           4.0780393212586770E+00, &
           3.0715577369670211E+00, &
           1.7067500341042576E+00, &
           1.5354771422730309E-01, &
          -1.1626104903808991E+00, &
          -1.9391108802232515E+00, &
          -2.2669365075997252E+00, &
          -2.3465522176469396E+00, &
          -2.3061781721086212E+00, &
          -2.2036128189362643E+00, &
          -2.0639194679595287E+00, &
          -1.9009844916334115E+00, &
          -1.7265115183443687E+00, &
          -1.5507794199191016E+00, &
          -1.3823344958660737E+00, &
          -1.2267877639085096E+00, &
          -1.0873526167639007E+00, &
          -9.6490714865967786E-01, &
          -8.5916160995349111E-01, &
          -7.6879436814340385E-01, &
          -6.9235946367812229E-01, &
          -6.2810479374063310E-01, &
          -5.7459351357511934E-01, &
          -5.3032372853293286E-01, &
          -4.9421710451491380E-01, &
          -4.6516482931792025E-01, &
          -4.4247449795325572E-01, &
          -4.2539926453022475E-01, &
          -4.1357850722944678E-01, &
         -4.0656935672788841E-01/
        
        data d2ys /2.2397639298787909E-14, &
           4.3228717903649599E-02, &
           8.6774066045732637E-02, &
           1.3093107616720331E-01, &
           1.7596849247570090E-01, &
           2.2207762090558647E-01, &
           2.6937191503572905E-01, &
           3.1778768294816045E-01, &
           3.6706972112173503E-01, &
           4.1659383787982818E-01, &
           4.6531240091703552E-01, &
           5.1146000092071464E-01, &
           5.5244390030430168E-01, &
           5.8443099817145905E-01, &
           6.0227752891516873E-01, &
           5.9919498879418109E-01, &
           5.6714637843143223E-01, &
           4.9713343126053788E-01, &
           3.8053397725910365E-01, &
           2.0942863512170251E-01, &
          -2.4083230341830048E-02, &
          -3.3484907243463580E-01, &
          -7.5926290839857002E-01, &
          -1.3834306027360208E+00, &
          -2.3732257194429063E+00, &
          -3.9548821145599335E+00, &
          -6.1565098444263775E+00, &
          -8.2498209714552857E+00, &
          -9.0263109749039128E+00, &
          -8.3880672432698002E+00, &
          -7.0080428502813534E+00, &
          -4.9938353100392128E+00, &
          -2.4667534172277041E+00, &
          -1.5375588231449058E-01, &
           1.3498748104184550E+00, &
           2.0467047085736030E+00, &
           2.2183619907907963E+00, &
           2.1203902815016260E+00, &
           1.9091524660950809E+00, &
           1.6652531185896020E+00, &
           1.4257075035309397E+00, &
           1.2052897905804536E+00, &
           1.0081441962407383E+00, &
           8.3363821022049489E-01, &
           6.7920996895834651E-01, &
           5.4166310283054708E-01, &
           4.1775954580626373E-01, &
           3.0441659963632106E-01, &
           1.9877249944364450E-01, &
           9.8147813952012372E-02, &
           9.7202246251981703E-14, &
          -9.8147813952172105E-02, &
          -1.9877249944348477E-01, &
          -3.0441659963649270E-01, &
          -4.1775954580624786E-01, &
          -5.4166310283053265E-01, &
          -6.7920996895810570E-01, &
          -8.3363821022064999E-01, &
          -1.0081441962408684E+00, &
          -1.2052897905802957E+00, &
          -1.4257075035311584E+00, &
          -1.6652531185893422E+00, &
          -1.9091524660951125E+00, &
          -2.1203902815019449E+00, &
          -2.2183619907901377E+00, &
          -2.0467047085745040E+00, &
          -1.3498748104177167E+00, &
           1.5375588231377321E-01, &
           2.4667534172285475E+00, &
           4.9938353100391160E+00, &
           7.0080428502803969E+00, &
           8.3880672432713244E+00, &
           9.0263109749023798E+00, &
           8.2498209714565967E+00, &
           6.1565098444255364E+00, &
           3.9548821145601405E+00, &
           2.3732257194429009E+00, &
           1.3834306027362948E+00, &
           7.5926290839824961E-01, &
           3.3484907243429979E-01, &
           2.4083230342698072E-02, &
          -2.0942863512252349E-01, &
          -3.8053397725842997E-01, &
          -4.9713343126099330E-01, &
          -5.6714637843108018E-01, &
          -5.9919498879449373E-01, &
          -6.0227752891534181E-01, &
          -5.8443099817090682E-01, &
          -5.5244390030480250E-01, &
          -5.1146000092020361E-01, &
          -4.6531240091746101E-01, &
          -4.1659383787988363E-01, &
          -3.6706972112123737E-01, &
          -3.1778768294871496E-01, &
          -2.6937191503538072E-01, &
          -2.2207762090578315E-01, &
          -1.7596849247573032E-01, &
          -1.3093107616689692E-01, &
          -8.6774066046077958E-02, &
          -4.3228717903517122E-02/

        do i = 1, n
          ps(i) = xs(i)
          zs(i) = ys(i)
          dpdt(i) = dxs(i)
          dzdt(i) = dys(i)
          dpdt2(i) = d2xs(i)
          dzdt2(i) = d2ys(i)
        enddo
        
        return
        end


        


        subroutine load_ex1_n200(ps, zs, dpdt, dzdt, dpdt2, &
          dzdt2)
        implicit real *8 (a-h, o-z)

        parameter n = 200
        real *8 ps(*), zs(*), dpdt(*), dzdt(*), dpdt2(*)
        real *8 dzdt2(*)

        real *8 xs(n), ys(n), dxs(n), dys(n), d2xs(n), d2ys(n)
        
        data xs /1.9499999972733317E+00, &
           1.9498004467958838E+00, &
           1.9492012271243773E+00, &
           1.9482006296186944E+00, &
           1.9467957934385880E+00, &
           1.9449826857743038E+00, &
           1.9427560739514769E+00, &
           1.9401094891940001E+00, &
           1.9370351817695743E+00, &
           1.9335240671866081E+00, &
           1.9295656630595936E+00, &
           1.9251480162154491E+00, &
           1.9202576195786811E+00, &
           1.9148793183530874E+00, &
           1.9089962050179272E+00, &
           1.9025895026850066E+00, &
           1.8956384364301626E+00, &
           1.8881200923316781E+00, &
           1.8800092641360857E+00, &
           1.8712782877501022E+00, &
           1.8618968641522282E+00, &
           1.8518318718604738E+00, &
           1.8410471708204672E+00, &
           1.8295034005318742E+00, &
           1.8171577764535527E+00, &
           1.8039638902594173E+00, &
           1.7898715213873397E+00, &
           1.7748264695396512E+00, &
           1.7587704203224448E+00, &
           1.7416408589540020E+00, &
           1.7233710497385135E+00, &
           1.7038901014730210E+00, &
           1.6831231406667135E+00, &
           1.6609916147832029E+00, &
           1.6374137459388636E+00, &
           1.6123051508829174E+00, &
           1.5855796351625946E+00, &
           1.5571501582457583E+00, &
           1.5269299532018761E+00, &
           1.4948337720763933E+00, &
           1.4607792210890547E+00, &
           1.4246881551905379E+00, &
           1.3864881282731927E+00, &
           1.3461139538570825E+00, &
           1.3035095324823667E+00, &
           1.2586302572536974E+00, &
           1.2114465266967180E+00, &
           1.1619491739940901E+00, &
           1.1101579338197538E+00, &
           1.0561343013362918E+00, &
           9.9999999999999967E-01, &
           9.4196116852619427E-01, &
           8.8233545906563315E-01, &
           8.2157420413759741E-01, &
           7.6026686188004478E-01, &
           6.9911615849987563E-01, &
           6.3888498437035568E-01, &
           5.8033380613279151E-01, &
           5.2417246825848585E-01, &
           4.7103540555155454E-01, &
           4.2146950934694349E-01, &
           3.7591744284751571E-01, &
           3.3468868667314355E-01, &
           2.9792644237796095E-01, &
           2.6558875272108995E-01, &
           2.3745892613817821E-01, &
           2.1318575536963391E-01, &
           1.9233973447561326E-01, &
           1.7446751962501650E-01, &
           1.5913283116382204E-01, &
           1.4594068109181968E-01, &
           1.3454764917477713E-01, &
           1.2466288212063037E-01, &
           1.1604402322851592E-01, &
           1.0849100094955755E-01, &
           1.0183939883154836E-01, &
           9.5954274776324122E-02, &
           9.0724780331789545E-02, &
           8.6059652975242096E-02, &
           8.1883522942762021E-02, &
           7.8133925635828216E-02, &
           7.4758900945578577E-02, &
           7.1715069555062905E-02, &
           6.8966091714042443E-02, &
           6.6481430471110547E-02, &
           6.4235356443141489E-02, &
           6.2206144062880941E-02, &
           6.0375419787762508E-02, &
           5.8727631208044584E-02, &
           5.7249612691072982E-02, &
           5.5930228473155097E-02, &
           5.4760078254903144E-02, &
           5.3731253613860885E-02, &
           5.2837136114930905E-02, &
           5.2072230027327748E-02, &
           5.1432024164298706E-02, &
           5.0912878638986303E-02, &
           5.0511933345752436E-02, &
           5.0227035784869423E-02, &
           5.0056686492572777E-02, &
           5.0000000853774762E-02, &
           5.0056686492572555E-02, &
           5.0227035784869423E-02, &
           5.0511933345752436E-02, &
           5.0912878638986303E-02, &
           5.1432024164298817E-02, &
           5.2072230027327859E-02, &
           5.2837136114930905E-02, &
           5.3731253613860885E-02, &
           5.4760078254903366E-02, &
           5.5930228473154986E-02, &
           5.7249612691073204E-02, &
           5.8727631208044584E-02, &
           6.0375419787762619E-02, &
           6.2206144062880830E-02, &
           6.4235356443141489E-02, &
           6.6481430471110325E-02, &
           6.8966091714042665E-02, &
           7.1715069555063016E-02, &
           7.4758900945578688E-02, &
           7.8133925635828216E-02, &
           8.1883522942762021E-02, &
           8.6059652975242096E-02, &
           9.0724780331789434E-02, &
           9.5954274776324122E-02, &
           1.0183939883154836E-01, &
           1.0849100094955721E-01, &
           1.1604402322851592E-01, &
           1.2466288212063015E-01, &
           1.3454764917477724E-01, &
           1.4594068109181924E-01, &
           1.5913283116382249E-01, &
           1.7446751962501694E-01, &
           1.9233973447561337E-01, &
           2.1318575536963413E-01, &
           2.3745892613817809E-01, &
           2.6558875272109039E-01, &
           2.9792644237796140E-01, &
           3.3468868667314378E-01, &
           3.7591744284751594E-01, &
           4.2146950934694349E-01, &
           4.7103540555155532E-01, &
           5.2417246825848651E-01, &
           5.8033380613279184E-01, &
           6.3888498437035612E-01, &
           6.9911615849987563E-01, &
           7.6026686188004600E-01, &
           8.2157420413759819E-01, &
           8.8233545906563338E-01, &
           9.4196116852619416E-01, &
           9.9999999999999967E-01, &
           1.0561343013362929E+00, &
           1.1101579338197545E+00, &
           1.1619491739940906E+00, &
           1.2114465266967180E+00, &
           1.2586302572536974E+00, &
           1.3035095324823676E+00, &
           1.3461139538570832E+00, &
           1.3864881282731929E+00, &
           1.4246881551905379E+00, &
           1.4607792210890544E+00, &
           1.4948337720763942E+00, &
           1.5269299532018765E+00, &
           1.5571501582457583E+00, &
           1.5855796351625946E+00, &
           1.6123051508829183E+00, &
           1.6374137459388642E+00, &
           1.6609916147832033E+00, &
           1.6831231406667138E+00, &
           1.7038901014730210E+00, &
           1.7233710497385140E+00, &
           1.7416408589540024E+00, &
           1.7587704203224450E+00, &
           1.7748264695396514E+00, &
           1.7898715213873397E+00, &
           1.8039638902594177E+00, &
           1.8171577764535529E+00, &
           1.8295034005318742E+00, &
           1.8410471708204672E+00, &
           1.8518318718604736E+00, &
           1.8618968641522282E+00, &
           1.8712782877501024E+00, &
           1.8800092641360857E+00, &
           1.8881200923316781E+00, &
           1.8956384364301626E+00, &
           1.9025895026850068E+00, &
           1.9089962050179272E+00, &
           1.9148793183530874E+00, &
           1.9202576195786816E+00, &
           1.9251480162154491E+00, &
           1.9295656630595936E+00, &
           1.9335240671866081E+00, &
           1.9370351817695743E+00, &
           1.9401094891939996E+00, &
           1.9427560739514769E+00, &
           1.9449826857743036E+00, &
           1.9467957934385876E+00, &
           1.9482006296186942E+00, &
           1.9492012271243775E+00, &
           1.9498004467958838E+00/        

        data ys /0.0000000000000000E+00, &
           2.9848681529101227E-02, &
           5.9718679361286220E-02, &
           8.9631349040684899E-02, &
           1.1960812410048663E-01, &
           1.4967055380274025E-01, &
           1.7984033930976578E-01, &
           2.1013936765777022E-01, &
           2.4058974280766723E-01, &
           2.7121381292331964E-01, &
           3.0203419286986333E-01, &
           3.3307378073015398E-01, &
           3.6435576690071819E-01, &
           3.9590363404436557E-01, &
           4.2774114583892542E-01, &
           4.5989232206453096E-01, &
           4.9238139711014056E-01, &
           5.2523275844970052E-01, &
           5.5847086103838539E-01, &
           5.9212011291323663E-01, &
           6.2620472656105286E-01, &
           6.6074852986120880E-01, &
           6.9577472965929310E-01, &
           7.3130562033738955E-01, &
           7.6736222920472341E-01, &
           8.0396389025941739E-01, &
           8.4112773803030749E-01, &
           8.7886811400235987E-01, &
           9.1719587980285522E-01, &
           9.5611763413790396E-01, &
           9.9563483465234404E-01, &
           1.0357428315655341E+00, &
           1.0764298270006378E+00, &
           1.1176757818466150E+00, &
           1.1594512995869226E+00, &
           1.2017165216977299E+00, &
           1.2444200687184428E+00, &
           1.2874980504449562E+00, &
           1.3308731423219002E+00, &
           1.3744536768679916E+00, &
           1.4181326230182199E+00, &
           1.4617862182098327E+00, &
           1.5052718761072326E+00, &
           1.5484248191581831E+00, &
           1.5910526878904003E+00, &
           1.6329271784763293E+00, &
           1.6737716063957422E+00, &
           1.7132433059522600E+00, &
           1.7509102190056245E+00, &
           1.7862224312254344E+00, &
           1.8184826471945426E+00, &
           1.8468255628506360E+00, &
           1.8702243224141186E+00, &
           1.8875477585431852E+00, &
           1.8976831918968446E+00, &
           1.8997058100126538E+00, &
           1.8930312058271479E+00, &
           1.8774786254759299E+00, &
           1.8532258794291723E+00, &
           1.8207080763502261E+00, &
           1.7805337668368331E+00, &
           1.7334559881555234E+00, &
           1.6803845611911801E+00, &
           1.6223962666606881E+00, &
           1.5607033440509870E+00, &
           1.4965711224403908E+00, &
           1.4312097978471883E+00, &
           1.3656789262485405E+00, &
           1.3008310392854920E+00, &
           1.2372984654824812E+00, &
           1.1755118055551439E+00, &
           1.1157344476640014E+00, &
           1.0581008121221764E+00, &
           1.0026512421908311E+00, &
           9.4936065866493857E-01, &
           8.9816060116845187E-01, &
           8.4895544410213686E-01, &
           8.0163393923461723E-01, &
           7.5607720938593082E-01, &
           7.1216414001035155E-01, &
           6.6977490815092355E-01, &
           6.2879320160587426E-01, &
           5.8910753047978437E-01, &
           5.5061191845732138E-01, &
           5.1320617650684675E-01, &
           4.7679590062930188E-01, &
           4.4129229174431600E-01, &
           4.0661186506335123E-01, &
           3.7267609471211305E-01, &
           3.3941102427081832E-01, &
           3.0674686338717139E-01, &
           2.7461758332212555E-01, &
           2.4296051925183662E-01, &
           2.1171598369502043E-01, &
           1.8082689308858191E-01, &
           1.5023840796251547E-01, &
           1.1989758613274273E-01, &
           8.9753047671435660E-02, &
           5.9754650010107310E-02, &
           2.9853171296386878E-02, &
          -3.0554330316396606E-16, &
          -2.9853171296387076E-02, &
          -5.9754650010107511E-02, &
          -8.9753047671435840E-02, &
          -1.1989758613274293E-01, &
          -1.5023840796251611E-01, &
          -1.8082689308858210E-01, &
          -2.1171598369502059E-01, &
          -2.4296051925183682E-01, &
          -2.7461758332212566E-01, &
          -3.0674686338717205E-01, &
          -3.3941102427081837E-01, &
          -3.7267609471211366E-01, &
          -4.0661186506335140E-01, &
          -4.4129229174431622E-01, &
          -4.7679590062930272E-01, &
          -5.1320617650684708E-01, &
          -5.5061191845732205E-01, &
          -5.8910753047978448E-01, &
          -6.2879320160587437E-01, &
          -6.6977490815092378E-01, &
          -7.1216414001035189E-01, &
          -7.5607720938593159E-01, &
          -8.0163393923461779E-01, &
          -8.4895544410213730E-01, &
          -8.9816060116845209E-01, &
          -9.4936065866493924E-01, &
          -1.0026512421908325E+00, &
          -1.0581008121221771E+00, &
          -1.1157344476640014E+00, &
          -1.1755118055551439E+00, &
          -1.2372984654824823E+00, &
          -1.3008310392854932E+00, &
          -1.3656789262485411E+00, &
          -1.4312097978471885E+00, &
          -1.4965711224403901E+00, &
          -1.5607033440509885E+00, &
          -1.6223962666606888E+00, &
          -1.6803845611911801E+00, &
          -1.7334559881555238E+00, &
          -1.7805337668368331E+00, &
          -1.8207080763502277E+00, &
          -1.8532258794291734E+00, &
          -1.8774786254759304E+00, &
          -1.8930312058271477E+00, &
          -1.8997058100126538E+00, &
          -1.8976831918968444E+00, &
          -1.8875477585431850E+00, &
          -1.8702243224141195E+00, &
          -1.8468255628506365E+00, &
          -1.8184826471945426E+00, &
          -1.7862224312254336E+00, &
          -1.7509102190056240E+00, &
          -1.7132433059522596E+00, &
          -1.6737716063957420E+00, &
          -1.6329271784763295E+00, &
          -1.5910526878903999E+00, &
          -1.5484248191581826E+00, &
          -1.5052718761072319E+00, &
          -1.4617862182098327E+00, &
          -1.4181326230182203E+00, &
          -1.3744536768679907E+00, &
          -1.3308731423218996E+00, &
          -1.2874980504449558E+00, &
          -1.2444200687184428E+00, &
          -1.2017165216977295E+00, &
          -1.1594512995869219E+00, &
          -1.1176757818466148E+00, &
          -1.0764298270006376E+00, &
          -1.0357428315655341E+00, &
          -9.9563483465234315E-01, &
          -9.5611763413790318E-01, &
          -9.1719587980285455E-01, &
          -8.7886811400235976E-01, &
          -8.4112773803030760E-01, &
          -8.0396389025941661E-01, &
          -7.6736222920472297E-01, &
          -7.3130562033738911E-01, &
          -6.9577472965929288E-01, &
          -6.6074852986120880E-01, &
          -6.2620472656105219E-01, &
          -5.9212011291323596E-01, &
          -5.5847086103838506E-01, &
          -5.2523275844970041E-01, &
          -4.9238139711014056E-01, &
          -4.5989232206453029E-01, &
          -4.2774114583892481E-01, &
          -3.9590363404436529E-01, &
          -3.6435576690071814E-01, &
          -3.3307378073015398E-01, &
          -3.0203419286986266E-01, &
          -2.7121381292331920E-01, &
          -2.4058974280766693E-01, &
          -2.1013936765777003E-01, &
          -1.7984033930976584E-01, &
          -1.4967055380273961E-01, &
          -1.1960812410048616E-01, &
          -8.9631349040684608E-02, &
          -5.9718679361286130E-02, &
          -2.9848681529101292E-02/

        data dxs /2.6734687979288209E-14, &
          -1.2706790476320168E-02, &
          -2.5449776976106495E-02, &
          -3.8265405406205683E-02, &
          -5.1190623451262629E-02, &
          -6.4263136396038922E-02, &
          -7.7521668936436680E-02, &
          -9.1006234741319650E-02, &
          -1.0475841579492876E-01, &
          -1.1882165300739338E-01, &
          -1.3324154991655482E-01, &
          -1.4806619044084740E-01, &
          -1.6334647194765267E-01, &
          -1.7913645358049524E-01, &
          -1.9549371989021580E-01, &
          -2.1247975781094297E-01, &
          -2.3016034464681334E-01, &
          -2.4860594168863057E-01, &
          -2.6789208681180360E-01, &
          -2.8809977480095050E-01, &
          -3.0931581140431202E-01, &
          -3.3163312030475017E-01, &
          -3.5515097732117612E-01, &
          -3.7997513653034704E-01, &
          -4.0621780563004928E-01, &
          -4.3399741549982918E-01, &
          -4.6343811994505080E-01, &
          -4.9466894865231853E-01, &
          -5.2782252991660417E-01, &
          -5.6303329288270909E-01, &
          -6.0043506521242407E-01, &
          -6.4015799531105699E-01, &
          -6.8232476441788636E-01, &
          -7.2704610600709496E-01, &
          -7.7441573223528604E-01, &
          -8.2450486369727272E-01, &
          -8.7735667035651932E-01, &
          -9.3298101840036862E-01, &
          -9.9134995255942959E-01, &
          -1.0523942527329162E+00, &
          -1.1160011351422621E+00, &
          -1.1820126358889242E+00, &
          -1.2502233798470137E+00, &
          -1.3203752465790604E+00, &
          -1.3921449069741245E+00, &
          -1.4651183264559264E+00, &
          -1.5387442708737129E+00, &
          -1.6122570927930395E+00, &
          -1.6845591839453999E+00, &
          -1.7540593266358226E+00, &
          -1.8184826472134639E+00, &
          -1.8747117276674317E+00, &
          -1.9187878489179149E+00, &
          -1.9462534451173528E+00, &
          -1.9529353220235779E+00, &
          -1.9359567657634842E+00, &
          -1.8944181492073238E+00, &
          -1.8292876700665248E+00, &
          -1.7426834583360087E+00, &
          -1.6372174857809738E+00, &
          -1.5158810134851053E+00, &
          -1.3823926151646018E+00, &
          -1.2415404825294429E+00, &
          -1.0990447693374323E+00, &
          -9.6080862266626299E-01, &
          -8.3188628165805933E-01, &
          -7.1570017690902799E-01, &
          -6.1382568095164747E-01, &
          -5.2628198158703998E-01, &
          -4.5205514998884255E-01, &
          -3.8960531154486289E-01, &
          -3.3723827267205631E-01, &
          -2.9332786397929816E-01, &
          -2.5642356841389163E-01, &
          -2.2528571776908157E-01, &
          -1.9888145661268591E-01, &
          -1.7636286030348869E-01, &
          -1.5703930815033032E-01, &
          -1.4035019668033277E-01, &
          -1.2584061817763106E-01, &
          -1.1314080377868355E-01, &
          -1.0194926389502397E-01, &
          -9.2019197776960979E-02, &
          -8.3147646298305705E-02, &
          -7.5166876372315450E-02, &
          -6.7937551527693152E-02, &
          -6.1343315685994021E-02, &
          -5.5286490538686624E-02, &
          -4.9684646141246985E-02, &
          -4.4467856792914165E-02, &
          -3.9576492849697067E-02, &
          -3.4959432618353936E-02, &
          -3.0572601814568941E-02, &
          -2.6377768825392001E-02, &
          -2.2341537862731436E-02, &
          -1.8434495041605915E-02, &
          -1.4630470745468803E-02, &
          -1.0905889891992553E-02, &
          -7.2391868476933928E-03, &
          -3.6102669884367521E-03, &
          -1.6253147534197851E-14, &
           3.6102669884436802E-03, &
           7.2391868477143361E-03, &
           1.0905889891986239E-02, &
           1.4630470745467701E-02, &
           1.8434495041604358E-02, &
           2.2341537862732252E-02, &
           2.6377768825383827E-02, &
           3.0572601814586341E-02, &
           3.4959432618331052E-02, &
           3.9576492849720923E-02, &
           4.4467856792896797E-02, &
           4.9684646141261245E-02, &
           5.5286490538681912E-02, &
           6.1343315685992383E-02, &
           6.7937551527683535E-02, &
           7.5166876372321167E-02, &
           8.3147646298309189E-02, &
           9.2019197776970887E-02, &
           1.0194926389501985E-01, &
           1.1314080377866997E-01, &
           1.2584061817762604E-01, &
           1.4035019668034571E-01, &
           1.5703930815031575E-01, &
           1.7636286030349863E-01, &
           1.9888145661267650E-01, &
           2.2528571776908257E-01, &
           2.5642356841390723E-01, &
           2.9332786397928390E-01, &
           3.3723827267205858E-01, &
           3.8960531154486033E-01, &
           4.5205514998887841E-01, &
           5.2628198158700890E-01, &
           6.1382568095166423E-01, &
           7.1570017690900611E-01, &
           8.3188628165807443E-01, &
           9.6080862266627587E-01, &
           1.0990447693374137E+00, &
           1.2415404825294514E+00, &
           1.3823926151645813E+00, &
           1.5158810134851297E+00, &
           1.6372174857809911E+00, &
           1.7426834583359885E+00, &
           1.8292876700665248E+00, &
           1.8944181492073107E+00, &
           1.9359567657635153E+00, &
           1.9529353220235881E+00, &
           1.9462534451173286E+00, &
           1.9187878489179064E+00, &
           1.8747117276674021E+00, &
           1.8184826472135243E+00, &
           1.7540593266358144E+00, &
           1.6845591839453835E+00, &
           1.6122570927930409E+00, &
           1.5387442708736849E+00, &
           1.4651183264559722E+00, &
           1.3921449069741301E+00, &
           1.3203752465790348E+00, &
           1.2502233798470122E+00, &
           1.1820126358888881E+00, &
           1.1160011351423207E+00, &
           1.0523942527329178E+00, &
           9.9134995255940017E-01, &
           9.3298101840035652E-01, &
           8.7735667035653142E-01, &
           8.2450486369732090E-01, &
           7.7441573223525495E-01, &
           7.2704610600709618E-01, &
           6.8232476441786627E-01, &
           6.4015799531105666E-01, &
           6.0043506521245082E-01, &
           5.6303329288268511E-01, &
           5.2782252991659906E-01, &
           4.9466894865231792E-01, &
           4.6343811994503897E-01, &
           4.3399741549987636E-01, &
           4.0621780563001425E-01, &
           3.7997513653036846E-01, &
           3.5515097732114503E-01, &
           3.3163312030474096E-01, &
           3.0931581140435144E-01, &
           2.8809977480093912E-01, &
           2.6789208681177784E-01, &
           2.4860594168863870E-01, &
           2.3016034464680479E-01, &
           2.1247975781098544E-01, &
           1.9549371989019412E-01, &
           1.7913645358049962E-01, &
           1.6334647194765181E-01, &
           1.4806619044082744E-01, &
           1.3324154991658030E-01, &
           1.1882165300738713E-01, &
           1.0475841579491259E-01, &
           9.1006234741302414E-02, &
           7.7521668936445062E-02, &
           6.4263136396059781E-02, &
           5.1190623451257980E-02, &
           3.8265405406222010E-02, &
           2.5449776976102036E-02, &
           1.2706790476284820E-02/
        
        data dys /9.4999999727930229E-01, &
           9.5033915241861622E-01, &
           9.5135724372109265E-01, &
           9.5305614086089063E-01, &
           9.5543893377655476E-01, &
           9.5850989177362322E-01, &
           9.6227440420893984E-01, &
           9.6673889938489455E-01, &
           9.7191073892386515E-01, &
           9.7779808239082522E-01, &
           9.8440971734126759E-01, &
           9.9175484720331908E-01, &
           9.9984282948666736E-01, &
           1.0086828537031780E+00, &
           1.0182835480808308E+00, &
           1.0286525006885898E+00, &
           1.0397956799334127E+00, &
           1.0517167357080066E+00, &
           1.0644161617264871E+00, &
           1.0778902961672585E+00, &
           1.0921301376774997E+00, &
           1.1071199518131514E+00, &
           1.1228356452732691E+00, &
           1.1392428871078304E+00, &
           1.1562949638306261E+00, &
           1.1739303650051793E+00, &
           1.1920701138764154E+00, &
           1.2106148803833627E+00, &
           1.2294419471350593E+00, &
           1.2484021381186274E+00, &
           1.2673168673892179E+00, &
           1.2859755116167590E+00, &
           1.3041333496566450E+00, &
           1.3215103242128621E+00, &
           1.3377908429408649E+00, &
           1.3526247136448388E+00, &
           1.3656290631199000E+00, &
           1.3763906822451433E+00, &
           1.3844676424619149E+00, &
           1.3893882358789214E+00, &
           1.3906443274354114E+00, &
           1.3876751418398743E+00, &
           1.3798364368064240E+00, &
           1.3663490938139093E+00, &
           1.3462206358454214E+00, &
           1.3181336507652626E+00, &
           1.2802979466938706E+00, &
           1.2302717185988992E+00, &
           1.1647774363098828E+00, &
           1.0795808834635581E+00, &
           9.6957621597132482E-01, &
           8.2931332158450477E-01, &
           6.5423060854187909E-01, &
           4.4261549813566520E-01, &
           1.9763250703413313E-01, &
          -7.2004314585835019E-02, &
          -3.5384440392818961E-01, &
          -6.3526013496309475E-01, &
          -9.0636183654987112E-01, &
          -1.1605433497133339E+00, &
          -1.3930479127860491E+00, &
          -1.5991985880616850E+00, &
          -1.7737049327119123E+00, &
          -1.9114675109307004E+00, &
          -2.0092524227848365E+00, &
          -2.0670585251765767E+00, &
          -2.0883214135338131E+00, &
          -2.0789746739735251E+00, &
          -2.0460265005323892E+00, &
          -1.9963176195846861E+00, &
          -1.9357668084900339E+00, &
          -1.8690835720553411E+00, &
          -1.7997888074373751E+00, &
          -1.7303851244826296E+00, &
          -1.6625704954347027E+00, &
          -1.5974397456844829E+00, &
          -1.5356522023988413E+00, &
          -1.4775618309313918E+00, &
          -1.4233140310510755E+00, &
          -1.3729156197523933E+00, &
          -1.3262844237651714E+00, &
          -1.2832838693606876E+00, &
          -1.2437467789975942E+00, &
          -1.2074915081439992E+00, &
          -1.1743327126556855E+00, &
          -1.1440883776281885E+00, &
          -1.1165842727872195E+00, &
          -1.0916566457375132E+00, &
          -1.0691537299166358E+00, &
          -1.0489364614721177E+00, &
          -1.0308786871972049E+00, &
          -1.0148670509265332E+00, &
          -1.0008006950859452E+00, &
          -9.8859086316893885E-01, &
          -9.7816046841648496E-01, &
          -9.6944366478413091E-01, &
          -9.6238545065074133E-01, &
          -9.5694131744180666E-01, &
          -9.5307695713760121E-01, &
          -9.5076802996804954E-01, &
          -9.4999999917814548E-01, &
          -9.5076802996802390E-01, &
          -9.5307695713761742E-01, &
          -9.5694131744179789E-01, &
          -9.6238545065075698E-01, &
          -9.6944366478413047E-01, &
          -9.7816046841647053E-01, &
          -9.8859086316894706E-01, &
          -1.0008006950859265E+00, &
          -1.0148670509265656E+00, &
          -1.0308786871971956E+00, &
          -1.0489364614721077E+00, &
          -1.0691537299166609E+00, &
          -1.0916566457374848E+00, &
          -1.1165842727872488E+00, &
          -1.1440883776281765E+00, &
          -1.1743327126557004E+00, &
          -1.2074915081439757E+00, &
          -1.2437467789975838E+00, &
          -1.2832838693607027E+00, &
          -1.3262844237651634E+00, &
          -1.3729156197523984E+00, &
          -1.4233140310510854E+00, &
          -1.4775618309313792E+00, &
          -1.5356522023988473E+00, &
          -1.5974397456844798E+00, &
          -1.6625704954347265E+00, &
          -1.7303851244826216E+00, &
          -1.7997888074373551E+00, &
          -1.8690835720553303E+00, &
          -1.9357668084900550E+00, &
          -1.9963176195847121E+00, &
          -2.0460265005323786E+00, &
          -2.0789746739735167E+00, &
          -2.0883214135337704E+00, &
          -2.0670585251766074E+00, &
          -2.0092524227848951E+00, &
          -1.9114675109306243E+00, &
          -1.7737049327119501E+00, &
          -1.5991985880616526E+00, &
          -1.3930479127860769E+00, &
          -1.1605433497133557E+00, &
          -9.0636183654984359E-01, &
          -6.3526013496307032E-01, &
          -3.5384440392817340E-01, &
          -7.2004314585877291E-02, &
           1.9763250703417598E-01, &
           4.4261549813562512E-01, &
           6.5423060854187343E-01, &
           8.2931332158453097E-01, &
           9.6957621597132915E-01, &
           1.0795808834635814E+00, &
           1.1647774363098629E+00, &
           1.2302717185989080E+00, &
           1.2802979466938578E+00, &
           1.3181336507652532E+00, &
           1.3462206358454345E+00, &
           1.3663490938139153E+00, &
           1.3798364368064224E+00, &
           1.3876751418398396E+00, &
           1.3906443274354428E+00, &
           1.3893882358789522E+00, &
           1.3844676424618911E+00, &
           1.3763906822451408E+00, &
           1.3656290631198791E+00, &
           1.3526247136448744E+00, &
           1.3377908429408416E+00, &
           1.3215103242128761E+00, &
           1.3041333496566148E+00, &
           1.2859755116167813E+00, &
           1.2673168673892372E+00, &
           1.2484021381186108E+00, &
           1.2294419471350511E+00, &
           1.2106148803833370E+00, &
           1.1920701138764493E+00, &
           1.1739303650051875E+00, &
           1.1562949638306066E+00, &
           1.1392428871078391E+00, &
           1.1228356452732431E+00, &
           1.1071199518131740E+00, &
           1.0921301376775212E+00, &
           1.0778902961672421E+00, &
           1.0644161617264718E+00, &
           1.0517167357080166E+00, &
           1.0397956799334147E+00, &
           1.0286525006886058E+00, &
           1.0182835480808248E+00, &
           1.0086828537031667E+00, &
           9.9984282948665060E-01, &
           9.9175484720333684E-01, &
           9.8440971734128357E-01, &
           9.7779808239079569E-01, &
           9.7191073892387603E-01, &
           9.6673889938488056E-01, &
           9.6227440420895105E-01, &
           9.5850989177363943E-01, &
           9.5543893377653955E-01, &
           9.5305614086088919E-01, &
           9.5135724372107522E-01, &
           9.5033915241862876E-01/
        
        data d2xs /-4.0427794058706623E-01, &
          -4.0485335893240859E-01, &
          -4.0658357556942876E-01, &
          -4.0948054786350130E-01, &
          -4.1356428302092868E-01, &
          -4.1886303662322988E-01, &
          -4.2541349107416448E-01, &
          -4.3326107561328520E-01, &
          -4.4246026157293700E-01, &
          -4.5307499169493554E-01, &
          -4.6517907331678005E-01, &
          -4.7885668862427122E-01, &
          -4.9420284421353822E-01, &
          -5.1132390268139316E-01, &
          -5.3033800459662628E-01, &
          -5.5137550526973067E-01, &
          -5.7457921072979623E-01, &
          -6.0010450666225323E-01, &
          -6.2811912607786258E-01, &
          -6.5880260109404287E-01, &
          -6.9234508633692404E-01, &
          -7.2894552975043037E-01, &
          -7.6880879953728654E-01, &
          -8.1214165716801245E-01, &
          -8.5914710004287997E-01, &
          -9.1001689106306771E-01, &
          -9.6492175901496191E-01, &
          -1.0239991100046408E+00, &
          -1.0873378627758079E+00, &
          -1.1549605360223312E+00, &
          -1.2268027112377542E+00, &
          -1.3026907897096183E+00, &
          -1.3823192702133937E+00, &
          -1.4652298448484320E+00, &
          -1.5507950377537714E+00, &
          -1.6382099547423048E+00, &
          -1.7264953232677174E+00, &
          -1.8145141847210360E+00, &
          -1.9010015365204862E+00, &
          -1.9846017397917455E+00, &
          -2.0639011342406843E+00, &
          -2.1374345049697259E+00, &
          -2.2036331191717706E+00, &
          -2.2606710319341246E+00, &
          -2.3061548421701112E+00, &
          -2.3365931127172046E+00, &
          -2.3465801121807330E+00, &
          -2.3276551209502205E+00, &
          -2.2669022078855949E+00, &
          -2.1456397849547155E+00, &
          -1.9391524306707131E+00, &
          -1.6193298495174298E+00, &
          -1.1625651508102919E+00, &
          -5.6322345168999166E-01, &
           1.5350881326440913E-01, &
           9.3207815067570277E-01, &
           1.7067726877272889E+00, &
           2.4279360257174698E+00, &
           3.0715509102501826E+00, &
           3.6269776830754186E+00, &
           4.0780367764521701E+00, &
           4.3946354385439266E+00, &
           4.5415174359270383E+00, &
           4.4981299130890875E+00, &
           4.2750011588971919E+00, &
           3.9141868962748156E+00, &
           3.4742712600552683E+00, &
           3.0114562576302397E+00, &
           2.5674053813772804E+00, &
           2.1663881273889780E+00, &
           1.8183948661450757E+00, &
           1.5240751790646418E+00, &
           1.2790596333038500E+00, &
           1.0769137151083421E+00, &
           9.1085040930093042E-01, &
           7.7458035879833342E-01, &
           6.6264720762884788E-01, &
           5.7048947219333401E-01, &
           4.9437482731169607E-01, &
           4.3128640565472692E-01, &
           3.7880117205403835E-01, &
           3.3497798917733440E-01, &
           2.9826184817461804E-01, &
           2.6740491738927552E-01, &
           2.4140302758780030E-01, &
           2.1944511020419341E-01, &
           2.0087337569430544E-01, &
           1.8515194920269992E-01, &
           1.7184234552646488E-01, &
           1.6058418295545138E-01, &
           1.5108015630536908E-01, &
           1.4308421386692591E-01, &
           1.3639240290082802E-01, &
           1.3083567946894939E-01, &
           1.2627442032929909E-01, &
           1.2259414214416559E-01, &
           1.1970232653069850E-01, &
           1.1752597978162896E-01, &
           1.1600992326086050E-01, &
           1.1511552221025294E-01, &
           1.1481991657676986E-01, &
           1.1511552221196610E-01, &
           1.1600992325988756E-01, &
           1.1752597978158250E-01, &
           1.1970232653052491E-01, &
           1.2259414214469061E-01, &
           1.2627442032843844E-01, &
           1.3083567947007885E-01, &
           1.3639240289989243E-01, &
           1.4308421386726347E-01, &
           1.5108015630580868E-01, &
           1.6058418295446375E-01, &
           1.7184234552779146E-01, &
           1.8515194920133018E-01, &
           2.0087337569513219E-01, &
           2.1944511020349025E-01, &
           2.4140302758885462E-01, &
           2.6740491738855865E-01, &
           2.9826184817529078E-01, &
           3.3497798917613553E-01, &
           3.7880117205454739E-01, &
           4.3128640565493376E-01, &
           4.9437482731194110E-01, &
           5.7048947219241930E-01, &
           6.6264720763025420E-01, &
           7.7458035879647824E-01, &
           9.1085040930322558E-01, &
           1.0769137151064323E+00, &
           1.2790596333048900E+00, &
           1.5240751790636193E+00, &
           1.8183948661469840E+00, &
           2.1663881273875631E+00, &
           2.5674053813770144E+00, &
           3.0114562576310777E+00, &
           3.4742712600539933E+00, &
           3.9141868962770969E+00, &
           4.2750011588950123E+00, &
           4.4981299130900245E+00, &
           4.5415174359265968E+00, &
           4.3946354385439257E+00, &
           4.0780367764538434E+00, &
           3.6269776830735498E+00, &
           3.0715509102504130E+00, &
           2.4279360257175049E+00, &
           1.7067726877275033E+00, &
           9.3207815067716804E-01, &
           1.5350881326201374E-01, &
          -5.6322345168910570E-01, &
          -1.1625651508113872E+00, &
          -1.6193298495157784E+00, &
          -1.9391524306694239E+00, &
          -2.1456397849581070E+00, &
          -2.2669022078833523E+00, &
          -2.3276551209519432E+00, &
          -2.3465801121793390E+00, &
          -2.3365931127159483E+00, &
          -2.3061548421727798E+00, &
          -2.2606710319330046E+00, &
          -2.2036331191727370E+00, &
          -2.1374345049687760E+00, &
          -2.0639011342384328E+00, &
          -1.9846017397957050E+00, &
          -1.9010015365187132E+00, &
          -1.8145141847220430E+00, &
          -1.7264953232647136E+00, &
          -1.6382099547445348E+00, &
          -1.5507950377542392E+00, &
          -1.4652298448478549E+00, &
          -1.3823192702140981E+00, &
          -1.3026907897080848E+00, &
          -1.2268027112382185E+00, &
          -1.1549605360239155E+00, &
          -1.0873378627736803E+00, &
          -1.0239991100067494E+00, &
          -9.6492175901226229E-01, &
          -9.1001689106455397E-01, &
          -8.5914710004340633E-01, &
          -8.1214165716684728E-01, &
          -7.6880879953960690E-01, &
          -7.2894552974751992E-01, &
          -6.9234508633746006E-01, &
          -6.5880260109584754E-01, &
          -6.2811912607669740E-01, &
          -6.0010450666245263E-01, &
          -5.7457921072904661E-01, &
          -5.5137550526940038E-01, &
          -5.3033800459880054E-01, &
          -5.1132390267899064E-01, &
          -4.9420284421593508E-01, &
          -4.7885668862239739E-01, &
          -4.6517907331707775E-01, &
          -4.5307499169560783E-01, &
          -4.4246026157328677E-01, &
          -4.3326107561270666E-01, &
          -4.2541349107344389E-01, &
          -4.1886303662338309E-01, &
          -4.1356428302169257E-01, &
          -4.0948054786203869E-01, &
          -4.0658357557229502E-01, &
          -4.0485335892986940E-01/
        
        data d2ys /-8.5863708681129217E-13, &
           2.1594611792018607E-02, &
           4.3229024455985546E-02, &
           6.4942541183698244E-02, &
           8.6773447409284402E-02, &
           1.0875844528969800E-01, &
           1.3093201817391362E-01, &
           1.5332569851206002E-01, &
           1.7596720957213891E-01, &
           1.9887944794419282E-01, &
           2.2207926921260218E-01, &
           2.4557603437977499E-01, &
           2.6936986872793406E-01, &
           2.9344957913483954E-01, &
           3.1779016943271499E-01, &
           3.4234988843858344E-01, &
           3.6706674060106775E-01, &
           3.9185438874166401E-01, &
           4.1659738070386515E-01, &
           4.4114564356445418E-01, &
           4.6530820926817901E-01, &
           4.8884617406964348E-01, &
           5.1146495117551405E-01, &
           5.3280596445284401E-01, &
           5.5243805131160462E-01, &
           5.6984900921825410E-01, &
           5.8443792575428177E-01, &
           5.9550918298119770E-01, &
           6.0226928973676308E-01, &
           6.0382794202559398E-01, &
           5.9920484365999738E-01, &
           5.8734372902581367E-01, &
           5.6713450699838774E-01, &
           5.3744327681954540E-01, &
           4.9714785079031076E-01, &
           4.4517315371207755E-01, &
           3.8051630126638186E-01, &
           3.0224548949650482E-01, &
           2.0945050567997611E-01, &
           1.0111667677466769E-01, &
          -2.4110512173282795E-02, &
          -1.6830475507077466E-01, &
          -3.3481490397834324E-01, &
          -5.2902238449148420E-01, &
          -7.5930544217511542E-01, &
          -1.0381682813563673E+00, &
          -1.3833792017926294E+00, &
          -1.8187026287116261E+00, &
          -2.3732827988785159E+00, &
          -3.0778421697495917E+00, &
          -3.9548317845141203E+00, &
          -4.9998677451367062E+00, &
          -6.1565272455500191E+00, &
          -7.2989909048993811E+00, &
          -8.2498627764485040E+00, &
          -8.8475886746170840E+00, &
          -9.0262174029664486E+00, &
          -8.8365557837065474E+00, &
          -8.3881752992048071E+00, &
          -7.7693412243249282E+00, &
          -7.0079474185852595E+00, &
          -6.0874648304711911E+00, &
          -4.9939100732399542E+00, &
          -3.7583728750366934E+00, &
          -2.4666996544654309E+00, &
          -1.2321353349438107E+00, &
          -1.5379121402478405E-01, &
           7.1096257966629484E-01, &
           1.3498962627598150E+00, &
           1.7831307187815200E+00, &
           2.0466924355267064E+00, &
           2.1799136148141720E+00, &
           2.2183686586861620E+00, &
           2.1911073319612075E+00, &
           2.1203869062998910E+00, &
           2.0225362180737014E+00, &
           1.9091539438159912E+00, &
           1.7882592201760401E+00, &
           1.6652527186563677E+00, &
           1.5436637676812217E+00, &
           1.4257073084037029E+00, &
           1.3126882990620581E+00, &
           1.2052902950992208E+00, &
           1.1037788538427196E+00, &
           1.0081435544935333E+00, &
           9.1819631762564091E-01, &
           8.3363888505206574E-01, &
           7.5410864461830640E-01, &
           6.7920932529920153E-01, &
           6.0853114929399543E-01, &
           5.4166367601586518E-01, &
           4.7820359610382263E-01, &
           4.1775906759336534E-01, &
           3.5995170013361588E-01, &
           3.0441696807381330E-01, &
           2.5080358967163291E-01, &
           1.9877224977309851E-01, &
           1.4799391872639384E-01, &
           9.8147939916313764E-02, &
           4.8919993065564424E-02, &
           1.3866069039170485E-12, &
          -4.8919993067309736E-02, &
          -9.8147939915931778E-02, &
          -1.4799391872644635E-01, &
          -1.9877224977360186E-01, &
          -2.5080358967056893E-01, &
          -3.0441696807435109E-01, &
          -3.5995170013351924E-01, &
          -4.1775906759319864E-01, &
          -4.7820359610552626E-01, &
          -5.4166367601273391E-01, &
          -6.0853114929715768E-01, &
          -6.7920932529686529E-01, &
          -7.5410864461951477E-01, &
          -8.3363888505200057E-01, &
          -9.1819631762525245E-01, &
          -1.0081435544938515E+00, &
          -1.1037788538413782E+00, &
          -1.2052902951007858E+00, &
          -1.3126882990617981E+00, &
          -1.4257073084031311E+00, &
          -1.5436637676823231E+00, &
          -1.6652527186552746E+00, &
          -1.7882592201764760E+00, &
          -1.9091539438158429E+00, &
          -2.0225362180742041E+00, &
          -2.1203869063000038E+00, &
          -2.1911073319599494E+00, &
          -2.2183686586863520E+00, &
          -2.1799136148147782E+00, &
          -2.0466924355277505E+00, &
          -1.7831307187806495E+00, &
          -1.3498962627590954E+00, &
          -7.1096257966627319E-01, &
           1.5379121402571586E-01, &
           1.2321353349389748E+00, &
           2.4666996544702129E+00, &
           3.7583728750361565E+00, &
           4.9939100732380561E+00, &
           6.0874648304737118E+00, &
           7.0079474185813675E+00, &
           7.7693412243285396E+00, &
           8.3881752992037306E+00, &
           8.8365557837080999E+00, &
           9.0262174029633631E+00, &
           8.8475886746187680E+00, &
           8.2498627764489267E+00, &
           7.2989909048967112E+00, &
           6.1565272455533977E+00, &
           4.9998677451351750E+00, &
           3.9548317845151639E+00, &
           3.0778421697485330E+00, &
           2.3732827988783605E+00, &
           1.8187026287122301E+00, &
           1.3833792017915238E+00, &
           1.0381682813574578E+00, &
           7.5930544217510831E-01, &
           5.2902238449141525E-01, &
           3.3481490397746200E-01, &
           1.6830475507073686E-01, &
           2.4110512176182784E-02, &
          -1.0111667677756586E-01, &
          -2.0945050567924398E-01, &
          -3.0224548949741853E-01, &
          -3.8051630126489555E-01, &
          -4.4517315371239713E-01, &
          -4.9714785079109747E-01, &
          -5.3744327681878790E-01, &
          -5.6713450699972645E-01, &
          -5.8734372902287602E-01, &
          -5.9920484366261906E-01, &
          -6.0382794202472478E-01, &
          -6.0226928973788540E-01, &
          -5.9550918298003996E-01, &
          -5.8443792575312026E-01, &
          -5.6984900922032755E-01, &
          -5.5243805131074153E-01, &
          -5.3280596445319506E-01, &
          -5.1146495117578128E-01, &
          -4.8884617406766567E-01, &
          -4.6530820926985261E-01, &
          -4.4114564356483377E-01, &
          -4.1659738070337027E-01, &
          -3.9185438874117667E-01, &
          -3.6706674060137118E-01, &
          -3.4234988843809361E-01, &
          -3.1779016943395333E-01, &
          -2.9344957913434028E-01, &
          -2.6936986872831992E-01, &
          -2.4557603437774603E-01, &
          -2.2207926921497048E-01, &
          -1.9887944794341933E-01, &
          -1.7596720957191825E-01, &
          -1.5332569851256295E-01, &
          -1.3093201817260097E-01, &
          -1.0875844529078744E-01, &
          -8.6773447409416282E-02, &
          -6.4942541183570276E-02, &
          -4.3229024456252103E-02, &
          -2.1594611790703554E-02/

        do i = 1, n
          ps(i) = xs(i)
          zs(i) = ys(i)
          dpdt(i) = dxs(i)
          dzdt(i) = dys(i)
          dpdt2(i) = d2xs(i)
          dzdt2(i) = d2ys(i)
        enddo
                
        return
        end
