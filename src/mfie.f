c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       this is the end of the debugging code, and the beginning
c       of the routines that will be used to solve the
c       axisymmetric mfie
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       a few user-callable routines are available in this file,
c       they are:
c
c       axisolve_mfie - solves a full scattering problem, mode by mode
c
c       eloop -
c
c       hloop - 
c
c       curlaeval - evaluates the curl of A at an arbitrary point
c           off the surface, curl of A is just the H field
c
c       creanxcurlmat - constructs a 2n x 2n matrix which maps
c           surface currents to surface H fields
c
c       efieldeval - evaluates the E field i*k*A - grad phi at an
c           arbitrary point off the surface
c
c       efieldeval2 - evaluates the E field i*k*A - grad phi at an
c           arbitrary point off the surface using explicit knowledge
c           of the electric charge rho - otherwise same as efieldeval
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
        subroutine axisolve_mfie(eps,zk,modemax,n,rl,ps,zs,dpdt,
     1      dzdt,norder,esurf,hsurf,nmodes,ind,cintb,zjmodes,
     2      rhomodes,nphi,npts,xyzs,whts,zcurrent,zcharge,aint)
        implicit real *8 (a-h,o-z)
        integer *4 ipvt(10000)
        real *8 ps(1),zs(1),dpdt(1),
     1      dzdt(1),dpdt2(10000),dzdt2(10000),dsdt(10000),
     2      xyzs(3,1),whts(1)
        complex *16 zk,cd,ima,cd1,cd2,cd3,esurf(3,n,nmodes),
     1      hsurf(3,n,nmodes),zjmodes(2,n,-modemax:modemax),
     2      zcurrent(3,1),zcharge(1),
     2      rhomodes(n,-modemax:modemax),amat(4000000),
     3      wz(10000000),wz2(10000000),wz3(10000000),sz(100000),
     4      work(20000000),cintb,htan(2,10000),
     5      rhs(100000),sol(100000),
     6      vals1(100),vals2(100),vals3(100),
     7      ctemp1(10000),u(10000),zjrhs(2,10000),avals(1000000),
     8      aint
c
        complex *16, allocatable :: emodes(:,:,:),hmodes(:,:,:)
        complex *16, allocatable :: vecmat(:,:),amodes(:)
c
c       this routine solves, mode by mode, a full exterior surface of
c       revolution scattering problem by first solving the MFIE, and
c       then solving a secondary equation (n \cdot E = \rho) for the
c       surface charge. This avoids low-frequency breakdown in the
c       recovery of \rho. In the zero-mode, an additional stabilizing
c       condition on the vector potential is added to the system matrix
c       (line integral of the vector potential). See our IEEE paper...
c
c       input:
c
c         eps -
c         zk -
c         modemax -
c         n -
c         rl -
c         ps,zs -
c         dpdt,dzdt -
c         norder -
c         esurf,hsurf -
c         nmodes -
c         ind -
c         cintb -
c         nphi - 
c
c       output:
c
c         zjmodes -
c         rhomodes -
c         zcurrent - 
c         zcharge -
c
c
        done=1
        ima=(0,1)
        pi=4*atan(1.0d0)
        h=rl/n
c
        do 1400 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 1400   continue

        allocate( vecmat(3*n,2*n) )
        allocate( amodes(-modemax:modemax) )


c
c       decompose the incoming field esurf, hsurf
c
        allocate( emodes(3,n,nmodes) )
        allocate( hmodes(3,n,nmodes) )
        call axideco(n,nmodes,esurf,hsurf,emodes,hmodes)


        do 2000 i=1,nmodes

          dd1=0
          dd2=0
          do 1800 j=1,n
            do 1600 k=1,3
              dd1=dd1+abs(emodes(k,j,i))**2
              dd2=dd2+abs(hmodes(k,j,i))**2
 1600       continue
 1800     continue
          
          if (i-1 .eq. modemax) prec=sqrt(dd2/3/n)
        
cccc        call prinf('mode=*',i,1)
cccc        call prin2('e norm=*',sqrt(dd1/3/n),1)
cccc        call prin2('h norm=*',sqrt(dd2/3/n),1)
 2000 continue


        print *
        print *
        call prin2('precision of resolution in H=*',prec,1)
c
c       loop over all modes from -modemax to modemax, solve each
c       problem, then recombine
c
        do ijkmode=-modemax,modemax
c
        mode=ijkmode
        call prinf('. . . calculating mode*',mode,1)

c
c       construct the mfie matrix to invert, add in extra condition
c       if in the mode=0 case
c
        diag=-.5d0
        call creanxcurlmat(eps,zk,mode,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,amat)
c
        iftan=1
        call creavecpotmat(eps,zk,mode,n,rl,ps,zs,dpdt,dzdt,
     1      norder,iftan,vecmat,wz,wz2,wz3)


        iffix=0
        if ((mode .eq. 0) .and. (iffix .ne. 0)) then
          i3=3*ind-1
          call getrow(3*n,2*n,vecmat,i3,u)
          do i=1,2*n
            u(i)=u(i)*2*pi*ps(ind)*ima
          enddo
          ifrand=0
          call addrowall(2*n,2*n,amat,ifrand,u)
        endif

c
c       maybe compute the svd . . .
c
        if (1 .eq. 0) then
          nnn=2*n
          eps2=1.0d-14
          lw=10000000
          call csvdpiv(ier,amat,nnn,nnn,wz,wz2,sz,ncols,eps2,
     1        work,lw,ltot)
          call prinf('after csvdpivot, ncols=*',ncols,1)
          call prin2('singular values=*',sz,2*n)
          dd=sz(1)/sz(n)
          if (ncols .lt. nnn) dd=1.0d15
          call prin2('est condition number of full matrix=*',dd,1)
          stop
        endif

c
c       construct the rhs for the mfie
c
        if (ijkmode .eq. 0) modeind=1
        if (ijkmode .gt. 0) modeind=ijkmode+1
        if (ijkmode .lt. 0) modeind=nmodes+ijkmode+1
c
        do 3400 i=1,n
          dpds=dpdt(i)/dsdt(i)
          dzds=dzdt(i)/dsdt(i)
          htan(1,i)=dpds*hmodes(1,i,modeind)
     1        +dzds*hmodes(3,i,modeind)
          htan(2,i)=hmodes(2,i,modeind)
 3400 continue
c
        do 3500 i=1,n
          zjrhs(1,i)=-htan(2,i)
          zjrhs(2,i)=htan(1,i)
 3500   continue

        if ((mode .eq. 0) .and. (iffix .ne. 0)) then
          do i=1,n
            zjrhs(1,i)=zjrhs(1,i)-cintb
            zjrhs(2,i)=zjrhs(2,i)-cintb
          enddo
        endif

c
c       solve the system for the current
c
        nnn=2*n
        call ccopy601(nnn,zjrhs,sol)
        call ccopy601(nnn**2,amat,work)
c
        ijob=0
        call zgeco(work,nnn,nnn,ipvt,rcond,wz)
        call prin2('for mfie, from zgeco, rcond=*',1/rcond,1)
        call zgesl(work,nnn,nnn,ipvt,sol,ijob)
c
        call ccopy601(nnn,sol,zjmodes(1,1,mode))


c
c       compute the vector potential onsurface
c
        call cmatvec601(3*n,2*n,vecmat,zjmodes(1,1,mode),ctemp1)
        i3=3*ind-1
        amodes(mode)=ctemp1(i3)

c
c       now solve an additional equation for the charge, rho
c
        diag=-done/2
        call creaauge(eps,zk,mode,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,zjmodes(1,1,mode),amat,rhs)
c
        if (mode .eq. 0) call addones(n,amat,ps,h,dsdt)

c
c       calculate E_in \cdot n and adjust the rhs
c
        call ndotdirect(n,dpdt,dzdt,emodes(1,1,modeind),ctemp1)
c
        do 4200 i=1,n
          rhs(i)=rhs(i)-ctemp1(i)
 4200 continue

c
c       and solve
c
        call ccopy601(n,rhs,sol)
c
        ijob=0
        call zgeco(amat,n,n,ipvt,rcond,wz)
        call prin2('for rho, from zgeco, rcond=*',1/rcond,1)
        call zgesl(amat,n,n,ipvt,sol,ijob)
c
        call ccopy601(n,sol,rhomodes(1,mode))
c
      end do

c
c       synthesize the current and charge, evaluate at nphi points
c       and construct the smooth quadrature weights
c
        call axisynth1(n,nphi,rl,ps,zs,dpdt,dzdt,modemax,rhomodes,
     1      npts,xyzs,whts,zcharge)
c
        call axisynth3(n,nphi,rl,ps,zs,dpdt,dzdt,modemax,zjmodes,
     1      npts,xyzs,whts,zcurrent)

c
c       synthesize the vector potential
c
        h=2*pi/nphi
        do j=1,nphi
          phi=(j-1)*h
          sn=sin(phi)
          cs=cos(phi)
          avals(j)=0
          do mode=-modemax,modemax
            cd=exp(ima*mode*phi)
            avals(j)=avals(j)+amodes(mode)*cd
          enddo
        enddo
c
c       and compute the integral a \cdot ds on the ps(ind) loop
c
        ds=ps(ind)*2*pi/nphi
        aint=0
	do i=1,nphi
          aint=aint+avals(i)*ds
        enddo
c
        call prin2('integral of A scattered dot ds is =*',aint,2)
        write(6,*) 'aint = ',aint
        write(13,*) 'aint = ',aint
c
        return
        end
c
c
c
c
c
        subroutine creaauge(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,zjs,amat,rhs)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000),
     1      rnp(10000),rnz(10000)
        complex *16 zk,zjs(2,1),amat(n,n),rhs(1),ima,
     1      wa(20000000),wcc(20000000),wss(20000000),w(20000000),
     2      work(10000000),vecpot(3,10000),cdiag
        external ghfun4n1,ghfun2,ghfun2cc,ghfun2ss
c
c       this routine solves the n \cdot E for \rho, assuming knownledge
c       of the surface current j - used in conjunction with the MFIE to
c       accurate reconstruct the charge (and therefore the electric
c       field)
c
        ima=(0,1)
        done=1
        pi=4*atan(done)

c
c       first construct the rhs, which is n dot ikA(J)
c
        do 1400 i=1,n
          dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
 1400 continue                
c

        h=rl/n
        call formslpmatbac(ier,w,norder,ps,zs,dsdt,h,n,
     1      ghfun2,eps,zk,m)
c
        call formslpmatbac(ier,wcc,norder,ps,zs,dsdt,h,n,
     1      ghfun2cc,eps,zk,m)
c
        call formslpmatbac(ier,wss,norder,ps,zs,dsdt,h,n,
     1      ghfun2ss,eps,zk,m)

        iftan=0
        call creavecpotmat7(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,iftan,wa,wcc,wss,w)
c
        call cmatvec601(3*n,2*n,wa,zjs,vecpot)
        call ndotdirect(n,dpdt,dzdt,vecpot,rhs)

        do 1600 i=1,n
        rhs(i)=-rhs(i)*ima*zk
 1600 continue


c
c       construct s'_k
c
        do 2000 i=1,n
        rnp(i)=dzdt(i)/dsdt(i)
        rnz(i)=-dpdt(i)/dsdt(i)
 2000 continue
c
        h=rl/n
        call formsprimematbac(ier,amat,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun4n1,eps,zk,m)

c
c       and add in the diagonal
c
        do 2800 i=1,n
        do 2600 j=1,n
        amat(i,j)=-amat(i,j)
 2600 continue
        amat(i,i)=amat(i,i)+diag
 2800 continue
c
        return
        end
c
c
c
c
c

        subroutine cart2cyl(n,pts,vecin,vecout)
        implicit real *8 (a-h,o-z)
        real *8 pts(3,1)
        complex *16 vecin(3,1),vecout(3,1),ima,cdx,cdy
c
c       convert a cartesian vector field into a cylindrical one
c
        done=1
        ima=(0,1)
        pi=4*atan(done)
c
        do 2200 i=1,n
c
        x=pts(1,i)
        y=pts(2,i)
        r=sqrt(x**2+y**2)
        phi=atan2(y,x)
c
        cdx=vecin(1,i)
        cdy=vecin(2,i)
        vecout(1,i)=cdx*cos(phi)+cdy*sin(phi)
        vecout(2,i)=-cdx*sin(phi)+cdy*cos(phi)
        vecout(3,i)=vecin(3,i)
c
 2200 continue
c
        return
        end
c
c
c
c
c
        subroutine addrowall(m,n,a,ifrand,u)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n),u(1)
        complex *16, allocatable :: rands(:)
c
c       add the vector u to all rows in a
c
        if (ifrand .ne. 0) goto 2000
c
        do 1600 i=1,m
        do 1400 j=1,n
        a(i,j)=a(i,j)+u(j)
 1400 continue
 1600 continue
c
        return

 2000 continue
c
        allocate(rands(4*m))
        call corrand(2*m,rands)
c
        do 2600 i=1,m
        do 2400 j=1,n
        a(i,j)=a(i,j)+rands(i)*u(j)
 2400 continue
 2600 continue
c
        return
        end
c
c
c
c
c
        subroutine getrow(m,n,a,ind,u)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n),u(1)
c
c       extract the ind-th row from a
c
        do 2000 j=1,n
        u(j)=a(ind,j)
 2000 continue
c
        return
        end
c
c
c
c
c
        subroutine addones(n,a,ps,h,dsdt)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),dsdt(1)
        complex *16 a(n,n),cd
c
        done=1
        pi=4*atan(done)
c
        cd=0
        do 1600 i=1,n
        cd=cd+2*pi*ps(i)*dsdt(i)*h
 1600 continue
c
        do 2800 i=1,n
        do 2600 j=1,n
        a(i,j)=a(i,j)+ps(j)*h*dsdt(j)*2*pi/cd/n
 2600   continue
 2800   continue
c
        return
        end
c
c
c
c
c
        subroutine curla3d(zk,npts,xyzs,whts,zjvec,targ,hfield)
        implicit real *8 (a-h,o-z)
        real *8 xyzs(3,1),whts(1),targ(1)
        complex *16 zk,ima,zjvec(3,1),hfield(1),gkval,gkx,gky,gkz
c
c       evaluate the quantity del \times A at the point targ, using
c       cartesian coordinates
c
        done=1
        ima=(0,1)
        pi=4*atan(done)
c
c       sum over all the xyzs
c
        x=targ(1)
        y=targ(2)
        z=targ(3)
c
        hfield(1)=0
        hfield(2)=0
        hfield(3)=0
c

        do 1800 i=1,npts
c
        x0=xyzs(1,i)
        y0=xyzs(2,i)
        z0=xyzs(3,i)
c
        dx=x-x0
        dy=y-y0
        dz=z-z0
c
        r=sqrt(dx**2+dy**2+dz**2)
        gkval=exp(ima*zk*r)/4/pi/r
        gkx=gkval*(ima*zk*r-done)*dx/r**2
        gky=gkval*(ima*zk*r-done)*dy/r**2
        gkz=gkval*(ima*zk*r-done)*dz/r**2
c
        hfield(1)=hfield(1)+whts(i)*(gky*zjvec(3,i)-gkz*zjvec(2,i))
        hfield(2)=hfield(2)+whts(i)*(gkz*zjvec(1,i)-gkx*zjvec(3,i))
        hfield(3)=hfield(3)+whts(i)*(gkx*zjvec(2,i)-gky*zjvec(1,i))
c
 1800 continue
c
        return
        end
c
c
c
c
c
        subroutine efield3d(zk,npts,xyzs,whts,zjvec,zcharge,
     1      targ,efield)
        implicit real *8 (a-h,o-z)
        real *8 xyzs(3,1),whts(1),targ(1)
        complex *16 zk,ima,zjvec(3,1),zcharge(1),efield(1),
     1      gkval,gkx,gky,gkz
c
c       evaluate the quantity ikA - grad phi at the point targ, using
c       cartesian coordinates
c
        done=1
        ima=(0,1)
        pi=4*atan(done)
c
c       sum over all the xyzs
c
        x=targ(1)
        y=targ(2)
        z=targ(3)
c
        efield(1)=0
        efield(2)=0
        efield(3)=0
c

        do 1800 i=1,npts
c
        x0=xyzs(1,i)
        y0=xyzs(2,i)
        z0=xyzs(3,i)
c
        dx=x-x0
        dy=y-y0
        dz=z-z0
c
        r=sqrt(dx**2+dy**2+dz**2)
        gkval=exp(ima*zk*r)/4/pi/r
        gkx=gkval*(ima*zk*r-done)*dx/r**2
        gky=gkval*(ima*zk*r-done)*dy/r**2
        gkz=gkval*(ima*zk*r-done)*dz/r**2
c
        efield(1)=efield(1)+whts(i)*(ima*zk*gkval*zjvec(1,i) - 
     1      gkx*zcharge(i))
c
        efield(2)=efield(2)+whts(i)*(ima*zk*gkval*zjvec(2,i) - 
     1      gky*zcharge(i))
c
        efield(3)=efield(3)+whts(i)*(ima*zk*gkval*zjvec(3,i) - 
     1      gkz*zcharge(i))
c
 1800 continue


c
        return
        end
c
c
c
c
c
        subroutine hloop(eps,zk,p,phi,z,p0,phi0,z0,rad,sigma,
     1       ifcyl,hfield)
        implicit real *8 (a-h,o-z)
        complex *16 zk,sigma,ima,hfield(1),u1,u2
c
c       evaluate the magnetic field due to a current loop which
c       is off-center
c
c       input:
c
c         eps - absolute precision with which to calculate the field
c         zk - helmholtz parameter, omega*sqrt(epsilon*mu)
c         p,phi,z - target point in cylindrical coordinates
c         p0,phi0,z0 - the location of the center of the current loop
c             in cylindrical coordinates
c         rad - the radius of the current loop
c         sigma - strength of current loop, see comment above
c         ifcyl - if 1, return field in cylindrical coordinates
c
c       output:
c
c         hfield - the H field due to a current loop
c
c
        x=p*cos(phi)
        y=p*sin(phi)
c
        x0=p0*cos(phi0)
        y0=p0*sin(phi0)
c
        p2=sqrt((x-x0)**2+(y-y0)**2)
        dx=x-x0
        dy=y-y0
        phi2=atan2(y-y0,x-x0)
c
c       return the field in cartesian coordinates, no matter what
c       and then convert to cylindrical if necessary
c
        ifcyl777=0
        call hloop0(eps,zk,p2,phi2,z,rad,z0,sigma,ifcyl777,hfield)
c
        if (ifcyl .eq. 0) return

c
c       . . . convert to cylindrical
c
        u1=hfield(1)
        u2=hfield(2)
c
        hfield(1)=u1*cos(phi)+u2*sin(phi)
        hfield(2)=-u1*sin(phi)+u2*cos(phi)
c
        return
        end
c
c
c
c
c
        subroutine hloop0(eps,zk,p,phi,z,rad,z0,sigma,ifcyl,hfield)
        implicit real *8 (a-h,o-z)
        complex *16 zk,ima,sigma,hfield(1),dp,dz,val,val2,u1,u2
c
c       evaluate the magnetic field due to a constant current loop.
c       the strength of the loop is such that the integral of J is
c       equal to sigma - i.e. the current density is sigma/(2*pi*rad).
c       The loop is located at (0,0,z0)
c
c       input:
c
c         eps - absolute precision with which to calculate the field
c         zk - helmholtz parameter, omega*sqrt(epsilon*mu)
c         p,phi,z - target point in cylindrical coordinates
c         sigma - strength of current loop, see comment above
c         ifcyl - if 1, return field in cylindrical coordinates
c
c       output:
c
c         hfield - the H field due to a current loop
c
c
        done=1
        ima=(0,1)
        pi=4*atan(done)
c
c       call the correct axisymmetric kernel routines, and combine them
c
        mode=0
        call ghfun2cc(eps,zk,mode,p,z,rad,z0,val)
        call ghfun2ccgrad(eps,zk,mode,p,z,rad,z0,dp,dz)


        !call prin2('val = *', val, 2)
        !call prin2('dp = *', dp, 2)
        !call prin2('dz = *', dz, 2)

c
        hfield(1)=-dz*sigma/(2*pi*rad)
        hfield(2)=0
        hfield(3)=done/p*sigma/(2*pi*rad)*(val+p*dp)
        !call prin2('hfield = *', hfield, 6)
c
        if (ifcyl .eq. 1) return

c
c       turn into cartesian coordinates
c
        u1=hfield(1)
        u2=hfield(2)
c
        hfield(1)=u1*cos(phi)-u2*sin(phi)
        hfield(2)=u1*sin(phi)+u2*cos(phi)
c
        return
        end
c
c
c
c
c
        subroutine eloop(eps,zk,p,phi,z,p0,phi0,z0,rad,sigma,
     1       ifcyl,efield)
        implicit real *8 (a-h,o-z)
        complex *16 zk,sigma,ima,efield(1),u1,u2
c
c       evaluate the electric field due to a current loop which
c       is off-center
c
c       input:
c
c         eps - absolute precision with which to calculate the field
c         zk - helmholtz parameter, omega*sqrt(epsilon*mu)
c         p,phi,z - target point in cylindrical coordinates
c         p0,phi0,z0 - the location of the center of the current loop
c             in cylindrical coordinates
c         rad - the radius of the current loop
c         sigma - strength of current loop, see comment above
c         ifcyl - if 1, return field in cylindrical coordinates
c
c       output:
c
c         efield - the E field due to a current loop
c
c
        x=p*cos(phi)
        y=p*sin(phi)
c
        x0=p0*cos(phi0)
        y0=p0*sin(phi0)
c
        p2=sqrt((x-x0)**2+(y-y0)**2)
        dx=x-x0
        dy=y-y0
        phi2=atan2(y-y0,x-x0)
c
c       return the field in cartesian coordinates, no matter what
c       and then convert to cylindrical if necessary
c
        ifcyl777=0
        call eloop0(eps,zk,p2,phi2,z,rad,z0,sigma,ifcyl777,efield)
c

        if (ifcyl .eq. 0) return

c
c       . . . convert to cylindrical
c
        u1=efield(1)
        u2=efield(2)
c
        efield(1)=u1*cos(phi)+u2*sin(phi)
        efield(2)=-u1*sin(phi)+u2*cos(phi)
c
        return
        end
c
c
c
c
c
        subroutine eloop0(eps,zk,p,phi,z,rad,z0,sigma,ifcyl,efield)
        implicit real *8 (a-h,o-z)
        complex *16 zk,sigma,ima,efield(1),val,u1,u2
c
c       evaluate the electric field due to an infinitely thin current
c       loop at the points p,phi,z - the resulting vector is given
c       in cylindrical coordinates. note that the efield generated will
c       be singular near the loop. the loop is located at (0,0,z0)
c
c       input:
c
c         eps - absolute precision with which to calculate the field
c         zk - helmholtz parameter, omega*sqrt(epsilon*mu)
c         p,phi,z - target point in cylindrical coordinates
c         sigma - strength of current loop, see comment above
c         ifcyl - if 1, return field in cylindrical coordinates
c
c       output:
c
c         efield - the E field due to a current loop
c
c
        done=1
        ima=(0,1)
        pi=4*atan(done)
c
c       call the correct axisymmetric kernel routines, and combine them
c
        mode=0
        call ghfun2cc(eps,zk,mode,p,z,rad,z0,val)
c
        efield(1)=0
        efield(2)=ima*zk*val*sigma/(2*pi*rad)
        efield(3)=0
c
        if (ifcyl .eq. 1) return

c
c       turn into cartesian coordinates
c
        u1=efield(1)
        u2=efield(2)
c
        efield(1)=u1*cos(phi)-u2*sin(phi)
        efield(2)=u1*sin(phi)+u2*cos(phi)
c
        return
        end
c
c
c
c
c
        subroutine creanxcurlmat(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,amat)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000),rnp(10000),
     1      rnz(10000)
        complex *16 ima,zk,amat(2*n,2*n),
     1      dp1,dp2,ds1,ds2,dz1,dz2,
     2      apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1,asdz2,as1,as2,
     3      asdp1,asdp2,ap1,ap2
        external ghfun2,ghfun2cc,ghfun2ss,ghfun4n1,ghfun2ccn1,
     1      ghfun2ssn1
        complex *16, allocatable :: w(:,:),wp(:,:)
        complex *16, allocatable :: wcc(:,:),wccp(:,:),wccz(:,:)
        complex *16, allocatable :: wss(:,:),wssp(:,:),wssz(:,:)
c
c       this subroutine constructs a 2n by 2n matrix which maps
c       a surface current specified in tangential and azimuthal
c       coordinates into the tangential H field, i.e. this matrix
c       computes the quantity (n x curl a) where n is the normal
c       to the surface ps,zs and a is the vector potential generated
c       by the surface current.
c
c       input:
c         eps - the precision with which to evaluate the kernels
c         zk - the complex helmholtz parameter
c         m - the fourier mode to compute
c         n - the number of points in the surface discritization
c         rl - the length of the parameterizing interval for ps,zs
c         ps,zs - the points on the generating curve
c         dpdt,dzdt - tanget vectors to ps,zs
c         norder - order of the Kapur-Rokhlin quadratures to use
c         diag - real number that should be put on the diagonal of the
c             matrix, either +.5d0 or -.5d0, depending on whether
c             constructing the matrix for the interior or exterior
c             scattering problem
c
c       output:
c         amat - the 2n by 2n matrix taking surface currents to
c             surface H fields.
c
c       NOTE:  no assumption on arclength parameterization is made.
c
c       NOTE:  keep in mind that this routine computes the matrix for
c       the operator (n x curl a) always using an outward normal.  depending
c       on your application you may have to multiply the entire matrix
c       by -1, or flip the sign on the diagonal.
c
c
        allocate(w(n,n),wp(n,n))
        allocate(wcc(n,n),wccp(n,n),wccz(n,n))
        allocate(wss(n,n),wssp(n,n),wssz(n,n))
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
        sc=dpdt(i)**2+dzdt(i)**2
        sc=sqrt(sc)
        dsdt(i)=sc
 1400   continue
c
c       first create all of the individual matrices that will get
c       pieced together to make the large one, first the single
c       layer matrices...
c
        call formslpmatbac(ier,w,norder,ps,zs,dsdt,h,n,
     1      ghfun2,eps,zk,m)
c
        call formslpmatbac(ier,wcc,norder,ps,zs,dsdt,h,n,
     1      ghfun2cc,eps,zk,m)
c
        call formslpmatbac(ier,wss,norder,ps,zs,dsdt,h,n,
     1      ghfun2ss,eps,zk,m)
c
c       ...the d/dp matrices
c
        do 1800 i=1,n
        rnp(i)=1
        rnz(i)=0
 1800   continue
c
        call formsprimematbac(ier,wp,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun4n1,eps,zk,m)
c
        call formsprimematbac(ier,wccp,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ccn1,eps,zk,m)
c
        call formsprimematbac(ier,wssp,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ssn1,eps,zk,m)
c
c       ...the d/dz matrices
c
        do 2200 i=1,n
        rnp(i)=0
        rnz(i)=1
 2200   continue
c
        call formsprimematbac(ier,wccz,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ccn1,eps,zk,m)
c
        call formsprimematbac(ier,wssz,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ssn1,eps,zk,m)
c
c       now assemble the matrix
c
        do 3600 i=1,n
        i1=2*(i-1)+1
        i2=i1+1
c
        dpdsi=dpdt(i)/dsdt(i)
        dzdsi=dzdt(i)/dsdt(i)
c
        do 3200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)
c
c       compute all the kernel partial derivatives
c
        apdz1=dpdsj*wccz(i,j)
        apdz2=-ima*wssz(i,j)
c
        azdp1=dzdsj*wp(i,j)
        azdp2=0
c
        asdp1=ima*dpdsj*wssp(i,j)
        asdp2=wccp(i,j)
c
        as1=ima*dpdsj*wss(i,j)
        as2=wcc(i,j)
c
        ap1=dpdsj*wcc(i,j)
        ap2=-ima*wss(i,j)
c
        az1=dzdsj*w(i,j)
        az2=0
c
        asdz1=ima*dpdsj*wssz(i,j)
        asdz2=wccz(i,j)
c
c       and compute the vector components, adding in the non-cartesian
c       correction to the theta-hat component
c
        dp1=-dpdsi*(apdz1-azdp1)
        dp2=-dpdsi*(apdz2-azdp2)
c
        ds1=dzdsi/ps(i)*(ps(i)*asdp1+as1-ima*m*ap1)
     1      +dpdsi*(ima*m/ps(i)*az1-asdz1)
        ds2=dzdsi/ps(i)*(ps(i)*asdp2+as2-ima*m*ap2)
     1      +dpdsi*(ima*m/ps(i)*az2-asdz2)
c
        dz1=-dzdsi*(apdz1-azdp1)
        dz2=-dzdsi*(apdz2-azdp2)
c
        amat(i1,j1)=-(dpdsi*dp1+dzdsi*dz1)
        amat(i2,j1)=-ds1
c
        amat(i1,j2)=-(dpdsi*dp2+dzdsi*dz2)
        amat(i2,j2)=-ds2
c
        if (i .eq. j) then
          amat(i1,j1)=amat(i1,i1)+diag
          amat(i2,j2)=amat(i2,j2)+diag
        endif
c
 3200   continue
 3600   continue
c
        return
        end
c
c
c
c
c
c$$$        subroutine efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta,z,vals)
c$$$        implicit real *8 (a-h,o-z)
c$$$        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
c$$$        complex *16 zk,zjs(2,1),ima,vp1(10),vals(1),phi(10000),
c$$$     1      wp1(10)
c$$$c
c$$$c       this subroutine calculates ima*zk*A - grad(phi), i.e.
c$$$c       the E field for a given surface current zjs, specified
c$$$c       in tangential coordinates on a surface of rotation
c$$$c
c$$$c       input:
c$$$c         eps - the precision with which to evaluate the kernels
c$$$c         zk - the complex helmholtz parameter
c$$$c         m - the fourier mode to compute
c$$$c         n - the number of points in the surface discritization
c$$$c         rl - the length of the parameterizing interval for ps,zs
c$$$c         ps,zs - the points on the generating curve
c$$$c         dpdt,dzdt - tanget vectors to ps,zs
c$$$c         zjs - 2 by n array containing tangential and azimuthal
c$$$c             components of the surface current
c$$$c         p,theta,z - point at which to evaluation the E field
c$$$c
c$$$c       output:
c$$$c         vals - complex length 3 vector containing the p, theta, z
c$$$c             components of the E field
c$$$c
c$$$c
c$$$        ima=(0,1)
c$$$        done=1.0d0
c$$$        pi=4*atan(1.0d0)
c$$$c
c$$$        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta,z,vp1)
c$$$c
c$$$c       scale by ik
c$$$c
c$$$        vp1(1)=vp1(1)*ima*zk
c$$$        vp1(2)=vp1(2)*ima*zk
c$$$        vp1(3)=vp1(3)*ima*zk
c$$$c
c$$$c       now compute phi, div zjs
c$$$c
c$$$        call surfdivaxi(ps,zs,dpdt,dzdt,n,m,rl,zjs,phi)
c$$$c
c$$$        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      phi,p,theta,z,wp1)
c$$$c
c$$$        wp1(1)=wp1(1)/ima/zk
c$$$        wp1(2)=wp1(2)/ima/zk
c$$$        wp1(3)=wp1(3)/ima/zk
c$$$c
c$$$c       now add them together
c$$$c
c$$$        vals(1)=vp1(1)-wp1(1)
c$$$        vals(2)=vp1(2)-wp1(2)
c$$$        vals(3)=vp1(3)-wp1(3)
c$$$c
c$$$        return
c$$$        end
c$$$C
c$$$C
c$$$C
c$$$C
c$$$C
c$$$        subroutine efieldeval2(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,rho,p,theta,z,vals)
c$$$        implicit real *8 (a-h,o-z)
c$$$        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
c$$$        complex *16 zk,zjs(2,1),rho(1),ima,vp1(10),vals(1),
c$$$     1      phi(10000),wp1(10)
c$$$c
c$$$c       this subroutine calculates ima*zk*A - grad(phi), i.e.
c$$$c       the E field for a given surface current zjs AND charge rho
c$$$c
c$$$c       NOTE: this is exactly the same routine as efieldeval, except
c$$$c             that this routine should be used whenever rho is known
c$$$c             to avoid catastrophic low frequency breakdown when
c$$$c             computing the gradient of the scalar potential
c$$$c
c$$$c       input:
c$$$c         eps - the precision with which to evaluate the kernels
c$$$c         zk - the complex helmholtz parameter
c$$$c         m - the fourier mode to compute
c$$$c         n - the number of points in the surface discritization
c$$$c         rl - the length of the parameterizing interval for ps,zs
c$$$c         ps,zs - the points on the generating curve
c$$$c         dpdt,dzdt - tanget vectors to ps,zs
c$$$c         zjs - 2 by n array containing tangential and azimuthal
c$$$c             components of the surface current
c$$$c         p,theta,z - point at which to evaluation the E field
c$$$c
c$$$c       output:
c$$$c         vals - complex length 3 vector containing the p, theta, z
c$$$c             components of the E field
c$$$c
c$$$c
c$$$        ima=(0,1)
c$$$        done=1.0d0
c$$$        pi=4*atan(1.0d0)
c$$$c
c$$$        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta,z,vp1)
c$$$c
c$$$c       scale by ik
c$$$c
c$$$        vp1(1)=vp1(1)*ima*zk
c$$$        vp1(2)=vp1(2)*ima*zk
c$$$        vp1(3)=vp1(3)*ima*zk
c$$$c
c$$$c       now compute phi, div zjs
c$$$c
c$$$cccc        call surfdivaxi(ps,zs,dpdt,dzdt,n,m,rl,zjs,phi)
c$$$c
c$$$cccc        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$cccc     1      phi,p,theta,z,wp1)
c$$$c
c$$$        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      rho,p,theta,z,wp1)
c$$$c
c$$$cccc        wp1(1)=wp1(1)/ima/zk
c$$$cccc        wp1(2)=wp1(2)/ima/zk
c$$$cccc        wp1(3)=wp1(3)/ima/zk
c$$$c
c$$$c       now add them together
c$$$c
c$$$        vals(1)=vp1(1)-wp1(1)
c$$$        vals(2)=vp1(2)-wp1(2)
c$$$        vals(3)=vp1(3)-wp1(3)
c$$$c
c$$$        return
c$$$        end
c
c
c
c
c
        subroutine curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,vals)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 ima,zk,vals(1),zjs(2,1),ee,ss,cc,eep,ssp,ccp,
     1      ssz,ccz,dp,ds,dz,zint1,zint2,zint3,zint4,zint5,zint6,
     2      zint7,zint8,zint9,zint10,zint11,zint12
c
c       this routine evaluates the curl of the vector potetial of a
c       surface current at an arbitrary point.  the surface current
c       can be arbitrarily specified in t-hat and theta-hat components.
c
c       input:
c         eps - the precision with which to evaluate the kernels
c         zk - complex helmholtz parameter
c         m - the fourier mode we are computing
c         n - the number of points sampled on the curve ps,zs
c         rl - the length of the curve parameterizing interval, not
c             necessarily arclength
c         ps,zs - the generating curve
c         dpdt,dzdt - the curve tangent vectors, not necessarily
c             unit vectors
c         zjs - 2 x n array containing surface current, t-hat and
c             theta-hat components, in that order
c         p,theta,z - the point in 3D at which to evaluate the
c             curl of the potential
c
c       output:
c         vals - length 3 array containing p,theta,z components of the
c             curl of the vector potential
c
c       NOTE: the routine only uses the trapezoidal rule since
c       the current is periodic and p,z does NOT lie on ps,zs.
c       the routine will break when p,z is equal to one of the
c       ps,zs.
c
c       NOTE: the output vals IS modulated by exp(ima*m*theta) in order
c       to fully evaluate curl A at a point in 3D.
c
c
        done=1.0d0
        ima=(0,1)
        pi=4*atan(done)
c
c       evaluate each one of the integrals separately, then glue together
c
        zint1=0
        zint2=0
        zint3=0
        zint4=0
        zint5=0
        zint6=0
        zint7=0
        zint8=0
        zint9=0
        zint10=0
        zint11=0
        zint12=0
c
        h=rl/n
        do 2600 i=1,n
c
c       call each of the kernel routines, first single layer
c
        call ghfun2(eps,zk,m,p,z,ps(i),zs(i),ee)
        call ghfun2cc(eps,zk,m,p,z,ps(i),zs(i),cc)
        call ghfun2ss(eps,zk,m,p,z,ps(i),zs(i),ss)
c
c       now rho derivatives
c
        vnp=1
        vnz=0
        call ghfun4n1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,eep)
        call ghfun2ccn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ccp)
        call ghfun2ssn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ssp)
c
c       and lastly z derivatives
c
        vnp=0
        vnz=1
        call ghfun2ccn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ccz)
        call ghfun2ssn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ssz)


c
c       do each of the integrals now, they are calculated in the order
c       they appear in the curl equation my notes
c
        dsdt=dpdt(i)**2+dzdt(i)**2
        dsdt=sqrt(dsdt)
        dpds=dpdt(i)/dsdt
        dzds=dzdt(i)/dsdt
c
        zint1=zint1+h*zjs(1,i)*dzds*ee*dsdt
        zint2=zint2+h*zjs(1,i)*dpds*ssz*dsdt
        zint3=zint3+h*zjs(2,i)*ccz*dsdt
c
        zint4=zint4+h*zjs(1,i)*dpds*ccz*dsdt
        zint5=zint5+h*zjs(2,i)*ssz*dsdt
        zint6=zint6+h*zjs(1,i)*dzds*eep*dsdt
c
        zint7=zint7+h*zjs(1,i)*dpds*ss*dsdt
        zint8=zint8+h*zjs(2,i)*cc*dsdt
        zint9=zint9+h*zjs(1,i)*dpds*ssp*dsdt
        zint10=zint10+h*zjs(2,i)*ccp*dsdt
        zint11=zint11+h*zjs(1,i)*dpds*cc*dsdt
        zint12=zint12+h*zjs(2,i)*ss*dsdt
c
 2600   continue
c
c       finally compute the curl in cylindrical coordinates
c
        dp=ima*m/p*zint1-(ima*zint2+zint3)
        ds=zint4-ima*zint5-zint6
        dz=1/p*(ima*zint7+zint8+p*(ima*zint9+zint10)-
     1      ima*m*(zint11-ima*zint12))
c
        vals(1)=dp*exp(ima*m*theta)
        vals(2)=ds*exp(ima*m*theta)
        vals(3)=dz*exp(ima*m*theta)
c
        return
        end
c
c
c
c
c
c$$$        subroutine curlfield(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,ifefield,zjs,p,theta,z,h,curl)
c$$$        implicit real *8 (a-h,o-z)
c$$$        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
c$$$        complex *16 zk,zjs(2,1),curl(1),ima,vp1(10),vp2(10),
c$$$     1      vs1(10),vs2(10),vz1(10),vz2(10),dp(10),ds(10),
c$$$     2      dz(10),vals(10)
c$$$c
c$$$c       compute the curl of an H field or an E field using
c$$$c       finite differences in cylindrical coordinates - this is
c$$$c       most likely only a routine that would be used in debugging
c$$$c
c$$$        done=1.0d0
c$$$        pi=4*atan(done)
c$$$        ima=(0,1)
c$$$c
c$$$        if (ifefield .eq. 1) goto 2000
c$$$c
c$$$c       if here, construct the curl of an h field
c$$$c
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p-h,theta,z,vp1)
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p+h,theta,z,vp2)
c$$$c
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta-h,z,vs1)
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta+h,z,vs2)
c$$$c
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta,z-h,vz1)
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta,z+h,vz2)
c$$$c
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta,z,vals)
c$$$c
c$$$        goto 2400
c$$$c
c$$$ 2000   continue
c$$$c
c$$$c       if here, construct the curl of an e field
c$$$c
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p-h,theta,z,vp1)
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p+h,theta,z,vp2)
c$$$c
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta-h,z,vs1)
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta+h,z,vs2)
c$$$c
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta,z-h,vz1)
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta,z+h,vz2)
c$$$c
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,
c$$$     1      dzdt,zjs,p,theta,z,vals)
c$$$c
c$$$ 2400   continue
c$$$c
c$$$c       compute the individual derivatives
c$$$c
c$$$        dp(1)=(vp2(1)-vp1(1))/2/h
c$$$        dp(2)=(vp2(2)-vp1(2))/2/h
c$$$        dp(3)=(vp2(3)-vp1(3))/2/h
c$$$c
c$$$        ds(1)=(vs2(1)-vs1(1))/2/h
c$$$        ds(2)=(vs2(2)-vs1(2))/2/h
c$$$        ds(3)=(vs2(3)-vs1(3))/2/h
c$$$c
c$$$        dz(1)=(vz2(1)-vz1(1))/2/h
c$$$        dz(2)=(vz2(2)-vz1(2))/2/h
c$$$        dz(3)=(vz2(3)-vz1(3))/2/h
c$$$c
c$$$c       and assemble the curl
c$$$c
c$$$        curl(1)=1/p*ds(3)-dz(2)
c$$$        curl(2)=dz(1)-dp(3)
c$$$        curl(3)=1/p*(vals(2)+p*dp(2)-ds(1))
c$$$c
c$$$        return
c$$$        end
c$$$c
c$$$c
c$$$c
c$$$c
c$$$c
c$$$        subroutine divefield(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta,z,h)
c$$$        implicit real *8 (a-h,o-z)
c$$$        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
c$$$        complex *16 zjs(2,1),ima,zk,vp1(10),vp2(10),vs1(10),vs2(10),
c$$$     1      vz1(10),vz2(10),dp,ds,dz,dd,phi(10000)
c$$$c
c$$$c       take the divergence of ima*zk*A - grad phi in 
c$$$c       cylindrical coordinates using finite differences - this
c$$$c       is most likely on a routine that would be used in debugging
c$$$c
c$$$        ima=(0,1)
c$$$        done=1.0d0
c$$$        pi=4*atan(1.0d0)
c$$$c
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p-h,theta,z,vp1)
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p+h,theta,z,vp2)
c$$$c
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta-h,z,vs1)
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta+h,z,vs2)
c$$$c
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta,z-h,vz1)
c$$$        call efieldeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs,p,theta,z+h,vz2)
c$$$c
c$$$        dp=1/p*((p+h)*vp2(1)-(p-h)*vp1(1))/2/h
c$$$        ds=1/p*(vs2(2)-vs1(2))/2/h
c$$$        dz=(vz2(3)-vz1(3))/2/h
c$$$c
c$$$        dd=dp+ds+dz
c$$$        call prin2('divergence of the e field*',dd,2)
c$$$c
c$$$        return
c$$$        end
c
c
c
c
c
        subroutine divhfield(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,h)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 zjs(2,1),ima,zk,vp1(10),vp2(10),vs1(10),vs2(10),
     1      vz1(10),vz2(10),dp,ds,dz,dd
c
c       take the divergence of the curl in cylindrical coordinates
c       using finite differences to make sure it is zero - this is
c       most likely only a routine that would be used in debugging
c
        ima=(0,1)
        done=1.0d0
        pi=4*atan(1.0d0)
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt,zjs,p-h,theta,z,vp1)
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt,zjs,p+h,theta,z,vp2)
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt,zjs,p,theta-h,z,vs1)
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt,zjs,p,theta+h,z,vs2)
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt,zjs,p,theta,z-h,vz1)
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt,zjs,p,theta,z+h,vz2)
c
c       and now compute the divergence
c
        dp=1/p*((p+h)*vp2(1)-(p-h)*vp1(1))/2/h
        ds=1/p*(vs2(2)-vs1(2))/2/h
        dz=(vz2(3)-vz1(3))/2/h
c
        dd=dp+ds+dz
        call prin2('divergence of the h field=*',dd,2)
c
        return
        end
