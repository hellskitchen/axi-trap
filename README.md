# Axisymmetric trapezoidal-rule scattering library

The general layout is that all examples, driver files are located in
[examples/](examples) and library files (kernel evaluations, matrix builders,
etc.) are located in [src/](src). Other directories are being re-organized.

## Laplace
Nothing...

## Helmholtz
Nothing...

## Maxwell

There are several examples of exterior Maxwell scattering codes:
- Perfect electric conductor
  - Magnetic field integral equation (MFIE)
  - Augmented MFIE (MFIE solve, additional solve for electric charge)
  - Generalized Debye

- Dielectric interface
  - Mueller
  - Generalized Debye

## Force-free, Beltrami fields
  - Exterior, genus one
  - Interior, genus one
  - Interior, genus two
  - Eigenvalue solver, interior, genus one and two

# References

There are several papers in which these codes have been used, for example:

  - A high-order wideband direct solver for electromagnetic scattering from
bodies of revolution. C. L. Epstein, L. Greengard, and M. O'Neil, J. Comput.
Phys., 387:205-229, 2019.

  - An integral equation-based numerical solver for Taylor states in toroidal
geometries. M. O'Neil and A. Cerfon, J. Comput. Phys., 359:263-282, 2018.


