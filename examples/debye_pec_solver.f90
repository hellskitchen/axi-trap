program debye_pec_pipe
  implicit real *8 (a-h,o-z)
  real *8 :: ps(10000),zs(10000),dpdt(10000)
  real *8 :: dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000)
  real *8 :: center(10),xyz(10)
  real *8 :: targ(10), ts(10000), kvec(10), pvec(10)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)
  real *8 :: dtds(10000), dtds2(10000), errs(10)
  double precision :: vert1(10), vert2(10), vert3(10), vert4(10)

  real *8, allocatable :: rnodes(:,:,:), pts(:,:,:), xyzs(:,:,:)
  real *8, allocatable :: testpts(:,:), whts(:,:), vals(:)
  real *8, allocatable :: triaskel(:,:,:), tempvecs(:,:,:)
  real *8, allocatable :: dein(:,:,:), dhin(:,:,:), detot(:,:)

  complex *16 :: zk, ima, cinta, cinta2, cintb2, surfa
  complex *16 :: aint, cintb
  complex *16 :: etest(3,10000), htest(3,10000), etau
  complex *16 :: cd, evec(10), hvec(10)

  complex *16, allocatable :: esurf(:,:,:),hsurf(:,:,:)
  complex *16, allocatable :: ein(:,:,:), hin(:,:,:)
  complex *16, allocatable :: rho(:,:), sigma(:,:)
  complex *16, allocatable :: jcurr(:,:,:), kcurr(:,:,:)
  complex *16, allocatable :: zvals(:,:)
  complex *16, allocatable :: efield(:,:,:), hfield(:,:,:)

  external funcurvewrap
  
  done=1
  pi=4*atan(done)
  ima=(0,1)

  call prini(6,13)


  !
  ! calculate the length of the generating curve
  !
  n = 5000
  rl = 2*pi
  call funcurve_resampler(n, rl, ts, h, xys, dxys, d2xys, rltot)
  call prin2('length of generating curve = *', rltot, 1)

  !
  ! set some subroutine parameters
  !
  dlam = 6.0d0/50
  dlam = 6.0d0/20
  dlam = 0.5d0
  dlam = dlam/2/2/2/2
  dlam = .5d0
  zk = 2*pi/dlam
  !zk = 2*pi/dlam + ima*1.0d-10
  !zk = 2.2d0+ima*1.242359537d-10
  !zk = 20.0d0 +ima*1.242359537d-10
  !zk = 40.0d0 +ima*1.242359537d-10
  !!!!zk = 3.43d0+ima/100000


  ptsper = 10
  !n = 2049
  n = (int(rltot/dlam)+1)*ptsper
  nover = 1
  n = n*nover
  n = 1024

  !if (n .lt. 100) n = 101
  if (mod(n,2) .eq. 0) n = n+1
  call prinf('n = *', n, 1)

  call prin2(' *',pi,0)
  call prinf('n = *', n, 1)
  call prin2('zk=*',zk,2)
  call prin2('wavelength = *', dlam, 1)

  norder = 2
  norder = 4
  norder = 8
  norder = 16
  call prinf('alpert order=*',norder,1)

  !
  ! generate the points on the scatterer
  !
  ifresamp = 1
  if (ifresamp .eq. 1) then

    print *
    print *, '- - - resampling the geometry - - - '
    
    call funcurve_resampler(n, rl, ts, h, xys, dxys, d2xys, rltot)

    do i = 1,n
      dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    end do

    duds = 2*pi/rltot
    dsdu = 1/duds
    do i = 1,n
      dxys(1,i) = dxys(1,i)*dsdu
      dxys(2,i) = dxys(2,i)*dsdu
      d2xys(1,i) = d2xys(1,i)*dsdu*dsdu
      d2xys(2,i) = d2xys(2,i)*dsdu*dsdu
      dpdt(i) = dxys(1,i)
      dzdt(i) = dxys(2,i)
      dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    end do
    h = h*duds
    
    print *
    print *, '- - - - from arclength resampler - - - -'
    !call prin2('dxys = *', dxys, 2*n)
    !call prin2('dsdt = *', dsdt, n)
    !call prin2('d2xys = *', dxys, 2*n)
    call prin2('h = *', h, 1)
    call prin2('rltot = *', rltot, 1)
    print *, '- - - - - - - - - - - - - - - - - '
    print *
    
  end if


  call axi_geometry_resolution(n, xys, dxys, d2xys, errs, res)
  call prin2('errs = *', errs, 6)
  call prin2('geometry resolution = *', res, 1)


  print *, '- - - - - - - - - - -- - - - - '
  call prin2('length of gen curve in wavelengths = *', rltot/dlam, 1)
  call prin2('points per wavelength = *', n/(rltot/dlam), 1)

  
  !
  ! find the minimum ps to construct spanning surface for
  ! the m=0 mode
  !
  ind=1
  dmax = -1
  top = -10000
  bot = 10000
  diam = -1
  do i=1,n
    if (xys(1,i) .lt. xys(1,ind)) ind=i
    if (xys(1,i) .gt. dmax) dmax=xys(1,i)
    if (xys(2,i) .lt. bot) bot = xys(2,i)
    if (xys(2,i) .gt. top) top = xys(2,i)
    ddd = sqrt(xys(1,i)**2 + xys(2,i)**2)
    if (ddd .gt. diam) diam=ddd
  enddo
  diam = 2*diam
  
  call prin2('horizontal size of scatterer in wavelengths = *', &
      2*dmax/dlam, 1)
  call prin2('vertical size of scatterer in wavelengths = *', &
      (top-bot)/dlam, 1)

  dltot = 2*pi*dmax
  !nphi = dltot/rltot*n
  !nphi = 256
  !nphi = 1024

  nphi = (int(dltot/dlam)+1)*ptsper
  nphi = nover*nphi
  nphi = 512
  !if (nphi .lt. 128) nphi=128
  call prin2('azimuthal circum in wavelengths = *', dltot/dlam, 1)
  call prinf('setting nphi = *', nphi, 1)
  call prin2('azimuthal points per wavelength = *', &
      nphi/(dltot/dlam), 1)

  !stop

  !
  ! evaluate an incoming field on the surface of the scatterer
  !
  ifplot = 1
  if (ifplot .eq. 1) then
    iw = 11
    print *, '. . . plotting the surface'
    call axi_vtk_surface(iw, n, xys, nphi, 'The body of revolution')
  end if
  

  allocate( esurf(3,n,nphi) )
  allocate( hsurf(3,n,nphi) )
  allocate( ein(3,nphi,n) )
  allocate( hin(3,nphi,n) )

  ! !
  ! ! drive the thing with a current loop
  ! !
  ! al = .34d0
  ! !al = 0
  ! !center(1) = 2d0*cos(al)
  ! !center(2) = 2d0*sin(al)
  ! !center(3) = .1d0

  ! center(1) = 0.5d0*cos(al)
  ! center(2) = 0.5d0*sin(al)
  ! center(3) = .1d0
  ! radloop = .1d0

  ! call prin2('center = *', center, 3)


  kvec(1) = -1/sqrt(2.0d0)
  kvec(2) = 0
  kvec(3) = -1/sqrt(2.0d0)

  pvec(1) = 0
  pvec(2) = 1
  pvec(3) = 0
  phase = 0

  
  !
  ! now evaluate the e/h fields at each point on the scatterer
  !
  print *, '. . . evaluating the data on the scatterer'

  allocate(xyzs(3,nphi,n))

  !$omp parallel do default(shared) &
  !$omp     private(phi, targ)
  do j=1,nphi
    phi = (j-1)*2*pi/nphi
    do i=1,n
      targ(1) = xys(1,i)*cos(phi)
      targ(2) = xys(1,i)*sin(phi)
      targ(3) = xys(2,i)
      xyzs(1,j,i) = targ(1)
      xyzs(2,j,i) = targ(2)
      xyzs(3,j,i) = targ(3)
      call em3dpw(zk, kvec, pvec, phase, targ, ein(1,j,i), hin(1,j,i))          
    enddo
  enddo
  !$omp end parallel do



  allocate(dein(3,nphi,n))
  allocate(dhin(3,nphi,n))
  do j = 1,n
    do i = 1,nphi
      do k = 1,3
        dein(k,i,j) = ein(k,i,j)
        dhin(k,i,j) = hin(k,i,j)
      end do
    end do
  end do
  
  if (ifplot .eq. 1) then
    iw = 31
    print *, '. . . plotting the incoming field'
    call axi_vtk_vector(iw, n, xys, nphi, dein, 'e field data')
    iw = 32
    call axi_vtk_vector(iw, n, xys, nphi, dhin, 'h field data')
  end if


  surfa = 0
  dph = 2*pi/nphi
  do i = 1,nphi
    ph = 2*pi*(i-1)/nphi
    do j = 1,n
      dx = dxys(1,j)/dsdt(j)*cos(ph)
      dy = dxys(1,j)/dsdt(j)*sin(ph)
      dz = dxys(2,j)/dsdt(j)
      etau = ein(1,i,j)*dx + ein(2,i,j)*dy + ein(3,i,j)*dz
      da = dph*xys(1,j)*h*dsdt(j)
      surfa = surfa + etau*da
    end do
  end do

  call prin2('from cartesian surface integral, surfa = *', surfa, 2)
  
  
  cintb = 0
  hhh = 2*pi*xys(1,ind)/nphi
  do i = 1,nphi
    phi = 2*pi/nphi*(i-1)
    dx = -sin(phi)
    dy = cos(phi)
    dz = 0
    cintb = cintb + hhh*(dx*ein(1,i,ind) + &
        dy*ein(2,i,ind) + dz*ein(3,i,ind))
  end do

  cintb = cintb/zk

  !
  ! finally done setting up the problem, now solve the Gen Debye PEC
  ! problem with data esurf,hsurf
  !
  allocate(whts(nphi,n))
  allocate(rho(nphi,n), sigma(nphi,n))
  allocate(jcurr(3,nphi,n), kcurr(3,nphi,n))

  !
  ! . . . don't forget to flip the sign on teh data...
  !
  surfa = -surfa
  cintb = -cintb
  do i = 1,3
    do j = 1,nphi
      do k = 1,n
        ein(i,j,k) = -ein(i,j,k)
        hin(i,j,k) = -hin(i,j,k)
      end do
    end do
  end do
  
  call cpu_time(tstart)
  !$ tstart = OMP_get_wtime()

  call axidebye_pec_solver(zk, n, h, xys, &
      dxys, d2xys, norder, nphi, xyzs, ein, hin, ind, &
      surfa, cintb, whts, rho, sigma, jcurr, &
      kcurr, modemax, esterr)

  call cpu_time(tend)
  !$ tend = OMP_get_wtime()

  call prin2('time for axidebye_pec_solver = *', tend-tstart, 1)
  
  !!!!call prin2('after axidebye_pec_solver, esterr = *', esterr, 1)

  if (ifplot .eq. 1) then
    print *, '. . . plotting the debye sources on the object'
    iw = 41
    m = 2
    call axi_vtk_scalar(iw, n, xys, nphi, m, rho, 'rho')
    iw = 42
    call axi_vtk_scalar(iw, n, xys, nphi, m, sigma, 'sigma')
  end if


  !
  ! plot the total fields on flat surfaces
  !
  w = 2
  zbot = -4.5d0
  vert1(1) = -1.25d0
  vert1(2) = -1.25
  vert1(3) = zbot

  vert2(1) = 2
  vert2(2) = -1.25
  vert2(3) = zbot

  vert3(1) = 2
  vert3(2) = 2
  vert3(3) = zbot

  vert4(1) = -1.25d0
  vert4(2) = 2
  vert4(3) = zbot

  nover = 7
  maxtri = 100000
  allocate(triaskel(3,3,maxtri))
  call axi_rectmesh(vert1, vert2, vert3, vert4, nover, maxtri, &
      ntri, triaskel)

  !
  ! now evaluate the scattered field on the same plane as constructed earlier
  !
  print *, '. . . evaluating the scattered field now'
  nsrc = n*nphi
  ntest = 3*ntri
  call prinf('nsrc = *', nsrc, 1)
  call prinf('ntest = *', ntest, 1)
  allocate(efield(3,3,ntri), hfield(3,3,ntri))
  call axidebye_eval3d(zk, nsrc, xyzs, whts, rho, sigma, jcurr,&
      kcurr, ntest, triaskel, efield, hfield)


  ! use Manas's new fmm
  ! The files are in Rachh/H3Dgqbx/
  ! Fmm file: hfmm3dgqbx6.f
  ! Make file: h3dfmmpart6.make
  ! Driver: hfmm3dpart_dr.f

  allocate(detot(3,ntri))
  do i = 1,ntri
    do j = 1,3
      call em3dpw(zk, kvec, pvec, phase, triaskel(1,j,i), &
          evec, hvec)
      !detot(j,i) = dble(evec(1)+efield(1,j,i))**2 &
      !    + dble(evec(2)+efield(2,j,i))**2 &
      !    + dble(evec(3)+efield(3,j,i))**2
      detot(j,i) = evec(1)+efield(1,j,i)
    end do
  end do

  iw = 51
  m = 1
  print *, '. . . plotting the total field on a plane'
  call axi_vtk_flat_scalars(iw, ntri, triaskel, m, detot, &
      'abs of real part of total field')

  !
  ! evaluate on a side panel
  !
  xslice = -1.25
  vert1(1) = xslice
  vert1(2) = -1.25d0
  vert1(3) = zbot

  vert2(1) = xslice
  vert2(2) = 2
  vert2(3) = zbot

  vert3(1) = xslice
  vert3(2) = 2
  vert3(3) = -zbot

  vert4(1) = xslice
  vert4(2) = -1.25d0
  vert4(3) = -zbot

  call axi_rectmesh(vert1, vert2, vert3, vert4, nover, maxtri, &
      ntri, triaskel)

  !
  ! now evaluate the scattered field on the same plane as constructed earlier
  !
  print *, '. . . evaluating the scattered field now'
  nsrc = n*nphi
  ntest = 3*ntri
  call prinf('nsrc = *', nsrc, 1)
  call prinf('ntest = *', ntest, 1)
  call axidebye_eval3d(zk, nsrc, xyzs, whts, rho, sigma, jcurr,&
      kcurr, ntest, triaskel, efield, hfield)

  do i = 1,ntri
    do j = 1,3
      call em3dpw(zk, kvec, pvec, phase, triaskel(1,j,i), &
          evec, hvec)
      detot(j,i) = evec(1)+efield(1,j,i)
    end do
  end do

  iw = 52
  m = 1
  print *, '. . . plotting the total field on a plane'
  call axi_vtk_flat_scalars(iw, ntri, triaskel, m, detot, &
      'abs of real part of total field')



  !
  ! evaluate on a side panel
  !
  yslice = -1.25d0
  vert1(1) = -1.25d0
  vert1(2) = yslice
  vert1(3) = zbot

  vert2(1) = 2
  vert2(2) = yslice
  vert2(3) = zbot

  vert3(1) = 2
  vert3(2) = yslice
  vert3(3) = -zbot

  vert4(1) = -1.25d0
  vert4(2) = yslice
  vert4(3) = -zbot

  call axi_rectmesh(vert1, vert2, vert3, vert4, nover, maxtri, &
      ntri, triaskel)

  !
  ! now evaluate the scattered field on the same plane as constructed earlier
  !
  print *, '. . . evaluating the scattered field now'
  nsrc = n*nphi
  ntest = 3*ntri
  call prinf('nsrc = *', nsrc, 1)
  call prinf('ntest = *', ntest, 1)
  call axidebye_eval3d(zk, nsrc, xyzs, whts, rho, sigma, jcurr,&
      kcurr, ntest, triaskel, efield, hfield)

  do i = 1,ntri
    do j = 1,3
      call em3dpw(zk, kvec, pvec, phase, triaskel(1,j,i), &
          evec, hvec)
      detot(j,i) = evec(1)+efield(1,j,i)
    end do
  end do

  iw = 53
  m = 1
  print *, '. . . plotting the total field on a plane'
  call axi_vtk_flat_scalars(iw, ntri, triaskel, m, detot, &
      'abs of real part of total field')
  
  

  stop
  


end program debye_pec_pipe






subroutine em3dpw(zk, kvec, pvec, phase, xyz, efield, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: kvec(3), pvec(3), xyz(3)
  complex *16 :: zk, efield(3), hfield(3)

  real *8 :: hvec(10)
  complex *16 :: ima, cd

  !
  ! it must be the case that kvec \cdot pvec = 0
  !
  done = 1
  ima = (0,1)

  x = xyz(1)
  y = xyz(2)
  z = xyz(3)
  cd = zk*(kvec(1)*x + kvec(2)*y + kvec(3)*z) + phase

  efield(1) = pvec(1)*exp(ima*cd)
  efield(2) = pvec(2)*exp(ima*cd)
  efield(3) = pvec(3)*exp(ima*cd)

  call cross_prod3d(kvec, pvec, hvec)
  hfield(1) = hvec(1)*exp(ima*cd)
  hfield(2) = hvec(2)*exp(ima*cd)
  hfield(3) = hvec(3)*exp(ima*cd)


  return
end subroutine em3dpw





subroutine crelfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the relative error between the two fields
  !c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  dd2=0

  do i=1,n
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
    dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2
  end do

  err=sqrt(dd1/dd2)

  return
end subroutine crelfielderr





subroutine cfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the rmse between the two fields
  !c       x and y, i.e. calculates |x - y|/sqrt(n) where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  do i=1,n
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
  end do

  err=sqrt(dd1/n)

  return
end subroutine cfielderr
