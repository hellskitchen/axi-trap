program belatrmi_example1
  implicit real *8 (a-h,o-z)
  real *8 ps(10000),zs(10000),dpdt(10000), &
      dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000), &
      psg(10000),zsg(10000),dpdtg(10000),dsdtg(10000),&
      dzdtg(40000),dpdtg2(10000),dzdtg2(10000),&
      errs(10000),xs(10000),whts(10000),&
      tks(10000),yes(10000),yhs(10000),&
      ptest(10000),ztest(10000),&
      err1(10000),err2(10000),err3(10000),err4(10000),&
      ts(10000),ts2(10000),ts3(10000),dtds(10000),&
      dtds2(10000),&
      ps1(10000),zs1(10000),&
      dpds1(10000),dzds1(10000),dpds2(10000),dzds2(10000),&
      dpds21(10000),dzds21(10000),&
      surfrhom(4, 1000000),&
      volh(6, 100000), surfm(6, 1000000), vals(100000),&
      rvals(100000), rks(100000), slice(4,100000)
  real *8 :: svals(10000)
  
  integer ipvt(10000)
  complex *16 zk,zk0,cd,ima,cdh,cdb,zk2,cdt,cds,&
      zkinit,cda, flux, hdotn,&
      wz(1000000),wz2(1000000),sz(100000),&
      work(2000000), work2(2000000),&
      rho1(10000),rhom1(10000),alpha1,beta1,&
      rho(10000),rhom(10000),alpha,beta,&
      efield(3,10000),hfield(3,10000),&
      etest(3,10000),htest(3,10000),&
      etan(2,10000),htan(2,10000), zroot,&
      wndot(2000000),rhs(100000),sol(100000),&
      hin(2,10000),zjs(2,10000),zms(2,10000),&
      zjs1(2,10000),zms1(2,10000),wsave(2000000),&
      ctemp1(10000),ctemp2(10000), u1, u2, u3, uu,&
      f1, f2, f3, ff, w, f12, f13, f23, f123,&
      vec1(10),vec2(10),db1dp, db2dp, db3dp,&
      evals1(3,10000),evals2(3,10000),evals3(3,10000),&
      hvals1(3,10000),hvals2(3,10000),hvals3(3,10000),&
      u(10000),ux(10000),uxx(10000), det(10),&
      bfield1(10), bfield2(10), db1dz, db2dz, db3dz,&
      r1(10000), r2(10000), denom1, denom2,&
      db1da, db2da, db3da, diver, aflux, bflux
  complex *16 :: umat(1000000), vtmat(1000000)
  complex *16 :: aflux26, aflux50, aflux100, aflux200
  
  complex *16 :: rhomover(1000000), rhomhat(10000)
  complex *16 :: zmshat(2,10000)
  complex *16 :: zmsover(2,100000), cd1, cd2, cd3, phas

  real *8, allocatable :: rzplane(:,:,:)
  real *8, allocatable :: rcomp(:,:), tcomp(:,:), zcomp(:,:)
  real *8, allocatable :: pzgrid(:,:,:), pvals(:,:), zvals(:,:)
  real *8, allocatable :: tvals(:,:)
  
  complex *16, allocatable :: amat(:,:), ainv(:,:)
  complex *16, allocatable :: sigma(:,:), zcurrent(:,:,:)

  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)

  call prini(6,13)

  !
  ! load  the boundary curve
  !
  n = 26
  n = 50
  n = 100
  !n = 200
  call prinf('n=*', n, 1)

  if (n .eq. 26) then
    call load_ex1_n26(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  elseif (n .eq. 50) then
    call load_ex1_n50(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  elseif (n .eq. 100) then
    call load_ex1_n100(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  elseif (n .eq. 200) then
    call load_ex1_n200(ps, zs, dpdt, dzdt, dpdt2, dzdt2)
  endif

  rl = 2*pi
  h = rl/n

  rlen = 0
  dd = 0
  do i=1,n
    dd=dpdt(i)**2+dzdt(i)**2
    dsdt(i)=sqrt(dd)
    rlen = rlen + dsdt(i)*h
  end do

  call prin2('total arclength of boundary curve = *', rlen, 1)
        
  iw=11
  itype=2
  call pyplot(iw, ps, zs, n, itype, 'boundary curve*')

  !
  ! set some subroutine parameters
  !
  zk = 2.281569790667874d0
  call prin2('zk=*',zk,2)

  mode = 0 
  m = mode
  call prinf('mode=*',mode,1)
  
  eps=1.0d-12
  call prin2('kernel eval precision=*',eps,1)
  
  norder=4
  norder=8
  norder=16
  call prinf('alpert order=*',norder,1)
  
  !
  ! construct the rhs
  !
  do i = 1,n
    rhs(i) = 0
  end do

  tau = 1
  ind = 1
  nnn = n+1

  aflux = -zk*8.349343947095354d0
  bflux = 0
  rhs(n+1) = aflux*tau + bflux*(1-tau)

  !
  ! construct the matrix
  !
  allocate( amat(nnn,nnn) )
  call cpu_time(t0)
  call creabeltrami_interior(eps, zk, m, n, rl, ps, zs, dpdt, &
      dzdt, dpdt2, dzdt2, norder, ind, tau, amat)
  call cpu_time(t1)
  call prin2('matrix build time = *', t1-t0, 1)

  
  !
  ! now solve the system
  !
  print *
  print *, '... solving system ...'
  call zgausselim(nnn, amat, rhs, info, sol, dcond)
  call prin2('from zgausselim, dcond=*', dcond, 1)

  call zsvd(nnn, nnn, amat, umat, svals, vtmat)
  call prin2('sing vvals = *', svals, nnn)
  call prin2('actual cond = *', svals(1)/svals(nnn), 1)
  stop
  
  !
  ! compute fourier decay of the solution
  !        
  call ccopy601(n, sol, rhom)
  do i = 1,n
    rhom(i) = rhom(i)*dsdt(i)/n
  end do

  call zffti(n, work)
  call zfftf(n, rhom, work)
  !call prin2('fft of density = *', rhom, 2*n)

  bnorm = abs(rhs(n+1))
  nhalf = n/2
  err = abs(rhom(nhalf-1))**2 + abs(rhom(nhalf))**2 + &
      abs(rhom(nhalf+1))**2
  err = sqrt(err/3)/bnorm
  call prin2('est rel error from spectral decay = *', err, 1)

  call ccopy601(n, sol, rhom)
  alpha = sol(n+1)
  call prin2('beltrami density = *', rhom, 30)
  call prin2('alpha=*', alpha, 2)

  !
  !  construct the beltrami current
  !
  call beltramicurrent(eps, zk, m, n, rl, ps, zs, dpdt, &
      dzdt, dpdt2, dzdt2, norder, rhom, alpha, rho, &
      zjs, zms)
  
  cds = 0
  do i = 1, n
    cds = cds + rhom(i)*ps(i)*h*dsdt(i)
  enddo

  if (m .eq. 0) then
    call prin2('integral of rhom around curve=*',cds,2)
  endif

  !
  ! evaluate at a test point vs. exact solution
  !
  p = 1.2d0
  z = .25d0
  p = .5d0
  z = -1
  theta = 0

  call beltramieva(eps, zk, m, n, rl, ps, zs,  &
      dpdt, dzdt, dpdt2, dzdt2, rhom, &
      alpha, zms, p, theta, z, bfield1)
  
  call prin2('bfield at p,z = *', bfield1, 6)

  write(6,*) 'Br = ', bfield1(1)
  write(6,*) 'Bphi = ', bfield1(2)
  write(6,*) 'Bz = ', bfield1(3)


  !
  ! compute the boundary values of B at another location
  !

  n2 = 100
  if (n2 .eq. 26) then
    call load_interior_n26(psg, zsg, dpdtg, dzdtg, dpdtg2, dzdtg2)
  elseif (n2 .eq. 50) then
    call load_interior_n50(psg, zsg, dpdtg, dzdtg, dpdtg2, dzdtg2)
  elseif (n2 .eq. 100) then
    call load_interior_n100(psg, zsg, dpdtg, dzdtg, dpdtg2, dzdtg2)
  elseif (n2 .eq. 200) then
    call load_interior_n200(psg, zsg, dpdtg, dzdtg, dpdtg2, dzdtg2)
  endif


  do i = 1,n2

    theta = 0
    call beltramieva(eps, zk, m, n, rl, ps, zs,  &
        dpdt, dzdt, dpdt2, dzdt2, rhom, &
        alpha, zms, psg(i), theta, zsg(i), hfield(1,i))

    dsdtg(i) = sqrt(dpdtg(i)**2 + dzdtg(i)**2)
    dpdsg = dpdtg(i)/dsdtg(i)
    dzdsg = dzdtg(i)/dsdtg(i)

    htan(1,i) = dpdsg*hfield(1,i) + dzdsg*hfield(3,i)
    htan(2,i) = hfield(2,i)
  end do

  call ndotdirect(n2, dpdtg, dzdtg, hfield, vals)
  call prin2('ndot b on inner boundary = *', vals, 2*n2)
    

  cda = 0
  h2 = 2*pi/n2
  do i = 1,n2
    cda = cda + h2*dsdtg(i)*htan(1,i)
  end do

  cda = -cda/zk
  print *, 'zk = ', zk
  call prin2('toroidal flux for inner surface = *', cda, 2)
  print *, 'cda = ', cda
  

  p1 = psg(1)
  z1 = zsg(1)
  theta = 0
  print *
  print *
  print *, 'p1 = ', p1
  print *, 'z1 = ', z1
  call beltramieva(eps, zk, m, n, rl, ps, zs,  &
      dpdt, dzdt, dpdt2, dzdt2, rhom, &
      alpha, zms, p1, theta, z1, bfield1)

  call prin2('at edge of inner, bfield = *', bfield1, 6)
  write(6,*) 'Br = ', bfield1(1)
  write(6,*) 'Bphi = ', bfield1(2)
  write(6,*) 'Bz = ', bfield1(3)
  
  stop




  
  ifplot = 1
  if (ifplot .eq. 0) stop
  !
  ! make a heat plot of the thing
  !
  a = 0.0d0
  b = 2.0d0
  nr = 101

  c = -2.0d0
  d = 2.0d0
  ni = 101

  allocate(pzgrid(2,ni,nr))
  do i = 1,nr
    do j = 1,ni
      p1 = a + (b-a)*i/(nr+1)
      z1 = d - (d-c)*j/(ni+1)
      pzgrid(1,j,i) = p1
      pzgrid(2,j,i) = z1
    end do
  end do
  
  print *
  print *, '---making plot of the beltrami field---'

  dtot = nr*ni*done
  ntot = 0

  allocate(pvals(ni,nr), tvals(ni,nr), zvals(ni,nr))

  !$omp parallel do default(shared) &
  !$omp    private(i,j,p1,z1,theta1,bfield1)
  do i = 1, nr
    do j = 1, ni

      ntot = ntot + 1
      if (mod(ntot,250) .eq. 1) then
        print *, 'percent done = ', ntot/dtot
      endif
          
      p1 = pzgrid(1,j,i)
      z1 = pzgrid(2,j,i)
      theta1 = 0

      call beltramieva(eps, zk, m, n, rl, ps, zs, &
          dpdt, dzdt, dpdt2, dzdt2, rhom,& 
          alpha, zms, p1, theta1, z1, bfield1)

      pvals(j,i) = bfield1(1)
      tvals(j,i) = bfield1(2)
      zvals(j,i) = bfield1(3)
          
    enddo
  enddo
  !$omp end parallel do

  iw = 65
  call pyimage2_interior(iw, ni, nr, pvals, ps, zs, n, a, b, &
      c, d, 'r component*')

  iw = 66
  call pyimage2_interior(iw, ni, nr, tvals, ps, zs, n, a, b, &
      c, d, 'theta component*')

  iw = 67
  call pyimage2_interior(iw, ni, nr, zvals, ps, zs, n, a, b, &
      c, d, 'z component*')
  
  stop
end program belatrmi_example1


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
