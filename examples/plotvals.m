

X = load('xmesh.dat');
Y = load('ymesh.dat');
Z = load('zmesh.dat');
C = load('colors.dat');

h = surf(X,Y,Z,C);
set(h, 'edgealpha', .1);
axis equal;