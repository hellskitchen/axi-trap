
HOST = osx-gfortran
HOST = osx-intel
HOST = linux-gfortran
PROJECT = int2

ifeq ($(HOST),osx-gfortran)
  FC = gfortran -c -w
  FFLAGS = -O2
  FLINK = gfortran -w -o $(PROJECT) -framework accelerate
endif

ifeq ($(HOST),linux-gfortran)
  FC = gfortran -c -w
  FFLAGS = -O2 -std=legacy
  FLINK = gfortran -w -o $(PROJECT) -llapack -lblas
endif

ifeq ($(HOST),osx-gfortran-openmp)
  FC = gfortran -c -w
  FFLAGS = -O2 -fopenmp
  FLINK = gfortran -w -fopenmp -Wl,-stack_size,0x40000000\
        -o $(PROJECT) -framework accelerate
  export OMP_NUM_THREADS=2
  export OMP_STACKSIZE=2048
endif

ifeq ($(HOST),osx-intel)
  FC = ifort -c -w
  FFLAGS = -O2
  FLINK = ifort -w -mkl -o $(PROJECT)
endif

.PHONY: all clean list

SOURCES =  beltrami_shell_test1.f90 \
  ../src/beltrami.f \
  ../src/beltrami_new.f90 \
  ../src/kernels.f \
  ../../utils/gammanew_eval.f \
  ../../utils/prini.f \
  ../../utils/legeexps.f \
  ../../utils/lapack_wrap.f90 \
  ../../utils/pplot.f \
  ../../utils/cadapgau_k16.f \
  ../../utils/elliptic_ke.f \
  ../../utils/chebexps.f \
  ../../utils/csvdpiv.f \
  ../../utils/mysecond.f \
  ../../utils/dfft.f \
  ../../utils/corrand.f \
  ../../utils/alpert.f \
  ../../hellskitchen/Greengard/Trapquads2d/fdiffc.f \
  ../../hellskitchen/Axi3DLibraries/surfdivaxi.f \
  ../../hellskitchen/Axi3DLibraries/surflapaxi.f \
  ../../hellskitchen/Axi3DLibraries/surfgradaxi.f \
  ../../hellskitchen/Axi3DLibraries/surfcurlaxi.f \
  ../../hellskitchen/Common/zgecoall.f \
  ../../hellskitchen/Axi3DLibraries/ode_periodic_surflap.f \
  ../../hellskitchen/Axi3DLibraries/surflapinteq.f

OBJECTS = $(patsubst %.f,%.o,$(patsubst %.f90,%.o,$(SOURCES)))

#
# use only the file part of the filename, then manually specify
# the build location
#

%.o : %.f
	$(FC) $(FFLAGS) $< -o $@

%.o : %.f90
	$(FC) $(FFLAGS) $< -o $@

all: $(OBJECTS)
	rm -f $(PROJECT)
	$(FLINK) $(OBJECTS)
	./$(PROJECT)

clean:
	rm -f $(OBJECTS)
	rm -f $(PROJECT)

list: $(SOURCES)
	$(warning Requires:  $^)
