program beltrami_interior_test1
  implicit real *8 (a-h,o-z)
  real *8 ps(10000),zs(10000),dpdt(10000), &
      dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000), &
      psg(10000),zsg(10000),dpdtg(10000),dsdtg(10000), &
      dzdtg(40000),dpdtg2(10000),dzdtg2(10000), &
      errs(10000),xs(10000),whts(10000), &
      tks(10000),yes(10000),yhs(10000),&
      ptest(10000),ztest(10000),&
      err1(10000),err2(10000),err3(10000),err4(10000),&
      ts(10000),ts2(10000),ts3(10000),dtds(10000),&
      dtds2(10000),&
      ps1(10000),zs1(10000),&
      dpds1(10000),dzds1(10000),dpds2(10000),dzds2(10000),&
      dpds21(10000),dzds21(10000),&
      surfrhom(4, 1000000),&
      volh(6, 100000), surfm(6, 1000000), vals(100000),&
      rvals(100000), rks(100000), slice(4,100000)
  
  integer ipvt(10000)
  complex *16 zk,zk0,cd,ima,cdh,cdb,zk2,cdt,cds,cd2,&
      amat(4000000),zkinit,cda, flux, hdotn,&
      wz(1000000),wz2(1000000),sz(100000),&
      work(2000000), work2(2000000),&
      rho1(10000),rhom1(10000),alpha1,beta1,&
      rho(10000),rhom(10000),alpha,beta,&
      efield(3,10000),hfield(3,10000),&
      etest(3,10000),htest(3,10000),&
      etan(2,10000),htan(2,10000), zroot,&
      wndot(2000000),rhs(100000),sol(100000),&
      vals1(100),vals2(100),vals3(100),&
      hin(2,10000),zjs(2,10000),zms(2,10000),&
      zjs1(2,10000),zms1(2,10000),wsave(2000000),&
      ctemp1(10000),ctemp2(10000), u1, u2, u3, uu,&
      f1, f2, f3, ff, w, f12, f13, f23, f123,&
      vec1(10),vec2(10),db1dp, db2dp, db3dp,&
      evals1(3,10000),evals2(3,10000),evals3(3,10000),&
      hvals1(3,10000),hvals2(3,10000),hvals3(3,10000),&
      u(10000),ux(10000),uxx(10000), det(10),&
      bfield1(10), bfield2(10), db1dz, db2dz, db3dz,&
      r1(10000), r2(10000), denom1, denom2,&
      db1da, db2da, db3da, diver
  
  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)

  call prini(6,13)
  print *, 'enter n:'
  read *, n
  call prinf('n=*',n,1)

  !
  ! create the scattering surface
  !
  rl=2*pi
  h=rl/n
  do i=1,n
    t=h*(i-1)
    call funcurve(t,ps(i),zs(i),dpdt(i),dzdt(i), &
        dpdt2(i),dzdt2(i))
    dd=dpdt(i)**2+dzdt(i)**2
    dsdt(i)=sqrt(dd)
  end do

  iw=11
  itype=1
  call pyplot(iw, ps, zs, n, itype, 'scattering curve*')

  !
  ! build a source surface in the exterior
  !
  sc = 0
  do i=1,n
    t=h*(i-1)
    call fsource(t,psg(i),zsg(i),dpdtg(i),dzdtg(i), &
        dpdtg2(i),dzdtg2(i))
    dd=dpdtg(i)**2+dzdtg(i)**2
    dsdtg(i)=sqrt(dd)
    sc = sc + h*dsdtg(i)*2*pi*psg(i)
  end do
  
  iw=12
  itype=1
  call pyplot2(iw, ps, zs, n, itype, psg, zsg, n, itype, &
      'scattering and source*')
  
  !
  ! set some subroutine parameters
  !
  !
  ! zk is the beltrami parameter
  !
  zk = 1.0d0 + ima*1.0d-15
  zk = 2.3d0 + ima*1.0d-15

  call prin2(' *',pi,0)
  call prin2('zk=*',zk,2)

  mode = 0
  m = mode
  call prinf('mode=*',mode,1)

  eps=1.0d-12
  call prin2('kernel eval precision=*',eps,1)

  norder=4
  norder=8
  call prinf('alpert order=*',norder,1)
  
  !
  ! construct a rhom, alpha on the source curve, then build the
  ! OUTGOING current
  !
  do i = 1, n
    t=2*pi*(i-1)/n
    rhom1(i)=(pi-ima)*sin(2*t)/psg(i)/dsdtg(i)/sc*100
  enddo

  if (m .eq. 0) then
    alpha1 = (3.2d0 + ima*5.2d0)/sc
  else
    alpha1=0
    beta1=0
  endif

  call beltrami_current_outgoing(zk, m, n, rl, psg, &
      zsg, dpdtg, dzdtg, dpdtg2, dzdtg2, rhom1, alpha1, zms1)

  !
  ! test that the field just generated is in fact, a beltrami
  ! field
  !

  iffinite = 0
  if (iffinite .eq. 1) then

    ifj1=1

    p1 = .8d0
    theta1 = 0
    z1 = -.4d0

    print *
    print *, '. . . testing finite diff approx . . . '
    call prin2('test point, p1 = *', p1, 1)
    call prin2('test point, theta1 = *', theta1, 1)
    call prin2('test point, z1 = *', z1, 1)

    call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
        dpdtg2, dzdtg2, rhom1, zms1, p1, theta1, z1, bfield1)
    call prin2('bfield1 = *', bfield1, 6)

    !
    ! compute the curl via finite differences
    !
    h3 = 1.0d-5

    call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
        dpdtg2, dzdtg2, rhom1, zms1, p1+h3, theta1, z1, vec2)

    call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
        dpdtg2, dzdtg2, rhom1, zms1, p1-h3, theta1, z1, vec1)

    db1dp = (vec2(1) - vec1(1))/2/h3
    db2dp = (vec2(2) - vec1(2))/2/h3
    db3dp = (vec2(3) - vec1(3))/2/h3
    
    
    call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
        dpdtg2, dzdtg2, rhom1, zms1, p1, theta1+h3, z1, vec2)

    call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
        dpdtg2, dzdtg2, rhom1, zms1, p1, theta1-h3, z1, vec1)

    db1da = (vec2(1) - vec1(1))/2/h3
    db2da = (vec2(2) - vec1(2))/2/h3
    db3da = (vec2(3) - vec1(3))/2/h3
    
    call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
        dpdtg2, dzdtg2, rhom1, zms1, p1, theta1, z1+h3, vec2)

    call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
        dpdtg2, dzdtg2, rhom1, zms1, p1, theta1, z1-h3, vec1)

    db1dz = (vec2(1) - vec1(1))/2/h3
    db2dz = (vec2(2) - vec1(2))/2/h3
    db3dz = (vec2(3) - vec1(3))/2/h3
    
    bfield2(1) = 1/p1*db3da - db2dz
    bfield2(2) = db1dz - db3dp
    bfield2(3) = bfield1(2)/p1 + db2dp - bfield1(1)*ima*mode/p1
    
    call prin2('by finite difference, bfield2 = *', bfield2, 6)
    
    dd1 = abs(zk*bfield1(1) - bfield2(1))
    dd2 = abs(zk*bfield1(2) - bfield2(2))
    dd3 = abs(zk*bfield1(3) - bfield2(3))
    dd = sqrt(dd1**2 + dd2**2 + dd3**2)
    
    rr1 = abs(bfield1(1))
    rr2 = abs(bfield1(2))
    rr3 = abs(bfield1(3))
    rnorm = sqrt(rr1**2 + rr2**2 + rr3**2)
    
    call prin2('rel l2 err in mag beltrami = *', dd/rnorm, 1)
    
    diver = db1dp + bfield1(1)/p1 + db2da/p1 + db3dz
    call prin2('and divergence = *', diver, 2)

  end if
  
  !
  !       calculate the incoming data, n \cdot H
  !
  print *
  print *
  print *, '... calculating incoming data ...'

  theta = 0
  do i = 1, n

    call beltramieva(eps, zk, m, n, rl, psg, zsg, &
        dpdtg, dzdtg, dpdtg2, dzdtg2, rhom1, alpha1, zms1,&
        ps(i), theta, zs(i), hfield(1,i))
    
    dpds = dpdt(i)/dsdt(i)
    dzds = dzdt(i)/dsdt(i)
    
    htan(1,i) = dpds*hfield(1,i) + dzds*hfield(3,i)
    htan(2,i) = hfield(2,i)
  enddo

  call ndotdirect(n, dpdt, dzdt, hfield, rhs)

  !
  ! ...add in (flux) conditions for harmonic vectorfields
  !
  ind = 1
  
  if (m .ne. 0) then
    rhs(n+1) = 0
  else

    call beltrami_line_integrals(n, ps, zs, h, dsdt, &
        htan, ind, cdt, cdb)
    call prin2('integral on A cycle = *', cdt, 2)
    call prin2('integral on B cycle = *', cdb, 2)

    tau = 1.0d0
    tau = .5d0
    call prin2('tau = *', tau, 1)
    rhs(n+1) = tau*cdt + (1-tau)*cdb
    call prin2('extra condition = *', rhs(n+1), 2)
  endif

  !
  ! construct the matrix and solve
  !
  call creabeltrami_interior(eps, zk, m, n, rl, ps, zs, dpdt, &
      dzdt, dpdt2, dzdt2, norder, ind, tau, amat)

  print *
  print *, '... solving system ...'

  nnn = n+1
  call zgausselim(nnn, amat, rhs, info, sol, dcond)
  call prin2('from zgausselim, dcond=*', dcond, 1)

  call ccopy601(n, sol, rhom)

  alpha = sol(n+1)
  call prin2('alpha=*', alpha, 2)


  if (m .eq. 0) then
    cds = 0
    do i = 1, n
      cds = cds + rhom(i)*ps(i)*h*dsdt(i)
    enddo
    call prin2('integral of rhom around curve=*',cds,2)
  endif

  !
  ! test the interior field at a point
  !
  print * 
  print *, '... testing solution ...'
  p1 = .7d0
  theta1 = 0
  z1 = .5d0
  call prin2('test point, p1 = *', p1, 1)
  call prin2('test point, theta1 = *', theta1, 1)
  call prin2('test point, z1 = *', z1, 1)

  call beltrami_eval(zk, m, n, rl, psg, zsg, dpdtg, dzdtg,&
      dpdtg2, dzdtg2, rhom1, zms1, p1, theta1, z1, bfield1)
  call prin2('known beltrami field = *', bfield1, 6)
  
  call beltrami_current_incoming(zk, m, n, rl, ps, &
      zs, dpdt, dzdt, dpdt2, dzdt2, rhom, alpha, zms)

  call beltrami_eval(zk, m, n, rl, ps, zs, dpdt, dzdt,&
      dpdt2, dzdt2, rhom, zms, p1, theta1, z1, bfield2)
  call prin2('scattered beltrami field = *', bfield2, 6)
  
  call crelfielderr(1, bfield1, bfield2, err)
  call prin2('relative error = *', err, 1)

  stop
end program beltrami_interior_test1

!
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!
subroutine crelfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !
  ! calculates the relative error between the two fields
  ! x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
  ! sum of squares of the field vector at each of the n points
  !
  dd1=0
  dd2=0

  do i=1,n

    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)

    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
    dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2

  end do
  
  err=sqrt(dd1/dd2)

  return
end subroutine crelfielderr





        subroutine cfielderr(n,x,y,err)
        implicit real *8 (a-h,o-z)
        complex *16 x(3,1),y(3,1),cd1,cd2,cd3
!c
!c       calculates the rmse between the two fields
!c       x and y, i.e. calculates |x - y|/sqrt(n) where |y|**2 is the
!c       sum of squares of the field vector at each of the n points
!c
        dd1=0
        do 1600 i=1,n

        cd1=x(1,i)-y(1,i)
        cd2=x(2,i)-y(2,i)
        cd3=x(3,i)-y(3,i)

        dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
 1600 continue

        err=sqrt(dd1/n)

        return
        end





        subroutine fsource(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
!c
!c       controls the surface of the field testing torus
!c
!c       a small circle
!c
        a=.2d0
        b=.2d0
!c
!cc        a = 3.6d0
!cc        b = 3.7d0
!c
        x0=2.5d0
        y0=2.5d0

        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)

        return
        end





        subroutine funcurve(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
!c
!c       controls the scattering surface
!c
        done=1
        pi=4*atan(done)
!c
!c       an antoine-jeff like geometry
!c
        e = .32d0
        alpha = 0.336d0
        rkap = 1.7d0

        x0 = 1.0d0
        y0 = 0
     
        x = x0 + e*cos(t+alpha*sin(t))
        y = y0 + e*rkap*sin(t)

        dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
        dydt = e*rkap*cos(t)

        dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
         +e*sin(t+alpha*sin(t))*alpha*sin(t)
        dydt2 = -e*rkap*sin(t)

        !!!return

!c
!c       ellipse
!c
        x0 = 1.00d0
        y0 = 0
        
        a = .95d0
        b = 1.25d0

        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)
        return

!c
!c       squiggly thing
!c
        a=.2d0
        b=.3d0

        n=4

        x=2+(1+a*cos(n*t))*cos(t)
        y=2+(1+b*sin(t))*sin(t)

        dxdt=-(1+a*cos(n*t))*sin(t)-a*n*sin(n*t)*cos(t)
        dydt=(1+b*sin(t))*cos(t)+b*cos(t)*sin(t)

        dxdt2=-(1+a*cos(n*t))*cos(t)+a*n*sin(n*t)*sin(t) &
           +a*n*sin(n*t)*sin(t)-a*n*n*cos(n*t)*cos(t)

        dydt2=-(1+b*sin(t))*sin(t)+b*cos(t)*cos(t) &
           +b*cos(t)*cos(t)-b*sin(t)*sin(t)


        return
        end
