program debye_test1

  implicit real *8 (a-h,o-z)

  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)
  real *8 :: gxys(2,10000), gdxys(2,10000), gd2xys(2,10000)
  real *8 :: dsdt(10000), center(10)
  
  real *8 :: ps(10000),zs(10000),dpdt(10000)
  real *8 :: dzdt(10000),dpdt2(10000),dzdt2(10000)
  real *8 :: psg(10000),zsg(10000),dpdtg(10000),dsdtg(10000)
  real *8 :: dzdtg(40000),dpdtg2(10000),dzdtg2(10000)
  real *8 :: errs(10000),xs(10000),whts(10000)
  real *8 :: tks(10000),yes(10000),yhs(10000)
  real *8 :: ptest(10000),ztest(10000)
  real *8 :: err1(10000),err2(10000),err3(10000),err4(10000)
  real *8 :: ts(10000),ts2(10000),ts3(10000),dtds(10000)
  real *8 :: dtds2(10000)
  real *8 :: ps1(10000),zs1(10000)
  real *8 :: dpds1(10000),dzds1(10000),dpds2(10000),dzds2(10000)
  real *8 :: dpds21(10000),dzds21(10000)
  real *8 :: xyz1(10), xyz(10), ptz(10)

  real *8, allocatable :: dws0all(:,:,:), dws0gradall(:,:,:,:)
  real *8, allocatable :: dws0grad0all(:,:,:,:)
  
  integer :: ipvt(10000)

  complex *16 :: zk,zk0,cd,ima,cdh,cdb,zk2,cdt,cds,cd2
  complex *16 :: amat(4000000),zkinit,cda
  complex *16 :: wz(1000000),wz2(1000000),sz(100000)
  complex *16 :: work(2000000)
  complex *16 :: rho1(10000),rhom1(10000),alpha1,beta1
  complex *16 :: rho(10000),rhom(10000),alpha,beta
  complex *16 :: efield(3,10000),hfield(3,10000)
  complex *16 :: etest(3,10000),htest(3,10000)
  complex *16 :: etan(2,10000),htan(2,10000)
  complex *16 :: wndot(2000000),rhs(100000),sol(100000)
  complex *16 :: rhs2(100000)
  complex *16 :: vals1(100),vals2(100),vals3(100)
  complex *16 :: hin(2,10000),zjs(2,10000),zms(2,10000)
  complex *16 :: zjs1(2,10000),zms1(2,10000),wsave(2000000)
  complex *16 :: ctemp1(10000),ctemp2(10000)
  complex *16 :: vec1(3,10000),vec2(3,10000)
  complex *16 :: evals1(3,10000),evals2(3,10000),evals3(3,10000)
  complex *16 :: hvals1(3,10000),hvals2(3,10000),hvals3(3,10000)
  complex *16 :: u(10000),ux(10000),uxx(10000)
  complex *16 :: evec(10), hvec(10)

  complex *16, allocatable :: ws0all(:,:,:), ws0gradall(:,:,:,:)
  complex *16, allocatable :: ws0grad0all(:,:,:,:)
  complex *16, allocatable :: ws0divall(:,:,:)
  complex *16, allocatable :: ws0div(:,:)
  
  external ghfun2,funcurve,funcurve2, g0mall

  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)

  call prini(6,13)
  !call prin2('enter n:*',pi,0)
  !read *, n
  n = 101
  call prinf('n=*',n,1)

  !
  ! create the scattering surface
  !
  rl=2*pi
  h=rl/n
  par1 = 0
  par2 = 0
  do i = 1,n
    t=h*(i-1)
    call funcurve_new(t, par1, par2, xys(1,i), dxys(1,i), d2xys(1,i))
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    dpdt2(i) = d2xys(1,i)
    dzdt2(i) = d2xys(2,i)
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  enddo

  iw = 10
  itype = 1
  call zpyplot(iw, xys, n, itype, 'scattering curve*')

  !
  ! set some subroutine parameters
  !
  zk=2.2d0+ima*1.242359537d-10
  !zk = zk/1000
  call prin2(' *',pi,0)
  call prin2('zk=*',zk,2)

  mode = 1
  m = mode
  maxm = abs(mode) + 5
  call prinf('mode=*',mode,1)
  eps = 1.0d-10
  call prin2('kernel eval precision=*',eps,1)

  norder = 0
  norder = 4
  norder = 8
  !norder=16
  call prinf('alpert order=*',norder,1)


  !
  ! build a source surface
  !
  do i=1,n
    t=h*(i-1)
    call fcurve_source(t,psg(i),zsg(i),dpdtg(i),dzdtg(i), &
        dpdtg2(i),dzdtg2(i))
    dd=dpdtg(i)**2+dzdtg(i)**2
    dsdtg(i)=sqrt(dd)
    gxys(1,i) = psg(i)
    gxys(2,i) = zsg(i)
    gdxys(1,i) = dpdtg(i)
    gdxys(2,i) = dzdtg(i)
    gd2xys(1,i) = dpdtg2(i)
    gd2xys(2,i) = dzdtg2(i)
  end do
  
  iw = 11
  itype = 3
  call zpyplot2(iw, xys, n, itype, gxys, n, &
      itype, 'scatterer and source curves*')




  !
  ! construct a rho, rhom, alpha, and beta on the source curve
  !
  print *, ''
  print *, ' . . . generating data'
  do i = 1,n
    t=2*pi*(i-1)/n
    rho1(i) = (5+ima)*cos(1*t)/psg(i)/dsdtg(i)
    rhom1(i) = (pi-ima)*sin(2*t)/psg(i)/dsdtg(i)
  enddo

  if (m .eq. 0) then
    alpha1=1+ima
    beta1=3-2*ima
  else
    alpha1=0
    beta1=0
  endif

  
  
  !
  ! construct E on the scattering curve as induced
  ! from the source curve, and grab tangential projection
  !

  call axidebye_jmbuilder(zk, m, n, gxys, gdxys, gd2xys, h, &
    rho1, rhom1, alpha1, beta1, zjs1, zms1)


  !$omp parallel do default(shared) &
  !$omp     private(p, theta, z, xyz, dpds, dzds)
  do i = 1,n
    p = xys(1,i)
    theta = 0
    z = xys(2,i)
    xyz(1) = p*cos(theta)
    xyz(2) = p*sin(theta)
    xyz(3) = z
    
    call axidebye_mode_eval(eps, zk, m, n, rl, psg, zsg, &
        dpdtg, dzdtg, rho1, rhom1, &
        zjs1, zms1, xyz, efield(1,i), hfield(1,i))

    dpds=dpdt(i)/dsdt(i)
    dzds=dzdt(i)/dsdt(i)
    etan(1,i)=dpds*efield(1,i)+dzds*efield(3,i)
    etan(2,i)=efield(2,i)

  end do
  !$omp end parallel do

  
  !
  ! evaluate S0 surf div of etan now
  !

  allocate(ws0div(n,2*n))
  call crea_s0div1(norder, n, xys, dxys, h, mode, ws0div)
  call zmatvec(n, 2*n, ws0div, etan, rhs)

  !
  ! construct n dot H on the scattering curve
  !  
  call ndotdirect(n,dpdt,dzdt,hfield,rhs(n+1))

  !
  ! ...add in conditions for harmonic vectorfields
  !
  if (m .ne. 0) then
    rhs(2*n+1)=0
    rhs(2*n+2)=0
  else
    
    !
    ! if here, mode is 0 calculate integral conditions for harmonic forms
    ! first the condition e dot tangent
    !    
    cdt=0
    do i=1,n
      cdt=cdt+h*dsdt(i)*etan(1,i)
    end do
    
    rhs(2*n+1)=2*pi*cdt
    call prin2('integral of e dot dtau = *', cdt, 2)
    
    !
    ! calculate the integral of E_in dot theta using
    ! stokes, which is H_in dot da on spanning surface
    !
    ind = 1
    do i = 1,n
      if (ps(i) .lt. ps(ind)) ind=i
    end do
    
    a=0
    b=ps(ind)
    itype1 = 1
    k = 64
    call legeexps(itype1,k,xs,par1,par2,whts)

    do i=1,k
      xs(i)=(xs(i)+1)/2*(b-a)
      whts(i)=whts(i)/2*(b-a)
    end do
    
    cdh=0
    theta=0


    do i = 1,k
      xyz(1) = xs(i)*cos(theta)
      xyz(2) = xs(i)*sin(theta)
      xyz(3) = zs(ind)
      call axidebye_mode_eval(eps, zk, m, n, rl, psg, zsg, &
          dpdtg, dzdtg, rho1, rhom1, &
          zjs1, zms1, xyz, evec, hvec)
      cdh = cdh + whts(i)*hvec(3)*xs(i)
    end do
    
    cdh=2*pi*ima*cdh
    rhs(2*n+2)=cdh
    
    ! cd = etan(2,ind)*2*pi*ps(ind)/zk
    ! call prin2('integral of e dot dtheta over zk =*', cd, 2)
    ! stop

    !
    !  now calculate the integral of A_in dot theta and compare
    !
    ! cda=0
    ! theta=0    
    ! call adebyeeva(eps,zk,m,n,rl,psg,zsg,dpdtg,dzdtg, &
    !     dpdtg2,dzdtg2,norder,ifj1,rho1,rhom1,zjs1,zms1, &
    !     alpha1,beta1,ps(ind),theta,zs(ind),vals1)
    
    ! cda=vals1(2)*2*pi*ps(ind)
    ! call prin2('integral of a_in dot theta=*',cda,2)
    ! call prin2('absolute difference=*',cda-cdh,2)
    ! stop

  end if
  


  !
  ! construct the debye matrix to invert
  !
  print *, ''
  print *, ' . . . building matrix'
  call axidebye_pec_mat1(ier, norder, n, xys, dxys, d2xys, h, &
      zk, mode, ind, amat)

  stop

  print *, ' . . . solving'
  nnn=2*n+2
  !!!!call prin2('rhs=*',rhs,nnn*2)
  call zgausselim(nnn, amat, rhs, info, sol, dcond)
  call prin2('from lapack, dcond = *', dcond, 1)
        
  call ccopy601(n,sol(1),rho)
  call ccopy601(n,sol(n+1),rhom)
  alpha = sol(2*n+1)
  beta = sol(2*n+2)

  print *
  call prin2('rho=*',rho,30)
  call prin2('rhom=*',rhom,30)
  call prin2('alpha=*',alpha,2)
  call prin2('beta=*',beta,2)

  cdt=0
  cds=0
  do i=1,n
    cdt=cdt+rho(i)*ps(i)*h*dsdt(i)
    cds=cds+rhom(i)*ps(i)*h*dsdt(i)
  end do
  
  if (m .eq. 0) then
    call prin2('integral of rho around curve=*',cdt,2)
    call prin2('integral of rhom around curve=*',cds,2)
  endif


  !
  ! test the exterior field at a point 
  !
  xyz1(1) = 5
  xyz1(2) = 6
  xyz1(3) = 4

  p1 = sqrt(xyz1(1)**2 + xyz1(2)**2)
  theta1 = atan2(xyz1(2), xyz1(1))
  z1 = xyz1(3)

  call axidebye_jmbuilder(zk, m, n, xys, dxys, d2xys, h, &
      rho, rhom, alpha, beta, zjs, zms)

  call axidebye_mode_eval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      rho, rhom, zjs, zms, xyz1, etest, htest)


  !
  ! calculate the exact solution
  !
  call axidebye_mode_eval(eps, zk, m, n, rl, psg, zsg, &
      dpdtg, dzdtg, rho1, rhom1, &
      zjs1, zms1, xyz1, efield, hfield)

  print *
  call prin2('test point, xyz1 = *', xyz1, 3)
  print *
  
  call prin2('at single test point, e exact = *', efield, 6)
  call prin2('at single test point, h exact = *', hfield, 6)
  
  print *
  call prin2('at single test point, e solve = *', etest, 6)
  call prin2('at single test point, h solve = *', htest, 6)
  
  print *
  call cfielderr(1, etest, efield, eerr)
  call cfielderr(1, htest, hfield, herr)
  call prin2('rmse e error = *', eerr, 1)
  call prin2('rmse h error = *', herr, 1)
  

  stop
        
end program debye_test1





subroutine crelfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the relative error between the two fields
  !c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  dd2=0
  
  do i=1,n

    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
    dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2
    
  end do
  
  err=sqrt(dd1/dd2)

  return
end subroutine crelfielderr





subroutine cfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the rmse between the two fields
  !c       x and y, i.e. calculates |x - y|/sqrt(n) where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  do i=1,n
    
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
  end do
  
  err=sqrt(dd1/n)

  return
end subroutine cfielderr







subroutine funcurve_new(t, par1, par2, xy, dxy, d2xy)
  implicit real *8 (a-h,o-z)
  real *8 :: xy(2), dxy(2), d2xy(2)

  x0 = 2
  y0 = 2
  a = 1.4d0
  b = 1d0

  xy(1) = x0 + a*cos(t)
  xy(2) = y0 + b*sin(t)

  dxy(1) = -a*sin(t)
  dxy(2) = b*cos(t)

  d2xy(1) = -a*cos(t)
  d2xy(2) = -b*sin(t)

  return

  !
  ! squiggly thing
  !
  a=.2d0
  b=.3d0
  n=4

  xy(1) = 2+(1+a*cos(n*t))*cos(t)
  xy(2) = 2+(1+b*sin(t))*sin(t)
  dxy(1) = -(1+a*cos(n*t))*sin(t)-a*n*sin(n*t)*cos(t)
  dxy(2) = (1+b*sin(t))*cos(t)+b*cos(t)*sin(t)

  d2xy(1) = -(1+a*cos(n*t))*cos(t)+a*n*sin(n*t)*sin(t) &
      +a*n*sin(n*t)*sin(t)-a*n*n*cos(n*t)*cos(t)
  d2xy(2) = -(1+b*sin(t))*sin(t)+b*cos(t)*cos(t) &
      +b*cos(t)*cos(t)-b*sin(t)*sin(t)

  return
end subroutine funcurve_new





subroutine fcurve_source(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !
  ! controls the source surface
  !
  done=1
  pi=4*atan(done)

  !
  ! ellipse
  !
  a=.1d0
  b=.15d0

  x0=1.95d0
  y0=2.1d0
  
  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)
  
  return
end subroutine fcurve_source
