program mfie_exterior
  implicit real *8 (a-h,o-z)
  real *8 :: ps(10000),zs(10000),dpdt(10000)
  real *8 :: dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000)
  real *8 :: center(10),xyz(10)
  real *8 :: targ(10)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)

  real *8, allocatable :: rnodes(:,:,:), pts(:,:,:), xyzs(:,:)
  real *8, allocatable :: testpts(:,:), whts(:)

  complex *16 :: zk, ima
  complex *16 :: aint, cintb, efield(3,10000), hfield(3,10000)
  complex *16 :: etest(3,10000), htest(3,10000)

  complex *16, allocatable :: ampole(:,:),bmpole(:,:)
  complex *16, allocatable :: emodes(:,:,:),hmodes(:,:,:)
  complex *16, allocatable :: zjmodes(:,:,:),rhomodes(:,:)
  complex *16, allocatable :: esurf(:,:,:),hsurf(:,:,:)
  complex *16, allocatable :: zcharge(:), zcurrent(:)

  done=1
  pi=4*atan(done)
  ima=(0,1)

  call prini(6,13)
  call prin2('enter n:*',pi,0)
  read *, n
  call prinf('n=*',n,1)

  !
  ! set some subroutine parameters
  !
  zk=1.0d-16
  zk=15.43d0+ima/10000

  call prin2(' *',pi,0)
  call prin2('zk=*',zk,2)

  eps=1.0d-12
  call prin2('kernel eval precision=*',eps,1)

  norder=4
  norder=8
  ! norder=16
  call prinf('alpert order=*',norder,1)

  !
  ! generate the points on the scatterer
  !
  h = 2*pi/n
  do i=1,n
    t=h*(i-1)
    call funcurve(t, par1, par2, xys(1,i), dxys(1,i), d2xys(1,i))
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do

  iw=11
  itype=2
  call zpyplot(iw, xys, n, itype, 'scattering surface*')

  !
  ! find the minimum ps to construct spanning surface for
  ! the m=0 mode
  ! 
  ind=1
  do i=1,n
    if (xys(1,i) .lt. xys(1,ind)) ind=i
  enddo

  ! 
  ! generate some points to test the field at insidethe scatterer
  !   - the EXTERIOR MFIE is tested via the extinction theorem
  !   - the field must be zero in the interior
  ! 
  ntheta=5
  nphi=5

  ang=pi/8
  x0=2.1d0*cos(ang)
  y0=2.1d0*sin(ang)
  z0 = 2.1d0

  rad=.1d0

  htheta=pi/(ntheta+1)
  hphi=2*pi/nphi

  ntest = 0
  allocate(testpts(3,nphi*ntheta))
  do i=1,ntheta
    theta=htheta*i

    do j=1,nphi
      phi=hphi*(j-1)
      x=x0+rad*sin(theta)*cos(phi)
      y=y0+rad*sin(theta)*sin(phi)
      z=z0+rad*cos(theta)
      ntest = ntest+1
      testpts(1,ntest)=x
      testpts(2,ntest)=y
      testpts(3,ntest)=z
    enddo
  enddo
  
  iw=31
  itype=3
  call troisplot(iw,testpts,ntest,itype,'testing points*')

  ! 
  ! evaluate an incoming field on the surface of the scatterer
  ! 

  ifclassical = 0
  ifloop = 1
  ifdebye = 0
  ifplanewave = 0

  nmodes = 512
  call prinf('max number of fourier modes, nmodes=*',nmodes,1)

  allocate( esurf(3,n,nmodes) )
  allocate( hsurf(3,n,nmodes) )
  allocate( pts(3,n,nmodes) ) 


  if (ifloop .eq. 1) then
    ! 
    ! if here, drive the thing with a current loop
    ! 
    center(1) = 2
    center(2) = 4
    center(3) = 5
    rad=.42d0

    !
    ! now evaluate the e/h fields at each point
    !
    !$omp parallel do default(shared) &
    !$omp     private(j, i, p, z, phi, targ)
    do j=1,nmodes
      do i=1,n
        phi = (j-1)*2*pi/nmodes
        targ(1) = xys(1,i)*cos(phi)
        targ(2) = xys(1,i)*sin(phi)
        targ(3) = xys(2,i)
        pts(1,i,j) = targ(1)
        pts(2,i,j) = targ(2)
        pts(3,i,j) = targ(3)        
        call loopfields(zk, center, targ, rad, par1, &
            esurf(1,i,j), hsurf(1,i,j))
      enddo
    enddo
    !$omp end parallel do

    call cart2cyl_self(n*nmodes, pts, esurf)    
    call cart2cyl_self(n*nmodes, pts, hsurf)    

    !
    ! compute the integral of the incoming field on a B cycle
    !
    cintb = 0
    hhh = 2*pi*xys(1,ind)/nmodes
    do i = 1,nmodes
      cintb = cintb + hhh*esurf(2,ind,i)/zk
    enddo

    call prin2('calculated directly, b cycle integral=*',cintb,2)

    !
    ! . . . and the data on the test surface
    !
    do i=1,ntest
      call loopfields(zk, center, testpts(1,i), rad, par1, &
          etest(1,i), htest(1,i))
    enddo

    call cart2cyl_self(ntest, testpts, etest)    
    call cart2cyl_self(ntest, testpts, htest)

  endif

  
  ! 
  ! finally done setting up the problem, now solve the MFIE
  ! with data esurf,hsurf - first determine the number of fourier
  ! modes required, based on the data
  !
  nphi = 2048
  call prinf('output phi angles, nphi=*',nphi,1)

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()

  allocate(zcharge(n*nphi))
  allocate(zcurrent(3*n*nphi))
  allocate(xyzs(3,n*nphi))
  allocate(whts(n*nphi))
  call axisolve_mfie_new(zk, n, h, xys, dxys, &
      norder, esurf, hsurf, nmodes, ind, cintb, &
      aint, nphi, xyzs, whts, zcurrent,  &
      zcharge)

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  call prin2('total solve time = *', t1-t0, 1)

  print *
  print *
  call prin2('diff in b-cycle intetgrals = *', aint/ima-cintb, 2)
  
  ! 
  ! test the extinction theorem
  ! 
  ! eval e and h via a smooth rule now, on the synthesized current
  ! and charge (i.e. everything is in 3d, cartesian coordinates)
  !
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()

  nsrc = n*nphi
  ntarg = ntest
  call emfields3d(zk, nsrc, xyzs, whts, zcurrent, zcharge, &
      ntarg, testpts, efield, hfield)

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  call prin2('time to eval fields = *', t1-t0, 1)

  ! 
  ! negate and switch to cylindrical coordinates (so that we can
  ! take the difference to test the extinction theorem)
  ! 
  do i=1,ntest
    hfield(1,i) = -hfield(1,i)
    hfield(2,i) = -hfield(2,i)
    hfield(3,i) = -hfield(3,i)
    efield(1,i) = -efield(1,i)
    efield(2,i) = -efield(2,i)
    efield(3,i) = -efield(3,i)
  enddo

  call cart2cyl_self(ntarg, testpts, hfield)    
  call cart2cyl_self(ntarg, testpts, efield)    

  !
  ! calculate the errors
  !
  print *
  call prin2('scattered efield from mfie=*',efield,30)
  call prin2('known efield=*',etest,30)

  print *
  call prin2('scattered hfield = *', hfield, 30)
  call prin2('known hfield = *', htest, 30)

  call crelfielderr(ntest,htest,hfield,herr)
  call cfielderr(ntest,htest,hfield,herr2)
  call crelfielderr(ntest, etest, efield, eerr)
  call cfielderr(ntest, etest, efield, eerr2)

  !print *
  !call prin2('on test surface, relative e error=*',eerr,1)
  !call prin2('on test surface, relative h error=*',herr,1)

  print *
  call prin2('on test surface, rmse e error=*',eerr2,1)
  call prin2('on test surface, rmse h error=*',herr2,1)


  stop
end program mfie_exterior


!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!       local debugging routines....
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!

subroutine crelfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !
  ! calculates the relative error between the two fields
  ! x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
  ! sum of squares of the field vector at each of the n points
  !
  dd1=0
  dd2=0

  do i=1,n

    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
    dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2

  end do

  err=sqrt(dd1/dd2)
  
  return
end subroutine crelfielderr





subroutine cfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !
  ! calculates the rmse between the two fields
  ! x and y, i.e. calculates |x - y|/sqrt(n) where |y|**2 is the
  ! sum of squares of the field vector at each of the n points
  !
  dd1=0
  do  i=1,n
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
  end do

  err=sqrt(dd1/n)
  
  return
end subroutine cfielderr





subroutine funcurve(t, par1, par2, xy, dxy, d2xy)
  implicit real *8 (a-h,o-z)
  real *8 :: xy(2), dxy(2), d2xy(2)

  x0 = 2
  y0 = 2
  a = 1.0d0
  b = 1.2d0

  xy(1) = x0 + a*cos(t)
  xy(2) = y0 + b*sin(t)

  dxy(1) = -a*sin(t)
  dxy(2) = b*cos(t)

  d2xy(1) = -a*cos(t)
  d2xy(2) = -b*sin(t)

  return

  !
  ! squiggly thing
  !
  a=.2d0
  b=.3d0
  n=4

  xy(1) = 2+(1+a*cos(n*t))*cos(t)
  xy(2) = 2+(1+b*sin(t))*sin(t)
  dxy(1) = -(1+a*cos(n*t))*sin(t)-a*n*sin(n*t)*cos(t)
  dxy(2) = (1+b*sin(t))*cos(t)+b*cos(t)*sin(t)

  d2xy(1) = -(1+a*cos(n*t))*cos(t)+a*n*sin(n*t)*sin(t) &
      +a*n*sin(n*t)*sin(t)-a*n*n*cos(n*t)*cos(t)
  d2xy(2) = -(1+b*sin(t))*sin(t)+b*cos(t)*cos(t) &
      +b*cos(t)*cos(t)-b*sin(t)*sin(t)

  return
end subroutine funcurve





        subroutine funcurve3(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
!c
!c       controls the surface of the field testing torus
!c
!c       a small circle
!c
        a=.1d0
        b=.1d0
        x0=2.1d0
        y0=2.1d0

        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)

        return
        end


        



        subroutine funcurve2(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
!c
!c       controls the source surface
!c
        done=1
        pi=4*atan(done)
!c
!c       ellipse
!c
        a=.2d0
        b=.2d0

        x0=5d0
        y0=5d0

        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)

        return
        end
