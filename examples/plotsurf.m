

X = load('xmesh.dat');
Y = load('ymesh.dat');
Z = load('zmesh.dat');
h = surf(X,Y,Z);
set(h, 'edgealpha', .1);
axis equal;