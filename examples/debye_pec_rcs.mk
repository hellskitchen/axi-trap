

#HOST = osx-gcc-7
#HOST = osx-intel
HOST = osx-intel-openmp
#HOST = amd-linux-gfortran
#HOST = amd-linux-gfortran-openmp
#HOST = linux-intel-openmp
#HOST = amd-linux-gfortran-openmp
HOST = linux-intel-openmp
PROJECT = int2

ifeq ($(HOST),osx-gcc-7)
  FC = gfortran-7 -c -w
  FFLAGS = -O2
  FLINK = gfortran-7 -w -o $(PROJECT) -framework accelerate
endif

ifeq ($(HOST),osx-intel)
  FC = ifort -c -w 
  FFLAGS = -O2
  FLINK = ifort -mkl -o $(PROJECT)
endif

ifeq ($(HOST),osx-intel-openmp)
  FC = ifort -c -w -qopenmp
  FFLAGS = -O2
  FLINK = ifort -w -mkl=parallel -qopenmp \
    -Wl,-stack_size,0x40000000 -o $(PROJECT)
  export OMP_NUM_THREADS=4
  export OMP_STACKSIZE=2048M
endif

ifeq ($(HOST),linux-intel-openmp)
  FC = ifort -c -w -qopenmp
  FFLAGS = -O2
  FLINK = ifort -w -mkl=parallel -qopenmp -o $(PROJECT)
  export OMP_NUM_THREADS=32
  export OMP_STACKSIZE=2048M
endif

ifeq ($(HOST),amd-linux-gfortran)
  FC = gfortran -c -w
  FFLAGS = -O2
  FLINK = gfortran -w -o $(PROJECT) -lblas -llapack
endif

ifeq ($(HOST),amd-linux-gfortran-openmp)
  FC = gfortran -c -w -fopenmp
  FFLAGS = -O2
#  FLINK = gfortran -w -fopenmp -o $(PROJECT) /usr/lib/libblas.so.3 \
#        /usr/lib/liblapack.so.3
  FLINK = gfortran -w -fopenmp -o $(PROJECT) -lblas -llapack
  export OMP_NUM_THREADS=8
  export OMP_STACKSIZE=1024M
endif



.PHONY: all clean list

HELLSKITCHEN = ../../hellskitchen
UTILS = ../../utils
SPECIAL = ../../special

SOURCES = debye_pec_rcs.f90 \
  ../src/debye_new.f90 \
  ../src/jh2bcompute.f \
  ../src/mfie_new.f90 \
  ../src/axi_geometries.f90 \
  ../src/axideco.f \
  ../src/axideco_new.f90 \
  ../src/axiplot.f90 \
  ../src/axikernels.f90 \
  ../src/zalpert_modes.f90 \
  ../src/zalpert.f90 \
  ../src/dalpert.f90 \
  ../src/dalpert_modes.f90 \
  ../src/kernels.f \
  $(SPECIAL)/src/elliptic.f \
  $(UTILS)/lapack_wrap.f90 \
  $(UTILS)/dotcross3d.f \
  $(UTILS)/pplot.f \
  $(UTILS)/anaresa.f \
  $(UTILS)/next235.f \
  $(UTILS)/cadapgaus_quad.f \
  $(UTILS)/adapgaus_quad.f \
  $(UTILS)/prini.f \
  $(UTILS)/corrand.f \
  $(UTILS)/legeexps.f \
  $(UTILS)/cadapgau_k16.f \
  $(UTILS)/elliptic_ke.f \
  $(UTILS)/chebexps.f \
  $(UTILS)/alpert.f \
  $(UTILS)/dfft.f \
  $(HELLSKITCHEN)/Greengard/Trapquads2d/fdiffc.f

#  $(HELLSKITCHEN)/Common/rotviarecur3.f \
#  $(HELLSKITCHEN)/Common/rotprojvar.f \
#  $(HELLSKITCHEN)/Common/yrecursion.f \
#  $(HELLSKITCHEN)/Gimbutas/lib/hjfuns3d.f \
#  $(HELLSKITCHEN)/Gimbutas/emlib/emdyadic.f \
#  $(HELLSKITCHEN)/Gimbutas/emlib/emrouts3.f \
#  $(HELLSKITCHEN)/Gimbutas/emlib/emabrot3.f \
#  $(HELLSKITCHEN)/Gimbutas/lib/xrecursion.f \
#  $(HELLSKITCHEN)/H3DLibraries/projections.f \
#  $(HELLSKITCHEN)/H3DLibraries/helmrouts2.f


OBJECTS = $(patsubst %.f,%.o,$(patsubst %.f90,%.o,$(SOURCES)))

#
# use only the file part of the filename, then manually specify
# the build location
#

%.o : %.f
	$(FC) $(FFLAGS) $< -o $@

%.o : %.f90
	$(FC) $(FFLAGS) $< -o $@

all: $(OBJECTS)
	rm -f $(PROJECT)
	$(FLINK) $(OBJECTS)
	@echo $(TTT)
	@echo $(DYLD_LIBRARY_PATH)
	./$(PROJECT)

clean:
	rm -f $(OBJECTS)
	rm -f $(PROJECT)

list: $(SOURCES)
	$(warning Requires:  $^)



