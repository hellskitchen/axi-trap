
HOST = osx-gcc-7
#HOST = osx-gcc-7-openmp
HOST = osx-intel
HOST = osx-intel-openmp
#HOST = linux-gfortran
#HOST = amd-gfortran
#HOST = amd-gfortran-openmp
#HOST = linux-intel
PROJECT = int2

ifeq ($(HOST),osx-gcc-7)
  FC = gfortran-7 -c -w
  FFLAGS = -O2
  FLINK = gfortran-7 -w -o $(PROJECT) -framework accelerate
endif

ifeq ($(HOST),osx-gcc-7-openmp)
  FC = gfortran-7 -c -fopenmp -w
  FFLAGS = -O2
  FLINK = gfortran-7 -fopenmp -w \
    -Wl,-stack_size,0x40000000 -o $(PROJECT) -framework accelerate
  export OMP_NUM_THREADS = 4
  export OMP_STACKSIZE = 2048M
endif



ifeq ($(HOST),osx-intel)
  FC = ifort -c -w 
  FFLAGS = -O2
  FLINK = ifort -mkl -o $(PROJECT)
endif

ifeq ($(HOST),osx-intel-openmp)
  FC = ifort -c -w -qopenmp
  FFLAGS = -O2
  FLINK = ifort -w -mkl=parallel -qopenmp \
    -Wl,-stack_size,0x40000000 -o $(PROJECT)
  export OMP_NUM_THREADS=4
  export OMP_STACKSIZE=2048M
endif

ifeq ($(HOST),linux-gfortran)
  FC = gfortran -c -w
  FFLAGS = -O2 -mcmodel=medium 
  FLINK = gfortran -w -mcmodel=medium -o $(PROJECT) -llapack -lblas
endif

ifeq ($(HOST),amd-gfortran)
  FC = gfortran -c -w
  FFLAGS = -O2 -mcmodel=medium 
  #FLINK = gfortran -w -mcmodel=medium -o $(PROJECT) /usr/lib/liblapack.so.3 /usr/lib/libblas.so.3
  FLINK = gfortran -w -mcmodel=medium -o $(PROJECT) -llapack -lblas
endif

ifeq ($(HOST),amd-gfortran-openmp)
  FC = gfortran -c -w
  FFLAGS = -O2 -mcmodel=medium -fopenmp
  FLINK = gfortran -w -mcmodel=medium -fopenmp -o $(PROJECT) -llapack -lblas
  export OMP_NUM_THREADS=8
  export OMP_STACKSIZE=1024M
endif

ifeq ($(HOST),linux-intel)
  FC = ifort -c -w
  FFLAGS = -O2 -mcmodel=medium
  FLINK = ifort -w -mcmodel=medium -o $(PROJECT) -mkl
endif




.PHONY: all clean list

HELLSKITCHEN = ../../hellskitchen

SOURCES =    debye_dielectric_test1.f90 \
  ../../utils/pplot.f \
  ../../utils/prini.f \
  ../../utils/quaplot.f \
  ../../utils/legeexps.f \
  ../../utils/cadapgau_omp.f \
  ../../utils/elliptic_ke.f \
  ../../utils/chebexps.f \
  ../../utils/csvdpiv.f \
  ../../utils/cqrsolve.f \
  ../src/debye.f \
  ../src/debye_new.f90 \
  ../src/kernels.f \
  ../src/mfie.f \
  ../src/mfie_new.f90 \
  ../../utils/alpert.f \
  ../../hellskitchen/Greengard/Trapquads2d/fdiffc.f \
  ../../hellskitchen/Axi3DLibraries/surfdivaxi.f \
  ../../hellskitchen/Axi3DLibraries/surflapaxi.f \
  ../../hellskitchen/Axi3DLibraries/surfgradaxi.f \
  ../../hellskitchen/Axi3DLibraries/surfcurlaxi.f \
  ../../utils/dfft.f \
  ../../hellskitchen/Common/zgecoall.f \
  ../../hellskitchen/Axi3DLibraries/ode_periodic_surflap.f \
  ../../hellskitchen/Axi3DLibraries/surflapinteq.f \
  ../../utils/corrand.f \
  ../src/axideco.f \
  ../src/axideco_new.f90 \
  ../src/axikernels.f90 \
  ../../utils/adapgaus_quad.f \
  ../../utils/next235.f \
  ../../utils/cadapgaus_quad.f \
  ../../utils/lapack_wrap.f90 \
  ../../special/src/elliptic.f \
  ../src/zalpert.f90 \
  ../src/zalpert_modes.f90 \
  ../src/dalpert.f90 \
  ../src/dalpert_modes.f90 \
  ../../utils/dotcross3d.f






OBJECTS = $(patsubst %.f,%.o,$(patsubst %.f90,%.o,$(SOURCES)))

#
# use only the file part of the filename, then manually specify
# the build location
#

%.o : %.f
	$(FC) $(FFLAGS) $< -o $@

%.o : %.f90
	$(FC) $(FFLAGS) $< -o $@

all: $(OBJECTS)
	rm -f $(PROJECT)
	$(FLINK) $(OBJECTS)
	./$(PROJECT)

clean:
	rm -f $(OBJECTS)
	rm -f $(PROJECT)

list: $(SOURCES)
	$(warning Requires:  $^)



