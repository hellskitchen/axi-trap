program beltramieig
  implicit real *8 (a-h,o-z)

  character *1 :: jobu, jobvt, jobvl, jobvr
  
  real *8 :: ps(10000), zs(10000), dpdt(10000), dzdt(10000)
  real *8 :: dpdt2(10000), dzdt2(10000), dsdt(10000)
  real *8 :: ts(10000), vals(100000)
  real *8 :: dwork(1000000), dnorms(10000), ab(10)
  real *8 :: dlams(10000), errs(10000), svals(10000)

  real *8, allocatable :: dets(:,:), pzgrid(:,:,:)
  real *8, allocatable :: pvals(:,:), tvals(:,:), zvals(:,:)
  real *8, allocatable :: pvals0(:,:), tvals0(:,:), zvals0(:,:)
  real *8, allocatable :: pvals1(:,:), tvals1(:,:), zvals1(:,:)

  integer :: ipvt(10000)
  
  complex *16 :: zk, work(1000000), ima, det
  complex *16 :: wz(1000000), wz2(1000000), sz(100000)
  complex *16 :: u1, u2, u3, f12, f13, f23, f1, f2, f3
  complex *16 :: w, f123, denom1, denom2, ff, uu
  complex *16 :: r1(10000), r2(10000)
  complex *16 :: vmat(1000000), amat2(1000000)
  complex *16 :: zlams(10000), vr(1000000), vl(1000000), zlam
  complex *16 :: zdet, zdet2, zks(10000), zfreds(10000)
  complex *16 :: zdft(10000), beigs(10000), ztheta
  complex *16 :: rhom1_null(10000), alpha1_null
  complex *16 :: rhom0_null(10000), alpha0_null
  complex *16 :: zms1_null(2,10000), zms0_null(2,10000)
  complex *16 :: ctemp1(10000), rhs(10000), sol(10000)
  complex *16 :: bfield0(100), bfield1(100), bfield(100)
  complex *16 :: umat(1000000), vtmat(1000000)

  complex *16, allocatable :: amat(:,:), cmat(:,:)
  
  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)

  call prini(6,13)
  print *, 'enter n:'
  read *, n
  call prinf('n=*',n,1)

  allocate ( amat(n+1,n+1) )
  
  !
  ! create the geometry
  !
  rl=2*pi
  h=rl/n
  do i = 1,n
    t=h*(i-1)
    call funcurve(t, ps(i), zs(i), dpdt(i), dzdt(i), &
        dpdt2(i),dzdt2(i))
    dd=dpdt(i)**2+dzdt(i)**2
    dsdt(i)=sqrt(dd)
  end do

  iw=11
  itype=2
  call pyplot(iw, ps, zs, n, itype, 'boundary curve*')

  !
  ! set some subroutine parameters
  !
  mode1 = 1
  mode0 = 0
  mode = mode1
  m = mode1
  call prinf('mode=*',mode,1)

  eps=1.0d-12
  call prin2('kernel eval precision=*',eps,1)

  !!!!norder=4
  !!!!norder=8
  norder=16
  call prinf('alpert order=*',norder,1)

  !
  ! set parameters controlling the extra condition in the 
  ! purely axisymmetric mode - not relevant for mode .neq. 0
  !
  tau = 1
  ind = 1
  a = 5.0d0
  b = 8.0d0

  !
  ! create a plot of the condition number
  ! 
  ifcond = 0
  if (ifcond .eq. 1) then
    
    nk = 200
    h = (b-a)/(nk-1)

    !!!$omp parallel do default(shared) &
    !!!$omp     private(i, zk, amat, wz, wz2, sz, ncols) &
    !!!$omp     private(work, dd)

    do i = 1,nk
      zk = a + h*(i-1)
      call creabeltrami_interior(eps, zk, m, n, rl, ps, zs, dpdt, &
          dzdt, dpdt2, dzdt2, norder, ind, tau, amat)
      
      nnn = n+1
      eps2=1.0d-13
      lw=10000000
      call csvdpiv(ier,amat,nnn,nnn,wz,wz2,sz,ncols,eps2, &
          work, lw, ltot)
      dd=sz(1)/sz(nnn)
      
      print *
      call prin2('zk = *', zk, 2)
      call prin2('cond number = *', dd, 1)
      
      ts(i) = zk
      vals(i) = log(dd)/log(10.0d0)
    end do

    !!!$omp end parallel do

    call prin2('cond nums = *', vals, nk)
    
    iw = 81
    itype = 1
    call pyplot(iw, ts, vals, nk, itype, 'log condition number*')

    !stop
    
  endif



  ! !
  ! ! create a heatplot of the determinant
  ! !
  ! ifheat = 1
  ! if (ifheat .eq. 1) then

  !   hxy = .025d0
  !   nr = (b-a)/hxy

  !   c = -.25d0
  !   d = .25
  !   ni = (d-c)/hxy

  !   allocate(dets(ni,nr))

  !   print *
  !   print *
  !   print *
  !   print *, '--- construct heatplot of determinant ---'

  !   do i = 1, nr

  !     do j = 1, ni

  !       rk = a + (b-a)*i/(nr-1)
  !       ri = d - (d-c)*j/(ni-1)
  !       zk = rk + ima*ri

  !       scale = 1.0d5
  !       call beltrami_interior_fredholm(eps, zk, m, n, rl, ps, zs, &
  !           dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, &
  !           scale, zdet)

  !       dets(j,i) = dble(zdet)
  !       write(55,*) '(', dble(zdet), '+', dimag(zdet),'j)'

  !       !!zdet = exp(ima*rk)
  !       !!!zdet = exp(ima*ri)
  !       !!write(56,*) '(', dble(zdet), '+', dimag(zdet),'j)'

  !     enddo

  !     print *, 'percent done = ', (i)/(done*nr)
      

  !   enddo

  !   iw = 91
  !   call pyimage(iw, ni, nr, dets, 'real part of determinant*')
  !   call prinf('nr (n) = *', nr, 1)
  !   call prinf('ni (m) = *', ni, 1)
    

  ! endif

  
  

  !
  ! interpolate the fredholm determinant and find its roots as
  ! eigenvalues of the companion matrix
  !
  ! ntheta = 101
  ! h = 2*pi/ntheta
  ! do i = 1,ntheta
  !   ts(i) = (i-1)*h
  !   zks(i) = (b+a)/2 + (b-a)/2*cos(ts(i))
  ! end do

  ! call prin2('ts = *', ts, ntheta)
  ! call prin2('zks = *', zks, 2*ntheta)

  ! scale = 1.0d5
  ! do i = 1,ntheta
  !   call prinf('i = *', i, 1)
  !   call beltrami_interior_fredholm(eps, zks(i), m, n, rl, ps, zs, &
  !       dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, &
  !       scale, zfreds(i))
  ! end do

  ! call prin2('zfreds = *', zfreds, 2*ntheta)

  ! nhalf = ntheta/2
  ! do i = 1,ntheta
  !   zdft(i) = 0
  !   mfreq = -nhalf + (i-1)    
  !   do j = 1,ntheta
  !     zdft(i) = zdft(i) + zfreds(j)*exp(-ima*2*pi*(j-1)*mfreq/ntheta)
  !   end do
  !   zdft(i) = zdft(i)/ntheta
  ! end do

  ! call prin2('zdft of zfreds = *', zdft, 2*ntheta)
  ! call prin2('est rel error = *', abs(zdft(1)/zdft(nhalf+1)), 1)

  ! !
  ! ! construct the companion matrix
  ! !
  ! nmat = ntheta-1
  ! allocate( cmat(nmat,nmat) )

  ! do i = 1,nmat
  !   do j = 1,nmat
  !     cmat(i,j) = 0
  !   end do
  ! end do

  ! do i = 1,nmat-1
  !   cmat(i+1,i) = 1
  ! end do

  ! do i = 1,nmat
  !   cmat(i,nmat) = -zdft(i)/zdft(ntheta)
  ! end do

  ! call zeigs(nmat, cmat, info, vl, zlams, vr)
  ! call prin2('after zgeev, zlams = *', zlams, 2*nmat)

  ! do i = 1,nmat
  !   dnorms(i) = abs(zlams(i))
  ! end do

  ! call prin2('norms of eigenvalues = *', dnorms, nmat)

  ! do i = 1,nmat
  !   dnorms(i) = dnorms(i) - 1
  ! end do

  ! call prin2('error from unit circle = *', dnorms, nmat)

  
  
  ! !do i = 1,nmat
  ! !  beigs(i) = (b+a)/2 + (b-a)/2*zlams(i)
  ! !end do
  
  ! !call prin2('converted back, beigs = *', beigs, 2*nmat)

  ! !
  ! ! print out the relevant roots
  ! !
  ! thresh = 1.0d-6
  ! nlam = 0
  ! do i = 1,nmat
  !   if (abs(dnorms(i)) .lt. thresh) then
  !     nlam = nlam + 1
  !     print *
  !     write(6,*) 'theta = ', log(zlams(i))/ima
  !     write(13,*) 'theta = ', log(zlams(i))/ima
  !     ztheta = log(zlams(i))/ima
  !     zlam = (b+a)/2 + (b-a)/2*cos(ztheta)
  !     zks(nlam) = zlam
  !     write(6,*) 'corresponding eig = ', zlam
  !     write(13,*) 'corresponding eig = ', zlam
  !     !write(6,*) beigs(i)
  !     !write(13,*) beigs(i)
  !   end if
  ! end do



  !
  ! test the wrapped version of the eigenvalue finder
  !
  ab(1) = a
  ab(2) = b
  tol = 1.0d-6
  !call beltrami_interior_eigs(m, n, rl, ps, zs, &
  !    dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, ab, &
  !    tol, nlam, dlams, errs)

  !nlam = 10
  !do i = 1,nlam
  !  dlams(i) = 5 + cos(i*1.0d0)**2
  !end do


  dlams(1) = 5.52819229376423d0
  dlams(2) = 5.56546068185120d0
  dlams(3) = 6.13551340936035d0
  dlams(4) = 6.34490415618452d0
  dlams(5) = 6.55792492110011d0
  dlams(6) = 6.63664744243726d0
  dlams(7) = 7.07387937977726d0
  dlams(8) = 7.14679867372607d0
  dlams(9) = 7.44941373173177d0
  dlams(10) = 7.81008353287563d0
  dlams(11) = 7.88508920256356d0

  nlam = 11
  
  call prinf('after eig finder, nlam = *', nlam, 1)
  call prin2('after eig finder, dlams = *', dlams, nlam)
  call prin2('and errs = *', errs, nlam)


  do i = 1,nlam
    write(6,*) 'zk = ', dlams(i)
    write(13,*) 'zk = ', dlams(i)
  end do
  

  nnn = n+1
  allocate( cmat(nnn,nnn) )

  ! do i = 1,nlam
  !   zk = dlams(i)
  !   print *
  !   print *    
  !   call prin2('zk = *', zk, 2)
  !   call creabeltrami_interior(eps, zk, mode1, n, rl, ps, zs, &
  !       dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, cmat)

  !   call zsvd(nnn, nnn, cmat, umat, svals, vtmat)
  !   call prin2('sing vals for mode 1 = *', svals, nnn)

  !   call creabeltrami_interior(eps, zk, mode0, n, rl, ps, zs, &
  !       dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, cmat)

  !   call zsvd(nnn, nnn, cmat, umat, svals, vtmat)
  !   call prin2('sing vals for mode 0 = *', svals, nnn)


  ! end do

  !stop
  
  !
  ! now construct plots of the first five modes
  !

  do ijk = 1,9

    !
    ! eigenvalue found, now compute eigenvector
    !
    zk = dlams(ijk)
    !!!zk = 5.52819229376423d0
    print *
    print *
    print *, '=================================='
    print *
    call prin2('processing field for zk = *', zk, 2)
    call creabeltrami_interior(eps, zk, mode1, n, rl, ps, zs, &
        dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, cmat)

    call znullspace(nnn, cmat, rhom1_null)

    call prin2('null vector, rhom1_null = *', rhom1_null, 30)
    call zmatvec(nnn, nnn, cmat, rhom1_null, ctemp1)

    dnorm = 0
    do i = 1,n
      dnorm = dnorm + dsdt(i)*abs(rhom1_null(i))**2
    end do
    dnorm = sqrt(dnorm)
    call prin2('before scaling, L2 norm of rhom1_null =*', dnorm, 1)

    do i = 1,n
      rhom1_null(i) = rhom1_null(i)/dnorm
    end do

    
    dnorm = 0
    do i = 1,nnn
      dnorm = dnorm + abs(ctemp1(i))**2
    end do
    
    dnorm = sqrt(dnorm/nnn)
    call prin2('rms norm of A times null = *', dnorm, 1)
    
    !
    ! compute the null current
    !
    alpha1_null = 0
    call beltrami_current_incoming(zk, mode1, n, rl, ps, zs, &
        dpdt, dzdt, dpdt2, dzdt2, rhom1_null, alpha1_null, zms1_null)

    !
    ! now solve the axisymmetric problem for unit flux
    !
    mode0 = 0
    call creabeltrami_interior(eps, zk, mode0, n, rl, ps, zs, &
      dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, cmat)

    !
    ! caluclate unit circulation, then scale
    !
    do i = 1,n
      rhs(i) = 0
    end do
    rhs(n+1) = -1

    call zgausselim(nnn, cmat, rhs, info, sol, dcond)
    call prin2('after zgausslim, dcond = *', dcond, 1)

    call ccopy601(n, sol, rhom0_null)

    dnorm = 0
    do i = 1,n
      dnorm = dnorm + dsdt(i)*abs(rhom0_null(i))**2
    end do
    dnorm = sqrt(dnorm)
    call prin2('L2 norm of rhom0_null =*', dnorm, 1)

    do i = 1,n
      rhom0_null(i) = rhom0_null(i)/dnorm
    end do
    
    alpha0_null = sol(n+1)/dnorm
    call prin2('rhom0_null = *', rhom0_null, 30)
    call prin2('alpha0_null = *', alpha0_null, 2)

    flux_tor = 1/dnorm/dlams(ijk)
    call prin2('implied toroidal flux = *', flux_tor, 1)

    !
    ! compute axisymmetric null current
    !
    call beltrami_current_incoming(zk, mode0, n, rl, ps, zs, &
        dpdt, dzdt, dpdt2, dzdt2, rhom0_null, alpha0_null, &
        zms0_null)
    
    !
    ! make a heat plot of the thing
    !
    x0 = 1
    x1 = 3
    nr = 51

    y0 = -2
    y1 = 2
    ni = 51

    allocate(pzgrid(2,ni,nr))
    
    do i = 1,nr
      do j = 1,ni
        p1 = x0 + (x1-x0)*i/(nr+1)
        z1 = y1 - (y1-y0)*j/(ni+1)
        pzgrid(1,j,i) = p1
        pzgrid(2,j,i) = z1
      end do
    end do
  
    print *
    print *, '---making plot of the beltrami field---'

    dtot = nr*ni*done
    ntot = 0

    allocate(pvals(ni,nr), tvals(ni,nr), zvals(ni,nr))
    allocate(pvals0(ni,nr), tvals0(ni,nr), zvals0(ni,nr))
    allocate(pvals1(ni,nr), tvals1(ni,nr), zvals1(ni,nr))

    print *
    print *, 'plotting fields for zk = ', zk
    do i = 1, nr
      do j = 1, ni

        ntot = ntot + 1
        if (mod(ntot,250) .eq. 1) then
          print *, 'percent done = ', ntot/dtot
        endif
          
        p1 = pzgrid(1,j,i)
        z1 = pzgrid(2,j,i)
        theta1 = 0

        call beltrami_eval(zk, mode0, n, rl, ps, zs, &
            dpdt, dzdt, dpdt2, dzdt2, rhom0_null, & 
            zms0_null, p1, theta1, z1, bfield0)

        call beltrami_eval(zk, mode1, n, rl, ps, zs, &
            dpdt, dzdt, dpdt2, dzdt2, rhom1_null, & 
            zms1_null, p1, theta1, z1, bfield1)

        pvals0(j,i) = bfield0(1)
        tvals0(j,i) = bfield0(2)
        zvals0(j,i) = bfield0(3)
      
        pvals1(j,i) = bfield1(1)
        tvals1(j,i) = bfield1(2)
        zvals1(j,i) = bfield1(3)
      
        pvals(j,i) = pvals0(j,i) + pvals1(j,i)
        tvals(j,i) = tvals0(j,i) + tvals1(j,i)
        zvals(j,i) = zvals0(j,i) + zvals1(j,i)

        !pvals(j,i) = bfield0(1)
        !tvals(j,i) = bfield0(2)
        !zvals(j,i) = bfield0(3)

      enddo
    enddo

    iw = 30 + ijk
    call pyimage2_interior(iw, ni, nr, pvals, ps, zs, n, &
        x0, x1, y0, y1, 'total r component*')

    iw = 40 + ijk
    call pyimage2_interior(iw, ni, nr, tvals, ps, zs, n, &
        x0, x1, y0, y1, 'total t component*')

    iw = 50 + ijk
    call pyimage2_interior(iw, ni, nr, zvals, ps, zs, n, &
        x0, x1, y0, y1, 'total z component*')

    iw = 60 + ijk
    call pyimage2_interior(iw, ni, nr, pvals1, ps, zs, n, &
        x0, x1, y0, y1, 'mode 1 r component*')

    iw = 70 + ijk
    call pyimage2_interior(iw, ni, nr, tvals1, ps, zs, n, &
        x0, x1, y0, y1, 'mode 1 t component*')

    iw = 80 + ijk
    call pyimage2_interior(iw, ni, nr, zvals1, ps, zs, n, &
        x0, x1, y0, y1, 'mode 1 z component*')

    deallocate(pzgrid)
    deallocate(pvals, tvals, zvals)
    deallocate(pvals0, tvals0, zvals0)
    deallocate(pvals1, tvals1, zvals1)

  end do
  





  
  stop
end program beltramieig

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



subroutine znullspace(n, cmat, zs)
  implicit real *8 (a-h,o-z)
  complex *16 :: cmat(n,n), zs(n)

  complex *16 :: zrs1(10000), zrs2(10000), zys(10000)
  !
  ! find the one dimensional nullspace of cmat
  !
  call corrand_norm(4*n, zrs1, zrs2)

  dnorm1 = 0
  dnorm2 = 0
  do i = 1,n
    dnorm1 = dnorm1 + abs(zrs1(i))**2
    dnorm2 = dnorm2 + abs(zrs2(i))**2
  end do

  do i = 1,n
    zrs1(i) = zrs1(i)/sqrt(dnorm1)
    zrs2(i) = zrs2(i)/sqrt(dnorm2)
  end do
  
  call zmatvec(n, n, cmat, zrs1, zys)

  do i = 1,n
    do j = 1,n
      cmat(i,j) = cmat(i,j) + zrs2(i)*zrs2(j)
    end do
  end do
  
  call zgausselim(n, cmat, zys, info, zs, dcond)

  do i = 1,n
    do j = 1,n
      cmat(i,j) = cmat(i,j) - zrs2(i)*zrs2(j)
    end do
  end do
  

  do i = 1,n
    zs(i) = zs(i) - zrs1(i)
  end do

  dnorm = 0
  do i = 1,n
    dnorm = dnorm + abs(zs(i))**2
  end do
  
  dnorm = sqrt(dnorm/n)
  do i = 1,n
    zs(i) = zs(i)/dnorm
  end do
  
  return
end subroutine znullspace






subroutine beltrami_interior_eigs(m, n, rl, ps, zs, &
    dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, ab, &
    tol, nlam, dlams, errs)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(n), zs(n), dpdt(n), dzdt(n), dpdt2(n), dzdt2(n)
  real *8 :: ab(2), dlams(*), errs(*)

  real *8 :: ts(10000), dnorms(10000)
  complex *16 :: ima, zks(10000), zfreds(10000), zdft(10000)
  complex *16 :: zlams(10000), vl(1000000), vr(1000000)
  complex *16 :: ztheta, zlam

  complex *16, allocatable :: cmat(:,:)

  !
  ! find all the belatrmi eigenvalues on the interval [ab(1), ab(2)]
  !

  done = 1
  ima = (0,1)
  pi = 4*atan(done)

  a = ab(1)
  b = ab(2)


  errfft = 1.0d-10

  do ijk = 1,5

    ifdone = 1
    ntheta = 32*(2**ijk) + 1
    call prinf('ntheta = *', ntheta, 1)
    h = 2*pi/ntheta
    do i = 1,ntheta
      ts(i) = (i-1)*h
      zks(i) = (b+a)/2 + (b-a)/2*cos(ts(i))
    end do

    !call prin2('ts = *', ts, ntheta)
    !call prin2('zks = *', zks, 2*ntheta)

    scale = 1.0d5
    eps = 1.0d-12
    
    !$omp parallel do default(shared) &
    !$omp     private(i)
    do i = 1,ntheta
      call prinf('i = *', i, 1)
      call beltrami_interior_fredholm(eps, zks(i), m, n, rl, ps, zs, &
          dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, &
          scale, zfreds(i))
    end do
    !$omp end parallel do
    
    !call prin2('zfreds = *', zfreds, 2*ntheta)
    
    nhalf = ntheta/2
    do i = 1,ntheta
      zdft(i) = 0
      mfreq = -nhalf + (i-1)    
      do j = 1,ntheta
        zdft(i) = zdft(i) + zfreds(j)*exp(-ima*2*pi*(j-1)*mfreq/ntheta)
      end do
      zdft(i) = zdft(i)/ntheta
    end do

    ddd = abs(zdft(1)/zdft(nhalf+1))
    if (ddd .gt. errfft) ifdone = 0
    !call prin2('zdft of zfreds = *', zdft, 2*ntheta)
    call prin2('est rel error of zfreds = *', ddd, 1)

    if (ifdone .eq. 1) then
      exit
    end if

  end do


    
  !
  ! construct the companion matrix
  !
  nmat = ntheta-1
  allocate( cmat(nmat,nmat) )

  do i = 1,nmat
    do j = 1,nmat
      cmat(i,j) = 0
    end do
  end do

  do i = 1,nmat-1
    cmat(i+1,i) = 1
  end do

  do i = 1,nmat
    cmat(i,nmat) = -zdft(i)/zdft(ntheta)
  end do

  call zeigs(nmat, cmat, info, vl, zlams, vr)
  !call prin2('after zgeev, zlams = *', zlams, 2*nmat)

  do i = 1,nmat
    dnorms(i) = abs(zlams(i))
  end do

  !call prin2('norms of eigenvalues = *', dnorms, nmat)

  do i = 1,nmat
    dnorms(i) = dnorms(i) - 1
  end do

  call prin2('error from unit circle = *', dnorms, nmat)

  
  
  !do i = 1,nmat
  !  beigs(i) = (b+a)/2 + (b-a)/2*zlams(i)
  !end do
  
  !call prin2('converted back, beigs = *', beigs, 2*nmat)

  !
  ! extract the the relevant roots
  !
  nlam = 0
  do i = 1,nmat
    if (abs(dnorms(i)) .lt. tol) then

      !write(6,*) 'zlams(i) = ', zlams(i)
      ! write(13,*) 'zlams(i) = ', zlams(i)
      
      if (imag(zlams(i)) .gt. 0) then
        nlam = nlam + 1
        !print *
        !write(6,*) 'theta = ', log(zlams(i))/ima
        !write(13,*) 'theta = ', log(zlams(i))/ima
        ztheta = log(zlams(i))/ima
        zlam = (b+a)/2 + (b-a)/2*cos(ztheta)
        zks(nlam) = zlam
        !write(6,*) 'corresponding eig = ', zlam
        !write(13,*) 'corresponding eig = ', zlam
        !write(6,*) beigs(i)
        !write(13,*) beigs(i)
      end if
    end if
  end do


  !!!!call prin2('zks = *', zks, 2*nlam)

  
  !
  ! now extract real part, and sort
  !

  do i = 1,nlam
    temp = zks(i)    
    do j = i+1,nlam
      temp2 = zks(j)
      if (temp2 .lt. temp) then
        zlam = zks(i)
        zks(i) = zks(j)
        zks(j) = zlam
        temp = temp2
      end if
    end do
  end do

  do i = 1,nlam
    dlams(i) = zks(i)
    errs(i) = -ima*zks(i)
  end do
  

  return
end subroutine beltrami_interior_eigs





! subroutine pyphase(iw, m, n, vals, title)
!   implicit real *8 (a-h,o-z)
!   complex *16 :: vals(m,n)
!   character *1 :: a1,a10,file1(10),file11(9),title(1)
!   character *1 :: temp(32), file1p(10)
!   character *9 :: file4
!   character *10 :: file8, file8p
!   character *32 :: title2
! c
!         equivalence (file1,file8), (file11,file4), (temp,title2),
!      1      (file1p,file8p)
! c
! c       output the data stored in vals to a file which python can
! c       read and plot as a heat map using matplotlib
! c
! c       this routine requires a very special formatting of the input
! c       vals - vals(1,1) is the value in the upper left
! c       hand corner of the plot, vals(m,1) is the value in the lower
! c       left hand corner, vals(1,n) is the value in the upper right
! c       hand corner, and vals(m,n) is the value in the lower right 
! c       hand corner.
! c
! c       input:
! c
! c         iw - file number, same as in quaplot
! c         m - number of y values on the grid
! c         n - number of x values on the grid
! c         vals - 'z' value at the x,y grid points
! c         title - the title of the plot, should be of the form 'title*',
! c             and can only be a maximum of 32 characters long
! c
! c       note that on exit, two files will be created - plotiw.py 
! c       and plotiw.dat
! c
! c
! c       first construct the file names using iw
! c
!         i1=mod(iw,10)
!         i10=(iw-i1)/10
!         call int2char2(i1,a1)
!         call int2char2(i10,a10)

!         file8='plotiw.dat'
!         file1(5)=a10
!         file1(6)=a1
! c
!         file8p='plotiw.pdf'
!         file1p(5)=a10
!         file1p(6)=a1
! c
!         file4='plotiw.py'
!         file11(5)=a10
!         file11(6)=a1
! c
! c       print out the contents of the scripting file, gniw
! c
!         iun87=877
!         open(unit=iun87,file=file4)
! c
!         call quamesslen3(title,nchar)
! c
!         title2=''
!         do 1200 i=1,nchar
!         temp(i)=title(i)
!  1200 continue
! c
!         write(iun87,'(a)') '#!/usr/bin/python'
!         write(iun87,'(a)') 'import matplotlib.pyplot as pt'
!         write(iun87,'(a)') 'import numpy as np'
!         write(iun87,'(a)') ''
!         write(iun87,'(a)') 'pt.rc("font", size=16)'
!         write(iun87,'(a,a,a)') 'x = np.loadtxt("',file8,'")'
!         write(iun87,'(a)') 'a = np.mean(x) - 2.2*np.std(x)'
!         write(iun87,'(a)') 'b = np.mean(x) + 2.2*np.std(x)'
!         write(iun87,'(a,i5,a,i5,a)') 
!      1      'x = x.reshape(', m, ',', n, ', order="F")'
!         write(iun87,'(a)') 'pt.imshow(x, aspect="auto")'
!         write(iun87,'(a)') 'cb=pt.colorbar(shrink=.9)'
!         write(iun87,'(a)') 'pt.clim([a,b])'
!         write(iun87,'(a,a,a)') 'pt.title("', title2, '")'
!         write(iun87,'(a,a,a)') 'pt.savefig("',file8p,'")'
!         write(iun87,'(a)') 'pt.show()'

! c
! c       now print out the data file, plotiw.dat
! c
!         iun88=888
!         open(unit=iun88,file=file8)
!         do 1600 j=1,n
!         do 1400 i=1,m
!         write(iun88,*) vals(i,j)
!  1400 continue
!  1600 continue
! c
!         return
!         end





subroutine beltrami_interior_fredholm(eps, zk, mode, n, rl, &
    ps, zs, dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, scale, zdet)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(n), zs(n), dpdt(n), dzdt(n), dpdt2(n), dzdt2(n)
  complex *16 :: zk, zdet

  real *8 :: dwork(1000000)
  character *1 :: jobvl, jobvr
  complex *16 :: ima, work(1000000), zeigs(10000), vl(1000000)
  complex *16 :: vr(1000000)
  complex *16, allocatable :: amat(:,:)

  !
  ! this routine computes the fredholm determinant of the beltrami
  ! integral equation with the following scaling, take note!!!!
  !
  ! if (mode .eq. 0)
  !   amat = -2*amat
  !
  ! if (mode .ne. 0)
  !   amat(1:n,1:n) = -amat(1:n,1:n)*2
  !
  ! the resulting zdet is scaled by 'scale' before returning
  !
  ! input:
  !   ps,zs - 
  !   ind - the index in ps,zs on which to compute the line integral
  !     for poloidal flux
  !   tau - tau = 1 means toroidal flux, tau=0 means poloidal flux
  !

  ima = (0,1)
  done = 1
  pi = 4*atan(done)
  
  ntot = n+1
  allocate( amat(ntot,ntot) )
  call creabeltrami_interior(eps, zk, mode, n, rl, ps, zs, dpdt, &
      dzdt, dpdt2, dzdt2, norder, ind, tau, amat)


  if (mode .eq. 0) then
    do ii = 1,ntot
      do jj = 1,ntot
        amat(ii,jj) = -amat(ii,jj)*2
      end do
    end do
  end if

  if (mode .ne. 0) then
    do ii = 1,n
      do jj = 1,n
        amat(ii,jj) = -amat(ii,jj)*2
      end do
    end do
  end if

  jobvl = 'N'
  jobvr = 'N'
  ldvl = ntot
  ldvr = ntot
  lwork = 1000000
  call zgeev(jobvl, jobvr, ntot, amat, ntot, zeigs, vl, &
      ldvl, vr, ldvr, work, lwork, dwork, info)

  zdet = 1/scale
  do ii = 1,ntot
    zdet = zdet*zeigs(ii)
  end do
        
  return
end subroutine beltrami_interior_fredholm





subroutine belcopy(n, x, y)
  implicit real *8 (a-h,o-z)
  real *8 :: x(n), y(n)

  do i = 1,n
    y(i) = x(i)
  end do
  return
end subroutine belcopy




        
subroutine funcurve(t, x, y, dxdt, dydt, dxdt2, dydt2)
  implicit real *8 (a-h,o-z)

  !
  ! determines the beltrami geometry
  !
  done=1
  pi=4*atan(done)

  !
  ! an antoine-jeff like geometry
  !
  e = .85d0
  delt = .3d0
  alpha = asin(delt)
  rkap = 2.0d0

  x0 = 2.0d0
  y0 = 0
     
  x = x0 + e*cos(t+alpha*sin(t))
  y = y0 + e*rkap*sin(t)
  
  dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
  dydt = e*rkap*cos(t)
  
  dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
      +e*sin(t+alpha*sin(t))*alpha*sin(t)
  dydt2 = -e*rkap*sin(t)
  return

  !
  ! an ellipse
  !
  x0 = 2.0d0
  y0 = 0
  a = 1.0d0
  b = 1.0d0

  x = x0 + a*cos(t)
  y = y0 + b*sin(t)
  dxdt = -a*sin(t)
  dydt = b*cos(t)
  dxdt2 = -a*cos(t)
  dydt2 = -b*sin(t)
  return

end subroutine funcurve






subroutine ftorus(t, x0, x, y, dxdt, dydt, dxdt2, dydt2)
  implicit real *8 (a-h,o-z)

  !
  ! a basic torus
  !
  y0 = 0
  a = 1.0d0
  b = 1.0d0

  x = x0 + a*cos(t)
  y = y0 + b*sin(t)
  dxdt = -a*sin(t)
  dydt = b*cos(t)
  dxdt2 = -a*cos(t)
  dydt2 = -b*sin(t)
  return

end subroutine ftorus
