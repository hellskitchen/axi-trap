"""
Simple demo with multiple subplots.
"""
import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rc

from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib as mpl

plt.rc("font", size=12)

x1 = np.linspace(0.0, 5.0)
x2 = np.linspace(0.0, 2.0)

y1 = np.cos(2 * np.pi * x1) * np.exp(-x1)
y2 = np.cos(2 * np.pi * x2)

files = iter(['plot61', 'plot71', 'plot81',
              'plot62', 'plot72', 'plot82',
              'plot63', 'plot73', 'plot83',
              'plot64', 'plot74', 'plot84',
              'plot65', 'plot75', 'plot85',
              'plot66', 'plot76', 'plot86',
              'plot67', 'plot77', 'plot87',
              'plot68', 'plot78', 'plot88',
              'plot69', 'plot79', 'plot89'])



rc('text', usetex=True)



nr = 3
nc = 5
fig, axes = plt.subplots(nrows=nr, ncols=nc, sharex=True, sharey=True)


for col in range(nc):
    for row in range(nr):
        s = files.next()
        sfile1 = s + '.dat1'
        sfile2 = s + '.dat2'

        ax = axes[row,col]

        x = np.loadtxt(sfile1)
        x = x.reshape(   51,   51, order="F")
        y = np.loadtxt(sfile2)
        path = Path(y)
        patch = PathPatch(path, facecolor="none")
        ax.add_patch(patch)
        im = ax.imshow(x, extent=[ 1.00, 3.00,-2.00, 2.00],
                       clip_path=patch, clip_on=True)

        im.set_clim([-.4,.4])
        
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        ax.set_aspect('equal')


fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.025, 0.7])
fig.colorbar(im, cax=cbar_ax)
        
        
        
for col in range(nc):
    ax = axes[nr-1,col]
    ax.set_xticks([1.0, 2.0, 3.0])


ax = axes[2,0]
ax.set_xlabel(r'$\lambda = 5.52819$',
              fontsize=10)

ax = axes[2,1]
ax.set_xlabel(r'$\lambda = 5.56546$',
              fontsize=10)

ax = axes[2,2]
ax.set_xlabel(r'$\lambda = 6.13551$',
              fontsize=10)

ax = axes[2,3]
ax.set_xlabel(r'$\lambda = 6.34490$',
              fontsize=10)

ax = axes[2,4]
ax.set_xlabel(r'$\lambda = 6.55792$',
              fontsize=10)

for row in range(nr):
    ax = axes[row,0]
    ax.set_yticks((-2.0, 0.0, 2.0))

ax = axes[0,0]
ax.set_ylabel(r'$r$-component')

ax = axes[1,0]
ax.set_ylabel(r'$\varphi$-component')

ax = axes[2,0]
ax.set_ylabel(r'$z$-component')

plt.subplots_adjust(hspace=.2, wspace=.1)
plt.savefig('alleigs1.pdf', transparent=True)
plt.show()
exit()

