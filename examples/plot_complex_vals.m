

X = load('xmesh.dat');
Y = load('ymesh.dat');
Z = load('zmesh.dat');
Cr = load('rcolors.dat');
Ci = load('icolors.dat');

ax = subplot(1,2,1);
hr = surf(X,Y,Z,Cr);
set(hr, 'edgealpha', .1);
axis equal;

ax = subplot(1,2,2);
hi = surf(X,Y,Z,Ci);
set(hi, 'edgealpha', .1);
axis equal;
