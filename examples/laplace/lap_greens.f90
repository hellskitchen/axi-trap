program testkernels

  implicit real *8 (a-h,o-z)
  real *8 :: targ(10), src(10), as(100), ks(100)
  real *8 :: ctarg(10), csrc(10), powers(1000000)
  real *8 :: srcs(2,10000), targs(2,10000), par1(1000)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)
  real *8 :: u(10000), dudn(10000), dnorms(2,10000)
  real *8 :: dgrad(100), dgrad0(100)
  real *8 :: dsdt(10000), w(1000000), w2(1000000)
  real *8 :: src3(10), targ3(10)
  real *8 :: xyz(10), wsave(1000000)

  real *8, allocatable :: dvals(:), dgrads(:,:)
  real *8, allocatable :: dgrad0s(:,:), uall(:,:)
  real *8, allocatable :: dudnall(:,:)

  complex *16 :: ima, cd, cfac, cd2, cdtot
  
  complex *16, allocatable :: umodes(:,:), dudnmodes(:,:)
  complex *16, allocatable :: uall2(:,:), dudnall2(:,:)
  complex *16, allocatable :: zuall(:,:), zdudnall(:,:)
  
  real *16 :: qpi, qone, qsrc(10), qtarg(10), qpar1, hq, rq, zq
  real *16 :: qsrcs(2,10000), qtargs(2,10000), parsq(100)
  real *16 :: r0q, z0q, doneq, piq, epsq, aq, bq, qval, qpar0

  real *16, allocatable :: qvals(:), qgrads(:,:), qgrad0s(:,:)
  real *16, allocatable :: qvals1(:), qvals2(:), qgrads2(:,:)
  
  external fgreen0q
  
  ima = (0,1)
  
  call prini(6,13)
  
  done = 1
  pi = 4*atan(done)

  write(*,*) 'pi = ', pi


  !
  ! test green's identity for the laplace kernels
  !
  print *
  print *
  write(6,*) '---------- testing greens identity ------'
  write(6,*) '---------- single mode  ------'
  write(13,*) '---------- testing greens identity ------'
  write(13,*) '---------- single mode  ------'


  targ(1) = 5
  targ(2) = 5
  src(1) = 2.2d0
  src(2) = .9d0

  mode = 7
  call prinf('mode = *', mode, 1)
  call g0mone(par0, src, targ, par1, mode, dval, dgrad, dgrad0)

  call prin2('src = *', src, 2)
  call prin2('targ = *', targ, 2)
  call prin2('potential = *', dval, 1)
  !!call prin2('target gradient = *', dgrad, 2)
  !!call prin2('source gradient = *', dgrad0, 2)

  !
  ! generate a geometry
  !
  dltot = 2*pi
  n = 100
  h = dltot/n
  do i = 1,n
    t = (i-1)*h
    call funcurve(t, xys(1,i), dxys(1,i), d2xys(1,i))
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do

  call prin2('xys = *', xys, 20)
  call prin2('dxys = *', dxys, 20)
  call prin2('d2xys = *', d2xys, 20)
  call prin2('dsdt = *', dsdt, 20)
  call prin2('dnorms = *', dnorms, 20)
  
  do i = 1,n
    call g0mone(par0, src, xys(1,i), par1, mode, u(i), &
        dgrad, dgrad0)
    dudn(i) = dgrad(1)*dnorms(1,i) + dgrad(2)*dnorms(2,i)
  end do

  !call prin2('u = *', u, n)
  !call prin2('dudn = *', dudn, n)
  !stop
  
  pot = 0
  do i = 1,n
    call g0mone(par0, xys(1,i), targ, par1, mode, slp, &
        dgrad, dgrad0)
    dlp = dgrad0(1)*dnorms(1,i) + dgrad0(2)*dnorms(2,i)
    pot = pot + (dlp*u(i) - slp*dudn(i))*dsdt(i)*h*xys(1,i)*2*pi
  end do

  call prin2('directly, pot = *', dval, 1)
  call prin2('from greens, pot = *', pot, 1)
  call prin2('diff = *', pot-dval, 1)
  call prin2('ratio = *', pot/dval, 1)
  call prin2('inverse ratio = *', dval/pot, 1)


  !
  ! now do a multi-mode greens identity check by decomposining a
  ! point source
  !
  print *
  print *
  write(6,*) '---------- testing greens identity ------'
  write(6,*) '---------- multi-modes ------'
  write(13,*) '---------- testing greens identity ------'
  write(13,*) '---------- multi-modes ------'

  theta = pi/5
  theta0 = -pi/1.2340d0
  targ3(1) = targ(1)*cos(theta)
  targ3(2) = targ(1)*sin(theta)
  targ3(3) = targ(2)
  src3(1) = src(1)*cos(theta0)
  src3(2) = src(1)*sin(theta0)
  src3(3) = src(2)

  call prin2('targ3 = *', targ3, 3)
  call prin2('src3 = *', src3, 3)

  call g0cart(par0, src3, targ3, par1, par2, dval, dgrad, dgrad0)
  call prin2('freespace potential, dval = *', dval, 1)
  call prin2('freespace grad, dgrad = *', dgrad, 3)
  call prin2('freespace grad0, dgrad0 = *', dgrad0, 3)

  !
  ! now evaluate the potential and dudn everywhere on the curve and
  ! fourier decompose, then integrate
  !
  nphi = 300
  hphi = 2*pi/nphi
  allocate(uall(nphi,n))
  allocate(dudnall(nphi,n))
  allocate(zuall(nphi,n))
  allocate(zdudnall(nphi,n))

  do i = 1,n
    do j = 1,nphi
      phi = (j-1)*hphi
      xyz(1) = xys(1,i)*cos(phi)
      xyz(2) = xys(1,i)*sin(phi)
      xyz(3) = xys(2,i)
      call g0cart(par0, src3, xyz, par1, par2, uall(j,i), &
          dgrad, dgrad0)
      dudnall(j,i) = dnorms(1,i)*cos(phi)*dgrad(1) + &
          dnorms(1,i)*sin(phi)*dgrad(2) + dnorms(2,i)*dgrad(3)
      zuall(j,i) = uall(j,i)
      zdudnall(j,i) = dudnall(j,i)
    enddo
  enddo


  !
  ! initialize and call complex FFT
  !
  call  zffti(nphi, wsave)

  do i = 1,n
    call zfftf(nphi, zuall(1,i), wsave)
    call zfftf(nphi, zdudnall(1,i), wsave)
  end do

  do i = 1,n
    do j = 1,nphi
      zuall(j,i) = zuall(j,i)/nphi
      zdudnall(j,i) = zdudnall(j,i)/nphi
    end do
  end do

  !!!!call prin2('uall in place= *', zuall, 2*nphi)
  
  errmax = -1
  errmax2 = -1
  do i = 1,n
    nphi2 = nphi/2
    err = abs(zuall(nphi2,i))**2 + abs(zuall(nphi2-1,i))**2 &
        + abs(zuall(nphi2+1,i))**2 
    err = sqrt(err/3)
    if (err .gt. errmax) errmax = err
    err2 = abs(zdudnall(nphi2,i))**2 &
        + abs(zdudnall(nphi2-1,i))**2 &
        + abs(zdudnall(nphi2+1,i))**2 
    err2 = sqrt(err2/3)
    if (err2 .gt. errmax2) errmax2 = err2
  end do
  
  call prin2('max error estimate in zuall= *', errmax, 1)
  call prin2('max error estimate in zdudnall= *', errmax2, 1)
  
  
  !
  ! now apply the effective surface layer potential
  !
  maxm = nphi/2 + 20
  allocate(dvals(-maxm:maxm))
  allocate(dgrads(2,-maxm:maxm))
  allocate(dgrad0s(2,-maxm:maxm))
  
  nphi2 = nphi/2
  cdtot = 0
  
  do i = 1,n
    
    call g0mall(par0, xys(1,i), targ, par1, maxm, dvals, &
        dgrads, dgrad0s)
    wht = dsdt(i)*h*xys(1,i)

    cd = 0
    do j = 1,nphi2

      dlp = dgrad0s(1,j-1)*dnorms(1,i) + dgrad0s(2,j-1)*dnorms(2,i)
      cd = cd + (dlp*zuall(j,i) &
          - dvals(j-1)*zdudnall(j,i))*wht*exp(ima*(j-1)*theta)

      mode = nphi-j+1
      dlp = dgrad0s(1,-j)*dnorms(1,i) + dgrad0s(2,-j)*dnorms(2,i)
      cd = cd + (dlp*zuall(mode,i) &
          - dvals(-j)*zdudnall(mode,i))*wht*exp(ima*(-j)*theta)

    end do

    cdtot = cdtot + cd
    
  end do

  cdtot = cdtot*2*pi


  print *
  print *
  print *
  call prin2('exactly, pot = *', dval, 1)
  call prin2('from 3d greens, pot = *', cdtot, 2)
  call prin2('difference = *', dval - cdtot, 2)
  call prin2('ratio = *', cdtot/dval, 2)
  call prin2('inverse ratio = *', dval/cdtot, 2)


  
  
end program testkernels





subroutine funcurve(t, xy, dxy, d2xy)
  implicit real *8 (a-h,o-z)
  real *8 :: xy(2), dxy(2), d2xy(2)

  x0 = 2
  y0 = 1
  a = 1d0
  b = 1d0

  xy(1) = x0 + a*cos(t)
  xy(2) = y0 + b*sin(t)

  dxy(1) = -a*sin(t)
  dxy(2) = b*cos(t)

  d2xy(1) = -a*cos(t)
  d2xy(2) = -b*sin(t)
  return
end subroutine funcurve




  
