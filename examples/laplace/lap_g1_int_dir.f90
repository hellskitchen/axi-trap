program testkernels

  implicit real *8 (a-h,o-z)

  integer :: ipivs(100000)

  real *8 :: targ(10), src(10), as(100), ks(100)
  real *8 :: ctarg(10), csrc(10), powers(1000000)
  real *8 :: srcs(2,10000), targs(2,10000), par1(1000)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)
  real *8 :: u(10000), dudn(10000), dnorms(2,10000)
  real *8 :: dgrad(100), dgrad0(100)
  real *8 :: dsdt(10000), w(1000000), w2(1000000)
  real *8 :: src3(10), targ3(10)
  real *8 :: xyz(10), wsave(1000000)
  real *8 :: xs(10000), ys(10000), rnx(10000), rny(10000)
  real *8 :: sol(10000), unorms(10000)
  real *8 :: ur(10000), ui(10000), sol1(10000), sol2(10000)

  real *8, allocatable :: dvals(:), dgrads(:,:)
  real *8, allocatable :: dgrad0s(:,:), uall(:,:)
  real *8, allocatable :: dudnall(:,:), amat(:,:)
  real *8, allocatable :: amats(:,:,:),xyzs(:,:,:)
  real *8, allocatable :: dipvecs(:,:,:)

  complex *16 :: ima, cd, cfac, cd2, cdtot, zrhs(10000)
  complex *16 :: zsol(10000)
  
  complex *16, allocatable :: zuall(:,:), zdudnall(:,:)
  complex *16, allocatable :: zsigma(:,:), zdip(:,:)

  external g0mone, glap2d, g0mall

  
  ima = (0,1)  
  call prini(6,13)
  
  done = 1
  pi = 4*atan(done)

  write(*,*) 'pi = ', pi


  !
  ! test green's identity for the laplace kernels
  !
  print *
  print *
  write(6,*) '---------- testing dirichlet solver ------'
  write(6,*) '---------- interior - single mode  ------'
  write(13,*) '---------- testing dirichlet solver ------'
  write(13,*) '---------- interior - single mode  ------'


  targ(1) = 2.1d0
  targ(2) = .9d0
  src(1) = 5
  src(2) = 5

  mode = 0
  call prinf('mode = *', mode, 1)
  call g0mone(par0, src, targ, par1, mode, dval, dgrad, dgrad0)

  call prin2('src = *', src, 2)
  call prin2('targ = *', targ, 2)
  call prin2('exact potential = *', dval, 1)

  !
  ! generate a geometry
  !
  dltot = 2*pi
  n = 400
  h = dltot/n
  do i = 1,n
    t = (i-1)*h
    call funcurve(t, xys(1,i), dxys(1,i), d2xys(1,i))
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do

  !
  ! generate dirichlet data
  !
  do i = 1,n
    call g0mone(par0, src, xys(1,i), par1, mode, u(i), &
        dgrad, dgrad0)
  end do

  call prin2('dirichlet data = *', u, 30)
  
  !
  ! build the matrix and solve
  !
  allocate(amat(n,n))
  do i = 1,n
    do j = 1,n
      amat(j,i) = 0
    end do
  end do
  
  norder = 8
  do i = 1,n
    xs(i) = xys(1,i)
    ys(i) = xys(2,i)
    rnx(i) = dnorms(1,i)
    rny(i) = dnorms(2,i)
  end do

  diag = -.5d0
  ifscale = 1
  call dalpertmat_dlp(ier, norder, n, xys, dxys, &
      h, g0mone, par0, par1, mode, diag, ifscale, amat)


  call prin2('amat = *', amat, 30)

  call prin2('data, u = *', u, n)
  
  !
  ! solve and test
  !
  call dgausselim(n, amat, u, info, sol, dcond)
  call prinf('after dgausselim, info = *', info, 1)
  call prin2('after dgausselim, dcond = *', dcond, 1)
  call prin2('after dgausselim, sol = *', sol, 30)
  
  dval2 = 0
  do i = 1,n
    call g0mone(par0, xys(1,i), targ, par1, mode, pot, &
        dgrad, dgrad0)
    dval2 = dval2 + sol(i)*h*dsdt(i)*(dgrad0(1)*dnorms(1,i) &
        + dgrad0(2)*dnorms(2,i))*2*pi*xys(1,i)
  end do

  call prin2('exact potential = *', dval, 1)
  call prin2('from solve, dval2 = *', dval2, 1)
  call prin2('difference = *', dval2-dval, 1)
  call prin2('rel difference = *', (dval2-dval)/dval, 1)
  !call prin2('ratio = *', dval2/dval, 1)
  !call prin2('inverse ratio = *', dval/dval2, 1)
  

  !
  ! now test a multi-mode scattering problem
  !
  print *
  print *
  write(6,*) '---------- testing dirichlet solver ------'
  write(6,*) '---------- interior - multi-modes ------'
  write(13,*) '---------- testing dirichlet solver ------'
  write(13,*) '---------- interior - multi-modes ------'

  theta = 0
  theta0 = 0
  targ3(1) = targ(1)*cos(theta)
  targ3(2) = targ(1)*sin(theta)
  targ3(3) = targ(2)
  src3(1) = src(1)*cos(theta0)
  src3(2) = src(1)*sin(theta0)
  src3(3) = src(2)

  call prin2('targ3 = *', targ3, 3)
  call prin2('src3 = *', src3, 3)

  call g0cart(par0, src3, targ3, par1, par2, dval, dgrad, dgrad0)
  call prin2('freespace potential, dval = *', dval, 1)
  call prin2('freespace grad, dgrad = *', dgrad, 3)
  call prin2('freespace grad0, dgrad0 = *', dgrad0, 3)

  !
  ! now evaluate the potential and dudn everywhere on the curve and
  ! fourier decompose, then integrate
  !
  nphi = 400
  hphi = 2*pi/nphi
  allocate(uall(nphi,n))
  allocate(zuall(nphi,n))

  do i = 1,n

    do j = 1,nphi
      phi = (j-1)*hphi
      xyz(1) = xys(1,i)*cos(phi)
      xyz(2) = xys(1,i)*sin(phi)
      xyz(3) = xys(2,i)

      call g0cart(par0, src3, xyz, par1, par2, uall(j,i), &
          dgrad, dgrad0)
      zuall(j,i) = uall(j,i)
    enddo
  enddo

  !
  ! initialize and call complex FFT
  !
  call cpu_time(start)
  !$ start = OMP_get_wtime()


  call  zffti(nphi, wsave)

  do i = 1,n
    call zfftf(nphi, zuall(1,i), wsave)
  end do

  do i = 1,n
    do j = 1,nphi
      zuall(j,i) = zuall(j,i)/nphi
    end do
  end do

  errmax = -1
  errmax2 = -1
  do i = 1,n
    nphi2 = nphi/2
    err = abs(zuall(nphi2,i))**2 + abs(zuall(nphi2-1,i))**2 &
        + abs(zuall(nphi2+1,i))**2 
    err = sqrt(err/3)
    if (err .gt. errmax) errmax = err
  end do
  
  call prin2('max error estimate in zuall= *', errmax, 1)

  !
  ! determine how many modes are needed
  ! for each mode, calculate the L2 norm of u_n (the data)
  !
  do i = 1,nphi
    unorms(i) = 0
    do j = 1,n
      unorms(i) = unorms(i) + h*dsdt(j)*abs(zuall(i,j))**2
    end do
    unorms(i) = sqrt(unorms(i)/n)
  end do

  call prin2('unorms = *', unorms, 30)

  thresh = 1.0d-14
  do i = 1,nphi
    if (unorms(i) .le. thresh) then
      maxm = i-1
      exit
    end if
  end do

  maxm = maxm+5
  call prinf('maxm = *', maxm, 1)


  !
  ! now solve 2*maxm scattering problems...
  !

  allocate(zsigma(-maxm:maxm,n))
  allocate(amats(n,n,-maxm:maxm))

  !
  ! construct all matrices
  !
  call dalpertmodes_dlp(ier, norder, n, xys, dxys, &
      h, g0mall, par0, par1, maxm, diag, ifscale, amats)

  !
  ! and now solve
  !

  !$omp parallel do default(shared) &
  !$omp     private(m2, zrhs, info, zsol, dcond)
  do m = -maxm,maxm
    
    if (m .lt. 0) m2 = nphi+m+1
    if (m .ge. 0) m2 = m+1
    
    do i = 1,n
      zrhs(i) = zuall(m2,i)
    end do

    call dgausselim_zrhs(n, amats(1,1,m), zrhs, info, zsol, dcond)

    do i = 1,n
      zsigma(m,i) = zsol(i)
    end do
    
  end do
  !$omp end parallel do


  call cpu_time(finish)
  !$ finish = OMP_get_wtime()

  call prin2('time for solve = *', finish-start, 1)
  
  !
  ! now test the solution
  !
  allocate(dvals(-maxm:maxm))
  allocate(dgrads(2,-maxm:maxm))
  allocate(dgrad0s(2,-maxm:maxm))
  
  cd = 0
  do i = 1,n
    
    call g0mall(par0, xys(1,i), targ, par1, maxm, dvals, &
        dgrads, dgrad0s)
    
    wht = 2*pi*dsdt(i)*h*xys(1,i)
    
    do m = -maxm,maxm
      
      cd = cd &
          + zsigma(m,i)*wht*(dgrad0s(1,m)*dnorms(1,i) &
          + dgrad0s(2,m)*dnorms(2,i))*exp(ima*m*theta)
    end do
    
  end do
  
  call prin2('exact potential = *', dval, 1)
  call prin2('from solve, cd = *', cd, 2)
  call prin2('difference = *', cd-dval, 2)
  call prin2('ratio = *', cd/dval, 2)
  call prin2('inverse ratio = *', dval/cd, 2)


  !
  ! now dump the density out on the boundary and use the laplace FMM
  ! to evaluate the potential
  !
  allocate(zdip(nphi,n))

  do i = 1,n
    do j = 1,nphi
      zdip(j,i) = 0
    end do
  end do

  do i = 1,n
    do j = 0,maxm
      zdip(j+1,i) = zsigma(j,i)
    end do
    do j = 1,maxm
      zdip(nphi-j+1,i) = zsigma(-j,i)
    end do
  end do

  do i = 1,n
    call zfftf(nphi, zdip(1,i), wsave)
  end do

  allocate(xyzs(3,nphi,n))
  allocate(dipvecs(3,nphi,n))
  ntot = nphi*n

  do i = 1,n
    do j = 1,nphi
      phi = (j-1)*hphi
      xyzs(1,j,i) = xys(1,i)*cos(phi)
      xyzs(2,j,i) = xys(1,i)*sin(phi)
      xyzs(3,j,i) = xys(2,i)
      dipvecs(1,j,i) = dnorms(1,i)*cos(phi)
      dipvecs(2,j,i) = dnorms(1,i)*sin(phi)
      dipvecs(3,j,i) = dnorms(2,i)
    end do
  end do

  
  


  
    
end program testkernels





subroutine funcurve(t, xy, dxy, d2xy)
  implicit real *8 (a-h,o-z)
  real *8 :: xy(2), dxy(2), d2xy(2)

  x0 = 2
  y0 = 1
  a = 1.3d0
  b = 2.1d0

  xy(1) = x0 + a*cos(t)
  xy(2) = y0 + b*sin(t)

  dxy(1) = -a*sin(t)
  dxy(2) = b*cos(t)

  d2xy(1) = -a*cos(t)
  d2xy(2) = -b*sin(t)
  return
end subroutine funcurve




  
