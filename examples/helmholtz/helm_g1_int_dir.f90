program helm_g1_int_dir

  implicit real *8 (a-h,o-z)

  integer :: ipivs(100000)

  real *8 :: targ(10), src(10), as(100), ks(100)
  real *8 :: ctarg(10), csrc(10), powers(1000000)
  real *8 :: srcs(2,10000), targs(2,10000), par1(1000)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)
  real *8 :: dudn(10000), dnorms(2,10000)
  real *8 :: dgrad(100), dgrad0(100)
  real *8 :: dsdt(10000), w(1000000), w2(1000000)
  real *8 :: src3(10), targ3(10)
  real *8 :: xyz(10), wsave(1000000)
  real *8 :: xs(10000), ys(10000), rnx(10000), rny(10000)
  real *8 :: unorms(10000)
  real *8 :: ur(10000), ui(10000), sol1(10000), sol2(10000)
  real *8 :: pars1(10), pars2(10), dnorm3(10)

  real *8, allocatable :: dvals(:), dgrads(:,:)
  real *8, allocatable :: dgrad0s(:,:), uall(:,:)
  real *8, allocatable :: dudnall(:,:)
  real *8, allocatable :: xyzs(:,:,:)
  real *8, allocatable :: dipvecs(:,:,:)

  complex *16 :: ima, cd, cfac, cd2, cdtot, zrhs(100000)
  complex *16 :: zsol(10000), zk, zval, zgrad(10), zgrad0(10)
  complex *16 :: u(10000), zdiag, sol(100000), zval2, zpot

  complex *16 :: zval3, zgrad3(10), zhess3(10)

  complex *16, allocatable :: zuall(:,:), zdudnall(:,:)
  complex *16, allocatable :: zsigma(:,:), zdip(:,:), amat(:,:)
  complex *16, allocatable :: amats(:,:,:)
  complex *16, allocatable :: zvals(:), zgrads(:,:), zgrad0s(:,:)
  complex *16, allocatable :: zsigma3(:,:)
  
  external g0mone, glap2d, g0mall, gkmone, gkmall

  
  ima = (0,1)  
  call prini(6,13)
  
  done = 1
  pi = 4*atan(done)

  write(*,*) 'pi = ', pi


  targ(1) = 2.1d0
  targ(2) = .9d0
  src(1) = 5
  src(2) = 5

  zk = 5.234d0 + ima/100000
  
  mode = 1
  call prinf('mode = *', mode, 1)
  call gkmone(zk, src, targ, par1, mode, zval, zgrad, zgrad0)

  call prin2('src = *', src, 2)
  call prin2('targ = *', targ, 2)
  call prin2('exact potential = *', zval, 2)

  
  !
  ! generate a geometry
  !
  dltot = 2*pi
  n = 200
  allocate(amat(n,n))
  call prinf('disc points on gen curve = *', n, 1)
  
  h = dltot/n
  do i = 1,n
    t = (i-1)*h
    call funcurve(t, xys(1,i), dxys(1,i), d2xys(1,i))
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do


  ifsingle = 0
  if (ifsingle .eq. 1) then

     !
     ! test an interior solver of a single mode for helmholtz
     !
     print *
     print *
     write(6,*) '---------- testing dirichlet solver ------'
     write(6,*) '---------- interior - single mode  ------'
     write(13,*) '---------- testing dirichlet solver ------'
     write(13,*) '---------- interior - single mode  ------'


     !
     ! generate dirichlet data
     !
     do i = 1,n
        call gkmone(zk, src, xys(1,i), par1, mode, u(i), &
             zgrad, zgrad0)
     end do

     call prin2('dirichlet data = *', u, 30)

     !
     ! build the matrix and solve
     !
     do i = 1,n
        do j = 1,n
           amat(j,i) = 0
        end do
     end do

     norder = 8
     zdiag = -.5d0
     ifscale = 1
     call zalpertmat_dlp(ier, norder, n, xys, dxys, &
          h, gkmone, zk, par1, mode, zdiag, ifscale, amat)

     call zgausselim(n, amat, u, info, sol, dcond)
     call prinf('after dgausselim, info = *', info, 1)
     call prin2('after dgausselim, dcond = *', dcond, 1)
     call prin2('after dgausselim, sol = *', sol, 30)

     zval2 = 0
     do i = 1,n
        call gkmone(zk, xys(1,i), targ, par1, mode, zpot, &
             zgrad, zgrad0)
        zval2 = zval2 + sol(i)*h*dsdt(i)*(zgrad0(1)*dnorms(1,i) &
             + zgrad0(2)*dnorms(2,i))*2*pi*xys(1,i)
     end do

     call prin2('exact potential = *', zval, 1)
     call prin2('from solve, dval2 = *', zval2, 1)
     call prin2('difference = *', zval2-zval, 1)
     call prin2('rel difference = *', (zval2-zval)/zval, 1)
     !call prin2('ratio = *', dval2/dval, 1)
     !call prin2('inverse ratio = *', dval/dval2, 1)

  endif


  
  !
  ! now test a multi-mode scattering problem
  !
  print *
  print *
  write(6,*) '---------- testing dirichlet solver ------'
  write(6,*) '---------- interior - multi-modes ------'
  write(13,*) '---------- testing dirichlet solver ------'
  write(13,*) '---------- interior - multi-modes ------'

  theta = pi/6
  theta0 = pi/3
  targ3(1) = targ(1)*cos(theta)
  targ3(2) = targ(1)*sin(theta)
  targ3(3) = targ(2)
  src3(1) = src(1)*cos(theta0)
  src3(2) = src(1)*sin(theta0)
  src3(3) = src(2)

  call prin2('targ3 = *', targ3, 3)
  call prin2('src3 = *', src3, 3)

  call gk3d(zk, src3, targ3, par1, par2, zval, zgrad, zgrad0)
  call prin2('freespace potential, dval = *', zval, 1)
  call prin2('freespace grad, dgrad = *', zgrad, 3)
  call prin2('freespace grad0, dgrad0 = *', zgrad0, 3)

  !
  ! now evaluate the potential and dudn everywhere on the curve and
  ! fourier decompose, then integrate
  !
  nphi = 400
  hphi = 2*pi/nphi
  allocate(uall(nphi,n))
  allocate(zuall(nphi,n))

  do i = 1,n

     do j = 1,nphi
        phi = (j-1)*hphi
        xyz(1) = xys(1,i)*cos(phi)
        xyz(2) = xys(1,i)*sin(phi)
        xyz(3) = xys(2,i)

        call gk3d(zk, src3, xyz, par1, par2, zuall(j,i), &
             zgrad, zgrad0)
     enddo
  enddo

  ! plot the incoming potential
  lda = 2
  iw = 11
  call axi_vtk_scalar(iw, n, xys, nphi, lda, zuall, 'incoming potential')


  !
  ! initialize and call complex FFT
  !
  call cpu_time(start)
  !$ start = OMP_get_wtime()


  call  zffti(nphi, wsave)

  do i = 1,n
    call zfftf(nphi, zuall(1,i), wsave)
  end do

  do i = 1,n
    do j = 1,nphi
      zuall(j,i) = zuall(j,i)/nphi
    end do
  end do

  errmax = -1
  errmax2 = -1
  do i = 1,n
    nphi2 = nphi/2
    err = abs(zuall(nphi2,i))**2 + abs(zuall(nphi2-1,i))**2 &
        + abs(zuall(nphi2+1,i))**2 
    err = sqrt(err/3)
    if (err .gt. errmax) errmax = err
  end do
  
  call prin2('max error estimate in zuall= *', errmax, 1)
  
  !
  ! determine how many modes are needed
  ! for each mode, calculate the L2 norm of u_n (the data)
  !
  do i = 1,nphi
    unorms(i) = 0
    do j = 1,n
      unorms(i) = unorms(i) + h*dsdt(j)*abs(zuall(i,j))**2
    end do
    unorms(i) = sqrt(unorms(i)/n)
  end do

  call prin2('unorms = *', unorms, 30)

  thresh = 1.0d-14
  do i = 1,nphi
    if (unorms(i) .le. thresh) then
      maxm = i-1
      exit
    end if
  end do

  maxm = maxm+5
  call prinf('maxm = *', maxm, 1)

  !
  ! now solve 2*maxm scattering problems...
  !

  print *
  print *
  
  allocate(zsigma(-maxm:maxm,n))
  allocate(amats(n,n,-maxm:maxm))

  !
  ! construct all matrices
  !
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  
  norder = 8
  zdiag = -.5d0
  ifscale = 1
  call zalpertmodes_dlp(ier, norder, n, xys, dxys, &
      h, gkmall, zk, par1, maxm, zdiag, ifscale, amats)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()

  call prin2('time to build matrices = *', t1-t0, 1)

  
  !
  ! and now solve
  !

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  
  
  !$omp parallel do default(shared) &
  !$omp     private(m2, zrhs, info, zsol, dcond)
  do m = -maxm,maxm
    
    if (m .lt. 0) m2 = nphi+m+1
    if (m .ge. 0) m2 = m+1
    
    do i = 1,n
      zrhs(i) = zuall(m2,i)
    end do

    call zgausselim(n, amats(1,1,m), zrhs, info, zsol, dcond)

    do i = 1,n
      zsigma(m,i) = zsol(i)
    end do
    
  end do
  !$omp end parallel do


  call cpu_time(finish)
  !$ finish = OMP_get_wtime()

  call prin2('time for gaussian elim = *', finish-t0, 1)
  call prin2('total time for solve = *', finish-start, 1)
  
  !
  ! now test the solution
  !
  allocate(zvals(-maxm:maxm))
  allocate(zgrads(2,-maxm:maxm))
  allocate(zgrad0s(2,-maxm:maxm))
  
  cd = 0
  do i = 1,n

    call gkmall(zk, xys(1,i), targ, par1, maxm, zvals, &
        zgrads, zgrad0s)

    wht = 2*pi*dsdt(i)*h*xys(1,i)

    do m = -maxm,maxm

      cd = cd &
          + zsigma(m,i)*wht*(zgrad0s(1,m)*dnorms(1,i) &
          + zgrad0s(2,m)*dnorms(2,i))*exp(ima*m*theta)
    end do

  end do

  print *
  print *
  
  call prin2('exact potential = *', zval, 2)
  call prin2('from solve, cd = *', cd, 2)
  call prin2('difference = *', cd-zval, 2)
  !!call prin2('ratio = *', cd/zval, 2)
  !!call prin2('inverse ratio = *', zval/cd, 2)

  ! synthesize the solution at each of the nphi points and re-test the potential
  ! evaluation

  allocate( zsigma3(nphi,n) )

  do i = 1,n
     do j = 1,nphi
        phi = (j-1)*hphi
        zsigma3(j,i) = 0
        do m = -maxm,maxm
           zsigma3(j,i) = zsigma3(j,i) + zsigma(m,i)*exp(ima*m*phi)
        end do
     enddo
  enddo

  cd = 0
  do i = 1,n

     wht = -pi*dsdt(i)*h*xys(1,i)/n


     do j = 1,nphi
        phi = (j-1)*hphi
        src3(1) = xys(1,i)*cos(phi)
        src3(2) = xys(1,i)*sin(phi)
        src3(3) = xys(2,i)

        dnorm3(1) = dnorms(1,i)*cos(phi)
        dnorm3(2) = dnorms(1,i)*sin(phi)
        dnorm3(3) = dnorms(2,i)

        call gk3d(zk, src3, targ3, pars1, pars2, zval3, zgrad3, zhess3)


        cd = cd + wht*zsigma3(j,i)*(zgrad3(1)*dnorm3(1) &
          + zgrad3(2)*dnorm3(2) + zgrad3(3)*dnorm3(3))
    end do

  end do


  print *
  print *
  call prin2('exact potential = *', zval, 2)
  call prin2('after synthesis, from solve, cd = *', cd, 2)
  call prin2('difference = *', cd-zval, 2)
  call prin2('ratio = *', cd/zval, 2)

  ! now plot the solution zsigma3
  lda = 2
  iw = 12
  call axi_vtk_scalar(iw, n, xys, nphi, lda, zsigma3, 'the sol sigma')


  stop


    
end program helm_g1_int_dir





subroutine funcurve(t, xy, dxy, d2xy)
  implicit real *8 (a-h,o-z)
  real *8 :: xy(2), dxy(2), d2xy(2)

  x0 = 2
  y0 = 1
  a = 1.3d0
  b = 2.1d0

  xy(1) = x0 + a*cos(t)
  xy(2) = y0 + b*sin(t)

  dxy(1) = -a*sin(t)
  dxy(2) = b*cos(t)

  d2xy(1) = -a*cos(t)
  d2xy(2) = -b*sin(t)
  return
end subroutine funcurve




  
