program debye_dielectric_test1
  implicit real *8 (a-h,o-z)
  real *8 ps(10000),zs(10000),dpdt(10000), &
      dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000)
  real *8 :: psin(10000),zsin(10000),dpdtin(10000),dzdtin(10000), &
      dpdt2in(10000),dzdt2in(10000),dsdtin(10000), &
      psout(10000),zsout(10000),dpdtout(10000), &
      dzdtout(10000),dpdt2out(10000),dzdt2out(10000), &
      dsdtout(10000), svals(10000)
  real *8 :: targ(10), ptz(10)

  real *8, allocatable :: xys0(:,:), dxys0(:,:), d2xys0(:,:)
  real *8, allocatable :: xys(:,:), dxys(:,:), d2xys(:,:)
  real *8, allocatable :: xys1(:,:), dxys1(:,:), d2xys1(:,:)

  complex *16 zk1,zk0,zk000,cd,ima,cdh,cdb,zk2, &
      u1,u0,e1,e0,omega,zk,omega0, &
      cdt,cds,cd2, &
      u(10000000),v(10000000),sz(100000),work(20000000), &
      work2(5000000), &
      rhoin(10000),rhomin(10000),ain,bin, &
      rhoout(10000),rhomout(10000),aout,bout, &
      zjsin(2,10000),zmsin(2,10000), &
      zjsout(2,10000),zmsout(2,10000), &
      ein(3,10000),hin(3,10000), &
      eout(3,10000),hout(3,10000), &
      etanin(2,10000),htanin(2,10000), &
      etanout(2,10000),htanout(2,10000), &
      etan(2,10000),htan(2,10000), &
      rhs(10000),sol(10000), &
      vals1(100),vals2(100),vals3(100), &
      rho1(10000),rhom1(10000),aj,bj,am,bm, &
      rho0(10000),rhom0(10000), &
      zjs1(2,10000),zms1(2,10000), &
      zjs0(2,10000),zms0(2,10000), &
      rhsin(10000),rhsout(10000), &
      etest(3,10000),htest(3,10000), &
      ceacycle,cebcycle,chacycle,chbcycle

  complex *16 :: evec1(10), evec2(10), efield(10), hfield(10)
  complex *16 :: hvec1(10), hvec2(10)

  complex *16, allocatable :: amat(:,:), ws0div(:,:), umat(:,:)
  complex *16, allocatable :: vtmat(:,:)
  
  external ghfun2,ghfun4n1

  done=1
  ima=(0,1)
  pi=4*atan(done)

  call prini(6,13)
  !call prin2('enter n:*',pi,0)
  !read *, n
  n = 101
  call prinf('n=*',n,1)

  !
  ! create the scattering surface
  !
  rl=2*pi
  h=rl/n
  allocate(xys(2,n))
  allocate(dxys(2,n))
  allocate(d2xys(2,n))
  do i=1,n
    t=h*(i-1)
    call funcurve(t,ps(i),zs(i),dpdt(i),dzdt(i), &
        dpdt2(i),dzdt2(i))
    dd=dpdt(i)**2+dzdt(i)**2
    dsdt(i)=sqrt(dd)
    xys(1,i) = ps(i)
    xys(2,i) = zs(i)
    dxys(1,i) = dpdt(i)
    dxys(2,i) = dzdt(i)
    d2xys(1,i) = dpdt2(i)
    d2xys(2,i) = dzdt2(i)
  end do
  
  !
  ! find the minimum ps to construct spanning surface
  !
  ind=1
  do i=1,n
    if (ps(i) .lt. ps(ind)) ind=i
  end do
  
  !
  ! build 2 source/testing surfaces, first the one inside the surface...
  !
  allocate(xys0(2,n))
  allocate(dxys0(2,n))
  allocate(d2xys0(2,n))
  do i=1,n
    t=h*(i-1)
    call funcurve2(t,psin(i),zsin(i),dpdtin(i),dzdtin(i), &
        dpdt2in(i),dzdt2in(i))
    dd=dpdtin(i)**2+dzdtin(i)**2
    dsdtin(i)=sqrt(dd)
    xys0(1,i) = psin(i)
    xys0(2,i) = zsin(i)
    dxys0(1,i) = dpdtin(i)
    dxys0(2,i) = dzdtin(i)
    d2xys0(1,i) = dpdt2in(i)
    d2xys0(2,i) = dzdt2in(i)
  end do
  
  !
  ! ...and then the one outside
  !
  allocate(xys1(2,n))
  allocate(dxys1(2,n))
  allocate(d2xys1(2,n))
  do i=1,n
    t=h*(i-1)
    call funcurve3(t,psout(i),zsout(i),dpdtout(i),dzdtout(i), &
        dpdt2out(i),dzdt2out(i))
    dd=dpdtout(i)**2+dzdtout(i)**2
    dsdtout(i)=sqrt(dd)
    xys1(1,i) = psout(i)
    xys1(2,i) = zsout(i)
    dxys1(1,i) = dpdtout(i)
    dxys1(2,i) = dzdtout(i)
    d2xys1(1,i) = dpdt2out(i)
    d2xys1(2,i) = dzdt2out(i)
  end do
  
  iw=12
  itype=1
  call pyplot3(iw,ps,zs,n,itype,psin,zsin,n,itype, &
      psout,zsout,n,itype, &
      'scattering, inside source, and outside source*')

  !
  ! set permittivity, permeability, frequency, and mode
  !
  
  omega = 1.2d0 + ima/100000

  !
  ! set inside parameters
  !
  e0=.9d0
  u0=1.1d0
  zk0=omega*sqrt(e0*u0)

  !
  ! set outside parameters
  !
  e1=1.3d0
  u1=.83d0
  zk1=omega*sqrt(e1*u1)

  print *
  call prin2('omega=*',omega,2)

  print *
  call prin2('e0=*',e0,2)
  call prin2('u0=*',u0,2)
  call prin2('zk0=*',zk0,2)

  print *
  call prin2('e1=*',e1,2)
  call prin2('u1=*',u1,2)
  call prin2('zk1=*',zk1,2)

  mode=0
  m=mode
  print *
  call prinf('mode=*',mode,1)

  eps=1.0d-13
  call prin2('kernel precision=*',eps,1)

  norder=4
  norder=8
  call prinf('alpert quadrature order=*',norder,1)

  
  !
  ! place some electric and magnetic charges on the
  ! test surfaces and generate incoming fields
  !
  do i=1,n
    t=h*(i-1)
    rhoin(i)=exp(ima*t)/psin(i)/dsdtin(i)
    rhomin(i)=ima*exp(2*ima*t)/psin(i)/dsdtin(i)
    rhoout(i)=ima*exp(2*ima*t)/psout(i)/dsdtout(i)
    rhomout(i)=exp(1*ima*t)/psout(i)/dsdtout(i)
  end do
  
  if (m .eq. 0) then
    ain=1+ima
    bin=1-ima
    aout=1-ima
    bout=1+ima
  endif

  if (m .ne. 0) then
    ain=0
    bin=0
    aout=0
    bout=0
  endif

  call axidebye_jmbuilder(zk1, m, n, xys0, dxys0, d2xys0, h, &
      rhoin, rhomin, ain, bin, zjsin, zmsin)

  call axidebye_jmbuilder(zk0, m, n, xys1, dxys1, d2xys1, h, &
      rhoout, rhomout, aout, bout, zjsout, zmsout)

  !$omp parallel do default(shared) private(targ)
  do i=1,n
    targ(1) = ps(i)
    targ(2) = 0
    targ(3) = zs(i)

    call axidebye_mode_eval_dielectric(eps, omega, e1, u1, &
        m, n, rl, psin ,zsin, dpdtin,dzdtin, &
        rhoin, rhomin, zjsin, zmsin, targ, ein(1,i), hin(1,i))
    
    call axidebye_mode_eval_dielectric(eps, omega, e0, u0, &
        m, n, rl, psout, zsout, dpdtout, dzdtout, &
        rhoout, rhomout, zjsout, zmsout, targ, eout(1,i), hout(1,i))

  end do
  !$omp end parallel do
  
  
  !
  !       convert to tangent fields
  !
  call cyl2tan(n,dpdt,dzdt,ein,etanin)
  call cyl2tan(n,dpdt,dzdt,hin,htanin)
  call cyl2tan(n,dpdt,dzdt,eout,etanout)
  call cyl2tan(n,dpdt,dzdt,hout,htanout)


  !
  ! apply s0 div to e and h
  !
  allocate(ws0div(n,2*n))
  call crea_s0div1(norder, n, xys, dxys, h, mode, ws0div)

  call zmatvec(n, 2*n, ws0div, etanin, rhsin(1))
  call zmatvec(n, 2*n, ws0div, htanin, rhsin(n+1))
  call zmatvec(n, 2*n, ws0div, etanout, rhsout(1))
  call zmatvec(n, 2*n, ws0div, htanout, rhsout(n+1))


  !
  ! apply n dot to e and h
  !
  call ndotdirect(n,dpdt,dzdt,ein,rhsin(2*n+1))
  call ndotdirect(n,dpdt,dzdt,hin,rhsin(3*n+1))
  call ndotdirect(n,dpdt,dzdt,eout,rhsout(2*n+1))
  call ndotdirect(n,dpdt,dzdt,hout,rhsout(3*n+1))

  !
  ! and assemble rhs
  !
  do i=1,2*n
    rhs(i)=rhsin(i)-rhsout(i)
  end do
  
  do i=2*n+1,3*n
    rhs(i)=-e1*rhsin(i)+e0*rhsout(i)
  end do
  
  do i=3*n+1,4*n
    rhs(i)=u1*rhsin(i)-u0*rhsout(i)
  end do
  
  rhs(4*n+1)=0
  rhs(4*n+2)=0
  rhs(4*n+3)=0
  rhs(4*n+4)=0

  !
  ! add in integrals around a and b cycles if m=0
  !
  if (m .eq. 0) then
    
    ceacycle=0
    cebcycle=0
    chacycle=0
    chbcycle=0

    cebcycle=2*pi*ps(ind)*(etanin(2,ind)-etanout(2,ind))
    chbcycle=2*pi*ps(ind)*(htanin(2,ind)-htanout(2,ind))

    do i=1,n
      !ceacycle=ceacycle+dsdt(i)*h*ps(i)*(etanin(1,i)-etanout(1,i))
      !chacycle=chacycle+dsdt(i)*h*ps(i)*(htanin(1,i)-htanout(1,i))
      ceacycle=ceacycle+dsdt(i)*h*(etanin(1,i)-etanout(1,i))
      chacycle=chacycle+dsdt(i)*h*(htanin(1,i)-htanout(1,i))
    end do
    
    rhs(4*n+1)=ceacycle
    rhs(4*n+2)=cebcycle
    rhs(4*n+3)=chacycle
    rhs(4*n+4)=chbcycle

  end if

  !
  ! construct the dielectric matrix to invert
  !

  tclutch=0
  call prin2('tclutch=*',tclutch,1)
  nnn = 4*n+4
  allocate(amat(nnn,nnn))

  call axidebye_dielectric_mat1(ier, norder, n, xys, dxys, d2xys, h, &
    e1, u1, e0, u0, omega, mode, ind, amat)


  ! n4 = 4*n+4
  ! allocate(umat(n4,n4), vtmat(n4,n4))
  ! call zsvd(n4, n4, amat, umat, svals, vtmat)
  ! call prin2('sing vals = *', svals, n4)
  ! stop
  
  
  !
  ! solve the system
  !
  call zgausselim(nnn, amat, rhs, info, sol, dcond)
  call prin2('after zgausselim, dcond = *', dcond, 1)
        
  call ccopy601(n,sol(1),rho1)
  call ccopy601(n,sol(n+1),rhom1)
  call ccopy601(n,sol(2*n+1),rho0)
  call ccopy601(n,sol(3*n+1),rhom0)

  aj=sol(4*n+1)
  bj=sol(4*n+2)
  am=sol(4*n+3)
  bm=sol(4*n+4)
  
  print *
  call prin2('after solving, aj=*',aj,2)
  call prin2('after solving, bj=*',bj,2)
  call prin2('after solving, am=*',am,2)
  call prin2('after solving, bm=*',bm,2)
  print *

  !
  ! now construct the scattered fields and check accuracy, first in
  ! the exterior
  !
  call axidebye_jmbuilder_dielectric(omega,e1,e0,u1,u0, &
      m,n,rl,ps,zs,dpdt,dzdt,dpdt2,dzdt2,norder, &
      rho1,rho0,rhom1,rhom0,aj,bj,am,bm,tclutch, &
      zjs1,zjs0,zms1,zms0)

  targ(1) = 5
  targ(2) = 4.5d0
  targ(3) = 1.5d0

  call axidebye_mode_eval_dielectric(eps, omega, e1, u1, &
    m, n, rl, psin ,zsin, dpdtin,dzdtin, &
    rhoin, rhomin, zjsin, zmsin, targ, evec1, hvec1)
  
  call axidebye_mode_eval_dielectric(eps, omega, e1, u1, &
    m, n, rl, ps,zs, dpdt,dzdt, &
    rho1, rhom1, zjs1, zms1, targ, evec2, hvec2)

  call prin2('known efield in outside, evec1 = *', evec1, 6)
  call prin2('calculated efield in outside, evec2 = *', evec2, 6)
  
  evec2(1) = evec2(1)-evec1(1)
  evec2(2) = evec2(2)-evec1(2)
  evec2(3) = evec2(3)-evec1(3)
  call prin2('difference = *', evec2, 6)

  err=0
  err = sqrt(evec2(1)**2+evec2(2)**2+evec2(3)**2)
  call prin2('l2 err = *', err, 1)

  print *
  call prin2('known hfield in outside, hvec1 = *', hvec1, 6)
  call prin2('calculated hfield in outside, hvec2 = *', hvec2, 6)

  hvec2(1) = hvec2(1)-hvec1(1)
  hvec2(2) = hvec2(2)-hvec1(2)
  hvec2(3) = hvec2(3)-hvec1(3)
  call prin2('difference = *', hvec2, 6)

  err=0
  err = sqrt(hvec2(1)**2+hvec2(2)**2+hvec2(3)**2)
  call prin2('l2 err = *', err, 1)
  
  !
  ! and now test the fields in the interior
  !

  targ(1) = 1.4d0
  targ(2) = .4d0
  targ(3) = 3.1d0

  call axidebye_mode_eval_dielectric(eps, omega, e0, u0, &
    m, n, rl, psout ,zsout, dpdtout,dzdtout, &
    rhoout, rhomout, zjsout, zmsout, targ, evec1, hvec1)
  
  call axidebye_mode_eval_dielectric(eps, omega, e0, u0, &
    m, n, rl, ps,zs, dpdt,dzdt, &
    rho0, rhom0, zjs0, zms0, targ, evec2, hvec2)

  print *
  call prin2('known efield in inside, evec1 = *', evec1, 6)
  call prin2('calculated efield in inside, evec2 = *', evec2, 6)
  
  evec2(1) = evec2(1)-evec1(1)
  evec2(2) = evec2(2)-evec1(2)
  evec2(3) = evec2(3)-evec1(3)
  call prin2('difference = *', evec2, 6)

  err=0
  err = sqrt(evec2(1)**2+evec2(2)**2+evec2(3)**2)
  call prin2('l2 err = *', err, 1)

  print *
  call prin2('known hfield in inside, hvec1 = *', hvec1, 6)
  call prin2('calculated hfield in inside, hvec2 = *', hvec2, 6)

  hvec2(1) = hvec2(1)-hvec1(1)
  hvec2(2) = hvec2(2)-hvec1(2)
  hvec2(3) = hvec2(3)-hvec1(3)
  call prin2('difference = *', hvec2, 6)

  err=0
  err = sqrt(hvec2(1)**2+hvec2(2)**2+hvec2(3)**2)
  call prin2('l2 err = *', err, 1)

  stop
end program debye_dielectric_test1







subroutine funcurve(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !c
  !c       controls the scattering surface
  !c
  done=1
  pi=4*atan(done)

  itype=1

  if (itype .eq. 1) then
    !
    ! ellipse
    !
    x0=2
    y0=2.5
    a=1d0
    b=1d0

    x=x0+a*cos(t)
    y=y0+b*sin(t)
    dxdt=-a*sin(t)
    dydt=b*cos(t)
    dxdt2=-a*cos(t)
    dydt2=-b*sin(t)

    return
  end if
  
  if (itype .eq. 2) then  
    !
    ! squiggly thing
    !
    a=.2d0
    b=.3d0
    n=4

    x=2+(1+a*cos(n*t))*cos(t)
    y=2+(1+b*sin(t))*sin(t)

    dxdt=-(1+a*cos(n*t))*sin(t)-a*n*sin(n*t)*cos(t)
    dydt=(1+b*sin(t))*cos(t)+b*cos(t)*sin(t)

    dxdt2=-(1+a*cos(n*t))*cos(t)+a*n*sin(n*t)*sin(t) &
        +a*n*sin(n*t)*sin(t)-a*n*n*cos(n*t)*cos(t)
    
    dydt2=-(1+b*sin(t))*sin(t)+b*cos(t)*cos(t) &
        +b*cos(t)*cos(t)-b*sin(t)*sin(t)

    return
  end if
  
  return
end subroutine funcurve





subroutine funcurve2(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !
  ! Controls the source surface inside to generate a field in the
  ! exterior. This surface is also the testing surface for the
  ! field generated due to funcurve3 in the exterior.
  !
  done=1
  pi=4*atan(done)

  a=.13d0
  b=.13d0

  x0=2.1d0
  y0=2.2d0

  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)

  return
end subroutine funcurve2





subroutine funcurve3(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !
  ! Controls the source surface outside to generate a field in the
  ! interior. This surface is also the testing surface for the
  ! field generated due to funcurve2 in the interior.
  !
  a=.15d0
  b=.15d0
  x0=6d0
  y0=1d0

  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)

  return
end subroutine funcurve3










        subroutine cscalevec(n,x,sc)
        implicit real *8 (a-h,o-z)
       complex *16 x(1),sc

        do 1600 i=1,n
        x(i)=sc*x(i)
 1600   continue

        return
        end





        subroutine cyl2tan(n,dpdt,dzdt,f,ftan)
        implicit real *8 (a-h,o-z)
        real *8 dpdt(1),dzdt(1)
        complex *16 f(3,1),ftan(2,1)


        do 2000 i=1,n

        dsdt=sqrt(dpdt(i)**2+dzdt(i)**2)
        dpds=dpdt(i)/dsdt
        dzds=dzdt(i)/dsdt

        ftan(1,i)=dpds*f(1,i)+dzds*f(3,i)
        ftan(2,i)=f(2,i)
 2000   continue

        return
        end





        subroutine cl2diff(n,x,y,dd)
        implicit real *8 (a-h,o-z)
        complex *16 x(1),y(1)

        dd=0
        do 1600 i=1,n
        dd=dd+abs(x(i)-y(i))**2
 1600   continue

        dd=sqrt(dd)

        return
        end





        subroutine creldiff(n,x,y,dd)
        implicit real *8 (a-h,o-z)
        complex *16 x(1),y(1)

        dd=0
        dd1=0
        do 1600 i=1,n
        dd=dd+abs(x(i)-y(i))**2
        dd1=dd1+abs(y(i))**2
 1600   continue

        dd=sqrt(dd/dd1)

        return
        end





        subroutine crelfielderr(n,x,y,err)
        implicit real *8 (a-h,o-z)
        complex *16 x(3,1),y(3,1),cd1,cd2,cd3
!c
!c       calculates the relative error between the two fields
!c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
!c       sum of squares of the field vector at each of the n points
!c
        dd1=0
        dd2=0

        do 1600 i=1,n

        cd1=x(1,i)-y(1,i)
        cd2=x(2,i)-y(2,i)
        cd3=x(3,i)-y(3,i)

        dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
        dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2

 1600 continue

        err=sqrt(dd1/dd2)

        return
        end





        subroutine cfielderr(n,x,y,err)
        implicit real *8 (a-h,o-z)
        complex *16 x(3,1),y(3,1),cd1,cd2,cd3
!c
!c       calculates the relative error between the two fields
!c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
!       sum of squares of the field vector at each of the n points
!c
        dd1=0

        do 1600 i=1,n

        cd1=x(1,i)-y(1,i)
        cd2=x(2,i)-y(2,i)
        cd3=x(3,i)-y(3,i)

        dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
 1600 continue

        err=sqrt(dd1/n)

        return
        end



