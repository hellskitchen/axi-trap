program taylorstate2
  implicit real *8 (a-h,o-z)
  real *8 ps(10000),zs(10000),dpdt(10000), &
      dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000), &
      psg(10000),zsg(10000),dpdtg(10000),dsdtg(10000), &
      dzdtg(40000),dpdtg2(10000),dzdtg2(10000), &
      errs(10000),xs(10000),whts(10000), & 
      tks(10000),yes(10000),yhs(10000), &
      ptest(10000),ztest(10000), &
      err1(10000),err2(10000),err3(10000),err4(10000), &
      ts(10000),ts2(10000),ts3(10000),dtds(10000), &
      dtds2(10000), &
      ps1(10000),zs1(10000),&
      dpds1(10000),dzds1(10000),dpds2(10000),dzds2(10000),&
      dpds21(10000),dzds21(10000),&
      surfrhom(4, 1000000),&
      volh(6, 100000), surfm(6, 1000000), &
      rvals(100000), rks(100000), slice(4,100000)

  real *8 :: ps_in(10000), zs_in(10000)
  real *8 :: dpdt_in(10000), dzdt_in(10000)
  real *8 :: dpdt2_in(10000), dzdt2_in(10000)
  real *8 :: dsdt_in(10000)
  real *8 :: ps_out(10000), zs_out(10000)
  real *8 :: dpdt_out(10000), dzdt_out(10000)
  real *8 :: dpdt2_out(10000), dzdt2_out(10000)
  real *8 :: dsdt_out(10000)
  real *8 :: psg_out(10000), zsg_out(10000)
  real *8 :: dpdtg_out(10000), dzdtg_out(10000)
  real *8 :: dpdtg2_out(10000), dzdtg2_out(10000)
  real *8 :: dsdtg_out(10000)

  real *8, allocatable :: pzgrid(:,:,:), pvals(:,:)
  real *8, allocatable :: tvals(:,:), zvals(:,:)
  
  complex *16 :: hfield_in(3,10000), htan_in(2,10000)
  complex *16 :: hfield_out(3,10000), htan_out(2,10000)
  complex *16 :: flux_in, flux_out, flux_tor, flux_pol
  complex *16 :: rho_in(10000), rho_out(10000)
  complex *16 :: rhom_in(10000), rhom_out(10000)
  complex *16 :: zjs_in(2,10000), zjs_out(2,10000)
  complex *16 :: zms_in(2,10000), zms_out(2,10000)
  complex *16 :: alpha_in, alpha_out
  complex *16 :: bfield_in(10), bfield_out(10), bfield(10)
  complex *16 :: sigma(10000), field(10000)
  complex *16 :: field2(10000), sigma2(10000)

  complex *16 :: rhom2(10000), alpha2, beta2
  complex *16 :: rho2(10000), zwind1, zwind2
  complex *16 :: zjs2(2,10000), zms2(2,10000)

  complex *16, allocatable :: cmat(:,:), fieldimage(:,:,:)
  complex *16, allocatable :: cmat_out(:,:), cmat_in(:,:)

  complex *16 zk,zk0,cd,ima,cdh,cdb,zk2,cdt,cds,cd2, &
      zkinit,cda, flux, hdotn,&
      wz(1000000),wz2(1000000),sz(100000),&
      work(2000000), work2(2000000),&
      rho1(10000),rhom1(10000),alpha1,beta1,&
      rho(10000),rhom(10000),alpha,beta,&
      efield(3,10000),hfield(3,10000),&
      etest(3,10000),htest(3,10000),&
      etan(2,10000),htan(2,10000), zroot,&
      wndot(2000000),rhs(100000),sol(100000),&
      vals1(100),vals2(100),vals3(100),&
      hin(2,10000),zjs(2,10000),zms(2,10000),&
      zjs1(2,10000),zms1(2,10000),wsave(2000000),&
      ctemp1(10000),ctemp2(10000), u1, u2, u3, uu,&
      f1, f2, f3, ff, w, f12, f13, f23, f123,&
      vec1(10),vec2(10),db1dp, db2dp, db3dp,&
      evals1(3,10000),evals2(3,10000),evals3(3,10000),&
      hvals1(3,10000),hvals2(3,10000),hvals3(3,10000),&
      u(10000),ux(10000),uxx(10000), det(10),&
      bfield1(10), bfield2(10), db1dz, db2dz, db3dz,&
      r1(10000), r2(10000), denom1, denom2,&
      db1da, db2da, db3da, diver

  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)
  
  call prini(6,13)
  print *, 'enter n:'
  read *, n
  call prinf('n=*',n,1)

  !
  ! create the interior and exterior scattering surfaces
  !
  rl=2*pi
  h=rl/n
  
  do i=1,n

    t=h*(i-1)

    call fcurvein(t, ps_in(i), zs_in(i), dpdt_in(i), &
        dzdt_in(i), dpdt2_in(i),dzdt2_in(i))
    dd=dpdt_in(i)**2+dzdt_in(i)**2
    dsdt_in(i)=sqrt(dd)

    call fcurveout(t, ps_out(i), zs_out(i), dpdt_out(i), &
        dzdt_out(i), dpdt2_out(i),dzdt2_out(i))
    dd=dpdt_out(i)**2 + dzdt_out(i)**2
    dsdt_out(i)=sqrt(dd)

  end do
  
  iw=11
  itype=2
  call pyplot2(iw, ps_in, zs_in, n, itype, &
      ps_out, zs_out, n, itype, 'boundary of toroidal shell*')
  
  !
  !    set some subroutine parameters
  !

  zk = 1.0d-8 + ima*1.0d-15
  zk = 15.20234d0 + ima*1.0d-15
  zk = 5.5d0 + ima*1.0d-15
  call prin2(' *',pi,0)
  call prin2(' *',pi,0)
  call prin2('zk=*',zk,2)
  
  mode = 0
  m = mode
  call prinf('mode=*',mode,1)

  eps=1.0d-10
  call prin2('kernel eval precision=*',eps,1)

  norder=4
  norder=8
  norder=16
  call prinf('alpert order=*',norder,1)

  !
  ! specify enerate rhs
  !
  do i = 1,2*n
    rhs(i) = 0
  end do
  
  flux_tor = 1
  flux_pol = 2.1d0
  
  if (mode .eq. 0) then
    rhs(2*n+1) = flux_tor
    rhs(2*n+2) = flux_pol
  else
    rhs(2*n+1) = 0
    rhs(2*n+2) = 0
  end if
  
  print *
  print *
  call prin2('toroidal flux = *', flux_tor, 2)
  call prin2('poloidal flux = *', flux_pol, 2)
  
  !
  ! finally build the matrix and solve
  !
  lda = 2*n+2
  ind = 1
  allocate( cmat(lda,lda) )
  call creabeltrami_g2(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, ps_out, zs_out, &
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, &
      norder, ind, cmat)

  call zgausselim(lda, cmat, rhs, info, sol, dcond)
  call prin2('after zgausslim, dcond = *', dcond, 1)

  call ccopy601(n, sol, rhom_in)
  call ccopy601(n, sol(n+1), rhom_out)

  alpha_in = sol(2*n+1)
  alpha_out = sol(2*n+2)
  call prin2('alpha_in = *', alpha_in, 2)
  call prin2('alpha_out = *', alpha_out, 2)

  
  !
  ! test the field at a point
  !
  print *
  print *
  print *, '... evaluating beltrami field  ...'

  !
  ! form the current on the inner surface
  !
  call formbcurr1(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in, &
      alpha_in, zms_in)

  !
  ! form the current on the outer surface
  !
  call beltramicurrent(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, norder, &
      rhom_out, alpha_out, rho_out, zjs_out, zms_out)

  call prin2('rhom_in = *', rhom_in, 30)
  call prin2('rhom_out = *', rhom_out, 30)
  call prin2('zms_in = *', zms_in, 30)
  call prin2('zms_out = *', zms_out, 30)

  !
  ! and evaluate the separate fields
  !
  p1 = 1.2d0
  z1 = -.2d0
  theta1 = 0
  call beltramieva(eps, zk, m, n, rl, ps_in, zs_in,  &
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in, &
      alpha_in, zms_in, p1, theta1, z1, bfield_in)

  call beltramieva(eps, zk, m, n, rl, ps_out, zs_out,  &
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, rhom_out, &
      alpha_out, zms_out, p1, theta1, z1, bfield_out)

  bfield(1) = bfield_in(1) + bfield_out(1)
  bfield(2) = bfield_in(2) + bfield_out(2)
  bfield(3) = bfield_in(3) + bfield_out(3)
  call prin2('from integral equation, field = *', bfield, 6)

  
  !
  ! make a heat plot of the thing
  !
  a = 0.65d0
  b = 1.35d0
  nr = 101

  c = -.6d0
  d = .6d0
  ni = 101

  allocate(pzgrid(2,ni,nr))
  do i = 1,nr
    do j = 1,ni
      p1 = a + (b-a)*i/(nr+1)
      z1 = d - (d-c)*j/(ni+1)
      pzgrid(1,j,i) = p1
      pzgrid(2,j,i) = z1
    end do
  end do
  
  print *
  print *, '---making plot of the beltrami field---'

  dtot = nr*ni*done
  ntot = 0

  allocate(pvals(ni,nr), tvals(ni,nr), zvals(ni,nr))

  !$omp parallel do default(shared) &
  !$omp    private(i,j,p1,z1,theta1,bfield_in,bfield_out,bfield) &
  !$omp    private(zwind1, zwind2, dwind)
  do i = 1, nr
    do j = 1, ni

      ntot = ntot + 1
      if (mod(ntot,250) .eq. 1) then
        print *, 'percent done = ', ntot/dtot
      endif
          
      p1 = pzgrid(1,j,i)
      z1 = pzgrid(2,j,i)
      theta1 = 0

      call beltramieva(eps, zk, m, n, rl, ps_in, zs_in, &
          dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in,& 
          alpha_in, zms_in, p1, theta1, z1, bfield_in)

      call beltramieva(eps, zk, m, n, rl, ps_out, zs_out,  &
          dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, rhom_out, &
          alpha_out, zms_out, p1, theta1, z1, bfield_out)

      bfield(1) = bfield_in(1) + bfield_out(1)
      bfield(2) = bfield_in(2) + bfield_out(2)
      bfield(3) = bfield_in(3) + bfield_out(3)
      
      pvals(j,i) = bfield(1)
      tvals(j,i) = bfield(2)
      zvals(j,i) = bfield(3)
          
    enddo
  enddo
  !$omp end parallel do

  iw = 65
  call pyimage2(iw, ni, nr, pvals, ps_in, zs_in, n, a, b, &
      c, d, 'r component*')

  iw = 66
  call pyimage2(iw, ni, nr, tvals, ps_in, zs_in, n, a, b, &
      c, d, 'theta component*')

  iw = 67
  call pyimage2(iw, ni, nr, zvals, ps_in, zs_in, n, a, b, &
      c, d, 'z component*')

  stop
end program taylorstate2







!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        subroutine winding(p, z, n, ps, zs, h, dpdt, dzdt, zwind)
        implicit real *8 (a-h,o-z)
        real *8 :: ps(n), zs(n), dpdt(n), dzdt(n)
        complex *16 :: zwind, w, wtarg, ima, dw

        done = 1
        pi = 4*atan(done)
        ima = (0,1)

        zwind = 0
        wtarg = p + ima*z
        do i = 1,n
          dsdt = sqrt(dpdt(i)**2 + dzdt(i)**2)
          dw = h*(dpdt(i) + ima*dzdt(i))
          w = ps(i) + ima*zs(i)
          zwind = zwind + dw/(w-wtarg)
        end do

        zwind = zwind/2/pi/ima

        return
        end


        subroutine fcurveout(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
!c
!c       controls the surface of the outer boundary 
!c
!c       an antoine-jeff like geometry
!c
        done=1
        pi=4*atan(done)
!c
!c       an antoine-jeff like geometry
!c
        e = .32d0
        alpha = 0.336d0
        rkap = 1.7d0

        x0 = 1.0d0
        y0 = 0
     
        x = x0 + e*cos(t+alpha*sin(t))
        y = y0 + e*rkap*sin(t)

        dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
        dydt = e*rkap*cos(t)

        dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
         +e*sin(t+alpha*sin(t))*alpha*sin(t)
        dydt2 = -e*rkap*sin(t)

        return
!
!c       an ellipse
!c
        x0 = 2.01d0
        y0 = 0.1d0
        y0 = 0

        a = e
        b = e*rkap

        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)

        return
        end





        subroutine fcurvein(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
!c
        !c       controls the surface of the outer boundary
        !

        e = .32d0/2.5d0
        alpha = 0.336d0
        rkap = 1.7d0

        x0 = 1.0d0
        y0 = 0
     
        x = x0 + e*cos(t+alpha*sin(t))
        y = y0 + e*rkap*sin(t)

        dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
        dydt = e*rkap*cos(t)

        dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
         +e*sin(t+alpha*sin(t))*alpha*sin(t)
        dydt2 = -e*rkap*sin(t)

        return
!c
!c       an ellipse
!c
        x0 = 1d0
        y0 = 0d0

        a = .15d0
        b = .15d0

        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)

        return
        end
