
HOST = osx-gcc-7
HOST = osx-gcc-7-openmp
#HOST = osx-intel
HOST = osx-intel-openmp
#HOST = amd-linux-gfortran
#HOST = amd-linux-gfortran-openmp
PROJECT = int2

ifeq ($(HOST),osx-gcc-7)
  FC = gfortran-7 -c -w
  FFLAGS = -O2
  FLINK = gfortran-7 -w -o $(PROJECT) -framework accelerate
endif

ifeq ($(HOST),osx-gcc-7-openmp)
  FC = gfortran-7 -c -fopenmp -w
  FFLAGS = -O2
  FLINK = gfortran-7 -fopenmp -w \
    -Wl,-stack_size,0x40000000 -o $(PROJECT) -framework accelerate
  export OMP_NUM_THREADS = 4
  export OMP_STACKSIZE = 2048M
endif



ifeq ($(HOST),osx-intel)
  FC = ifort -c -w 
  FFLAGS = -O2
  FLINK = ifort -mkl -o $(PROJECT)
endif

ifeq ($(HOST),osx-intel-openmp)
  FC = ifort -c -w -qopenmp
  FFLAGS = -O2
  FLINK = ifort -w -mkl=parallel -qopenmp \
    -Wl,-stack_size,0x40000000 -o $(PROJECT)
  export OMP_NUM_THREADS=4
  export OMP_STACKSIZE=2048M
endif

ifeq ($(HOST),amd-linux-gfortran)
  FC = gfortran -c -w
  FFLAGS = -O2
  FLINK = gfortran -w -o $(PROJECT) /usr/lib/libblas.so.3 \
        /usr/lib/liblapack.so.3
endif

ifeq ($(HOST),amd-linux-gfortran-openmp)
  FC = gfortran -c -w -fopenmp
  FFLAGS = -O2
  FLINK = gfortran -w -fopenmp -o $(PROJECT) /usr/lib/libblas.so.3 \
        /usr/lib/liblapack.so.3
  export OMP_NUM_THREADS=8
  export OMP_STACKSIZE=1024M
endif



.PHONY: all clean list

HELLSKITCHEN = ../../hellskitchen

SOURCES = debye_pec_test1.f90 \
  ../src/debye.f \
  ../src/debye_new.f90 \
  ../src/mfie.f \
  ../src/mfie_new.f90 \
  ../src/axideco.f \
  ../src/axideco_new.f90 \
  ../src/axikernels.f90 \
  ../src/zalpert_modes.f90 \
  ../src/zalpert.f90 \
  ../src/dalpert.f90 \
  ../src/dalpert_modes.f90 \
  ../src/kernels.f \
  ../../special/src/elliptic.f \
  ../../utils/lapack_wrap.f90 \
  ../../utils/pplot.f \
  ../../utils/next235.f \
  ../../utils/cadapgaus_quad.f \
  ../../utils/adapgaus_quad.f \
  ../../utils/prini.f \
  ../../utils/corrand.f \
  ../../utils/quaplot.f \
  ../../utils/legeexps.f \
  ../../utils/cadapgau_k16.f \
  ../../utils/elliptic_ke.f \
  ../../utils/chebexps.f \
  ../../utils/csvdpiv.f \
  ../../utils/anaresa.f \
  ../../utils/cqrsolve.f \
  ../../utils/alpert.f \
  ../../utils/dfft.f \
  ../../common/rotviarecur3.f \
  ../../common/rotprojvar.f \
  ../../common/yrecursion.f \
  $(HELLSKITCHEN)/Greengard/Trapquads2d/fdiffc.f \
  $(HELLSKITCHEN)/Axi3DLibraries/surfdivaxi.f \
  $(HELLSKITCHEN)/Axi3DLibraries/surflapaxi.f \
  $(HELLSKITCHEN)/Axi3DLibraries/surfgradaxi.f \
  $(HELLSKITCHEN)/Axi3DLibraries/surfcurlaxi.f \
  $(HELLSKITCHEN)/Axi3DLibraries/ode_periodic_surflap.f \
  $(HELLSKITCHEN)/Axi3DLibraries/surflapinteq.f \
  $(HELLSKITCHEN)/Common/zgecoall.f \
  $(HELLSKITCHEN)/Gimbutas/lib/hjfuns3d.f \
  $(HELLSKITCHEN)/Gimbutas/emlib/emdyadic.f \
  $(HELLSKITCHEN)/H3DLibraries/projections.f \
  $(HELLSKITCHEN)/Gimbutas/emlib/emrouts3.f \
  $(HELLSKITCHEN)/Gimbutas/emlib/emabrot3.f \
  $(HELLSKITCHEN)/Gimbutas/lib/xrecursion.f \
  $(HELLSKITCHEN)/H3DLibraries/helmrouts2.f


OBJECTS = $(patsubst %.f,%.o,$(patsubst %.f90,%.o,$(SOURCES)))

#
# use only the file part of the filename, then manually specify
# the build location
#

%.o : %.f
	$(FC) $(FFLAGS) $< -o $@

%.o : %.f90
	$(FC) $(FFLAGS) $< -o $@

all: $(OBJECTS)
	rm -f $(PROJECT)
	$(FLINK) $(OBJECTS)
	@echo $(TTT)
	@echo $(DYLD_LIBRARY_PATH)
	@echo 'a = '$(a)
	@echo 'b = '$(b)
	./$(PROJECT)

clean:
	rm -f $(OBJECTS)
	rm -f $(PROJECT)

list: $(SOURCES)
	$(warning Requires:  $^)



